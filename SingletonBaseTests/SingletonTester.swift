//
//  SingletonTester.swift
//  FireMan1
//
//  Created by Gerald Boyd on 6/19/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

import XCTest
//import SingletonBase

class SingletonTester: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func createUniqueInstance() -> SingletonBase {
        return SingletonBase();
    }
    
    func getSharedInstance() -> SingletonBase {
        return SingletonBase.sharedSingletonObject();
    }
    
    func testSingletonSharedInstanceCreated() {
        XCTAssertNotNil(getSharedInstance(), "Should not be nil");
    }
    
    func testSingletonUniqueInstanceCreated() {
        XCTAssertNotNil(createUniqueInstance(), "Should not be nil");
    }
    
    func testSingletonReturnsSameSharedInstanceTwice() {
        let s1 = getSharedInstance()
        let s2 = getSharedInstance()
        XCTAssertEqual(s1, s2, "Should be same instance")
    }
    
    func testSingletonSharedInstanceSeparateFromUniqueInstance() {
        let s1 = getSharedInstance();
        XCTAssertNotEqualObjects(s1, createUniqueInstance(), "should not be equal")
    }
    
    func testSingletonReturnsSeparateUniqueInstances() {
        let s1 = createUniqueInstance();
        XCTAssertNotEqual(s1, createUniqueInstance(), "Should be unique instances")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
