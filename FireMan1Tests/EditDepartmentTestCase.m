//
//  EditDepartmentTestCase.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/24/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <KIF/KIF.h>
//#import "KIFUITestActor.h"

@interface EditDepartmentTests : KIFTestCase
@end

@implementation EditDepartmentTests

- (void)beforeEach
{
    // [tester navigateToLoginPage];
}

- (void)afterEach
{
    // [tester returnToLoggedOutHomeScreen];
}

- (void)testAddNeighborDepartment
{
    [tester tapViewWithAccessibilityLabel:@"Incident: Title"];
    [tester waitForTappableViewWithAccessibilityLabel:@"Incident Selection View"];
    [tester tapViewWithAccessibilityLabel:@"Edit Department"];
    [tester waitForViewWithAccessibilityLabel:@"Edit Department View"];
    [tester waitForViewWithAccessibilityLabel:@"Department Item Master Cell"];
    // go to Neighbor departments
    [tester tapViewWithAccessibilityLabel:@"Item Description Label Field" value:@"Neighbor Departments" traits:UIAccessibilityTraitNone];
    [tester waitForViewWithAccessibilityLabel:@"right side header cell"];
    [tester waitForViewWithAccessibilityLabel:@"Detail View Heading Label" value:@"Neighbor Departments" traits:UIAccessibilityTraitStaticText];
    // insert new department
    [tester tapViewWithAccessibilityLabel:@"Insert Item"];
    [tester tapViewWithAccessibilityLabel:@"text field in Detail edit cell"];
    [tester enterText:@"McKinney" intoViewWithAccessibilityLabel:@"text field in Detail edit cell"];
    [tester tapViewWithAccessibilityLabel:@"Basic Description Label" value:@"Prefix: <???????>" traits:UIAccessibilityTraitNone];
    [tester waitForViewWithAccessibilityLabel:@"Detail View Heading Label" value:@"McKinney" traits:UIAccessibilityTraitStaticText];
    [tester enterTextIntoCurrentFirstResponder:@"McK"];
    // verify that department name has changed
    [tester waitForViewWithAccessibilityLabel:@"Basic Description Label" value:@"Department: McKinney" traits:UIAccessibilityTraitNone];
    // go to units
    [tester tapViewWithAccessibilityLabel:@"Basic Description Label" value:@"Units" traits:UIAccessibilityTraitNone];
    // verify prefix change
    [tester waitForViewWithAccessibilityLabel:@"Item Description Label Field" value:@"Prefix: McK" traits:UIAccessibilityTraitNone];
    // todo Add units
    [tester waitForTimeInterval:1];
    [tester tapViewWithAccessibilityLabel:@"Back"];
    [tester tapViewWithAccessibilityLabel:@"Back"];
    [tester tapViewWithAccessibilityLabel:@"Back"];
}


- (void)testAddNeighborDepartment_Issue_51
{
    [tester tapViewWithAccessibilityLabel:@"Incident: Title"];
    [tester waitForTappableViewWithAccessibilityLabel:@"Incident Selection View"];
    [tester tapViewWithAccessibilityLabel:@"Edit Department"];
    [tester waitForViewWithAccessibilityLabel:@"Edit Department View"];
    [tester waitForViewWithAccessibilityLabel:@"Department Item Master Cell"];
    [tester tapViewWithAccessibilityLabel:@"Item Description Label Field" value:@"Neighbor Departments" traits:UIAccessibilityTraitNone];
    [tester waitForViewWithAccessibilityLabel:@"right side header cell"];
    [tester waitForViewWithAccessibilityLabel:@"Detail View Heading Label" value:@"Neighbor Departments" traits:UIAccessibilityTraitStaticText];
    [tester tapViewWithAccessibilityLabel:@"Insert Item"];
    [tester tapViewWithAccessibilityLabel:@"text field in Detail edit cell"];
    // [tester enterText:@"McKinney" intoViewWithAccessibilityLabel:@"text field in Detail edit cell"];
    [tester enterTextIntoCurrentFirstResponder:@"McKinney"];
    [tester tapViewWithAccessibilityLabel:@"Basic Description Label" value:@"Prefix: <???????>" traits:UIAccessibilityTraitNone];
    [tester waitForViewWithAccessibilityLabel:@"Detail View Heading Label" value:@"McKinney" traits:UIAccessibilityTraitStaticText];
    [tester enterTextIntoCurrentFirstResponder:@"McK"];
    // verify that department name has changed
    [tester waitForViewWithAccessibilityLabel:@"Basic Description Label" value:@"Department: McKinney" traits:UIAccessibilityTraitNone];
    
    [tester tapViewWithAccessibilityLabel:@"Basic Description Label" value:@"Units" traits:UIAccessibilityTraitNone];
    [tester waitForViewWithAccessibilityLabel:@"Item Description Label Field" value:@"Prefix: McK" traits:UIAccessibilityTraitNone];
    [tester waitForTimeInterval:1];
    [tester tapViewWithAccessibilityLabel:@"Back"];
    [tester tapViewWithAccessibilityLabel:@"Back"];
    [tester tapViewWithAccessibilityLabel:@"Back"];
}

@end

