//
//  DepartmentMasterVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 1/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "DepartmentMasterVC.h"
#import "EditableItem.h"
#import "DepartmentDetailVC.h"
#import "MasterViewState.h"
#import "EditDepartmentValueViewCell.h"
#import "UIColor+EmerResStrat.h"

@interface DepartmentMasterVC ()
- (IBAction)editingChanged:(UITextField *)sender;
- (IBAction)editDidEnd:(UITextField *)textField;
- (IBAction)backButtonPressed:(UIButton *)sender;
@property (nonatomic) BOOL isViewReady;

@end

@implementation DepartmentMasterVC
@dynamic delegate;

#pragma mark - view notifications

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.activeIndexPath = nil;
    self.isViewReady = NO;
    self.tableView.accessibilityIdentifier = @"Master Table";
    self.tableView.accessibilityLabel = @"Edit Department Master Table";

    [self setupView];

    self.descriptionBackgroundColor = [UIColor ersTextFieldBackgroundColor];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)setupView
{
    [self clearSortedCache];

    // push current state
    assert (self.delegate);
    [self.delegate pushState:self.state];

    if (self.isAutoSelect && self.editableItems.count) {
        // auto selection will push the state
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.index inSection:0];
        [self delayedSelection:indexPath withSegueIdentifier:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.isViewReady = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - state
- (id)state
{
    MasterViewState *currState = [[MasterViewState alloc]init];
    currState.editableItems = self.editableItems;
    currState.index = self.index;
    currState.isAutoSelect = self.isAutoSelect;
    currState.title = self.title;
    return currState;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.editableItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;

    NSString *cellIdentifier = @"editDepartmentBasicCell";

    if (self.activeIndexPath
            && self.activeIndexPath.section == section
            && self.activeIndexPath.row == row) {
        cellIdentifier = @"editDepartmentEditCell";
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                            forIndexPath:indexPath];
    // Configure the cell...
    NSInteger cellTag = row;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.accessoryView = nil;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.tag = cellTag;
    cell.backgroundColor = [UIColor whiteColor];

    id <FDedit> item = self.sortedItems[(NSUInteger) indexPath.item];

    if (item.itemName && !item.value) {
        cell.textLabel.text = item.itemName;
        cell.textLabel.accessibilityValue = cell.textLabel.text;
    }
    else if (item.value) {
        if ([cellIdentifier isEqualToString:@"editDepartmentEditCell"]) {
            [self setupEditMasterCell:cell cellTag:cellTag item:item];
        }
        else {
            [self setupPlainMasterCell:cell item:item];
        }
    }
    if ((item.itemName || item.value)
            && (item.editableItems || item.permittedValues)) {
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DetailArrow.png"]];
        cell.accessoryView.tag = cellTag;
        cell.accessoryView.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}

- (void)setupPlainMasterCell:(UITableViewCell *)cell item:(id <FDedit>)item
{
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    NSString *cellSeparator = @"";
    NSString *itemName = item.itemName;
    if (item.itemName && item.value) {
                cellSeparator = @": ";
            }
    if (!itemName) {
                itemName = @"";
            }
    NSString *itemValue = item.value;
    if (!itemValue) {
                itemValue = @"";
            }
    NSString *cellText = [NSString stringWithFormat:@"%@%@%@",itemName,cellSeparator,itemValue];
    cell.textLabel.text = cellText;
    cell.textLabel.accessibilityValue = cellText;
}

- (void)setupEditMasterCell:(UITableViewCell *)cell cellTag:(NSInteger)cellTag item:(id <FDedit>)item
{
    self.textFieldContents = nil;

    EditDepartmentValueViewCell *editCell = (EditDepartmentValueViewCell *) cell;
    editCell.itemTextField.text = item.value;
    if (item.itemName) {
                editCell.itemNameLabel.text = [NSString stringWithFormat:@"%@:",item.itemName];
                editCell.itemNameLabel.hidden = NO;
                [editCell.itemNameLabel setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal];
            }
            else {
                editCell.itemNameLabel.text = @"";
                editCell.itemNameLabel.hidden = YES;
                [editCell.itemNameLabel setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal];
            }

    editCell.itemTextField.tag = cellTag;
    editCell.itemTextField.delegate = self;
    editCell.itemTextField.selected = YES;
    editCell.itemTextField.backgroundColor = self.descriptionBackgroundColor;
    editCell.itemTextField.clearsOnBeginEditing = NO;
    editCell.itemTextField.clearButtonMode = UITextFieldViewModeAlways;
    [editCell.itemTextField becomeFirstResponder];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - table delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;

    if ([self.activeIndexPath isEqual:indexPath]) {
        return;  // do nothing - already selected
    }

    id <FDedit> item = self.sortedItems[(NSUInteger) indexPath.item];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSMutableArray *itemPaths = [NSMutableArray arrayWithCapacity:3];   // cells to bne reloaded

    // reload and close the description cell being edited
    if (self.activeIndexPath) {
        UITableViewCell *activeEditCell = [tableView cellForRowAtIndexPath:self.activeIndexPath];
        [itemPaths addObject:self.activeIndexPath];
        self.activeIndexPath = nil;
        if ([activeEditCell isKindOfClass:[EditDepartmentValueViewCell class]]) {
            EditDepartmentValueViewCell *editCell = (EditDepartmentValueViewCell *)activeEditCell;
            if (editCell.itemTextField.isFirstResponder) {
                [editCell.itemTextField resignFirstResponder];
            }
        }    
    }

    BOOL isUpdateRightSideView = NO;
    if (item.editableItems || item.permittedValues) {
        // bring up items in the detail view
        cell.tag = indexPath.item;
        isUpdateRightSideView = YES;
    }
    else if (item.value) {
        // new row selected - set it for editing
        self.activeIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [itemPaths addObject:self.activeIndexPath];
    }
    [tableView reloadRowsAtIndexPaths:itemPaths withRowAnimation:UITableViewRowAnimationAutomatic];

    if (isUpdateRightSideView) {
        if (self.delegate) {
            BOOL stateChange = NO;

            NSInteger index = indexPath.item;
            id <FDedit> selectedItem = nil;
            if (index >= 0) {
                selectedItem = self.sortedItems[(NSUInteger) index];
            }
            [self.delegate rightSideItem:selectedItem];

            // update left side state for the selected item
            if (!self.isAutoSelect) {
                stateChange = YES;
                self.isAutoSelect = YES;
            }
            if (self.index != index) {
                stateChange = YES;
                self.index = index;
            }
            if (stateChange) {
                [self.delegate popState];  // replace state
                [self.delegate pushState:self.state];
            }
        }
    }
}

#pragma text field notifications

- (IBAction)editingChanged:(UITextField *)textField
{
    self.textFieldContents = textField.text;
    textField.backgroundColor = self.descriptionBackgroundColor;
}


- (IBAction)editDidEnd:(UITextField *)textField
{
    // do nothing
}

- (IBAction)backButtonPressed:(UIButton *)sender
{
    // get view stackcount
    if (self.delegate.stackSize > 1) {
        [self showPreviousView];
    }
    else {
        // trigger the unwind segue since no stacked views
        [self performSegueWithIdentifier:@"unwindDepartmentMaster" sender:self];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSInteger row = textField.tag;
    NSInteger itemIndex = row;
    id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
    NSInteger section = 0;


    if (self.textFieldContents
            && ![item.value isEqualToString:self.textFieldContents]) {
        // something changed
        [item save:self.textFieldContents itemname:nil];
        [self unselectActiveEditRow];
    }
    else {
        // nothing changed (take row out of edit mode)
        [self unselectActiveEditRow];
        [self reloadTableRow:row section:section];
    }
    self.textFieldContents = nil;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger row = textField.tag;
    NSInteger itemIndex = row;
    id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
    if (![item.value isEqualToString:self.textFieldContents]) {
        // something changed in field
        if ([self value:self.textFieldContents uniqueIn:self.sortedItems]) {
            [self closeKeyboard:textField];
            return YES;
        }
        else {
            textField.backgroundColor = self.errorBackgroundColor;
        }
        return NO;
    }
    else {
        // nothing changed
        [self closeKeyboard:textField];
        return YES;
    }
}



#pragma mark - Navigation

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return self.isViewReady;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"arrayDetailSegue"]) {
        UIView *senderView = (UIView *)sender;
        NSInteger index = senderView.tag;
        DepartmentDetailVC *vc = segue.destinationViewController;
        if (index >= 0) {
            id <FDedit> item = self.sortedItems[(NSUInteger) index];
            vc.masterItem = item;
        }
        else {
            vc.masterItem = nil;
        }
    }
    else if ([segue.identifier isEqualToString:@"unwindDepartmentMaster"]) {

        // if needed, close the description cell being edited
        [self.delegate closingViews];

    }
}

#pragma mark - unwinding

- (void)showPreviousView
{
    assert (self.delegate);

    // discard current state
    [self closingView];          // clean up any text field being edited
    [self.delegate popState];

    // get previous state
    MasterViewState *previousState = [self.delegate popState];
    [self clearSortedCache];

    if (previousState) {
        [self.delegate rightSideItem:nil];
        self.index = previousState.index;
        self.editableItems = previousState.editableItems;
        self.isAutoSelect = previousState.isAutoSelect;
        self.title = previousState.title;
        

        // show previous master view
        [self setupView];
        [self.tableView reloadData];

        if (!self.isAutoSelect) {
            [self.delegate rightSideItem:nil];
        }
    }
    else {
        // return to incident command screen
        [self.delegate didCloseViews];
    }
}

#pragma mark delayedSelection

-(void)delayedSelection:(NSIndexPath *)idxPath withSegueIdentifier:(NSString *)segueID {
    if (!idxPath) idxPath = [NSIndexPath indexPathForRow:0 inSection:0];
    if (segueID) {
        [self performSelector:@selector(selectIndexPath:) withObject:@{@"NSIndexPath": idxPath, @"UIStoryboardSegue": segueID } afterDelay:0];
    }
    else {
        [self performSelector:@selector(selectIndexPath:) withObject:@{@"NSIndexPath": idxPath } afterDelay:0];

    }
}

-(void)selectIndexPath:(NSDictionary *)args {
    NSIndexPath *idxPath = args[@"NSIndexPath"];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idxPath];
    cell.tag = idxPath.item;

    [self.tableView selectRowAtIndexPath:idxPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];

    if ([self.tableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:idxPath];

    if (args[@"UIStoryboardSegue"]) {
        [self performSegueWithIdentifier:args[@"UIStoryboardSegue"] sender:cell];
    }
}

#pragma mark - EditorView

- (void)loadNewView
{
    [self clearSortedCache];
    [self setupView];
    [self.tableView reloadData];
}

- (void)setViewIndex:(NSInteger)index1
{
    self.index = index1;
}

- (void)setViewTitle:(NSString *)title
{
    self.title = title;
}

- (void)setViewItems:(NSArray *)items
{
    self.editableItems = items;
    [self clearSortedCache];
}

- (void)closingView
{
    if (self.activeIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.activeIndexPath];
        self.activeIndexPath = nil;
        if (cell && [cell isKindOfClass:[EditDepartmentValueViewCell class]]) {
            EditDepartmentValueViewCell *editCell = (EditDepartmentValueViewCell *)cell;
            if (editCell.itemTextField.isFirstResponder) {
                [editCell.itemTextField resignFirstResponder];
            }
        }
    }
    [self clearSortedCache];
}


@end
