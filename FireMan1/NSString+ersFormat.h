//
// Created by Gerald Boyd on 4/30/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ersFormat)
- (NSString *) minSize:(NSInteger)minSize;
@end