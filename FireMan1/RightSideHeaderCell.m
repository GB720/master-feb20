//
//  RightSideHeaderCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 3/24/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "RightSideHeaderCell.h"

@implementation RightSideHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
