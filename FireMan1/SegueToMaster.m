//
//  SegueToMaster.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "SegueToMaster.h"

@implementation SegueToMaster
- (void)perform
{

    [[self sourceViewController] presentViewController:self.destinationViewController
                                              animated:YES completion:nil];
}

@end
