//
//  GbElapsedTime.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/14/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GbElapsedTime : NSObject
- (GbElapsedTime *) initWithTimeInterval: (NSTimeInterval) timeInterval;
- (GbElapsedTime *)initSinceTime: (NSDate *) startTime;

@property(nonatomic, readonly) NSNumber *totalElapsedSeconds;
@property(nonatomic, readonly) NSUInteger totalElapsedMinutes;
@property(nonatomic, readonly) NSUInteger hours;
@property(nonatomic, readonly) NSUInteger mins;
@property(nonatomic, readonly) NSUInteger secs;
@end
