//
//  UserIdentificationViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 8/25/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+Create.h"
#import "FireDepartment+Create.h"

@interface UserIdentificationViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic) BOOL isUpdate;
@property (nonatomic, strong) FireDepartment *fireDepartment;
@property (nonatomic, readonly) User *user;

@end
