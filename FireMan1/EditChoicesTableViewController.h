//
//  EditChoicesTableViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/4/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Incident.h"

@interface EditChoicesTableViewController : UITableViewController
@property (nonatomic, strong) Incident *incident;
@end
