//
//  EditListItemCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "EditListItemCell.h"

@implementation EditListItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
