//
// Created by Gerald Boyd on 7/9/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "Preference+CoreDataClass.h"


@interface Preferences : NSObject
@property (strong, nonatomic) NSDictionary *preferenceDictionary;
+ (Preferences *) createWith: (NSSet *) preferences;

- (NSString *) valueFor: (NSString *) preferenceName;
- (NSUInteger) integerValueFor: (NSString *) preferenceName;
- (NSNumber *) numberFor:(NSString *) preferenceName;
- (Preferences *) initWith: (NSDictionary *) preferences;

@end
