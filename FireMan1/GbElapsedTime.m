//
//  GbElapsedTime.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/14/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "GbElapsedTime.h"

@implementation GbElapsedTime

// designated initializer
- (GbElapsedTime *) initWithTimeInterval: (NSTimeInterval) timeInterval
{
    self = [super init];
    if (self) {
        long seconds = lroundf((float) timeInterval); // Modulo (%) operator below needs int or long
        _totalElapsedSeconds = @(seconds);
        _hours = (NSUInteger) (seconds / 3600);
        _mins = (NSUInteger) ((seconds % 3600) / 60);
        _secs = (NSUInteger) (seconds % 60);
        _totalElapsedMinutes = (NSUInteger) (seconds / 60);
    }
    return self;
}

- (GbElapsedTime *)initSinceTime: (NSDate *) startTime
{
    return [self initWithTimeInterval:[[NSDate date] timeIntervalSinceDate:startTime]];
}
@end
