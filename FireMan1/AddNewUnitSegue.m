//
//  AddNewUnitSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "AddNewUnitSegue.h"
#import "FireUnitSelectionVC.h"
#import "AddFireUnitVC.h"
#import "UnitSelectionHeader.h"

@implementation AddNewUnitSegue

- (void)perform
{
    
    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect
    
    // Set the size of the popover because it will be max sized otherwise
    
    FireUnitSelectionVC *sourceController = (FireUnitSelectionVC *)self.sourceViewController;
    AddFireUnitVC *destinationController = (AddFireUnitVC *)self.destinationViewController;
    
    // present the controller
    destinationController.modalPresentationStyle = UIModalPresentationPopover;
    [sourceController presentViewController:destinationController animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [destinationController popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    popController.sourceView = sourceController.view;
    popController.sourceRect = destinationController.anchor.frame;
    // popController.delegate = sourceController;
    
//    // ------------
//    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:destinationController];
//    
//
//    CGSize size = CGSizeMake(250, 160);
//    pop.popoverContentSize = size;
//    
//    [pop presentPopoverFromRect:destinationController.anchor.frame
//                         inView:sourceController.view
//       permittedArrowDirections:UIPopoverArrowDirectionAny
//                       animated:YES];
    
    //[[self sourceViewController] presentViewController:destinationController animated:YES completion:nil];
    
}


@end
