//
// Created by Gerald Boyd on 2/12/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDedit.h"

@class EditableItem;

@protocol EditContainer <NSObject>
-(void) pushState:(id) state;
- (id) popState;
- (NSInteger) stackSize;
- (void) didCloseViews;
- (void) rightSideItem: (id <FDedit>) item;
- (void) leftSideEditableItems:(NSArray *)items index:(NSInteger)leftSideIndex title:(NSString *)leftSideTitle isAutoSelect:(BOOL) isLeftSideAutoSelect;
- (void) closingViews;
- (void) masterItemDidUpdate;
- (void) backButton;
@end
