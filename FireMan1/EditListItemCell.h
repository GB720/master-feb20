//
//  EditListItemCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditListItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *itemTextField;

@end
