//
//  IncidentDetailViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 7/29/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncidentDetailViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextView *incidentNotesField;
@property (weak, nonatomic) IBOutlet UITextField *incidentNameField;
@property (weak, nonatomic) IBOutlet UILabel *incidentDateField;
@property (weak, nonatomic) IBOutlet UITextView *textViewField;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *note;
@property(nonatomic) BOOL notesChanged;
@property(nonatomic) BOOL nameChanged;
@end
