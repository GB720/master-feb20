//
//  Incident.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/27/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, FireDepartment, FireUnit, IncidentLog, IncidentTaskList, ListItem, Preference, StagingGroup, User;

@interface Incident : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * originatingDeviceId;
@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSDate * stopTime;
@property (nonatomic, retain) NSDate * timeOfLastPar;
@property (nonatomic, retain) NSDate * createdTimeStamp;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSSet *activeUnits;
@property (nonatomic, retain) NSSet *assignedUnits;
@property (nonatomic, retain) NSSet *availableUnits;
@property (nonatomic, retain) FireUnit *commandUnit;
@property (nonatomic, retain) FireDepartment *forDepartment;
@property (nonatomic, retain) User *incidentCoordinator;
@property (nonatomic, retain) IncidentLog *logForIncident;
@property (nonatomic, retain) NSSet *managedTasks;
@property (nonatomic, retain) NSSet *preferences;
@property (nonatomic, retain) NSSet *rehabUnits;
@property (nonatomic, retain) NSSet *stagingGroups;
@property (nonatomic, retain) NSSet *taskLists;
@property (nonatomic, retain) NSSet *viewedByDevices;
@end

@interface Incident (CoreDataGeneratedAccessors)

- (void)addActiveUnitsObject:(FireUnit *)value;
- (void)removeActiveUnitsObject:(FireUnit *)value;
- (void)addActiveUnits:(NSSet *)values;
- (void)removeActiveUnits:(NSSet *)values;

- (void)addAssignedUnitsObject:(FireUnit *)value;
- (void)removeAssignedUnitsObject:(FireUnit *)value;
- (void)addAssignedUnits:(NSSet *)values;
- (void)removeAssignedUnits:(NSSet *)values;

- (void)addAvailableUnitsObject:(FireUnit *)value;
- (void)removeAvailableUnitsObject:(FireUnit *)value;
- (void)addAvailableUnits:(NSSet *)values;
- (void)removeAvailableUnits:(NSSet *)values;

- (void)addManagedTasksObject:(ListItem *)value;
- (void)removeManagedTasksObject:(ListItem *)value;
- (void)addManagedTasks:(NSSet *)values;
- (void)removeManagedTasks:(NSSet *)values;

- (void)addPreferencesObject:(Preference *)value;
- (void)removePreferencesObject:(Preference *)value;
- (void)addPreferences:(NSSet *)values;
- (void)removePreferences:(NSSet *)values;

- (void)addRehabUnitsObject:(FireUnit *)value;
- (void)removeRehabUnitsObject:(FireUnit *)value;
- (void)addRehabUnits:(NSSet *)values;
- (void)removeRehabUnits:(NSSet *)values;

- (void)addStagingGroupsObject:(StagingGroup *)value;
- (void)removeStagingGroupsObject:(StagingGroup *)value;
- (void)addStagingGroups:(NSSet *)values;
- (void)removeStagingGroups:(NSSet *)values;

- (void)addTaskListsObject:(IncidentTaskList *)value;
- (void)removeTaskListsObject:(IncidentTaskList *)value;
- (void)addTaskLists:(NSSet *)values;
- (void)removeTaskLists:(NSSet *)values;

- (void)addViewedByDevicesObject:(Device *)value;
- (void)removeViewedByDevicesObject:(Device *)value;
- (void)addViewedByDevices:(NSSet *)values;
- (void)removeViewedByDevices:(NSSet *)values;

@end
