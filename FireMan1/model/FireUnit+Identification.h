//
// Created by Gerald Boyd on 6/6/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "FireUnit+CoreDataClass.h"

@interface FireUnit (Identification)
@property (nonatomic, readonly) NSString *prefixedName;
@property (nonatomic, readonly) NSUInteger activeAge;
- (NSString *) prefixedNameInIncident:(Incident *)incident;
@property (nonatomic, readonly) UIColor *stagingColor;
@end
