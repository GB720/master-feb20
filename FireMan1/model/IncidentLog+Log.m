//
// Created by Gerald Boyd on 5/20/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "IncidentLog+Log.h"
#import "ListItem.h"
#import "FireUnit.h"
#import "LogItem.h"
#import "LogItem+Create.h"
#import "List.h"


@implementation IncidentLog (Log)
- (void) action:(NSString *)action
{
    [self action:action unit:nil task:nil];
}

- (void) action:(NSString *)action unit:(FireUnit *)fireUnit
{
    [self action:action unit:fireUnit task:nil];
}

- (void) action:(NSString *)action task:(ListItem *)task
{
    [self action:action unit:nil task:task];
}

- (void) task:(ListItem *) task isChecked:(BOOL) isChecked
{
    LogItem *logItem = [LogItem task:task
                           isChecked:isChecked
                         withContext:self.managedObjectContext];
    [self add:logItem];
}

- (void) list:(List *) list isCompleted: (BOOL) isCompleted
{
    LogItem *logItem = [LogItem list: list
                           isCompleted: isCompleted
                         withContext:self.managedObjectContext];
    [self add:logItem];
}

- (void) action:(NSString *)action unit:(FireUnit *)fireUnit task:(ListItem *)task
{
    LogItem *logItem = [LogItem action:action
               unit:fireUnit
               task:task
        withContext:self.managedObjectContext];

    [self add:logItem];

}

- (void)add:(LogItem *)logItem {
    logItem.itemNumber = self.nextItemNumber;
    self.nextItemNumber = @(self.nextItemNumber.intValue + 1);
    self.logItems = [self.logItems setByAddingObject:logItem];
}
@end