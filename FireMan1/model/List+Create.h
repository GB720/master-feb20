//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "List.h"
#import "FDcompare.h"
#import "FDedit.h"

@interface List (Create)  <FDcompare, FDedit>
- (void)createListItems:(NSArray *)listItemNames;
- (List *)addItemsFromList:(List *)list uniqueIdPrefix:(NSString *)prefix;
- (List *)addItemsFromList:(List *)list;
@property (nonatomic, readonly) BOOL isReorderable;
@property (nonatomic, readonly) BOOL isDeletable;
@property (nonatomic, readonly) BOOL hasSortValue;
@end
