//
// Created by Gerald Boyd on 8/5/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "Device+Create.h"

@implementation Device (Create)
+ (Device *)createWithId:(NSString *)IDString context:(NSManagedObjectContext *)context
{
    Device *device = [NSEntityDescription
            insertNewObjectForEntityForName:@"Device"
                     inManagedObjectContext:context];
    // set value of name
    device.deviceId = IDString;
    device.createdTimeStamp = [NSDate date];
    device.uniqueId = IDString;
    return device;
}

+ (Device *)deviceWithId:(NSString *)id context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Device"];
    
    // Set predicate to fetch device with the specified id
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceId like %@", id];
    [request setPredicate:predicate];
    
    // execute the fetch request
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    Device *device = fetchedArray[0];
    return device;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[Device class]]);
    Device *otherGroupName = (Device *) other;
    return [self.deviceId isEqualToString:otherGroupName.deviceId];
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[Device class]]);
    Device *otherDevice = (Device *) other;
    return ([self.createdTimeStamp compare:otherDevice.createdTimeStamp] == NSOrderedAscending);
}


- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    return nil;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (NSString *)value
{
    return self.deviceId;
}

- (void) save:(NSString *)newValue itemname:(NSString *)itemName
{
    if (newValue && ![newValue isEqualToString:self.deviceId]) {
        self.deviceId = newValue;
    }
}

- (NSDecimalNumber *)sortValue
{
    return [NSDecimalNumber zero];
}

- (void)setSortValue:(NSDecimalNumber *)value
{
    // do nothing
}

- (id)ownerObject
{
    return self;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)isAddable
{
    return NO;
}

- (BOOL)hasSortValue
{
    return NO;
}


@end
