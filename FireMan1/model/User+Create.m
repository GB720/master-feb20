//
// Created by Gerald Boyd on 8/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "User+Create.h"
#import "EditableItem.h"
#import "FireDepartment+Create.h"
#import "Role.h"
#import "PermittedValues.h"
#import "EntityPermittedValues.h"

static NSString *const ROLE = @"Role";
static NSString *const USER_NAME = @"Name";
static NSString *const PIN = @"PIN";


@implementation User (Create)
+ (User *)createWithName:(NSString *)newName onDevice:(NSString *)deviceId context:(NSManagedObjectContext *)context
{
    User *user = [NSEntityDescription
            insertNewObjectForEntityForName:@"User"
                     inManagedObjectContext:context];
    // set value of name
    user.name = newName;
    user.createdTimeStamp = [NSDate date];
    user.uniqueId = [NSString stringWithFormat:@"user-%@-%@",newName,deviceId];
    return user;
}

+ (User *)userWithName:(NSString *)name onDevice:(NSString *) deviceId context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    
    // Set predicate to fetch user with the specified id
    NSPredicate *predicate;
    if (deviceId) {
        NSString *uniqueId = [NSString stringWithFormat:@"user-%@-%@",name,deviceId];
        predicate = [NSPredicate predicateWithFormat:@"uniqueId like %@", uniqueId];
    }
    else {
        predicate = [NSPredicate predicateWithFormat:@"name like %@", name];
    }
    [request setPredicate:predicate];
    
    // execute the fetch request
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    User *user = fetchedArray[0];
    return user;
}

- (BOOL) hasPermission:(NSUInteger) permission
{
    NSUInteger rolePermission = (NSUInteger) self.role.type.integerValue;
    BOOL isPermission = ((rolePermission & permission) == permission);
    return isPermission;
}

- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:2];
    //[items addObject:[EditableItem fromString:self.name]];
    [items addObject:[EditableItem owner:self
                                   value:self.name
                                itemName:USER_NAME
                               sortValue:(items.count + 1)]];
    [items addObject:[EditableItem owner:self
                                   value:self.id
                                itemName:PIN
                               sortValue:(items.count + 1)]];
    [items addObject:[EditableItem owner:self value:self.role.name permittedValues:self.permittedValues itemName:ROLE sortValue:(items.count + 1)]];
    return items;
}

- (id <FDPermittedValues>)permittedValuesFor:(NSString *) itemName
{
    if ([itemName isEqualToString:ROLE]) {
        return [EntityPermittedValues withEntities:self.inDepartment.roles owner:self forItemName:ROLE];
    }
    return nil;
}


- (id <FDPermittedValues>)permittedValues
{
    return [self permittedValuesFor:ROLE];
}

- (NSString *)value
{
    return self.name;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    if (itemName && [itemName isEqualToString:ROLE]) {
        // do nothing - prevents owner object name from changing
    }
    else if ([itemName isEqualToString:PIN]) {
        self.id = newValue;
    }
    else if (newValue && ![newValue isEqualToString:self.name]) {
        self.name = newValue;
    }
}

- (void) updateRelationshipWith:(id <FDedit>)entity
{
    if (entity && [entity isKindOfClass:[Role class]]) {
        self.role = (Role *) entity;
    }
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[User class]]);
    User *otherUser = (User *) other;
    return [self.name isEqualToString:otherUser.name];
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[User class]]);
    User *otherUser = (User *) other;
    return ([self.createdTimeStamp compare:otherUser.createdTimeStamp] == NSOrderedAscending);
}


- (NSDecimalNumber *)sortValue
{
    return nil;
}

- (void)setSortValue:(NSDecimalNumber *)value
{
      // do nothing
}

- (id)ownerObject
{
    return self;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)hasSortValue
{
    return NO;
}

@end
