//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "List+Create.h"
#import "ListItem+Status.h"
#import "ListItem+Create.h"
#import "EditableItem.h"


@implementation List (Create)

- (void)createListItems:(NSArray *)listItemNames
{
    NSUInteger index = 1;
    for (NSString *name in listItemNames) {
        ListItem *listItem = [ListItem createItemNamed:name withValue:index++ withContext:self.managedObjectContext];
        [self addItemsObject:listItem];
    }
}

- (List *)addItemsFromList:(List *)list uniqueIdPrefix:(NSString *)prefix
{
    for (ListItem *listItem in list.items) {
        ListItem *newListItem = [ListItem fromListItem:listItem withContext:self.managedObjectContext];
        // set unique id using uniqueId prefix if it exists to make a common object between devices
        if (prefix) {
            newListItem.uniqueId = [prefix stringByAppendingFormat:@"+%@",listItem.name];
        }
        [self addItemsObject:newListItem];
    }
    return self;
}

- (List *)addItemsFromList:(List *)list
{
    [self addItemsFromList:list uniqueIdPrefix:nil];
    return self;
}

- (void)setSortValue:(NSDecimalNumber *)value
{
    self.sortOrderValue = value;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[List class]]);
    List *otherList = (List *) other;
    if ([self.listName isEqualToString:otherList.listName]) {
        return YES;
    }
    return NO;
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[List class]]);
    List *otherList = (List *) other;
    return ([self.createdTimeStamp compare:otherList.createdTimeStamp] == NSOrderedAscending);
}


- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    NSMutableArray *itemsToEdit = [NSMutableArray arrayWithCapacity:20];
    if (self.listName) {
        EditableItem *item = [EditableItem owner:self value:self.listName  itemName:@"Name" sortValue:1];
        item.isReorderable = NO;
        item.isDeletable = NO;
        item.hasSortValue = YES;
        [itemsToEdit addObject:item];
    }
    if (self.items) {
        for (ListItem *listItem in self.items) {
            EditableItem *item = [EditableItem owner:listItem value:listItem.name itemName:nil decimalSortValue:listItem.sortOrderValue];
            item.isReorderable = YES;
            item.isDeletable = YES;
            item.hasSortValue = YES;
            [itemsToEdit addObject:item];
        }
    }
    return itemsToEdit.copy;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (NSString *)value
{
    return self.listName;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    self.listName = newValue;
}

- (NSDecimalNumber *)sortValue
{
    return self.sortOrderValue;
}


- (id)ownerObject
{
    return self;
}

- (NSArray *)addEditableItemForItemName:(NSString *)itemName
{
    ListItem *newItem = [ListItem createItemNamed:FILLER_EMPTY
                                        withValue:(self.items.count * 2)     // times 2 to be sure sorts to the last spot
                                      withContext:self.managedObjectContext];
    [self addItemsObject:newItem];
    return self.items.allObjects;
}

- (void)deleteEditableItem:(id <FDedit>)item forItemName:(NSString *)itemName
{
    assert (item.ownerObject);
    if (item.ownerObject) {
        [self removeItemsObject:item.ownerObject];
    }
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)isDeletable
{
    return NO;
}

- (BOOL)hasSortValue
{
    return NO;
}

@end
