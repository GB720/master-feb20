//
//  IncidentTemplate+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 03/10/20.
//  Copyright (c) 2020 Gerald Boyd. All rights reserved.
//

#import "IncidentTemplate+CoreDataClass.h"
#import "DepartmentTaskList+CoreDataClass.h"

@interface IncidentTemplate (Create)
+ (IncidentTemplate *) createFor:(FireDepartment *)fireDepartment;
+ (IncidentTemplate *) fetchFor:(FireDepartment *)fireDepartment;
- (void) addStagingGroupToTemplate:(StagingGroup *)stagingGroup;
- (void) addTaskListToTemplate:(DepartmentTaskList *)taskList  sortValue:(NSUInteger)sortValue;
// - (NSString *) templateIdWithDepartmentUniqueId:(NSString *)departmentUniqueId;
@end
