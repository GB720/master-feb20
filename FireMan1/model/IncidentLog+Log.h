//
// Created by Gerald Boyd on 5/20/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "IncidentLog.h"

@class ListItem;
@class FireUnit;
@class List;

@interface IncidentLog (Log)
- (void) action:(NSString *)action;
- (void) action:(NSString *)action unit:(FireUnit *)fireUnit;
- (void) action:(NSString *)action task:(ListItem *)task;
- (void) action:(NSString *)action unit:(FireUnit *)fireUnit task:(ListItem *)task;
- (void) task:(ListItem *) task isChecked:(BOOL) isChecked;
- (void) list:(List *) list isCompleted: (BOOL) isCompleted;
@end