//
//  CurrentUser.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/19/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "SingletonBase.h"
#import "User+Create.h"
#import "Role+Create.h"
#import "Incident+Create.h"

@class User;
@class Device;

@interface CurrentUser : SingletonBase
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, readonly) NSString *userIdString;
@property (nonatomic, strong) User *userEntity;
@property (nonatomic, strong) Device *device;
@property (nonatomic, readonly) NSString *deviceName;
@property (nonatomic, strong) Incident *currentIncident;
@property (nonatomic, readonly) BOOL isCoordinator;
@property (nonatomic, strong) NSNumber *incidentId;
@property (nonatomic, strong) NSDate *incidentCreatedDate;
@property (nonatomic) BOOL isSingleRoleMode;
+ (CurrentUser *)sharedSingletonObject;
- (BOOL) hasPermission:(NSUInteger) permission;
- (void) reset;

@end
