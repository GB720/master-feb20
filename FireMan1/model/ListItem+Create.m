//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "ListItem+Create.h"
#import "List+Create.h"
#import "EditableItem.h"


@implementation ListItem (Create)
+(ListItem *) createItemNamed:(NSString *)name
        withValue:(NSUInteger)value
        withContext:(NSManagedObjectContext *)context
{
        // get entity
    ListItem *listItem = [NSEntityDescription
                insertNewObjectForEntityForName:@"ListItem"
                         inManagedObjectContext:context];
    listItem.name = name;
    listItem.currentValue = [NSString stringWithFormat:@"LI%lu",(unsigned long)value];
    listItem.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:value exponent:1 isNegative:NO];
    listItem.createdTimeStamp = [NSDate date];
    listItem.uniqueId = [[NSUUID UUID]UUIDString];
    return listItem;
}

+ (ListItem *) fromListItem: (ListItem *)item withContext:(NSManagedObjectContext *)context
{
    ListItem *newItem = [ListItem createItemNamed:item.name withValue:1 withContext:context];
    newItem.currentValue = item.currentValue;
    newItem.sortOrderValue = item.sortOrderValue;
    newItem.uniqueId = [[NSUUID UUID] UUIDString];
    return newItem;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[ListItem class]]);
    ListItem *otherListItem = (ListItem *) other;
    return [self.name isEqualToString:otherListItem.name];
}


- (NSString *)itemName
{
    return nil;
}


- (NSString *)value
{
    return self.name;
}

- (NSArray *)editableItems
{
    NSMutableArray *itemsToEdit = [NSMutableArray arrayWithCapacity:20];
    if (self.listName) {
        [itemsToEdit addObject:[EditableItem owner:self value:self.listName]];
    }
    if (self.items) {
        for (ListItem *listItem in self.items) {
            [itemsToEdit addObject:[EditableItem owner:listItem value:listItem.name]];
        }
    }
    return itemsToEdit.copy;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}

- (id)ownerObject
{
    return self;
}


- (NSDecimalNumber *) sortValue {
    return self.sortOrderValue;
}

- (void) setSortValue:(NSDecimalNumber *)value
{
    self.sortOrderValue = value;
}

- (void) save:(NSString *)newValue itemname:(NSString *)itemName
{
    if (newValue && ![newValue isEqualToString:self.name]) {
        self.name = newValue;
    }
}

@end
