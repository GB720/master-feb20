//
//  CurrentUser.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/19/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "CurrentUser.h"
#import "User.h"
#import "Device+Create.h"


@interface CurrentUser ()

@end

@implementation CurrentUser

+ (CurrentUser *)sharedSingletonObject
{
    return (CurrentUser *) [super sharedSingletonObject];
}

-(void) reset
{
    self.context = nil;
    self.device = nil;
}

- (NSString *) name {
    if (self.device) {  // will get user from device
        return self.userEntity.name;
    }
    return @"";
}

- (NSString *) userIdString {
    return [NSString stringWithFormat:@"User: %@",self.name];
}

- (void) setUserEntity:(User *)userEntity
{
    self.device.user = userEntity;
}

- (User *) userEntity
{
    return self.device.user;
}

- (BOOL) hasPermission:(NSUInteger) permission
{
    return [self.userEntity hasPermission:permission];
}

- (BOOL) isCoordinator
{
    if ([self.name isEqualToString:self.currentIncident.incidentCoordinator.name]
            && [self.device.deviceId isEqualToString:self.currentIncident.incidentCoordinator.usingDevice.deviceId])
    {
        return YES;
    }
    return NO;        
}

- (Device *)device
{
    if (!_device) {
        [self setupDevice];
    }
    return _device;
}

- (Device *)setupDevice
{
    NSString *deviceId = [self getVendorDeviceId];
    _device = [Device deviceWithId:deviceId context:self.context];
    if (!_device) {
        // create a new device entity in database
        _device = [Device createWithId:deviceId context:self.context];
    }
    return _device;
}

- (NSString *)getVendorDeviceId
{
    UIDevice *myDevice = [UIDevice currentDevice];
    NSUUID *myDeviceId = myDevice.identifierForVendor;
    return myDeviceId.UUIDString;
}

- (NSString *)deviceName
{
    UIDevice *myDevice = [UIDevice currentDevice];
    return myDevice.name;
}

- (void) setCurrentIncident:(Incident *)currentIncident
{
    self.device.currentIncident = currentIncident;
}

- (Incident *) currentIncident
{
    return self.device.currentIncident;
}

@end
