//
// Created by Gerald Boyd on 8/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User+CoreDataClass.h"
#import "FDedit.h"
#import "Role+Create.h"
#import "FDcompare.h"

@interface User (Create) <FDcompare, FDedit>
+ (User *) createWithName: (NSString *)newName onDevice:(NSString *)deviceId context: (NSManagedObjectContext *) context;
+ (User *) userWithName:(NSString *)name onDevice:(NSString *)deviceId context:(NSManagedObjectContext *)context;
- (BOOL) hasPermission:(NSUInteger) permission;
@end
