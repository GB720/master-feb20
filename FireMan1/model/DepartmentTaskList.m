//
//  DepartmentTaskList.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "DepartmentTaskList.h"
#import "FireDepartment.h"
#import "IncidentTaskList.h"


@implementation DepartmentTaskList

@dynamic createdTimeStamp;
@dynamic forFireDepartment;
@dynamic incidentListCopy;

@end
