//
//  ListItem.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "List.h"

@class FireUnit, Incident, List, LogItem;

@interface ListItem : List

@property (nonatomic, retain) NSString * currentValue;
@property (nonatomic, retain) NSString * itemType;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * timeLimit;
@property (nonatomic, retain) NSDate * createdTimeStamp;
@property (nonatomic, retain) NSSet *assignedUnits;
@property (nonatomic, retain) FireUnit *coordinatingUnit;
@property (nonatomic, retain) List *forList;
@property (nonatomic, retain) NSSet *logItems;
@property (nonatomic, retain) Incident *managedByIncident;
@end

@interface ListItem (CoreDataGeneratedAccessors)

- (void)addAssignedUnitsObject:(FireUnit *)value;
- (void)removeAssignedUnitsObject:(FireUnit *)value;
- (void)addAssignedUnits:(NSSet *)values;
- (void)removeAssignedUnits:(NSSet *)values;

- (void)addLogItemsObject:(LogItem *)value;
- (void)removeLogItemsObject:(LogItem *)value;
- (void)addLogItems:(NSSet *)values;
- (void)removeLogItems:(NSSet *)values;

@end
