//
//  DefaultStage+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/13/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "DefaultStage+CoreDataClass.h"
#import "FireDepartment+CoreDataProperties.h"
#import "FDcompare.h"
#import "FDedit.h"

static NSString *const TIME_REQD = @"Time Required";

static NSString *const NAME = @"Name";

@interface DefaultStage (Create) <FDcompare, FDedit>
+ (DefaultStage *) createWithName: (NSString *)newName forDepartment:(FireDepartment *)fireDepartment context: (NSManagedObjectContext *) context;
@end
