//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "IncidentTaskList+Create.h"
#import "DepartmentTaskList.h"
#import "ListItem.h"


@implementation IncidentTaskList (Create)
+ (IncidentTaskList *)createList:(NSString *)name withContext: (NSManagedObjectContext *) context
{
    // get entity
    IncidentTaskList *incidentTaskList = [NSEntityDescription
            insertNewObjectForEntityForName:@"IncidentTaskList"
                     inManagedObjectContext:context];
    incidentTaskList.listName = name;
    incidentTaskList.createdTimeStamp = [NSDate date];
    incidentTaskList.uniqueId = [[NSUUID UUID]UUIDString];
    return incidentTaskList;
}

+ (IncidentTaskList *)fromDepartmentList:(DepartmentTaskList *)departmentList uniqueId:(NSString *)uniqueIdPrefix withContext: (NSManagedObjectContext *) context
{
    IncidentTaskList *incidentTaskList = [IncidentTaskList createList:departmentList.listName withContext:context];
    if (uniqueIdPrefix) {
        incidentTaskList.uniqueId = uniqueIdPrefix;
    }
    NSString *itemPrefix = nil;
    if (uniqueIdPrefix) {
        itemPrefix = [uniqueIdPrefix stringByAppendingFormat:@"+%@",departmentList.listName];
    }
    incidentTaskList = (IncidentTaskList *) [incidentTaskList addItemsFromList:departmentList uniqueIdPrefix:itemPrefix];
    incidentTaskList.fromDepartmentList = departmentList;
    return incidentTaskList;
}

+ (IncidentTaskList *)fromDepartmentList:(DepartmentTaskList *)departmentList withContext: (NSManagedObjectContext *) context
{
    IncidentTaskList *incidentTaskList = [IncidentTaskList fromDepartmentList:departmentList uniqueId:nil withContext:context];
    return incidentTaskList;
}

- (void)deleteListItems
{
    NSSet *listItems = self.items.copy;
    for (ListItem *listItem in listItems) {
        [self removeItemsObject:listItem];
        [self.managedObjectContext deleteObject:listItem];
    }
}


@end
