//
// Created by Gerald Boyd on 5/20/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "LogItem+Create.h"
// #import "FireUnit.h"
// #import "ListItem.h"
// #import "Incident.h"
#import "CurrentUser.h"
#import "ListItem+Status.h"
#import "List.h"
#import "FireUnit+Identification.h"

@class CurrentUser, FireUnit, ListItem, Incident, List;

@implementation LogItem (Create)
+ (LogItem *) action:(NSString *)action
                unit:(FireUnit *)unit
                task:(ListItem *)task
         withContext: (NSManagedObjectContext *) context
{
    // get entity
    LogItem *logItem = [self createLogItemWithContext:context];
    logItem.action = action;
    if (unit){
        logItem.unitName = unit.prefixedName;
        logItem.forFireUnit = unit;
    }
    if (task){
        logItem.taskName = task.name;
        logItem.forListItem = task;
    }
    logItem.timeStamp = [NSDate date];
    logItem.uniqueId =[[NSUUID UUID]UUIDString];

    return logItem;
}

+ (LogItem *) task:(ListItem *)task
         isChecked:(BOOL) isChecked
       withContext: (NSManagedObjectContext *) context
{
    // get entity
    LogItem *logItem = [self createLogItemWithContext:context];
    NSString *state = @"UN-CHECKED";
    if (isChecked)
        state = @"CHECKED";
    logItem.action = [NSString stringWithFormat:@"%@", state];
    logItem.taskName = task.name;
    logItem.forListItem = task;
    logItem.timeStamp = [NSDate date];
    return logItem;
}

+ (LogItem *) list: (List *) list
       isCompleted:(BOOL) isCompleted
       withContext: (NSManagedObjectContext *) context
{
    // get entity
    LogItem *logItem = [self createLogItemWithContext:context];
    NSString *state = @"NOT Complete";
    if (isCompleted)
        state = @"COMPLETE";
    logItem.action = [NSString stringWithFormat:@"%@", state];
    logItem.taskName = list.listName;
    logItem.timeStamp = [NSDate date];
    return logItem;
}


+ (LogItem *) createLogItemWithContext:(NSManagedObjectContext *) context
{
    LogItem *logItem = [NSEntityDescription
            insertNewObjectForEntityForName:@"LogItem"
                     inManagedObjectContext:context];

    NSString *currentDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    logItem.deviceIdentity = currentDeviceId;
    logItem.byUserNamed = [[CurrentUser sharedSingletonObject] name];
    logItem.uniqueId = [[NSUUID UUID]UUIDString];
    return logItem;
}
@end
