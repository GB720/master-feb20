//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "IncidentTaskList.h"
#import "List+Create.h"

@interface IncidentTaskList (Create)
+ (IncidentTaskList *)createList: (NSString *)name withContext: (NSManagedObjectContext *) context;
+ (IncidentTaskList *)fromDepartmentList:(DepartmentTaskList *)departmentList uniqueId:(NSString *)uniqueIdPrefix withContext: (NSManagedObjectContext *) context;
+ (IncidentTaskList *)fromDepartmentList:(DepartmentTaskList *)departmentList withContext: (NSManagedObjectContext *) context;
- (void) deleteListItems;
@end
