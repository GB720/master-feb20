//
// Created by Gerald Boyd on 5/24/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "SortedTaskList.h"
#import "ListItem.h"
#import "List+Status.h"


@implementation SortedTaskList {}

- (id)initWithList: (List *)list {
    self = [super init];
    if (self) {
        self.name = list.listName;
        _list = nil;
        self.expand = [list.expanded boolValue];
        self.managedListObject = list;
    }
    return self;
}

// getter for list
- (NSArray *) list
{
    if (!_list) {
        // get and sort the list

        // setup sort descriptor on sortOrderValue
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrderValue" ascending:YES selector:@selector(compare:)];
        NSArray *sortDescriptors = @[sortDescriptor];

        // sort and return the list
        _list = [self.managedListObject.items sortedArrayUsingDescriptors:sortDescriptors];
        [self resetSortOrderValues];
    }
    return _list;
}

- (NSArray *) itemNames
{
    NSMutableArray *arrayOfNames = [NSMutableArray arrayWithCapacity:20];
    for (ListItem *item in self.list) {
        [arrayOfNames addObject:item.name];
    }
    return arrayOfNames.copy;
}

- (BOOL) isCompleted
{
    return self.managedListObject.isCompleted;
}

- (BOOL)hasCheckedItems
{
    return self.managedListObject.hasCheckedItems;
}

- (NSInteger)maxRow
{
    return self.list.count + 1;
}

- (void)resetSortOrderValues
{
    NSInteger index = 1;
    for (ListItem *listItem in self.list) {
        listItem.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:(unsigned long long int) index++
                                                                     exponent:1
                                                                   isNegative:NO];
    }
}
@end