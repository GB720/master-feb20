//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "DepartmentResources+Create.h"


@implementation DepartmentResources (Create)
    NSString *resourcesUniqueId;


+ (DepartmentResources *) withUniqueId:(NSString *)uniqueIdToUse createInContext:(NSManagedObjectContext *)context
{
    resourcesUniqueId = uniqueIdToUse;
    DepartmentResources *departmentResourcesEntity = [NSEntityDescription
             insertNewObjectForEntityForName:@"DepartmentResources"
                      inManagedObjectContext:context];
    return departmentResourcesEntity;
}

+ (DepartmentResources *)createInContext:(NSManagedObjectContext *)context
{
    resourcesUniqueId = nil;
    DepartmentResources *departmentResourcesEntity = [NSEntityDescription
            insertNewObjectForEntityForName:@"DepartmentResources"
                     inManagedObjectContext:context];
    NSAssert(departmentResourcesEntity, @"departmentResourcesEntity exists");
    return departmentResourcesEntity;
}

+ (DepartmentResources *)fetchFor:(FireDepartment *)department
{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DepartmentResources"];
        
        // Set predicate to fetch user with the specified id
        NSPredicate *predicate;
        NSString *uniqueId = [NSString stringWithFormat:@"resourceList-%@", department.uniqueId];
        predicate = [NSPredicate predicateWithFormat:@"uniqueId like %@", uniqueId];
        [request setPredicate:predicate];
        
        // execute the fetch request
        NSError *error = nil;
        NSArray *fetchedArray = [department.managedObjectContext executeFetchRequest:request error:&error];
        if (fetchedArray == nil || fetchedArray.count == 0) {
            return nil;
        }
        DepartmentResources *resourceList = fetchedArray[0];
        return resourceList;
}

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueId
        || self.uniqueId.length == 0
        || [self.uniqueId isEqualToString:@" "]) {
        self.uniqueId = resourcesUniqueId;
    }
}
@end
