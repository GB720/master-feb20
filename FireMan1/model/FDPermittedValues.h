//
// Created by Gerald Boyd on 8/14/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FDedit;

@protocol FDPermittedValues <NSObject>
- (NSUInteger) count;
- (NSString *) valueAt: (NSUInteger) index;
- (void) selectValue: (NSString *) value;
- (NSArray *) editableItems;
- (NSDictionary *) dictionary;
- (id <FDedit>) owner;
- (NSString *) itemName;
@end