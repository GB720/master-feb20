//
// Created by Gerald Boyd on 4/18/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "FireUnit+CoreDataClass.h"
#import "FDcompare.h"
#import "FDedit.h"

@class ListItem;

@interface FireUnit (Create) <FDcompare, FDedit>
+ (FireUnit *) createWithName: (NSString *)newName withContext: (NSManagedObjectContext *) context;
- (FireUnit *) clone;

- (BOOL)isGroupMember:(ListItem *)group;
- (BOOL) mayAddEditableItemForItemName:(NSString *)itemName;
@end
