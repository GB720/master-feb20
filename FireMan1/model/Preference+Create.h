//
//  Preference+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/2/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "Preference.h"
#import "FDcompare.h"

@interface Preference (Create) <FDcompare>
+ (Preference *) createWithName: (NSString *)newName withValue: (NSString *)value withContext: (NSManagedObjectContext *) context;
+ (NSSet *) updatePreferences:(NSSet *) fromValues with:(NSSet *) toValues;
@end
