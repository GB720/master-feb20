//
//  DefaultStage+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/13/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "DefaultStage+Create.h"
#import "EditableItem.h"

@implementation DefaultStage (Create)
+ (DefaultStage *) createWithName: (NSString *)newName forDepartment:(FireDepartment *)fireDepartment context: (NSManagedObjectContext *) context
{
    // get entity
    DefaultStage *defaultStage = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"DefaultStage"
                                      inManagedObjectContext:context];
    // set value of name
    defaultStage.name = newName;
    defaultStage.forFireDepartment = fireDepartment;
    defaultStage.uniqueId = [newName stringByAppendingFormat:@"+%@",fireDepartment.name];
    return defaultStage;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[DefaultStage class]]);
    DefaultStage *otherDefaultStage = (DefaultStage *) other;
    return [self.name isEqualToString:otherDefaultStage.name];
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[DefaultStage class]]);
    DefaultStage *otherDefaultStage = (DefaultStage *) other;
    return ([self.createdTimeStamp compare:otherDefaultStage.createdTimeStamp] == NSOrderedAscending);
}



- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:3];
    //[items addObject:[EditableItem fromString:self.name]];
    [items addObject:[EditableItem owner:self
                                   value:self.name
                                itemName:NAME
                               sortValue:(items.count + 1)]
    ];
    // treat value as empty if it less than 0
    NSString *timeReqdString = FILLER_EMPTY;
    if (self.timeRequired.floatValue >= 0) {
        timeReqdString = self.timeRequired.stringValue;
    }
    [items addObject:[EditableItem owner:self value:timeReqdString itemName:TIME_REQD sortValue:(items.count + 1)]];
    return items;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (NSString *)value
{
    return self.name;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    if ([itemName isEqualToString:TIME_REQD]) {
        self.timeRequired = @(newValue.floatValue);
    }
    else if ([itemName isEqualToString:NAME]) {
        self.name = newValue;
    }

}

- (NSDecimalNumber *)sortValue
{
    return [NSDecimalNumber zero];
}

- (void)setSortValue:(NSDecimalNumber *)value
{

}

- (id)ownerObject
{
    return self;
}

- (BOOL)hasSortValue
{
    return NO;
}

- (BOOL)isDeletable
{
    return YES;
}


@end
