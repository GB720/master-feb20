//
//  ListItem.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "ListItem.h"
#import "FireUnit.h"
#import "Incident.h"
#import "List.h"
#import "LogItem.h"


@implementation ListItem

@dynamic currentValue;
@dynamic itemType;
@dynamic name;
@dynamic status;
@dynamic timeLimit;
@dynamic createdTimeStamp;
@dynamic assignedUnits;
@dynamic coordinatingUnit;
@dynamic forList;
@dynamic logItems;
@dynamic managedByIncident;

@end
