//
//  FireDepartment+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 4/15/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireDepartment+CoreDataClass.h"
#import "FDcompare.h"
#import "FDedit.h"


static NSString *const PREFIX = @"Prefix";

// static NSString *const INCIDENTS = @"Incidents";

static NSString *const NEIGHBORS = @"Neighbor Departments";

static NSString *const GROUPS = @"Group Names";

static NSString *const TASKLISTS = @"Task Lists";

static NSString *const STAGING_GROUPS = @"Staging Groups";

static NSString *const DEPARTMENT = @"Department";

static NSString *const UNITS = @"Units";

static NSString *const ROLES = @"User Roles";

static NSString *const USERS = @"Users";

static NSString *const DEVICES = @"Devices";


@interface FireDepartment (Create) <FDcompare, FDedit>
+ (FireDepartment *) createWithName: (NSString *)newName withContext: (NSManagedObjectContext *) context;
+ (FireDepartment *)fetchDepartmentWithId:(NSInteger)id withContext:(NSManagedObjectContext *)context;

@property (nonatomic, readonly) NSArray *sortedTaskLists;

- (NSArray *)sortedIncidents;

- (NSArray *)sortedTaskListsExcludingListsForIncident:(Incident *) incident;

- (Incident *) incidentMatching: (Incident *) incident;

- (void)deleteDuplicateObjects;

- (DefaultStage *)makeDefaultStage:(NSString *)stageName timeRequired:(NSInteger)timeRequired;

- (NSSet *)cloudDeletes:(NSNotification *)notification;

- (NSSet *)cloudUpdates:(NSNotification *)notification;

- (BOOL)isFireDepartmentUpdate:(NSSet *)insertsAndUpdates;

- (NSSet *)removeDuplicatesIn:(NSSet *)set;

- (NSSet *)uniqueInSet:(NSSet *)set comparedTo:(NSSet *)comparisonSet;

- (FireDepartment *)mergeDept:(FireDepartment *)deptToMerge;

- (void)addFireUnit:(NSString *)fireUnitNames;

@end
