//
// Created by Gerald Boyd on 4/18/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "FireUnit+Create.h"
#import "FireUnit+Identification.h"
#import "DepartmentResources.h"
#import "StagingGroup.h"
#import "FireDepartment+Create.h"


@implementation FireUnit (Create)
+ (FireUnit *) createWithName: (NSString *)newName withContext: (NSManagedObjectContext *) context
{
    // get entity
    FireUnit *fireUnit = [NSEntityDescription
            insertNewObjectForEntityForName:@"FireUnit"
                     inManagedObjectContext:context];

    // set value of name
    fireUnit.name = newName;
    fireUnit.createdTimeStamp = [NSDate date];
    fireUnit.uniqueId = [[NSUUID UUID] UUIDString];
    return fireUnit;
}

- (FireUnit *) clone
{
    FireUnit *clonedUnit;
    clonedUnit = [FireUnit createWithName:self.name withContext:self.managedObjectContext];
    clonedUnit.clonedFromUnit = self;
    clonedUnit.parStatusOk = @YES;
    clonedUnit.coordinatorFor = nil;
    return clonedUnit;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[FireUnit class]]);
    FireUnit *otherFireUnit = (FireUnit *) other;
    if ([self.prefixedName isEqualToString:otherFireUnit.prefixedName]) {
        return YES;
    }
    return NO;
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[FireUnit class]]);
    FireUnit *otherFireUnit = (FireUnit *) other;
    return ([self.createdTimeStamp compare:otherFireUnit.createdTimeStamp] == NSOrderedAscending);
}


- (BOOL) isGroupMember:(ListItem *) group
{
    BOOL isMember = NO;
    if (self.stagingGroup
            && ([group isKindOfClass:[StagingGroup class]])
            && [self.stagingGroup isEqual:group]) {
        isMember = YES;
    }
    else if (self.assignedTo
            && (![group isKindOfClass:[StagingGroup class]])
        && ([self.assignedTo isEqual:group])) {
        isMember = YES;
    }
    return isMember;
}

#pragma mark FDedit protocol

- (BOOL) mayAddEditableItemForItemName:(NSString *)itemName
{
    return NO;
}

- (NSString *) itemName
{
    return nil;
}

- (NSString *)value
{
    return self.name;
}



- (NSDecimalNumber *)sortValue
{
    return [NSDecimalNumber zero];
}

- (void)setSortValue:(NSDecimalNumber *)value
{

}

- (id)ownerObject
{
    return self;
}


- (NSArray *) editableItems
{
    return nil;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)hasSortValue
{
    return NO;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName {
    // do nothing
}

@end
