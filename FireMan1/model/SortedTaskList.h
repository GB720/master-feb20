//
// Created by Gerald Boyd on 5/24/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "List.h"

@class List;


@interface SortedTaskList : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *list;
@property (strong, nonatomic) List *managedListObject;
@property(nonatomic) BOOL expand;
@property (nonatomic) NSInteger maxRow;
@property (nonatomic) BOOL isCompleted;
@property (nonatomic) BOOL hasCheckedItems;
@property (nonatomic, strong) NSArray *itemNames;

- (id)initWithList: (List *)managedListObject;
@end