//
//  GroupName+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/7/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "GroupName+Create.h"
#import "EditableItem.h"
#import "FireDepartment+CoreDataProperties.h"

@implementation GroupName (Create)
+ (GroupName *) createWithName: (NSString *)newName withContext: (NSManagedObjectContext *) context
{
    // get entity
    GroupName *groupName = [NSEntityDescription
            insertNewObjectForEntityForName:@"GroupName"
                     inManagedObjectContext:context];
    groupName.name = newName;
    groupName.createdTimeStamp = [NSDate date];
    groupName.uniqueId = [[NSUUID UUID]UUIDString];
    return groupName;
}

+ (GroupName *) createDefault: (NSString *)newName forDepartment: (FireDepartment *)department
{
    NSManagedObjectContext *context = department.managedObjectContext;
    GroupName *groupName = [self createWithName:newName withContext:context];
    groupName.uniqueId = [newName stringByAppendingString:department.name];
    return groupName;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[GroupName class]]);
    GroupName *otherGroupName = (GroupName *) other;
    return [self.name isEqualToString:otherGroupName.name];
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[GroupName class]]);
    GroupName *otherGroupName = (GroupName *) other;
    return ([self.createdTimeStamp compare:otherGroupName.createdTimeStamp] == NSOrderedAscending);
}


- (NSArray *)editableItems
{
    return nil;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (NSString *) value
{
    return self.name;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    self.name = newValue;
}

- (NSDecimalNumber *)sortValue
{
    return nil;
}

- (void)setSortValue:(NSDecimalNumber *)value
{
    // do nothing
}

- (id)ownerObject
{
    return self;
}


- (void) setValue:(NSString *) newValue
{
    if (newValue) {
        self.name = newValue;
    }
}

- (NSString *) itemName
{
    return nil;
}

- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)hasSortValue
{
    return NO;
}
@end
