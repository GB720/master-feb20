//
//  DepartmentResources.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FireDepartment, FireUnit;

@interface DepartmentResources : NSManagedObject

@property (nonatomic, retain) NSDate * createdTimeStamp;
@property (nonatomic, retain) NSSet *fireUnits;
@property (nonatomic, retain) FireDepartment *forDepartment;
@end

@interface DepartmentResources (CoreDataGeneratedAccessors)

- (void)addFireUnitsObject:(FireUnit *)value;
- (void)removeFireUnitsObject:(FireUnit *)value;
- (void)addFireUnits:(NSSet *)values;
- (void)removeFireUnits:(NSSet *)values;

@end
