//
// Created by Gerald Boyd on 8/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "Role+Create.h"
#import "User.h"
#import "User+Create.h"
#import "EditableItem.h"
#import "PermittedValues.h"

static NSString *const ROLE_NAME = @"Role Name";
static NSString *const ROLE_PERMISSION = @"Type";
static NSString *const ROLE_USERS = @"Users";

@implementation Role (Create)
+ (Role *)createWithName:(NSString *)newName withPermission:(enum FD_rolePermission)permissionValue context:(NSManagedObjectContext *)context
{
    Role *role = [NSEntityDescription
            insertNewObjectForEntityForName:@"Role"
                     inManagedObjectContext:context];
    // set value of name
    role.name = newName;
    role.type = @(permissionValue);
    role.uniqueId = [NSString stringWithFormat:@"role-%@",newName];

    return role;
}

+ (Role *)roleWithName:(NSString *)name context:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Role"];

    // Set predicate to fetch device with the specified id
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@", name];
    [request setPredicate:predicate];

    // execute the fetch request
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    Role *role = fetchedArray[0];
    return role;
}

- (User *)createUserWithName:(NSString *)newName onDevice:(NSString *) deviceId
{
    User *user = [User createWithName:newName onDevice:deviceId context:self.managedObjectContext];
    user.role = self;
    user.inDepartment = self.forDepartment;
    return user;
}

- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:30];
    //[items addObject:[EditableItem fromString:self.name]];
    [items addObject:[EditableItem owner:self
                                   value:self.name
                                itemName:ROLE_NAME
                               sortValue:(items.count + 1)]];
    id <FDPermittedValues> const permissionValues = [self permittedValuesFor:ROLE_PERMISSION];
    NSDictionary *permissionDictionary = permissionValues.dictionary;
    NSString *roleType =  [permissionDictionary allKeysForObject:self.type].firstObject;
    [items addObject:[EditableItem owner:self
                                   value:roleType
                                  permittedValues:permissionValues
                                itemName:ROLE_PERMISSION
                               sortValue:(items.count + 1)]];
    [items addObject:[EditableItem owner:self named:ROLE_USERS fromArray:self.users.allObjects sortValue:(items.count + 1)]];
    return items;
}

- (id <FDPermittedValues>)permittedValues
{
    return [self permittedValuesFor:ROLE_PERMISSION];
}

- (id <FDPermittedValues>)permittedValuesFor:(NSString *) itemName
{
    if ([itemName isEqualToString:ROLE_PERMISSION]) {
        NSDictionary *permittedValuesDictionary = @{
                ADMIN_TYPE : @(FDAdministrator),
                COORDINATOR_TYPE :@(FDIncidentManager),
                VIEW_ONLY_TYPE :@(FDViewOnly)
        };
        return [PermittedValues withDictionary:permittedValuesDictionary owner:self forItemName:ROLE_PERMISSION];
    }
    return nil;
}

- (NSDictionary *) permissionsDictionary
{
    return nil;
}

- (NSString *)value
{
    return self.name;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    if (newValue && ![newValue isEqualToString:self.name]) {
        self.name = newValue;
    }
}

- (void) saveValueObject:(id) valueObject forItemName:(NSString *)itemName
{
    if ([itemName isEqualToString:ROLE_PERMISSION]) {
        assert ([valueObject isKindOfClass:[NSNumber class]]);
        self.type = valueObject;
    }
}


- (NSDecimalNumber *)sortValue
{
    return nil;
}

- (void)setSortValue:(NSDecimalNumber *)value
{
    //do nothing
}

- (id)ownerObject
{
    return self;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)hasSortValue
{
    return NO;
}

@end
