//
// Created by Gerald Boyd on 5/31/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "ListItem+Status.h"


@implementation ListItem (Status)
- (BOOL) isChecked
{
    return [self.status isEqualToString:@"Checked"];
}

- (void) setIsChecked: (BOOL) value
{
    if (value) {
        self.status = @"Checked";
    }
    else {
        self.status = @"";
    }
}
@end