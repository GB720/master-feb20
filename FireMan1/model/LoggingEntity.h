//
//  LoggingEntity.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/12/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoggingEntity : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *eventLog;
- (void) logEvent: (NSString *) event;
- (void)logEvent:(NSString *)event atTime:(NSString *) timeStamp;
@end
