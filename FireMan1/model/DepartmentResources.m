//
//  DepartmentResources.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "DepartmentResources.h"
#import "FireDepartment.h"
#import "FireUnit.h"


@implementation DepartmentResources

@dynamic createdTimeStamp;
@dynamic fireUnits;
@dynamic forDepartment;

@end
