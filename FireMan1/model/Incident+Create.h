//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#ifndef Incident_h
#define Incident_h
#import "Incident+CoreDataProperties.h"

#endif /* Incident_h */

#import <Foundation/Foundation.h>
#import "FDcompare.h"
#import "FDedit.h"

@class IncidentLog, ListItem;

@interface Incident (Create) <FDcompare, FDedit>
+ (Incident *)createWithName:(NSString *)name withContext:(NSManagedObjectContext *)context;
+ (Incident *)fetchIncident:(NSNumber *)incidentId withDate:(NSDate *)createdDate withContext: (NSManagedObjectContext *) context;

- (void) beginIncident;

- (void)deleteIncidentItems;

@property (readonly, nonatomic) IncidentLog *log;
- (NSArray *)sortedFireUnits;
- (NSArray *)sortedFireUnitsWithActiveGroup: (ListItem *)activeGroup;
@property (nonatomic, readonly) NSInteger nextGroupValue;
@property (nonatomic, readonly) NSArray *sortedTaskLists;      // array of SortedTaskList
@property (nonatomic, readonly) NSArray *sortedStagingLevels; // array of StagingGroup
@end
