//
//  StagingGroup+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/13/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "StagingGroup+Create.h"

@implementation StagingGroup (Create)
+ (StagingGroup *) createWithName: (NSString *)newName context: (NSManagedObjectContext *) context
{
    // get entity
    StagingGroup *stagingGroup = [NSEntityDescription
            insertNewObjectForEntityForName:@"StagingGroup"
                     inManagedObjectContext:context];

    // set value of name
    stagingGroup.name = newName;
    stagingGroup.createdTimeStamp = [NSDate date];
    stagingGroup.uniqueId = [[NSUUID UUID]UUIDString];
    return stagingGroup;
}

+ (StagingGroup *) from: (StagingGroup *)stagingGroup
{
    StagingGroup *newStagingGroup = [StagingGroup createWithName:stagingGroup.name
                                                         context:stagingGroup.managedObjectContext];
    newStagingGroup.timeRequired = stagingGroup.timeRequired;
    newStagingGroup.sortOrderValue = stagingGroup.sortOrderValue;
    return newStagingGroup;
}

+ (StagingGroup *) fromDefaultStage: (DefaultStage *)defaultStage
{
    StagingGroup *newStagingGroup = [StagingGroup createWithName:defaultStage.name context:defaultStage.managedObjectContext];
    newStagingGroup.timeRequired = defaultStage.timeRequired;
    return newStagingGroup;
}

- (BOOL)isReorderable
{
    return YES;
}


- (BOOL)isDeletable
{
    return YES;
}


- (BOOL)hasSortValue
{
    return YES;
}


- (BOOL)mayDeleteEditableItem
{
    return NO;
}


@end
