//
// Created by Gerald Boyd on 5/31/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "List+Status.h"
#import "ListItem+Status.h"

@interface List ()
@property (nonatomic, readwrite) BOOL isCompleted;
@property (nonatomic, readwrite) BOOL hasCheckedItems;
@end

@implementation List (Status)

// list is marked as isCompleted when all the list items are checked
- (BOOL)isCompleted
{
    BOOL value = YES;
    if (self.items.count == 0) {
        value = NO;
    }
    else {
        for (ListItem *listItem in self.items)  {
            if (!listItem.isChecked) {
                value = NO;
                break;
            }
        }
    }
    return value;
}

// returns true if any of the list items are checkec
- (BOOL)hasCheckedItems
{
    BOOL value = NO;
    if (self.items.count > 0) {
        for (ListItem *listItem in self.items)  {
            if (listItem.isChecked) {
                value = YES;
                break;
            }
        }
    }
    return value;
}
@end
