//
//  LoggingEntity.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/12/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "LoggingEntity.h"

@implementation LoggingEntity

-(void) logEvent:(NSString *)event
{
    /* set timestamp and entity name */
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *currentDate = [NSDate date];
    NSString *currentDateAndTime =  [dateFormatter stringFromDate: currentDate];
    [self logEvent:event atTime: currentDateAndTime];
}

-(void) logEvent:(NSString *)event atTime:(NSString *) timeStamp
{
    // NSString *stampedEvent = [timeStamp stringByAppendingFormat:(@":%@"),event];
    // NSLog(@"%@ %@", stampedEvent, self.name);
}

@end
