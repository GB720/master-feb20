//
//  IncidentTemplate+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/18/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IncidentTemplate+Create.h"
#import "StagingGroup+Create.h"
#import "IncidentTaskList+Create.h"
#import "DepartmentTaskList+CoreDataClass.h"
#import "FireDepartment+CoreDataClass.h"

@implementation IncidentTemplate (Create) 
    

+ (IncidentTemplate *) createFor:(FireDepartment *)fireDepartment
{
    NSManagedObjectContext *context = fireDepartment.managedObjectContext;
    // get entity
    IncidentTemplate *incidentTemplate = [NSEntityDescription
                                  insertNewObjectForEntityForName:@"IncidentTemplate"
                                  inManagedObjectContext:context];
    
    // set linkage to fire department
    incidentTemplate.forFireDepartment = fireDepartment;
    incidentTemplate.uniqueId = [incidentTemplate templateIdWithDepartmentUniqueId:fireDepartment.uniqueId];
    // template one to one with fire department same across devices
    
    return incidentTemplate;
}

+ (IncidentTemplate *) fetchFor:(FireDepartment *)fireDepartment 
{
    NSManagedObjectContext *context = fireDepartment.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"IncidentTemplate"];
    NSString *uniqueId =  [NSString stringWithFormat:@"IncidentTemplate-%@",fireDepartment.uniqueId];
    // Set predicate to fetch template with the specified id
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uniqueId like %@", uniqueId];
    [request setPredicate:predicate];
    
    // execute the fetch request
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    IncidentTemplate *incidentTemplate = fetchedArray[0];
    return incidentTemplate;
}

- (NSString *) templateIdWithDepartmentUniqueId:(NSString *)departmentUniqueId
{
    return [NSString stringWithFormat:@"IncidentTemplate-%@",departmentUniqueId];
}


- (void) addStagingGroupToTemplate:(StagingGroup *)stagingGroup
{
    // add copy of stagingGroup, uniqueId defines same obj with multiple devices
    StagingGroup *stagingGroupForTemplate = [StagingGroup from:stagingGroup];
    stagingGroupForTemplate.uniqueId = [NSString stringWithFormat:@"template-stagingGroup-%@",stagingGroup.name];
    [self addStagingGroupsObject:stagingGroupForTemplate];
}

- (void) addTaskListToTemplate:(DepartmentTaskList *)taskList sortValue:(NSUInteger)sortValue
{
    IncidentTaskList *incidentTaskList = [IncidentTaskList fromDepartmentList:taskList uniqueId:[NSString stringWithFormat:@"template+taskList+%@",taskList.listName] withContext:self.managedObjectContext];
     incidentTaskList.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:sortValue exponent:1 isNegative:NO];
    [self addTaskListsObject:incidentTaskList];
    
}

@end
