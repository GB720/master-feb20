//
//  Preference.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FireDepartment, Incident;

@interface Preference : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSDate * createdTimeStamp;
@property (nonatomic, retain) FireDepartment *forFireDepartment;
@property (nonatomic, retain) Incident *forIncident;

@end
