//
// Created by Gerald Boyd on 8/14/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "PermittedValues.h"
#import "FDedit.h"
#import "EditableItem.h"


@interface PermittedValues ()
@property(nonatomic, strong) NSDictionary *dictionary;
@property(nonatomic, strong) id <FDedit> ownerObject;
@end

@implementation PermittedValues {

}

+ (id <FDPermittedValues>)withDictionary:(NSDictionary *)dictionary owner:(id <FDedit>) owner forItemName: (NSString *)itemName
{
    PermittedValues *permittedValues = [PermittedValues withOwner:owner forItemName:itemName];
    permittedValues.dictionary = dictionary;
    return permittedValues;
}

+ (id <FDPermittedValues>)withOwner:(id <FDedit>)owner
{
    PermittedValues *permittedValues = [[PermittedValues alloc]init];
    permittedValues.ownerObject = owner;
    return permittedValues;
}


+ (PermittedValues *)withOwner:(id <FDedit>)owner forItemName:(NSString *)itemName
{
    PermittedValues *permittedValues = [self withOwner:owner];
    permittedValues.itemName = itemName;
    return permittedValues;
}

// designated initializer
- (id) initWithOwner:(id <FDedit>) owner
{
    self = [self init];
    if (self) {
        self.ownerObject = owner;
        self.dictionary = nil;
    }
    return self;
}

- (NSUInteger)count
{
    if (self.dictionary) {
        return self.dictionary.count;
    }
    return 0;
}


- (NSString *)valueAt:(NSUInteger)index
{
    return nil;
}

- (id <FDedit>)owner
{
    return self.ownerObject;
}

- (void)selectValue:(NSString *)value
{
    if (self.dictionary) {
        id dictionaryObject = (self.dictionary)[value];
        if ([self.ownerObject respondsToSelector:@selector(saveValueObject:forItemName:)]) {
            [self.ownerObject performSelector:@selector(saveValueObject:forItemName:) withObject:dictionaryObject withObject:self.itemName];
        }
    }
}

- (NSArray *)editableItems
{
    if (!_editableItems && _dictionary) {
        // convert dictionary to array of editableItems
        _editableItems = [self editableItemsFromDictionary:_dictionary];
    }
    return _editableItems;
}

- (NSArray *)editableItemsFromDictionary:(NSDictionary *)dictionary
{
    assert (_dictionary);
    NSArray *sortedKeys = [dictionary.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableArray *editableItems = [NSMutableArray arrayWithCapacity:sortedKeys.count];
    for (NSString *key in sortedKeys) {
        [editableItems addObject:[EditableItem owner:self.ownerObject value:key itemName:nil]];
    }
    return editableItems;
}

- (NSDictionary *)dictionary
{
    if (_dictionary) {
        return _dictionary;
    }
    return nil;
}


@end
