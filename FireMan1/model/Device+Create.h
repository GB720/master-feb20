//
// Created by Gerald Boyd on 8/5/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"
#import "FDedit.h"
#import "FDcompare.h"

@interface Device (Create) <FDcompare, FDedit>
+ (Device *) createWithId: (NSString *)IDString context: (NSManagedObjectContext *) context;
+ (Device *) deviceWithId:(NSString *)id context:(NSManagedObjectContext *)context;
- (void)setSortValue:(NSDecimalNumber *)value;
@end