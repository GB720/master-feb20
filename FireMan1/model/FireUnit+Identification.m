//
// Created by Gerald Boyd on 6/6/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
//
#import "FireUnit+Identification.h"
#import "DepartmentResources.h"
#import "FireDepartment.h"
#import "Incident.h"
#import "GbElapsedTime.h"
#import "UIColor+EmerResStrat.h"

@implementation FireUnit (Identification)

// returns the fireunit name with the neighbor department prefixed if the unit belongs to a neighbor department
- (NSString *)prefixedName
{
    NSString *myName;
    Incident *incident = self.forIncident;
    if (!incident) {
        myName = self.name;
    }
    else {
        myName = [self prefixedNameInIncident:incident];
    }
    return myName;
}

- (NSString *) prefixedNameInIncident:(Incident *)incident
{
    // is my owning department different from the department for the incident
    FireDepartment *myDepartment;
    if (self.clonedFromUnit) {
        myDepartment = self.clonedFromUnit.departmentResourceList.forDepartment;
    }
    else {
        myDepartment = self.departmentResourceList.forDepartment;
    }
    NSString *prefix;
    if (myDepartment.id.integerValue == 0) {
        prefix = @"";       // home department
    }
    else {
        if (self.clonedFromUnit) {
            prefix = self.clonedFromUnit.departmentResourceList.forDepartment.neighborPrefix;
        }
        else {
            prefix = self.departmentResourceList.forDepartment.neighborPrefix;
        }
    }
    if (!prefix) {
        prefix = @"?";
    }
    return [NSString stringWithFormat:@"%@%@",prefix,self.name];
}

- (NSUInteger) activeAge
{
    if (!self.activeStartTime) {
        self.activeStartTime = NSDate.date;
    }
    GbElapsedTime *elapsedTime = [[GbElapsedTime alloc] initSinceTime:self.activeStartTime];

    return elapsedTime.totalElapsedMinutes;

}

- (UIColor *)stagingColor
{
    // UIColor *PINK_COLOR = [UIColor colorWithRed:1.0 green:0.74 blue:.73 alpha:1.0];
    UIColor *pinkColor = [UIColor ersPinkColor];
    return pinkColor;
}
@end