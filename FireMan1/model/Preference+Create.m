//
//  Preference+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/2/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "Preference+Create.h"

@implementation Preference (Create)
+ (Preference *) createWithName: (NSString *)newName withValue: (NSString *)value withContext: (NSManagedObjectContext *) context
{ 
    // get entity
    Preference *preference = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Preference"
                                    inManagedObjectContext:context];
    preference.name = newName;
    preference.value = value;
    preference.createdTimeStamp = [NSDate date];
    preference.uniqueId = [[NSUUID UUID] UUIDString];
    return preference;
}

+ (NSSet *) updatePreferences:(NSSet *) fromValues with:(NSSet *) toValues
{
    NSMutableSet *updatedValues = (NSMutableSet *) fromValues.mutableCopy;
    NSMutableSet *toValuesSet = (NSMutableSet *) toValues.mutableCopy;
    for (Preference *p in toValues) {
        // update matching preferences
        for (Preference *fromPref in updatedValues) {
            if ([fromPref.name isEqualToString:p.name]) {
                fromPref.value = p.value;
                [toValuesSet removeObject:p];
            }
        }
    }
    // add remaining preferences in the to-set to the updated-set
    for (Preference *p in toValuesSet) {
        [updatedValues addObject:[Preference createWithName:p.name
                                                   withValue:p.value
                                                 withContext:p.managedObjectContext]];
    }
    return updatedValues.copy;  // return immutable copy of the set
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[Preference class]]);
    Preference *otherPreference = (Preference *) other;
    if ([self.name isEqualToString:otherPreference.name]) {
        return YES;
    }
    return NO;
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[Preference class]]);
    Preference *otherPreference = (Preference *) other;
    return ([self.createdTimeStamp compare:otherPreference.createdTimeStamp] == NSOrderedAscending);
}

@end
