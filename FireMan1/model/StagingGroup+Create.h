//
//  StagingGroup+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/13/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "StagingGroup.h"
#import "DefaultStage.h"
#import "FDedit.h"

@interface StagingGroup (Create)
+ (StagingGroup *) createWithName: (NSString *)newName context: (NSManagedObjectContext *) context;
+ (StagingGroup *) from: (StagingGroup *)stagingGroup;
+ (StagingGroup *) fromDefaultStage: (DefaultStage *)defaultStage;
@end
