//
//  FireUnit.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DepartmentResources, FireUnit, Incident, ListItem, LogItem, StagingGroup;

@interface FireUnit : NSManagedObject

@property (nonatomic, retain) NSDate * activeStartTime;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSNumber * parStatusOk;
@property (nonatomic, retain) NSDecimalNumber * sortOrderValue;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * typeOf;
@property (nonatomic, retain) NSDate * createdTimeStamp;
@property (nonatomic, retain) Incident *activeForIncident;
@property (nonatomic, retain) ListItem *assignedTo;
@property (nonatomic, retain) Incident *availableInIncident;
@property (nonatomic, retain) FireUnit *clonedFromUnit;
@property (nonatomic, retain) NSSet *clonedUnits;
@property (nonatomic, retain) Incident *commanderFor;
@property (nonatomic, retain) ListItem *coordinatorFor;
@property (nonatomic, retain) DepartmentResources *departmentResourceList;
@property (nonatomic, retain) Incident *forIncident;
@property (nonatomic, retain) NSSet *logItems;
@property (nonatomic, retain) Incident *rehabForIncident;
@property (nonatomic, retain) StagingGroup *stagingGroup;
@end

@interface FireUnit (CoreDataGeneratedAccessors)

- (void)addClonedUnitsObject:(FireUnit *)value;
- (void)removeClonedUnitsObject:(FireUnit *)value;
- (void)addClonedUnits:(NSSet *)values;
- (void)removeClonedUnits:(NSSet *)values;

- (void)addLogItemsObject:(LogItem *)value;
- (void)removeLogItemsObject:(LogItem *)value;
- (void)addLogItems:(NSSet *)values;
- (void)removeLogItems:(NSSet *)values;

@end
