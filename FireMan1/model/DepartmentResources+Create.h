//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "DepartmentResources+CoreDataClass.h"
#import "FireDepartment+CoreDataClass.h"

@interface DepartmentResources (Create)
+ (DepartmentResources *)createInContext:(NSManagedObjectContext *)context;
+ (DepartmentResources *) withUniqueId:(NSString *)uniqueIdToUse createInContext:(NSManagedObjectContext *)context;
+ (DepartmentResources *)fetchFor:(FireDepartment *)department;
@end
