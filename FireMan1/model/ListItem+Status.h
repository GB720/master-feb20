//
// Created by Gerald Boyd on 5/31/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#ifndef ListItem_h
#define ListItem_h

#import "ListItem+CoreDataClass.h"
#endif /* ListItem_h */
#import <Foundation/Foundation.h>

@interface ListItem (Status)
@property (nonatomic) BOOL isChecked;
@end
