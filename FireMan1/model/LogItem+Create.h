//
// Created by Gerald Boyd on 5/20/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "LogItem.h"

@class ListItem;
@class FireUnit;
@class List;

@interface LogItem (Create)
+ (LogItem *) action:(NSString *)action
                unit:(FireUnit *)unit
                task:(ListItem *)task
         withContext: (NSManagedObjectContext *) context;
+ (LogItem *) task:(ListItem *)task
         isChecked:(BOOL) isChecked
         withContext: (NSManagedObjectContext *) context;
+ (LogItem *) list: (List *) list
         isCompleted:(BOOL) isCompleted
       withContext: (NSManagedObjectContext *) context;

@end