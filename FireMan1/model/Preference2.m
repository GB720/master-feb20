//
//  Preference.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "Preference.h"
#import "FireDepartment.h"
#import "Incident.h"


@implementation Preference

@dynamic name;
@dynamic value;
@dynamic createdTimeStamp;
@dynamic forFireDepartment;
@dynamic forIncident;

@end
