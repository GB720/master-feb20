//
// Created by Gerald Boyd on 8/14/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "PermittedValues.h"
#import "EntityPermittedValues.h"
#import "FDedit.h"


@interface EntityPermittedValues ()
@property(nonatomic, strong) NSArray *entities;
@end

@implementation EntityPermittedValues {

}


- (id)init
{
    self = [super init];
    if (self) {
        self.entities = nil;
    }
    return self;
}

// Designated Initializer
- (id) initWithOxwner:(id <FDedit>)owner
{
    self = [super initWithOwner:owner];
    return self;
}


+ (EntityPermittedValues *)withEntities:(NSSet *)set owner:(id <FDedit>) owner forItemName:(NSString *) itemName
{
    EntityPermittedValues *permittedValues = [[EntityPermittedValues alloc]initWithOwner:owner];
    permittedValues.itemName = itemName;
    permittedValues.entities = set.allObjects;
    return permittedValues;
}

- (NSArray *) editableItems
{
    return self.entities;
}


- (NSUInteger)count
{
    if (self.entities) {
        return self.entities.count;
    }
    return 0;
}

- (void)selectValue:(NSString *)value
{
    for (id <FDedit> entity in self.entities) {
        if ([entity.value isEqualToString:value]) {
            [self.owner updateRelationshipWith:entity];
        }
    }
}


@end
