//
//  FireDepartment+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 4/15/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireDepartment+Create.h"
#import "DepartmentResources+Create.h"
#import "SortedTaskList.h"
#import "DepartmentTaskList.h"
#import "DepartmentTaskList+Create.h"
#import "DefaultStage.h"
#import "DefaultStage+Create.h"
#import "EditableItem.h"
#import "FireUnit+Create.h"
#import "GroupName+Create.h"
#import "StagingGroup+Create.h"
#import "Device+Create.h"
#import "User+Create.h"
#import "Role+Create.h"
#import "Incident+Create.h"
#import "CurrentUser.h"

@implementation FireDepartment (Create)

NSString *resourcesUniqueId;
NSString *name;
NSDate *timeStamp;

+ (FireDepartment *)createWithName:(NSString *)newName withContext:(NSManagedObjectContext *)context
{
    // used in awakeFromInsert
    resourcesUniqueId = newName;
    name = newName;
    timeStamp = [NSDate date];
    
    // get entity
    FireDepartment *fireDepartment = [NSEntityDescription
            insertNewObjectForEntityForName:@"FireDepartment"
                     inManagedObjectContext:context];
    
    return fireDepartment;
}

+ (FireDepartment *)fetchDepartmentWithId:(NSInteger)id withContext:(NSManagedObjectContext *)context
{
    NSLog(@"%s %ld",__PRETTY_FUNCTION__, (long)id);
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"FireDepartment"];
    
    // Set predicate to fetch fireDepartment with the specified id
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %i", id];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    // eliminate any departments without a createdTimeStamp
    fetchedArray = [self removeInvalidFrom:fetchedArray withContext:context];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    
    // merge departments with id 0
    NSInteger numEntities = fetchedArray.count;
    NSLog(@"%s number of fire departments with id 0 = %ld",__PRETTY_FUNCTION__,(long)numEntities);
    NSUInteger index = 0;
    FireDepartment *fireDepartment = fetchedArray[0];
    
    // consolidate entities with the same id
    while (numEntities > 1) {
        // consolidate fire department entities
        FireDepartment *dept0 = fetchedArray[index++];
        FireDepartment *dept1 = fetchedArray[index];
        fireDepartment = [dept1 mergeDept:dept0];
        numEntities--;
    }
    
    return fireDepartment;
}

+ (NSArray *) removeInvalidFrom:(NSArray *) deptArray withContext:(NSManagedObjectContext *)context
{
    NSLog(@"%s checking:%lu",__PRETTY_FUNCTION__,(unsigned long)deptArray.count);
    NSMutableArray *validDepts = [NSMutableArray arrayWithCapacity:5];
    for (FireDepartment *dept in deptArray) {
        if (!dept.createdTimeStamp) {
            [context deleteObject:dept];
        }
        else {
            [validDepts addObject:dept];
        }
    }
    NSLog(@"%s returning:%lu",__PRETTY_FUNCTION__,(unsigned long)validDepts.count);
    return validDepts;
}

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if ((!self.uniqueId)
        || self.uniqueId.length == 0
        || [self.uniqueId isEqualToString:@" "]) {
        self.uniqueId = resourcesUniqueId;
        self.name = name;
        self.createdTimeStamp = timeStamp;
    }
}

- (NSArray *) sortedIncidents
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    return [self.incidents sortedArrayUsingDescriptors:sortDescriptors];
}


- (NSArray *)sortedTaskListsExcludingListsForIncident:(Incident *) incident
{
    NSSet *departmentTaskLists = self.taskLists;
    if (departmentTaskLists.count) {
        // setup sort descriptor on sortOrderValue
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrderValue" ascending:YES selector:@selector(compare:)];
        NSArray *sortDescriptors = @[sortDescriptor];

        // sort and return the lists
        NSArray *departmentLists = [departmentTaskLists sortedArrayUsingDescriptors:sortDescriptors];

        NSMutableArray *sortedTaskArray = [[NSMutableArray alloc] init];
        NSUInteger index = 1;
        for (DepartmentTaskList *list in departmentLists) {
            if (incident) {
                if (![list isInIncident:incident]) {
                    list.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:index++
                                                                            exponent:1
                                                                          isNegative:NO];
                    SortedTaskList *sortedTaskList = [[SortedTaskList alloc] initWithList:list];
                    [sortedTaskArray addObject:sortedTaskList];
                }
            }
        }
        return sortedTaskArray.copy;
    }
    return nil;
}

- (Incident *) incidentMatching: (Incident *) incident
{
    for (Incident *departmentIncident in self.incidents) {
        if ([incident isEqualValue:departmentIncident]) {
            return departmentIncident;
        }
    }
    return  nil;
}

- (void) deleteDuplicateObjects
{
    self.neighborDepartments = [self deleteDuplicateObjectsIn:self.neighborDepartments];
    self.groupNames = [self deleteDuplicateObjectsIn:self.groupNames];
    self.incidents = [self deleteDuplicateObjectsIn:self.incidents];
    self.predefinedStagingGroups = [self deleteDuplicateObjectsIn:self.predefinedStagingGroups];
    self.preferences = [self deleteDuplicateObjectsIn:self.preferences];
    self.taskLists = [self deleteDuplicateObjectsIn:self.taskLists];
    self.resourceList.fireUnits = [self deleteDuplicateObjectsIn:self.resourceList.fireUnits];
}

- (NSArray *)sortedTaskLists
{
    return [self sortedTaskListsExcludingListsForIncident:nil];
}

- (DefaultStage *)makeDefaultStage:(NSString *)stageName timeRequired:(NSInteger)timeRequired
{
    DefaultStage *defaultStage = [DefaultStage createWithName:stageName forDepartment:(FireDepartment *)self context:self.managedObjectContext];
    defaultStage.timeRequired = @(timeRequired);
    defaultStage.forFireDepartment = self;
    return defaultStage;
}

#pragma mark - cloud updates

- (NSSet *)cloudDeletes:(NSNotification *)notification
{
    NSLog(@"%s", __PRETTY_FUNCTION__);

    NSDictionary *changes = notification.userInfo;
    NSMutableSet *deletes = [NSMutableSet new];
    [deletes unionSet:changes[NSDeletedObjectsKey]];
    NSLog(@"deletes.count=%lu",(unsigned long)deletes.count);
    return deletes;
}

- (NSSet *)cloudUpdates:(NSNotification *)notification
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSDictionary *changes = notification.userInfo;
    NSLog(@"changes.count=%lu",(unsigned long)changes.count);
    NSMutableSet *insertsAndUpdates= [NSMutableSet new];
    [insertsAndUpdates unionSet:changes[NSInsertedObjectsKey]];
    [insertsAndUpdates unionSet:changes[NSUpdatedObjectsKey]];
    NSLog(@"insertsAndUpdates.count=%lu",(unsigned long)insertsAndUpdates.count);
    return insertsAndUpdates;
}

- (BOOL)isFireDepartmentUpdate:(NSSet *)insertsAndUpdates
{
    BOOL isFireDepartmentUpdate = NO;
    NSManagedObjectContext *moc = self.managedObjectContext;
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // do whatever you need to with the NSManagedObjectID
    // you can retrieve the object from with [moc objectWithID:objID]

    for (NSManagedObjectID *objID in insertsAndUpdates) {
        NSManagedObject *obj = [moc objectWithID:objID];

        // look for new or updated fire department object

        if ([obj isKindOfClass:[FireDepartment class]]) {
            FireDepartment *department = (FireDepartment *) obj;
            if (department.id.integerValue == 0) {
                isFireDepartmentUpdate = YES;
                break;
            }
        }
    }
    NSLog(@"isFireDepartmentUpdate=%d",isFireDepartmentUpdate);
    return isFireDepartmentUpdate;
}

#pragma mark - merge department entities

// remove duplicate ids from set but don't delete from core data
- (NSSet *)removeDuplicatesIn:(NSSet *)set
{
    return [self removeDuplicatesIn:set deletingDuplicates:NO];
}

- (NSSet *)removeDuplicatesIn:(NSSet *)set deletingDuplicates:(BOOL) isDeletingDuplicates
{
    NSMutableSet *unique = [NSMutableSet setWithCapacity:20];
    for (NSObject *object in set) {
        assert ([object conformsToProtocol:@protocol(FDcompare)]);
        id <FDcompare> o1 = (id <FDcompare>) object;
        if ([self isUnique:o1 inSet:unique]) {
            [unique addObject:o1];
        }
        else {
            // not unique object
            if (isDeletingDuplicates) {
                assert ([object isKindOfClass:[NSManagedObject class]]);
                NSManagedObject *managedObject = (NSManagedObject *) object;
                [self.managedObjectContext deleteObject:managedObject];
            }
        }
    }
    return unique.copy;
}

-(NSSet *) deleteDuplicateObjectsIn: (NSSet *) set
{
    return [self removeDuplicatesIn:set deletingDuplicates:YES];
}

// objects in the comparison sets must implement the FDcompare protocol
- (NSSet *) uniqueInSet:(NSSet *)set comparedTo:(NSSet *)comparisonSet
{
    NSMutableSet *uniqueObjects = [NSMutableSet setWithCapacity:20];
    for (NSObject *obj in set) {
        if ([obj conformsToProtocol:@protocol(FDcompare)]) {
            id <FDcompare> o1 = (id <FDcompare>) obj;
            if ([self isUnique:o1 inSet:comparisonSet]) {
                // add to unique objects
                [uniqueObjects addObject:o1];
            }
        }
    }
    return uniqueObjects.copy;
}

- (BOOL)isUnique:(id <FDcompare>)object inSet:(NSSet *)set
{
    BOOL isUnique = YES;
    for (NSObject *comparisonObj in set) {
        id <FDcompare> o2 = (id <FDcompare>) comparisonObj;
        if ([object isEqualValue:o2]) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[FireDepartment class]]);
    FireDepartment *otherDepartment = (FireDepartment *) other;
    return [self.name isEqualToString:otherDepartment.name];
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[FireDepartment class]]);
    FireDepartment *otherDepartment = (FireDepartment *) other;
    return ([self.createdTimeStamp compare:otherDepartment.createdTimeStamp] == NSOrderedAscending);
}

- (NSInteger) keeperCount
{
    NSInteger count = 1;
    // count += self.incidents.count + self.devices.count + self.appUsers.count;
    return count;
}

- (FireDepartment *) orKeep:(FireDepartment *) otherDept
{
    FireDepartment *keeper = otherDept;
    NSInteger myCount = self.keeperCount;
    NSInteger otherCount = otherDept.keeperCount;
    
    if (myCount > otherCount) {
        keeper = self;
    }
    else if (myCount == otherCount) {
        // use timestamp
        if ([self isOlderThan:otherDept]) {
            keeper = self;
        }
    }

    NSLog (@"%s keeping:%@",__PRETTY_FUNCTION__,keeper);
    return keeper;
}

- (FireDepartment *)mergeDept:(FireDepartment *)deptToMerge
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    FireDepartment *deptToKeep = [self orKeep:deptToMerge];

    // update group names
    NSMutableSet *allGroupNames = deptToKeep.groupNames.mutableCopy;
    NSSet *keepers = [self uniqueInSet:deptToMerge.groupNames comparedTo:allGroupNames];
    [allGroupNames unionSet:keepers];
    deptToKeep.groupNames = allGroupNames;

    // update incidents
    NSMutableSet *allIncidents = deptToKeep.incidents.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.incidents comparedTo:allIncidents];
    [allIncidents unionSet:keepers];
    deptToKeep.incidents = allIncidents;

    // update neighborDepartments
    NSMutableSet *allNeighbors = deptToKeep.neighborDepartments.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.neighborDepartments comparedTo:allNeighbors];
    [allNeighbors unionSet:keepers];
    deptToKeep.neighborDepartments = allNeighbors;

    // update predefined staging groups
    NSMutableSet *allStages = deptToKeep.predefinedStagingGroups.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.predefinedStagingGroups comparedTo:allStages];
    [allStages unionSet:keepers];
    deptToKeep.predefinedStagingGroups = allStages;

    // update preferences
    NSMutableSet *allPrefs = deptToKeep.preferences.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.preferences comparedTo:allPrefs];
    [allPrefs unionSet:keepers];
    deptToKeep.preferences = allPrefs;

    // update resources
    if (!deptToKeep.resourceList) {
        deptToKeep.resourceList = [DepartmentResources createInContext:deptToKeep.managedObjectContext];
        deptToKeep.resourceList.uniqueId = [NSString stringWithFormat:@"resourceList-%@", deptToKeep.uniqueId];
    }
    NSMutableSet *allResources = deptToKeep.resourceList.fireUnits.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.resourceList.fireUnits comparedTo:allResources];
    [allResources unionSet:keepers];
    deptToKeep.resourceList.fireUnits = allResources;

    // update taskLists
    NSMutableSet *allLists = deptToKeep.taskLists.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.taskLists comparedTo:allLists];
    [allLists unionSet:keepers];
    deptToKeep.taskLists = allLists;

    // update devices
    NSMutableSet *allDevices = deptToKeep.devices.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.devices comparedTo:allDevices];
    [allDevices unionSet:keepers];
    deptToKeep.devices = allDevices;

    // update users
    NSMutableSet *allUsers = deptToKeep.appUsers.mutableCopy;
    keepers = [self uniqueInSet:deptToMerge.appUsers comparedTo:allUsers];
    [allUsers unionSet:keepers];
    deptToKeep.appUsers = allUsers;

    // delete the previous department
    FireDepartment *deptToDelete = deptToMerge;
    if (self != deptToKeep) {
        deptToDelete = self;
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        [deptToKeep.managedObjectContext deleteObject:deptToDelete];
    });

    return deptToKeep;

}

#pragma mark - add department related entities

- (void) addFireUnit:(NSString *)fireUnitNames
{
    NSCharacterSet *separators = [NSCharacterSet characterSetWithCharactersInString:@", "];
    NSArray *names = [fireUnitNames componentsSeparatedByCharactersInSet:separators];
    for (NSString *fireUnitName in names) {
        // check if name and that it is unique name in department
        if (fireUnitName && fireUnitName.length > 0) {
            FireDepartment *currDepartment = self;
            FireUnit *newUnit = [FireUnit createWithName:fireUnitName withContext:self.managedObjectContext];
            if ([self isUnique:newUnit inSet:self.resourceList.fireUnits]) {
                newUnit.uniqueId = [fireUnitName stringByAppendingFormat:@"+%@",currDepartment.uniqueId];
                [currDepartment.resourceList addFireUnitsObject:newUnit];
            }
        }
    }
}


#pragma mark - FDedit protocol

- (NSArray *)editableItems
{
    CurrentUser *currentUser = [CurrentUser sharedSingletonObject];

    NSMutableArray *items = [NSMutableArray arrayWithCapacity:20];
    //[items addObject:[EditableItem fromString:self.name]];
    [items addObject:[EditableItem owner:self
                                   value:self.name
                                itemName:DEPARTMENT
                               sortValue:(items.count + 1)]
    ];
    if (self.id.integerValue == 0) {
    //    [items addObject:[EditableItem named:INCIDENTS fromArray:self.incidents.allObjects sortValue:(items.count + 1)]];
        [items addObject:[EditableItem owner:self named:NEIGHBORS fromArray:self.neighborDepartments.allObjects sortValue:(items.count + 1)]];
        [items addObject:[EditableItem owner:self named:GROUPS fromArray:self.groupNames.allObjects sortValue:(items.count + 1)]];
        [items addObject:[EditableItem owner:self named:TASKLISTS fromArray:self.taskLists.allObjects sortValue:(items.count + 1)]];
        [items addObject:[EditableItem owner:self named:STAGING_GROUPS fromArray:self.predefinedStagingGroups.allObjects sortValue:(items.count + 1)]];
        [items addObject:[EditableItem owner:self named:USERS fromArray:self.appUsers.allObjects sortValue:(items.count + 1)]];
        if (!currentUser.isSingleRoleMode) {
            [items addObject:[EditableItem owner:self named:ROLES fromArray:self.roles.allObjects sortValue:(items.count + 1)]];
        }
        [items addObject:[EditableItem owner:self named:DEVICES fromArray:self.devices.allObjects sortValue:(items.count + 1)]];
    }
    else {
        [items addObject:[EditableItem owner:self value:self.neighborPrefix itemName:PREFIX sortValue:(items.count + 1)]];
    }
    [items addObject:[EditableItem owner:self named:UNITS fromArray:self.resourceList.fireUnits.allObjects sortValue:(items.count + 1)]];
    return items;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (BOOL) mayAddEditableItemForItemName: (NSString *) itemName
{
    BOOL canAdd = YES;
    if ([itemName isEqualToString:DEVICES]) {
        canAdd = NO;
    }
    if ([itemName isEqualToString:USERS]) {
        canAdd = NO;
    }
    return canAdd;
}

- (NSArray *)addEditableItemForItemName:(NSString *)itemName
{
    if ([itemName isEqualToString:NEIGHBORS]) {
        FireDepartment *newDepartment = [FireDepartment createWithName:FILLER_EMPTY
                                                           withContext:self.managedObjectContext];
        newDepartment.neighborPrefix = FILLER_EMPTY;
        NSInteger numNeighbors = self.neighborDepartments.count + 1;
        newDepartment.id = @(numNeighbors);
        
        // create empty unit resource list
        newDepartment.resourceList = [DepartmentResources withUniqueId:[NSString stringWithFormat:@"resourceList-%@", newDepartment.uniqueId] createInContext:self.managedObjectContext];
        
        [self addNeighborDepartmentsObject:newDepartment];
        return self.neighborDepartments.allObjects;
    }
    else if ([itemName isEqualToString:GROUPS]) {
        GroupName *newGroupName = [GroupName createWithName:FILLER_EMPTY
                                                withContext:self.managedObjectContext];
        [self addGroupNamesObject:newGroupName];
        return self.groupNames.allObjects;
    }
    else if ([itemName isEqualToString:STAGING_GROUPS]) {
        DefaultStage *newStage = [DefaultStage createWithName:FILLER_EMPTY forDepartment:self context:self.managedObjectContext];
        newStage.timeRequired = @(-1);
        [self addPredefinedStagingGroupsObject:newStage];
        return self.predefinedStagingGroups.allObjects;
    }

    else if ([itemName isEqualToString:TASKLISTS]) {
        NSInteger value = self.taskLists.count + 1;
        DepartmentTaskList *taskList = [DepartmentTaskList createWithName:FILLER_EMPTY
                                                                sortValue:(NSUInteger) (value * 2)  // force to end of sort
                                                              withContext:self.managedObjectContext];
        taskList.uniqueId = [[NSUUID UUID]UUIDString];
        [self addTaskListsObject:taskList];
        return self.taskLists.allObjects;
    }
    else if ([itemName isEqualToString:UNITS]) {
        FireUnit *newUnit = [FireUnit createWithName:FILLER_EMPTY
                                         withContext:self.managedObjectContext];
        [self.resourceList addFireUnitsObject:newUnit];
        return self.resourceList.fireUnits.allObjects;
    }
    else if ([itemName isEqualToString:ROLES]) {
        Role *newRole = [Role createWithName:FILLER_EMPTY withPermission:FDIncidentManager context:self.managedObjectContext];
        [self addRolesObject:newRole];
        return self.roles.allObjects;
    }
    else if ([itemName isEqualToString:USERS]) {
        User *newUser = [User createWithName:FILLER_EMPTY onDevice:nil context:self.managedObjectContext];
        [self addAppUsersObject:newUser];
        return self.appUsers.allObjects;
    }

    assert (NO);   // should not get here - must have introduced a new item type
    return nil;
}

- (NSArray *)deleteEditableItem:(id <FDedit>)item forItemName:(NSString *)itemName
{
    id <FDedit> itemToDelete = item;
    if (item.ownerObject) {
        itemToDelete = item.ownerObject;
    }
    // todo keep the deleted objects around because of links to them from incidents.
    // todo If they have no links from incidents then they can be deleted.
    if ([itemName isEqualToString:NEIGHBORS]) {
        assert ([itemToDelete isKindOfClass:[FireDepartment class]]);
        if ([itemToDelete isKindOfClass:[FireDepartment class]]) {
            FireDepartment *neighborFireDepartment = (FireDepartment *) itemToDelete;
            [self removeNeighborDepartmentsObject:neighborFireDepartment];
        }
        return self.neighborDepartments.allObjects;
    }
    else if ([itemName isEqualToString:GROUPS]) {
        assert ([itemToDelete isKindOfClass:[GroupName class]]);
        if ([itemToDelete isKindOfClass:[GroupName class]]) {
            GroupName *groupName = (GroupName *) itemToDelete;
            [self removeGroupNamesObject:groupName];
        }
        return self.groupNames.allObjects;
    }
    else if ([itemName isEqualToString:STAGING_GROUPS]) {
        assert ([itemToDelete isKindOfClass:[DefaultStage class]]);
        if ([itemToDelete isKindOfClass:[DefaultStage class]]) {
            DefaultStage *stage = (DefaultStage *) itemToDelete;
            [self removePredefinedStagingGroupsObject:stage];
        }
        return self.predefinedStagingGroups.allObjects;
    }

    else if ([itemName isEqualToString:TASKLISTS]) {
        assert ([itemToDelete isKindOfClass:[DepartmentTaskList class]]);
        if ([itemToDelete isKindOfClass:[DepartmentTaskList class]]) {
            DepartmentTaskList *taskList = (DepartmentTaskList *) itemToDelete;
            [self removeTaskListsObject:taskList];
        }
        return self.taskLists.allObjects;
    }
    else if ([itemName isEqualToString:UNITS]) {
        assert ([itemToDelete isKindOfClass:[FireUnit class]]);
        if ([itemToDelete isKindOfClass:[FireUnit class]]) {
            FireUnit *fireUnit = (FireUnit *) itemToDelete;
            [self.resourceList removeFireUnitsObject:fireUnit];
        }
        return self.resourceList.fireUnits.allObjects;
    }
    else if ([itemName isEqualToString:DEVICES]) {
        assert ([itemToDelete isKindOfClass:[Device class]]);
        if ([itemToDelete isKindOfClass:[Device class]]) {
            Device *device = (Device *) itemToDelete;
            [self removeDevicesObject:device];
        }
        return self.devices.allObjects;
    }
    else if ([itemName isEqualToString:USERS]) {
        assert ([itemToDelete isKindOfClass:[User class]]);
        if ([itemToDelete isKindOfClass:[User class]]) {
            User *user = (User *) itemToDelete;
            [self removeAppUsersObject:user];
        }
        return self.appUsers.allObjects;
    }
    else if ([itemName isEqualToString:ROLES]) {
        assert ([itemToDelete isKindOfClass:[Role class]]);
        if ([itemToDelete isKindOfClass:[Role class]]) {
            Role *role = (Role *) itemToDelete;
            [self removeRolesObject:role];
        }
        return self.roles.allObjects;
    }

    assert (NO);   // should not get here  must be deleting an item not support
    return nil;
}

- (NSString *) itemName
{
    return nil;
}


- (NSString *) value
{
    return self.name;
}

- (NSDecimalNumber *)sortValue
{
    return [NSDecimalNumber zero];
}

- (void)setSortValue:(NSDecimalNumber *)value
{

}

- (id)ownerObject
{
    return self;
}

- (void) save:(NSString *)newValue itemname:(NSString *)itemName
{
    if ([itemName isEqualToString:PREFIX]) {
        self.neighborPrefix = newValue;
    }
    else if ([itemName isEqualToString:DEPARTMENT]) {
        self.name = newValue;
    }
}


- (BOOL)isDeletable
{
    // can not delete home/owning fire department
    return self.id.integerValue != 0;
}

- (BOOL)isReorderable
{
    return NO;
}

- (BOOL)hasSortValue
{
    return NO;
}




@end
