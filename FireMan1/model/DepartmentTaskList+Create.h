//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "DepartmentTaskList+CoreDataClass.h"
#import "Incident+CoreDataClass.h"
#import "FDedit.h"

@interface DepartmentTaskList (Create) <FDedit>
+ (DepartmentTaskList *) createWithName: (NSString *)name sortValue: (NSUInteger)value withContext: (NSManagedObjectContext *) context;
- (BOOL) isInIncident:(Incident *) incident;
- (NSSet *) listCopiesForIncident: (Incident *) incident;
@end
