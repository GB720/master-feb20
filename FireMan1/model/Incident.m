//
//  Incident.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/27/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "Incident.h"
#import "Device.h"
#import "FireDepartment.h"
#import "FireUnit.h"
#import "IncidentLog.h"
#import "IncidentTaskList.h"
#import "ListItem.h"
#import "Preference.h"
#import "StagingGroup.h"
#import "User.h"


@implementation Incident

@dynamic name;
@dynamic note;
@dynamic originatingDeviceId;
@dynamic startTime;
@dynamic stopTime;
@dynamic timeOfLastPar;
@dynamic createdTimeStamp;
@dynamic id;
@dynamic activeUnits;
@dynamic assignedUnits;
@dynamic availableUnits;
@dynamic commandUnit;
@dynamic forDepartment;
@dynamic incidentCoordinator;
@dynamic logForIncident;
@dynamic managedTasks;
@dynamic preferences;
@dynamic rehabUnits;
@dynamic stagingGroups;
@dynamic taskLists;
@dynamic viewedByDevices;

@end
