//
//  GroupName+Create.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/7/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "GroupName.h"
#import "FDcompare.h"
#import "FDedit.h"

@interface GroupName (Create) <FDcompare, FDedit>
+ (GroupName *) createWithName: (NSString *)newName withContext: (NSManagedObjectContext *) context;
+ (GroupName *) createDefault: (NSString *)newName forDepartment: (FireDepartment *)department;
@end
