//
// Created by Gerald Boyd on 8/14/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PermittedValues.h"
#import "FDPermittedValues.h"


@interface EntityPermittedValues : PermittedValues
+ (EntityPermittedValues *)withEntities:(NSSet *)set owner:(id <FDedit>)owner forItemName:(NSString *) itemName;
@end