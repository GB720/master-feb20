//
// Created by Gerald Boyd on 6/3/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "FDedit.h"
#import "DepartmentTaskList+Create.h"
#import "List+Create.h"
#import "Incident+CoreDataClass.h"
#import "IncidentTaskList.h"


@implementation DepartmentTaskList (Create)
+ (DepartmentTaskList *) createWithName: (NSString *)name sortValue:(NSUInteger)value withContext: (NSManagedObjectContext *) context
{
    // get entity
    DepartmentTaskList *departmentTaskList = [NSEntityDescription
            insertNewObjectForEntityForName:@"DepartmentTaskList"
                     inManagedObjectContext:context];

    // set value of name
    departmentTaskList.listName = name;
    departmentTaskList.createdTimeStamp = [NSDate date];
    [departmentTaskList setSortValue:[NSDecimalNumber decimalNumberWithMantissa:value exponent:1 isNegative:NO]];
    return departmentTaskList;
}

- (BOOL) isInIncident:(Incident *) incident
{
    BOOL isIn = NO;
    NSSet *listCopies = [self listCopiesForIncident:incident];
    for (IncidentTaskList *incidentTaskList in listCopies) {
        if ([incidentTaskList.fromDepartmentList.entity isEqual:self.entity]) {
            isIn = YES;
            break;
        }
    }
    return isIn;
}

- (NSSet *) listCopiesForIncident: (Incident *) incident
{
    NSMutableSet *listCopies = [NSMutableSet setWithCapacity:30];
    NSSet *taskListCopies = self.incidentListCopy;
    for (IncidentTaskList *incidentTaskList in taskListCopies) {
        if ([incidentTaskList.forIncident.name isEqualToString:incident.name]) {
            [listCopies addObject:incidentTaskList];
        }
    }
    return listCopies.copy;
}

- (BOOL) isReorderable
{
    return YES;
}

- (BOOL)isDeletable
{
    return YES;
}

- (BOOL)hasSortValue
{
    return YES;
}

- (BOOL)mayAddEditableItem
{
    return YES;
}

- (BOOL)mayDeleteEditableItem
{
    return YES;
}
@end
