//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//

#ifndef ListItem_h
#define ListItem_h

#import "ListItem+CoreDataClass.h"
#endif /* ListItem_h */

#import <Foundation/Foundation.h>
#import "FDcompare.h"
#import "FDedit.h"

#define FD_GROUP_TERMINATED @"_Terminated"

@interface ListItem (Create) <FDcompare, FDedit>

+ (ListItem *)createItemNamed:(NSString *)name withValue:(NSUInteger)value withContext:(NSManagedObjectContext *)context;
+ (ListItem *) fromListItem: (ListItem *) item withContext:(NSManagedObjectContext *)context;
@end
