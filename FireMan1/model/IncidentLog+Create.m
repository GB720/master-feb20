//
//  IncidentLog+Create.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/20/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IncidentLog+Create.h"

@interface IncidentLog()

@property(nonatomic, readwrite) NSDate *startTime;
@end

@implementation IncidentLog (Create)
+ (IncidentLog *) withContext: (NSManagedObjectContext *) context
{
    // get entity
    IncidentLog *incidentLog = [NSEntityDescription
                          insertNewObjectForEntityForName:@"IncidentLog"
                          inManagedObjectContext:context];
    incidentLog.nextItemNumber = @1;
    incidentLog.uniqueId =[[NSUUID UUID]UUIDString];
    return incidentLog;

}


@end
