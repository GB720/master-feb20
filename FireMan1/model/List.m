//
//  List.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "List.h"
#import "ListItem.h"


@implementation List

@dynamic expanded;
@dynamic listName;
@dynamic sortOrderValue;
@dynamic createdTimeStamp;
@dynamic items;

@end
