//
//  FireUnit.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "FireUnit.h"
#import "DepartmentResources.h"
#import "FireUnit.h"
#import "Incident.h"
#import "ListItem.h"
#import "LogItem.h"
#import "StagingGroup.h"


@implementation FireUnit

@dynamic activeStartTime;
@dynamic name;
@dynamic note;
@dynamic parStatusOk;
@dynamic sortOrderValue;
@dynamic status;
@dynamic typeOf;
@dynamic createdTimeStamp;
@dynamic activeForIncident;
@dynamic assignedTo;
@dynamic availableInIncident;
@dynamic clonedFromUnit;
@dynamic clonedUnits;
@dynamic commanderFor;
@dynamic coordinatorFor;
@dynamic departmentResourceList;
@dynamic forIncident;
@dynamic logItems;
@dynamic rehabForIncident;
@dynamic stagingGroup;

@end
