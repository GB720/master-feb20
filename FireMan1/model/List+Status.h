//
// Created by Gerald Boyd on 5/31/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "List.h"

@interface List (Status)
@property (nonatomic, readonly) BOOL isCompleted;
@property (nonatomic, readonly) BOOL hasCheckedItems;
@end