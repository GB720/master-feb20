//
// Created by Gerald Boyd on 8/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Role.h"
#import "FDedit.h"

// role permission
NS_ENUM(NSUInteger, FD_rolePermission) {
    FDIncidentManager = 1,
    FDAdministrator = 2,
    FDAdminAndManager = 3,
    FDViewOnly = 8,
};

static NSString *const ADMIN_TYPE = @"Administrator";

static NSString *const COORDINATOR_TYPE = @"Coordinator";

static NSString *const VIEW_ONLY_TYPE = @"View Only";

@interface Role (Create) <FDedit>
+ (Role *) createWithName: (NSString *)newName withPermission:(enum FD_rolePermission) permissionValue context: (NSManagedObjectContext *) context;

+ (Role *)roleWithName:(NSString *)name context:(NSManagedObjectContext *)context;

- (User *)createUserWithName:(NSString *)newName onDevice:(NSString *) deviceId;
@end
