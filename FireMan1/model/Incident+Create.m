//
// Created by Gerald Boyd on 4/19/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Incident+Create.h"
#import "IncidentTaskList+Create.h"
#import "IncidentLog.h"
#import "IncidentLog+Log.h"
#import "ListItem.h"
#import "FireUnit+Identification.h"
#import "SortedTaskList.h"
#import "EditableItem.h"


@implementation Incident (Create)
+ (Incident *)createWithName:(NSString *)name withContext: (NSManagedObjectContext *) context {
    // get entity
    Incident *incident = [NSEntityDescription
            insertNewObjectForEntityForName:@"Incident"
                     inManagedObjectContext:context];
    incident.name = name;
    incident.startTime = [NSDate date];
    incident.timeOfLastPar = [NSDate date];
    incident.stopTime = nil;
    incident.createdTimeStamp = [NSDate date];
    incident.uniqueId = [[NSUUID UUID]UUIDString];
    return incident;
}

+ (Incident *)fetchIncident:(NSNumber *)incidentId withDate:(NSDate *)createdDate withContext: (NSManagedObjectContext *) context
{
    // NSLog(@"%s %d",__PRETTY_FUNCTION__, incidentId.integerValue);
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Incident"];
    
    // Set predicate to fetch incident with the specified id
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %i", incidentId];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:request error:&error];
    
    // eliminate any incidents without a createdTimeStamp
    fetchedArray = [self removeInvalidFrom:fetchedArray withContext:context];
    if (fetchedArray == nil || fetchedArray.count == 0) {
        return nil;
    }
    
    NSInteger numEntities = fetchedArray.count;
    NSLog(@"%s number of incidents with id %ld = %ld",__PRETTY_FUNCTION__,(long)incidentId.integerValue, (long)numEntities);
    
    // get incident with correct createdTimeStamp
    for (Incident *incident in fetchedArray) {
        if ([createdDate isEqualToDate:incident.createdTimeStamp]) {
            NSLog(@"%s returning incident:%@",__PRETTY_FUNCTION__, incident);
            return incident;
        }
        
    }
    return nil;
}

+ (NSArray *) removeInvalidFrom:(NSArray *) incidentArray withContext:(NSManagedObjectContext *)context
{
    NSLog(@"%s checking:%lu",__PRETTY_FUNCTION__,(unsigned long)incidentArray.count);
    NSMutableArray *validIncidents = [NSMutableArray arrayWithCapacity:5];
    for (Incident *incident in incidentArray) {
        if (!incident.createdTimeStamp) {
            [context deleteObject:incident];
        }
        else {
            [validIncidents addObject:incident];
        }
    }
    NSLog(@"%s returning:%lu",__PRETTY_FUNCTION__,(unsigned long)validIncidents.count);
    return validIncidents;
}


- (IncidentLog *) log
{
    return self.logForIncident;
}

- (void)beginIncident
{
    [self.log action:[NSString stringWithFormat:@"%@ - Begin Incident %@", [NSDate date], self.name]];
}

#pragma mark - delete
- (void)deleteIncidentItems
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    [self deleteTaskLists];
    [self deleteUnits];
    [self deleteStagingGroups];
    [self deletePreferences];
    [self deleteManagedTasks];
    [self deleteLog];
}

- (void)deleteLog
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    IncidentLog *log = self.logForIncident;
    NSSet *logItems = log.logItems.copy;
    [log removeLogItems:logItems];
    [self deleteObjects:logItems];
    [self.managedObjectContext deleteObject:log];
}

- (void)deleteManagedTasks
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *managedTasks = self.managedTasks.copy;
    [self removeManagedTasks:managedTasks];
    [self deleteObjects:managedTasks];
}

- (void)deletePreferences
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *preferences = self.preferences.copy;
    [self removePreferences:preferences];
    [self deleteObjects:preferences];
}

- (void)deleteUnits
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *activeUnits = self.activeUnits.copy;
    [self removeActiveUnits:activeUnits];
    [self deleteObjects:activeUnits];
    NSSet *availableUnits = self.availableUnits.copy;
    [self removeAvailableUnits:availableUnits];
    [self deleteObjects:availableUnits];
}

- (void)deleteStagingGroups
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *stagingGroups = self.stagingGroups.copy;
    [self removeStagingGroups:stagingGroups];
    [self deleteObjects:stagingGroups];
}

- (void)deleteObjects:(NSSet *)set
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    for (NSManagedObject *managedObject in set) {
        [self.managedObjectContext deleteObject:managedObject];
    }
}

- (void)deleteTaskLists
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *taskLists = self.taskLists.copy;
    for (IncidentTaskList *taskList in taskLists) {
        [taskList deleteListItems];
        [self removeTaskListsObject:taskList];
        taskList.fromDepartmentList = nil;
        [self.managedObjectContext deleteObject:taskList];
    }
}

#pragma mark - helper methods
- (NSInteger) nextGroupValue
{
    NSDecimalNumber *zero = [NSDecimalNumber zero];
    NSDecimalNumber *ten = [NSDecimalNumber decimalNumberWithString:@"10"];
    NSDecimalNumber *greatestValue = zero;
    for (ListItem *managedTask in self.managedTasks) {
        NSDecimalNumber *taskSortValue = managedTask.sortOrderValue;
        if ([taskSortValue compare:greatestValue] == NSOrderedDescending) {
            greatestValue = taskSortValue;
        }
    }
    NSDecimalNumber *nextValue = zero;
    if ([greatestValue compare:zero] != NSOrderedSame) {
        nextValue = [greatestValue decimalNumberByDividingBy:ten];
    }
    nextValue = [nextValue decimalNumberByAdding:[NSDecimalNumber one]];
    NSLog (@"%s %ld",__PRETTY_FUNCTION__,(long)nextValue.integerValue);
    return nextValue.integerValue;
}

- (NSArray *)sortedFireUnits
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    return [self sortedFireUnitsWithActiveGroup:nil];
}

- (NSArray *)sortedFireUnitsWithActiveGroup: (ListItem *)activeGroup
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSArray *sortedUnits;

    // get available inactive units
    NSSet *unsortedFireUnits = self.availableUnits;

    // get all active units
    unsortedFireUnits = [unsortedFireUnits setByAddingObjectsFromSet:self.activeUnits];

    // get units assigned to this group
    NSSet *assignedUnits;
    if (activeGroup) {
        assignedUnits = activeGroup.assignedUnits;
    }

    // temporarily remove units assigned to this group from the list
    unsortedFireUnits = [self removeUnits:assignedUnits fromSet:unsortedFireUnits];

    // sort the remaining list
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"prefixedName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    sortedUnits = [unsortedFireUnits sortedArrayUsingDescriptors:sortDescriptors];

    // put units assigned to this active task at beginning of the list
    if (activeGroup && assignedUnits.count > 0)
    {
        NSMutableArray *units = sortedUnits.mutableCopy;
        for (FireUnit *assignedUnit in assignedUnits) {
            if (![assignedUnit.coordinatorFor.currentValue isEqualToString:activeGroup.currentValue]) {
                [units insertObject:assignedUnit atIndex:0];
            }
        }
        sortedUnits = units.copy;
    }
    return sortedUnits;
}

- (NSArray *)sortedTaskLists
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSet *incidentTaskLists = self.taskLists;
    if (incidentTaskLists.count) {
        // setup sort descriptor on sortOrderValue
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortOrderValue" ascending:YES selector:@selector(compare:)];
        NSArray *sortDescriptors = @[sortDescriptor];

        // sort and return the lists
        NSArray *incidentLists = [incidentTaskLists sortedArrayUsingDescriptors:sortDescriptors];

        NSMutableArray *sortedTaskArray = [[NSMutableArray alloc] init];
        NSUInteger index = 1;
        for (List *list in incidentLists) {
            list.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:index++
                                                                    exponent:1
                                                                  isNegative:NO];
            SortedTaskList *sortedTaskList = [[SortedTaskList alloc] initWithList:list];
            [sortedTaskArray addObject:sortedTaskList];
        }
        return sortedTaskArray.copy;
    }
    return nil;
}


// remove specified fire units from the total set of fire units
- (NSSet *)removeUnits:(NSSet *)unitsToRemove fromSet:(NSSet *)setOfUnits {
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSMutableSet *units = setOfUnits.mutableCopy;
    for (FireUnit *fireUnit in unitsToRemove) {
        for (FireUnit *unit in setOfUnits) {
            if ([unit.prefixedName isEqualToString:fireUnit.prefixedName]) {
                [units removeObject:unit];
            }
        }
    }
    return units.copy;
}

- (NSArray *) sortedStagingLevels
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    return [self sort:self.stagingGroups withKey:@"sortOrderValue"];
}

// sort supplied set into an ordered array using specified sort key
- (NSArray *) sort: (NSSet *)set withKey:(NSString *)key
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                 ascending:YES
                                                comparator:^(id obj1, id obj2)  {return [obj1 compare:obj2];}];
    NSArray *sortDescriptors = @[sortDescriptor];
    return [set sortedArrayUsingDescriptors:sortDescriptors];
}

- (BOOL)isEqualValue:(id <FDcompare>)other
{
    assert ([other isKindOfClass:[Incident class]]);
    Incident *otherIncident = (Incident *) other;
    return self.id.integerValue == otherIncident.id.integerValue
&& ([self.createdTimeStamp isEqualToDate:otherIncident.createdTimeStamp]);
}

- (BOOL) isOlderThan:(id<FDcompare>)other
{
    assert ([other isKindOfClass:[Incident class]]);
    Incident *otherIncident = (Incident *) other;
    return ([self.createdTimeStamp compare:otherIncident.createdTimeStamp] == NSOrderedAscending);
}


- (NSString *)itemName
{
    return nil;
}

- (NSArray *)editableItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:4];
    [items addObject:[EditableItem owner:self fromString:self.name]];
    [items addObject:[EditableItem owner:self fromString:self.note]];
    return items.copy;
}

- (id <FDPermittedValues>)permittedValues
{
    return nil;
}


- (NSString *)value
{
    return self.name;
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    // not used
}

- (NSDecimalNumber *)sortValue
{
    return nil;
}

- (void)setSortValue:(NSDecimalNumber *)value
{

}

- (id)ownerObject
{
    return self;
}


@end
