//
// Created by Gerald Boyd on 8/14/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDPermittedValues.h"
#import "FDedit.h"

@protocol FDedit;


@interface PermittedValues : NSObject <FDPermittedValues>

+ (PermittedValues *)withDictionary:(NSDictionary *)dictionary owner:(id <FDedit>) owner forItemName:(NSString *) itemName;

+ (PermittedValues *)withOwner:(id <FDedit>)owner forItemName:(NSString *)itemName;

- (id) initWithOwner:(id <FDedit>) owner;

- (void)selectValue:(NSString *)value;

@property (strong, nonatomic) NSArray *editableItems;
@property (strong, nonatomic) NSString *itemName;
@end