//
//  EditDepartmentVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditContainer.h"

@class FireDepartment;

@interface EditDepartmentVC : UIViewController <EditContainer>
@property (nonatomic, strong) FireDepartment *fireDepartment;
@end
