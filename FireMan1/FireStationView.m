//
//  FireStationView.m
//  FireMan1
//
//  Created by Gerald Boyd on 1/14/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "FireStationView.h"

@interface FireStationView ()
@property(nonatomic, strong) DragDropManager *dragManager;
@end

@implementation FireStationView {
    NSString *_taskId;
    id <DragDropDelegate> _dragDelegate;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSString *)taskId {
    return _taskId;
}

- (void)setTaskId:(NSString *)taskId {
    _taskId = taskId;
}

- (id <DragDropDelegate>)dragDelegate {
    return _dragDelegate;
}

- (void)setDragDelegate:(id <DragDropDelegate>)dragDelegate {
    _dragDelegate = dragDelegate;
}

- (void)setDragDropManager:(DragDropManager *)dragDropManager {
    self.dragManager = dragDropManager;
}

- (void)startingDrag:(DragDropManager *)dragManager atPoint:(CGPoint)point {

}

- (void)dragFromEnded:(UIView *)viewBeingDragged {

}

- (void)addTypeToAccept:(NSString *)dragIdentifier {
    [self.dragManager addDragType:dragIdentifier
                          forView:self];
}

- (BOOL)droppingItemOn:(UIView *)viewBeingDragged atPoint:(CGPoint)point {
    return YES;
}

- (BOOL)isDropTargetFor:(UIView *)viewBeingDragged atPoint:(CGPoint)point 
{
    BOOL isValidDropTarget = YES;
    id <DraggableUIView> itemBeingDropped = (id <DraggableUIView>) viewBeingDragged;

            // check that the item is a valid drop for this view
    if (![self.dragManager isValidDropItem:itemBeingDropped forView:self]) {
        isValidDropTarget = NO;
    }
    return isValidDropTarget;
}

- (BOOL)removeFromView:(UIView *)viewBeingDragged {
    return NO;
}

- (BOOL)addImage:(UIView *)viewBeingDragged atPoint:(CGPoint)point {
    return NO;
}

- (void)dragToEnded
{
    // todo
}

- (UIView *)viewAt:(CGPoint)point {
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
