//
//  Created by jve on 4/2/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface DragContext : NSObject
@property(nonatomic, strong) UIView *originalView;
@property(nonatomic) CGPoint originalPointInSuperView;
@property(nonatomic, weak) id fromGroup;
@property(nonatomic, strong) id movingObject;
@property(nonatomic, strong) UIView *viewCopyToDrag;
@property(nonatomic, strong) UIView *movingInView;


- (id)initWithDraggedView:(UIView *)draggedView;
- (void)snapToOriginalPosition;
- (void) removeDragImage;
@property(nonatomic, strong, readonly) UIView *draggedView;
@end