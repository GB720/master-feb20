//
//  StagingLevelViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/24/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "model/Incident.h"
#import "StagingLevelViewController.h"
#import "FireDepartment.h"
#import "StagingGroup.h"
#import "DefineNewStagingLevelVC.h"
#import "StagingGroup+Create.h"
#import "DefaultStage+Create.h"



NSUInteger CELL_HEIGHT = 44;
NSInteger MAX_TO_SHOW = 12;

@interface StagingLevelViewController ()
@property (nonatomic, strong) NSArray *sortedStagingGroups;
@end

@implementation StagingLevelViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSSet *predefinedStagingGroups = self.fireDepartment.predefinedStagingGroups;
    if (predefinedStagingGroups) {
        _sortedStagingGroups = [self sortStagingGroups:predefinedStagingGroups
                              excludeGroupsforIncident:self.activeIncident];
        NSInteger numStagingGroups =  self.sortedStagingGroups.count;
        if (numStagingGroups > MAX_TO_SHOW) {
            numStagingGroups = MAX_TO_SHOW;
        }
        NSInteger len = CELL_HEIGHT * numStagingGroups;
        CGSize size = CGSizeMake(300, len);
        self.preferredContentSize = size;
    }
    self.tableView.accessibilityIdentifier = @"stagingLevelTableView";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (NSArray *) sortStagingGroups:(NSSet *)predefinedStagingGroups excludeGroupsforIncident:(Incident *)incident
{
    NSMutableSet *groupsToSort = predefinedStagingGroups.mutableCopy;
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSSet *stagingGroupsToExclude = incident.stagingGroups;
    for (DefaultStage *defaultStage in predefinedStagingGroups) {
        for (StagingGroup *stagingGroup in stagingGroupsToExclude) {
            if ([defaultStage.name isEqualToString:stagingGroup.name]) {
                [groupsToSort removeObject:defaultStage];
                break;
            }
        }
    }
    return [groupsToSort.copy sortedArrayUsingDescriptors:sortDescriptors];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) viewDidAppear: (BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];

    if (!_sortedStagingGroups || _sortedStagingGroups.count == 0) {
        // go directly to define a new stage
        [self performSegueWithIdentifier:@"segueFromAddToDefineNewStagingGroup" sender:self];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.sortedStagingGroups) {
        return self.sortedStagingGroups.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StagingLevelSelectionViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSUInteger index = (NSUInteger)indexPath.item;
    StagingGroup *stagingGroup = self.sortedStagingGroups[index];
    cell.textLabel.text = stagingGroup.name;
    NSString *timeRequiredString = [NSString stringWithFormat:@"time: %li minutes",(long)stagingGroup.timeRequired.integerValue];
    cell.detailTextLabel.text = timeRequiredString;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger index = (NSUInteger) indexPath.item;
    DefaultStage *defaultStage = self.sortedStagingGroups[index];
    [self createStagingGroupToAdd:defaultStage];
    [self performSegueWithIdentifier:@"unwindAddStagingLevelToIncident" sender:self];
}

- (void)createStagingGroupToAdd:(DefaultStage *)defaultStage {
    self.addingStagingGroup = [StagingGroup createWithName:defaultStage.name context:self.fireDepartment.managedObjectContext];
    self.addingStagingGroup.timeRequired = defaultStage.timeRequired;
}


#pragma mark - Navigation

// Invoked immediately prior to initiating a segue. Return NO to prevent the segue from firing. The default implementation returns YES.
// This method is not invoked when -performSegueWithIdentifier:sender: is used.
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString:@"segueFromAddToDefineNewStagingGroup"]) {
        UINavigationController *nvc = segue.destinationViewController;
        DefineNewStagingLevelVC *vc = (DefineNewStagingLevelVC *)nvc.topViewController;
        vc.fireDepartment = self.fireDepartment;
    }
}

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    return action == @selector(unwindAddStagingLevel:);
}

- (IBAction)unwindAddStagingLevel:(UIStoryboardSegue *)unwindSegue
{
    // add staging level if managedObjectContext exists
    //  the managedObjectContext can be cleared if the iCloud store changes
    if (self.fireDepartment.managedObjectContext) {
        if ([unwindSegue.identifier isEqualToString:@"unwindAddStagingLevel"])
        {
            NSManagedObjectContext *context = self.fireDepartment.managedObjectContext;           // add staging level to list for the incident
            DefineNewStagingLevelVC *vc = unwindSegue.sourceViewController;
            if (vc.stagingGroupName) {
                NSString *newName = vc.stagingGroupName;
                NSNumber *newTime = vc.timeRequiredInStage;
                DefaultStage *newStagingGroup = [DefaultStage createWithName:newName forDepartment:self.fireDepartment context:context];
                newStagingGroup.timeRequired = newTime;

                [self.fireDepartment addPredefinedStagingGroupsObject:newStagingGroup];
                // re-display list for this view controller
                _sortedStagingGroups = [self sortStagingGroups:self.fireDepartment.predefinedStagingGroups
                                      excludeGroupsforIncident:self.activeIncident];
                [self createStagingGroupToAdd:newStagingGroup];
                [self performSegueWithIdentifier:@"unwindAddStagingLevelToIncident" sender:self];
            }
        }
        else if ([unwindSegue.identifier isEqualToString:@"unwindAddStagingLevelCancel"]) {
            // ignore any inputs
            [self performSegueWithIdentifier:@"unwindAddStagingLevelToIncident" sender:self];

        }
        if (!self.sortedStagingGroups || self.sortedStagingGroups.count == 0) {
            [self performSegueWithIdentifier:@"unwindAddStagingLevelToIncident" sender:self];
        }
    }


}

@end
