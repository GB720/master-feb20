//
//  AddListFromDepartmentVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireDepartment+Create.h"
#import "AddListFromDepartmentVC.h"
#import "CreateNewListVC.h"
// #import "FireDepartment.h"
#import "DepartmentTaskList.h"
#import "DepartmentTaskList+Create.h"
#import "ListItem+Create.h"
#import "SortedTaskList.h"
#import "List+Create.h"

@class CreateNewListVC, FireDepartment;

@interface AddListFromDepartmentVC ()
@property(nonatomic, strong) DepartmentTaskList *currentList;
@property(nonatomic, strong) NSArray *cachedDepartmentLists;
@property (nonatomic, strong) NSMutableArray *selectedLists;
@property(nonatomic, readwrite) NSArray *selectedDepartmentLists;
@end

@implementation AddListFromDepartmentVC {
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedLists = nil;
    self.tableView.accessibilityIdentifier = @"departmentTaskList";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSArray *)cachedDepartmentLists
{
    if (!_cachedDepartmentLists) {
        _cachedDepartmentLists = [self.fireDepartment sortedTaskListsExcludingListsForIncident:self.incident];
    }
    return _cachedDepartmentLists;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return self.cachedDepartmentLists.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    SortedTaskList *sortedList = self.cachedDepartmentLists[(NSUInteger) section];

    if (sortedList && sortedList.expand){
        // Return the number of rows in the section + header row
        numberOfRows = sortedList.list.count + 1;
    }
    else {
        numberOfRows = 1;
    }

    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"departmentListItemCell";
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;

    if (row == 0) {
        cellIdentifier = @"departmentListTitleCell";
    }

    if (self.cachedDepartmentLists.count == 0) {
        if (section == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.textLabel.text = @"Empty List";
        }
    }
    else {
        SortedTaskList *sortedList = self.cachedDepartmentLists[(NSUInteger) section];
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

        if (row == 0) {
            // get header
            cell.selected = NO;
            NSString *format = @"< %@ >"; // list is not expanded format
            if (sortedList.expand) {
                format = @"> %@ <";   // list is expanded format
            }
            if ([self.selectedLists indexOfObject:sortedList.managedListObject] != NSNotFound) {
                cell.selected = YES;
            }
            cell.textLabel.text = [NSString stringWithFormat:format, sortedList.name];
        }
        else {
            NSInteger listIndex = row - 1;
            if (sortedList.list.count)  {
                // set text
                ListItem *listItem = sortedList.list[(NSUInteger) listIndex];
                cell.textLabel.text = listItem.name;
            }
        }
    }
    cell.tag = section * 100 + row;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"defineNewListSegue"]) {
        CreateNewListVC *vc = segue.destinationViewController;
        vc.fireDepartment = self.fireDepartment;
        vc.currentList = self.currentList;
    }
    else if ([segue.identifier isEqualToString:@"editExistingListSegue"]) {
        CreateNewListVC *vc = segue.destinationViewController;
        vc.fireDepartment = self.fireDepartment;
        UITableViewCell *cell = sender;
        NSInteger tag = cell.tag;
        NSUInteger section = (NSUInteger) tag / 100;
        SortedTaskList *sortedTaskList = self.cachedDepartmentLists[section];
        vc.currentList = (DepartmentTaskList *) sortedTaskList.managedListObject;
    }
    else if ([segue.identifier isEqualToString:@"unwindFromAddList"]) {
        // going back to the incident task list 
        self.selectedDepartmentLists = self.selectedLists.copy;
    }

}

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    return action == @selector(unwindFromEditList:);
}

- (IBAction)unwindFromEditList:(UIStoryboardSegue *)unwindSegue
{
    _cachedDepartmentLists = nil;
    if (self.fireDepartment.managedObjectContext) {
        if ([unwindSegue.identifier isEqualToString:@"unwindSaveEditTaskList"]) {
            // edit the existing or new task list for the department
            CreateNewListVC *vc = (CreateNewListVC *)unwindSegue.sourceViewController;
            DepartmentTaskList *editedTaskList = vc.currentList;

            // create a new list if needed
            if (!editedTaskList) {
                // create a new list in Core Data
                editedTaskList = [DepartmentTaskList createWithName:vc.updatedListName
                                                          sortValue:0
                                                        withContext:self.fireDepartment.managedObjectContext];
                editedTaskList.forFireDepartment = self.fireDepartment;
                editedTaskList.uniqueId = [[NSUUID UUID]UUIDString];
                [self.selectedLists addObject:editedTaskList];
            }
            else {
                // remove the old list items
                NSSet *oldListItems = editedTaskList.items;
                if (oldListItems) {
                    [editedTaskList removeItems:oldListItems];
                }
            }

            // update list name
            editedTaskList.listName = vc.updatedListName;

            // create new list items
            NSUInteger taskCount = 0;
            for (NSString *taskDescription in vc.updatedTaskList) {
                ListItem *newTask = [ListItem createItemNamed:taskDescription
                                                    withValue:taskCount++
                                                  withContext:self.fireDepartment.managedObjectContext];
                [editedTaskList addItemsObject:newTask];
            }
            self.cachedDepartmentLists = nil;    // reload the cache
            [self.tableView reloadData];
        }
        else if ([unwindSegue.identifier isEqualToString:@"unwindCancelEditTaskList"]) {
            // ignore since it was a cancel
        }
    }

}

#pragma mark - Task Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    SortedTaskList *sortedTaskList = self.cachedDepartmentLists[section];
    DepartmentTaskList *taskList = (DepartmentTaskList *) sortedTaskList.managedListObject;

    if (!self.selectedLists) {
        self.selectedLists = [NSMutableArray arrayWithCapacity:3];
    }

    if (row == 0) {
        NSUInteger indexOfObjectInSelected = [self.selectedLists indexOfObject:taskList];
        if (indexOfObjectInSelected == NSNotFound) {
            [self.selectedLists addObject:taskList];
        }
    }
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    SortedTaskList *sortedTaskList = self.cachedDepartmentLists[section];
    DepartmentTaskList *taskList = (DepartmentTaskList *) sortedTaskList.managedListObject;

    if (self.selectedLists && row == 0) {
        NSUInteger indexOfObjectInSelected = [self.selectedLists indexOfObject:taskList];
        if (indexOfObjectInSelected != NSNotFound) {
            [self.selectedLists removeObjectAtIndex:indexOfObjectInSelected];
        }
    }
}

@end
