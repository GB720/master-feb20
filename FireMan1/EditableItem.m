//
// Created by Gerald Boyd on 1/23/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "EditableItem.h"
#import "FDPermittedValues.h"


@interface EditableItem ()

@end

@implementation EditableItem {

    NSDecimalNumber *_sortValue;
    NSArray *_editableItems;
    NSString *_value;
    id <FDPermittedValues> _permittedValues;
}

// Designated Initializer
- (id)init
{
    self = [super init];
    if (self) {
        _value = nil;
        _sortValue = nil;
        _editableItems = nil;
        _permittedValues = nil;
        _isReorderable = NO;
        _isDeletable = NO;
        _hasSortValue = NO;
    }
    return self;
}

- (void) setEditableItems:(NSArray *)editableItemArray
{
    _editableItems = editableItemArray;
}

- (NSArray *) editableItems
{
    return _editableItems;
}

- (void)setPermittedValues:(id <FDPermittedValues>)permittedValuesObject
{
    _permittedValues = permittedValuesObject;
}


- (void)updateRelationshipWith:(id <FDedit>)entity
{
    if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(updateRelationshipWith:)]) {
        [self.ownerObject performSelector:@selector(updateRelationshipWith:) withObject:entity];
    }
    [self save:entity.value];
}


- (id <FDPermittedValues>)permittedValues
{
    return _permittedValues;
}


+ (instancetype)owner:(id <FDedit>) ownerObject fromString:(NSString *)name
{
   return [EditableItem owner:ownerObject named:nil fromString:name];
}

+ (instancetype) owner:(id <FDedit>) ownerObject named:(NSString *) itemName
{
    EditableItem *editableItem = [[EditableItem alloc] init];
    editableItem.itemName = itemName;
    editableItem.ownerObject = ownerObject;
    return editableItem;
}

+ (instancetype) owner:(id <FDedit>) ownerObject named: (NSString *) itemName fromString:(NSString *) value
{
    EditableItem *editableItem = [EditableItem owner:ownerObject named:itemName];
    editableItem.value = value;
    return editableItem;
}

+ (instancetype) owner:(id <FDedit>) ownerObject named: (NSString *) itemName fromArray:(NSArray *) array
{
    return [EditableItem owner:ownerObject named:itemName fromArray:array sortValue:0];
}

+ (instancetype) owner:(id <FDedit>) ownerObject named: (NSString *) itemName fromArray:(NSArray *) array sortValue:(NSInteger) sortValue
{
    EditableItem *editableItem = [EditableItem owner:ownerObject named:itemName];
    editableItem.editableItems = array;

    if (sortValue >= 0) {
        editableItem.sortValue = [NSDecimalNumber decimalNumberWithMantissa:(NSUInteger) sortValue exponent:1 isNegative:NO];
        editableItem.hasSortValue = YES;
    }
    else {
        editableItem.sortValue = [NSDecimalNumber notANumber];
    }
    return editableItem;
}

+ (instancetype) owner:(id) ownerObject value:(NSString *) value;
{
    return [EditableItem owner:ownerObject value:value itemName:nil];
}

+ (instancetype) owner:(id) ownerObject value:(NSString *) value itemName:(NSString *) itemName;
{
    return [EditableItem owner:ownerObject value:value permittedValues:nil itemName:itemName sortValue:-1];
}

+ (instancetype) owner:(id) ownerObject value:(NSString *) value permittedValues:(id <FDPermittedValues>) permittedValues itemName:(NSString *) itemName;
{
    return [EditableItem owner:ownerObject value:value permittedValues:permittedValues itemName:itemName sortValue:-1];
}

+ (instancetype) owner:(id) ownerObject value:(NSString *) value permittedValues:(id <FDPermittedValues>) permittedValues itemName:(NSString *) itemName sortValue:(NSInteger)sortValue;
{
    NSDecimalNumber *decimalNumber = [self getDecimalNumber:sortValue];
    EditableItem *editableItem = [EditableItem owner:ownerObject value:value itemName:itemName decimalSortValue:decimalNumber];
    editableItem.permittedValues = permittedValues;
    return editableItem;
}

+ (NSDecimalNumber *)getDecimalNumber:(NSInteger)sortValue
{
    NSDecimalNumber *decimalNumber = [NSDecimalNumber notANumber];
    if (sortValue >= 0) {
        decimalNumber = [NSDecimalNumber decimalNumberWithMantissa:(NSUInteger)sortValue exponent:0 isNegative:NO];
    }
    return decimalNumber;
}


+ (instancetype)owner:(id)ownerObject value:(NSString *)value itemName:(NSString *)itemName sortValue:(NSInteger)sortValue
{
    return [self owner:ownerObject value:value permittedValues:nil itemName:itemName sortValue:sortValue];
}

+ (instancetype) owner:(id) ownerObject value:(NSString *) value itemName:(NSString *) itemName decimalSortValue:(NSDecimalNumber *) decimalSortValue;
{
    EditableItem *editableItem = nil;
    if (!value) {
        value = @"";
    }
    editableItem = [EditableItem owner:ownerObject fromString:value];
    editableItem.itemName = itemName;
    editableItem.sortValue = decimalSortValue;
    if ([decimalSortValue isEqualToNumber:[NSDecimalNumber notANumber]]) {
        editableItem.hasSortValue = NO;
    }
    return editableItem;
}

- (void) save: (NSString *) newValue
{
    assert (self.ownerObject);
    _value = newValue;
    if (self.ownerObject) {
        if (self.itemName) {
            [self.ownerObject save:newValue itemname:self.itemName];
        }
        else {
            [self.ownerObject save:newValue itemname:@"value"];
        }
    }
}

- (void)save:(NSString *)newValue itemname:(NSString *)itemName
{
    [self save:newValue];
}

- (NSDecimalNumber *) sortValue {
    if (!_sortValue) {
        if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(sortValue)]) {
            _sortValue = self.ownerObject.sortValue;
        }
        else {
            return [NSDecimalNumber notANumber];
        }
    }
    return _sortValue;
}

- (void) setSortValue: (NSDecimalNumber *) value {
    if ([value compare:[NSDecimalNumber zero]] == NSOrderedDescending) {
        // item name means that this owner object may have multiple properties in the table
        if (self.ownerObject && !self.itemName) {
            self.ownerObject.sortValue = value;
        }
        _sortValue = value;
    }
    else {
        _sortValue = nil;
    }
}

- (NSString *)localizedItemName
{
    NSString *localizedName = nil;
    if (self.itemName) {
        localizedName = NSLocalizedString(self.itemName, self.itemName);
    }
    return localizedName;
}

- (BOOL)mayAddEditableItemForItemName:(NSString *)itemName
{
    return self.ownerObject && [self.ownerObject respondsToSelector:@selector(mayAddEditableItemForItemName:)] && [self.ownerObject mayAddEditableItemForItemName:itemName];
}

- (NSArray *) addEditableItemForItemName:(NSString *)itemName
{
    if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(addEditableItemForItemName:)]) {
        return [self.ownerObject addEditableItemForItemName:itemName];
    }
    assert (NO); // should not get here
    return self.editableItems;
}

- (NSArray *)deleteEditableItem:(id <FDedit>)item forItemName:(NSString *)itemName
{
    if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(deleteEditableItem:forItemName:)]) {
        return [self.ownerObject deleteEditableItem:item forItemName:itemName];
    }
    return nil;
}

- (BOOL) mayAddEditableItem
{
    if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(addEditableItemForItemName:)]) {
        return YES;
    }
    return NO;
}

- (BOOL) mayDeleteEditableItem
{
    if (self.ownerObject && [self.ownerObject respondsToSelector:@selector(deleteEditableItem:forItemName:)]) {
        return YES;
    }
    return NO;
}

- (BOOL)isReorderable
{
    if (!_isReorderable) {
        BOOL value = NO;
        if (!self.itemName && self.ownerObject && [self.ownerObject respondsToSelector:@selector(isReorderable)]) {
            value = self.ownerObject.isReorderable;
        }
        _isReorderable = value;
    }
    return _isReorderable;
}

- (BOOL)isDeletable
{
    if (!_isDeletable) {
        BOOL value = NO;
        if (!self.itemName && self.ownerObject && [self.ownerObject respondsToSelector:@selector(isDeletable)]) {
            value = self.ownerObject.isDeletable;
        }
        _isDeletable = value;
    }
    return _isDeletable;
}

- (BOOL)hasSortValue
{
    if (!_hasSortValue) {
        BOOL value = NO;
        if (!self.itemName && self.ownerObject && [self.ownerObject respondsToSelector:@selector(hasSortValue)]) {
            value = self.ownerObject.hasSortValue;
        }
        else if (self.sortValue
                && (![self.sortValue isEqualToNumber:[NSDecimalNumber notANumber]])) {
            value = YES;
        }
        _hasSortValue = value;
    }
    return _hasSortValue;
}

- (NSString *) value
{
    return _value;
}

- (void) setValue: (NSString *) newValue
{
    _value = newValue;
}


@end