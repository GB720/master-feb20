//
//  DepartmentFireUnitCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/4/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "DepartmentFireUnitCell.h"

@implementation DepartmentFireUnitCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
