//
//  FireManViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Incident.h"
#import "FireManAppDelegate.h"
#import "FireUnitListDelegate.h"

@class FireResourcesView;
@class ManagementCollectionView;
@class DragDropManager;
@class FireDepartment;
@class FireunitListSegue;
@class Preferences;
@class IncidentTemplate;
@class Device;
@class CurrentUser;

static NSString *const PREF_STAGING_TIME = @"defaultStagingTime";
static NSString *const PREF_PAR_LIMIT = @"parLimit";
static NSString *const PREF_ACTIVE_LIMIT = @"unitActiveLimit";
static NSString *const PREF_NAME = @"fireDepartmentName";
static NSString *const PREF_SCAN_PERIOD = @"scanPeriod";
static NSString *const PREF_MAX_USERS = @"maxUsers";

static NSString *const REOPEN_INCIDENT_TITLE = @"Re-open Incident?";
static NSString *const TERMINATE_INCIDENT_TITLE = @"Terminate Incident?";
static NSString *const TAKE_OVER_INCIDENT_TITLE = @"Take-over Incident?";
static NSString *const RE_OPEN = @"Re-Open";
static NSString *const TERMINATE = @"Terminate";
static NSString *const TAKE_OVER = @"Take Over";

@interface FireManViewController : UIViewController <UIPopoverPresentationControllerDelegate, FireUnitListDelegate> {

}
@property(nonatomic, strong) NSArray *cachedTaskLists;
@property (strong, nonatomic) Incident *incident;
@property (strong, nonatomic) FireDepartment *fireDepartment;
@property (strong, nonatomic) Device *device;
@property (weak, nonatomic) IBOutlet FireResourcesView *fireResourcesView;
@property (weak, nonatomic) IBOutlet UIToolbar *mainToolbar;
@property (strong, nonatomic) IBOutlet ManagementCollectionView *managementView;
@property (strong, nonatomic) DragDropManager *dragAndDropManager;
@property(nonatomic, strong) NSManagedObjectContext *context;
@property(nonatomic, strong) NSDate *startTime;
@property(nonatomic, readonly) UIColor *colorForCoordinatorIsSet;
@property(nonatomic, readonly) UIColor *colorForNoCoordinator;
@property(nonatomic) BOOL isSegueActive;
@property (strong, nonatomic) Preferences *preference;

@property(nonatomic, strong) NSDictionary *cellDirectory;


- (IBAction)unwindToMain:(UIStoryboardSegue *)unwindSegue;

- (Preference *)getPreference:(NSString *)name;
- (void)setNeedsRefreshView;
@end
