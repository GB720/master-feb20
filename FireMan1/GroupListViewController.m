//
//  GroupListViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 4/25/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "GroupListViewController.h"
#import "FireDepartment+Create.h"
#import "GroupName.h"
#import "ManagementViewCell.h"
#import "GetNameViewController.h"
#import "GroupName+Create.h"
#import "Incident.h"
#import "ListItem+Create.h"
#import "Incident+Create.h"
#import "IncidentLog+Log.h"
#import "MessageViewController.h"

static NSUInteger const MAXIMUM_ROWS_TO_DISPLAY = 12;
static NSUInteger const MINIMUM_ROWS = 2;
static NSUInteger const LIST_WIDTH = 280;

@interface GroupListViewController ()
- (IBAction)terminateGroupButton:(UIBarButtonItem *)sender;
- (IBAction)cancelGroupNameButton:(UIBarButtonItem *)sender;
@property (strong, nonatomic) NSArray *sortedNames;
@property (weak, nonatomic) IBOutlet UITableView *namePicker;
@property NSInteger maxNameLength;
@property(nonatomic, copy) NSString *currentMessage;
@end


@implementation GroupListViewController

#pragma mark - view notifications

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSSet *groupNames = self.fireDepartment.groupNames;
    groupNames = [self.fireDepartment removeDuplicatesIn:groupNames];
    groupNames = [self removeActiveNamesFrom:groupNames];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    _sortedNames = [groupNames sortedArrayUsingDescriptors:sortDescriptors];
    _maxNameLength = [self maxLength:self.sortedNames];
    _namePicker.dataSource = self;
    _namePicker.delegate = self;
    self.namePicker.hidden = NO;
    if (groupNames.count < 1) {
        self.namePicker.hidden = YES;
    }
    self.view.autoresizesSubviews = YES;
    self.namePicker.accessibilityIdentifier = @"namePicker";
    
    NSInteger numRows = self.fireDepartment.groupNames.count + 1;
    NSInteger numGroups = self.incident.managedTasks.count - 1; // allow for dummy group
    numRows -= numGroups;
    if (numRows > MAXIMUM_ROWS_TO_DISPLAY) {
        numRows = MAXIMUM_ROWS_TO_DISPLAY;
    }
    if (numRows < MINIMUM_ROWS) {
        numRows = MINIMUM_ROWS;
    }
    NSInteger listHeight = numRows * 44;
    
    self.preferredContentSize = CGSizeMake(LIST_WIDTH, listHeight);

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self unwindFromVC:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - group name list management

- (NSSet *)removeActiveNamesFrom:(NSSet *)groupNames 
{
    NSMutableSet *nonactiveGroupNames = [NSMutableSet setWithCapacity:20];

    for (GroupName *groupName in groupNames) {
        BOOL isActive = NO;
        for (ListItem *listItem in self.incident.managedTasks) {
            if ([listItem.name isEqualToString:groupName.name]) {
                isActive = YES;
            }
        }
        if (!isActive) {
            [nonactiveGroupNames addObject:groupName];
        }
    }
    return nonactiveGroupNames.copy;
}

- (void)addName:(NSString *)newName
{
    for (GroupName *groupName in self.sortedNames) {
        if ([groupName.name isEqualToString:newName])
            return;   // name already exists
    }
    GroupName *newGroupName = [GroupName createWithName:newName withContext:self.fireDepartment.managedObjectContext];
    [self.fireDepartment addGroupNamesObject:newGroupName];
}

//
// get max length string size in array of GroupNames
- (NSInteger)maxLength:(NSArray *)arrayOfStrings
{
    NSInteger max = 0;
    if (arrayOfStrings.count > 0) {
        for (GroupName *groupName in arrayOfStrings) {
            if (groupName.name.length > max) {
                max = groupName.name.length;
            }
        }
    }
    return max;
}

#pragma mark - data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sortedNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupListViewCell"];
    GroupName *groupName = self.sortedNames[(NSUInteger) indexPath.row];
    cell.textLabel.text = groupName.name;
    return cell;
}

- (void)unwindFromVC:(NSString *)groupName
{
    self.selectedGroupName = groupName;
    if (groupName) {
        self.groupIndex = self.currentCell.buttonLabel.tag;
    }
    [self performSegueWithIdentifier:@"unwindFromGroupListView" sender:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupName *groupName = self.sortedNames[(NSUInteger)indexPath.row];
    self.currentCell.buttonLabel.titleLabel.text = groupName.name;
    [self.currentCell.buttonLabel setTitle:groupName.name forState:UIControlStateNormal];
    [self unwindFromVC:groupName.name];
}


#pragma mark - button and segue actions

- (void) createNewName: (NSString *) newName
{
    if (newName && newName.length > 0) {
        [self addName:newName];
        [self.currentCell.buttonLabel setTitle:newName forState:UIControlStateNormal];
        [self unwindFromVC:newName];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"getNameSegue"]) {
        if ([segue.destinationViewController isKindOfClass:[GetNameViewController class]]) {
            GetNameViewController *destinationVC = (GetNameViewController *) segue.destinationViewController;
            destinationVC.incident = self.incident;
        }
    }
    else if ([segue.identifier isEqualToString:@"messageSegue"]) {
        MessageViewController *destinationVC = (MessageViewController *) segue.destinationViewController;
        destinationVC.message = self.currentMessage;
    }
}

- (IBAction)unwindToGroupList:(UIStoryboardSegue *)unwindSegue
{
    if ([unwindSegue.identifier isEqualToString:@"UnwindFromMessage"]) {
        [self unwindFromVC:nil];
    }
    else if ([unwindSegue.identifier isEqualToString:@"UnwindFromNewName"]) {
        GetNameViewController *vc = unwindSegue.sourceViewController;
        NSString *newName = vc.groupName;
        [self createNewName:newName];
    }
}

- (IBAction)terminateGroupButton:(UIBarButtonItem *)sender
{
    if ([self.selectedDivision.name isEqualToString:self.dummyName]) {
        // treat as a cancel
        [self unwindFromVC:nil];
    }
    else {
        // check that division has no units assigned to it
        if (self.selectedDivision.assignedUnits.count > 0) {
            // has units assigned
            self.currentMessage = @"Fire Unit is still assigned to the group.  Group can not be terminated.";
            [self performSegueWithIdentifier:@"messageSegue" sender:self];
        }
        else {
            // terminate the division
            [self.incident.log action:@"Terminated group" task:self.selectedDivision];
            [self.currentCell.buttonLabel setTitle:FD_GROUP_TERMINATED forState:UIControlStateNormal];
            [self unwindFromVC:FD_GROUP_TERMINATED];
        }
    }
}

- (IBAction)cancelGroupNameButton:(UIBarButtonItem *)sender
{
    [self unwindFromVC:nil];
    // todo remove [self.popoverVwController dismissPopoverAnimated:YES];
}


@end
