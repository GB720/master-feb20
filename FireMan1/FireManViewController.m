//
//  FireManViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
#import <os/activity.h>
#import <os/trace.h>

#import "FireManViewController.h"
#import "FireResourcesView.h"
#import "FireResourcesCollectionView.h"
#import "ManagementCollectionView.h"
#import "ActiveDivisionCollectionView.h"
#import "ManagementViewCell.h"
// #import "ListItem.h"
#import "FireDepartment.h"
#import "IncidentTaskList.h"
#import "FireUnit+Create.h"
#import "IncidentTaskList+Create.h"
#import "ListItem+Create.h"
#import "NameListSegue.h"
#import "FireDepartment+Create.h"
#import "GroupName.h"
#import "GroupName+Create.h"
#import "IncidentLabel.h"
#import "FireunitListSegue.h"
#import "IncidentInfo.h"
#import "GbElapsedTime.h"
#import "IncidentLog.h"
#import "IncidentLog+Create.h"
#import "IncidentLog+Log.h"
#import "LogViewController.h"
#import "TaskTableView.h"
#import "SortedTaskList.h"
#import "Preferences.h"
#import "ListItem+Status.h"
#import "List+Status.h"
#import "DepartmentTaskList+Create.h"
#import "FireUnitSelectionVC.h"
#import "DepartmentResources+Create.h"
#import "Device.h"
#import "FireUnit+Identification.h"
#import "ParOkButton.h"
#import "IncidentSelectionViewController.h"
#import "UserIdentificationViewController.h"
#import "GroupListViewController.h"
#import "IcSelectionViewController.h"
#import "SettingsViewController.h"
#import "DepartmentMasterVC.h"
#import "Preference+Create.h"
#import "IncidentTemplate.h"
#import "UnitMovementView.h"
#import "DefaultStage.h"
#import "DefaultStage+Create.h"
#import "StagingGroup.h"
#import "StagingGroup+Create.h"
#import "StagingHeaderView.h"
#import "AddToStagingGroupSegue.h"
#import "StagingLevelViewController.h"
#import "EditStagingGroupsViewController.h"
#import "EditListOfListsVC.h"
#import "IncidentTemplate+Create.h"
#import "DragContext.h"
#import "FireUnitListVC.h"
#import "FireStationView.h"
#import "CurrentUser.h"
#import "EditDepartmentVC.h"
#import "UIColor+EmerResStrat.h"
#import "FireCellColor.h"
#import "NSAttributedString+ersCategory.h"
#import "IncidentSelectionDelegate.h"
#import "Device+Create.h"
#import "CurrentUser.h"
#import "Role+Create.h"
#import "User.h"

// *** Task name text/dropdown combo box
// *** need an Add division/group name
// *** need Elapsed time
// *** need appearance phase 1
// *** Add coordinating unit id in lower right corner of task heade
// *** need logs
// *** reset the sortvalue after creating the sortedlists
// *** add tasks and taskLists
// *** need an Add list popup?
// *** need an Add List item popup (parent list dropdown)?
// *** need PAR request button
// *** need timer/color indicator for incident age of unit
// *** need to update unit age/color every (x) seconds
// *** need an Add FireUnit popup?
// *** need ability to populate resource list from department resource list
// *** test on actual iPad
// *** need rehab pool
// *** three (3) department prefix
// *** each cell needs a PAR needed indicator and a way to click to show PAR for that unit
// *** clear age when going out of rehab
// *** clear PAR flag when going to rehab?
// *** need way to save/restore fire department data
// *** need incident selection view
// *** PAR button with counter in it (or colors)?

// *** need incident deletion
// *** need incident naming view for new incidents
// *** allow resuming any incident
// *** don't allow deletion of currently active incident
// *** need IC specification
// *** preferences page
// *** need fireunit status tableview

// *** log sorting
// *** create and use unitMovementView

// *** IOS7 migration
// *** staging collection view
// *** support adding a staging level
// *** filter out staging groups that are already in the incident
// *** if no staging groups available in the fire deparment go direct to name and time entry?

// *** add editing panel for task lists

// *** support editing a staging level (name and time)

// *** support incident template
// *** support export/import of fire department setup info

// *** need iCloud support?
// *** need delete on fireunits and neighbor departments
// todo need MAYDAY list button

// *** need way to edit tasks on fire department level
// *** need way to edit group names on fire department level

// *** move "edit department" to select incident popup?
 // *** need a "terminate incident" action
// *** need to save to url/file (icloud support)
// todo support and privacy URL's
// todo submit to apple store
// todo need automated testing

static NSString *const DUMMY_NAME = @"---?";

@interface FireManViewController () <UICollectionViewDataSource,
        UICollectionViewDelegate,
        UICollectionViewDelegateFlowLayout,
        DragDropDelegate,
        UITableViewDataSource,
        UITableViewDelegate,
        IncidentSelectionDelegate>

- (NSInteger)countOfUnitsFor:(NSString *)taskName;
- (IBAction)addUnitsToStageButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIProgressView *barProgressView;
@property (nonatomic) BOOL progressActive;
@property (strong, nonatomic) IBOutlet FireStationView *fireStationView;
@property (strong, nonatomic) IBOutlet UILabel *parWarningLabel;
@property (nonatomic) BOOL viewAppeared;
@property (strong, nonatomic) IBOutlet UIButton *userIdButton;


- (IBAction)StageIdentifierPressed:(UIButton *)sender;

- (NSSet *)assignedUnitsFor:(NSString *)taskName;
- (NSArray *)sort:(NSSet *)set;

- (User *)defaultUser;

- (IBAction)nameButtonPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *parButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *icButton;
- (IBAction)icButtonPress:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
- (IBAction)titleButtonPress:(UIButton *)sender;

- (IBAction)showLogButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)groupNameChanged:(UIButton *)sender;
- (IBAction)coordinatorButtonPress:(UIButton *)sender;
- (IBAction)parButtonPressed:(UIBarButtonItem *) sender;
- (IBAction)parIsOkButton:(ParOkButton *)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *terminateIncidentButton;
- (IBAction)terminateIncidentButtonPressed:(UIBarButtonItem *)sender;


@property (weak, nonatomic) IBOutlet UnitMovementView *unitMovementView;
@property(weak, nonatomic) IBOutlet TaskTableView *taskTableView;
@property(nonatomic, strong) UIButton *lastNameButtonPressed;
@property(weak, nonatomic) IBOutlet IncidentLabel *managementHeader;
@property(nonatomic, strong) IncidentInfo *incidentInfo;
@property(nonatomic, strong) FireunitListSegue *unitListSegue;

@property(nonatomic) enum UITableViewCellEditingStyle taskEditStyle;
@property(nonatomic, strong) UITableViewCell *activeCell;
@property(nonatomic, strong) NSIndexPath *activeIndexPath;
@property(nonatomic, readonly) BOOL isOtherViewControllerActive;
@property(nonatomic) NSInteger countOfUnitsNeedingPar;
@property(nonatomic) BOOL isLogOnly;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic, strong) UIColor *titleColor;
@property(nonatomic, strong) UIColor *titleBgColor;

@property(nonatomic) CGFloat titleSize;
@property (nonatomic, strong) NSArray *defaultStageNames;
@property (nonatomic, strong) NSArray *sortedStagingGroups;
@property (nonatomic, strong) UIColor *HeaderBackgroundColor;
@property(nonatomic, strong) IncidentTemplate *incidentTemplate;

@property (nonatomic, strong) UIManagedDocument *managedDocument;
@property (nonatomic, strong) NSMutableDictionary *divViewDictionary;
@property (nonatomic, strong) NSArray *sortedManagedTasks;
@property(nonatomic, weak) UIViewController *destinationVC;
@property(nonatomic) NSInteger previousCloudTransitionType;

@property(nonatomic) BOOL isResetting;
@property(nonatomic) BOOL isParCheckActive;
@property(nonatomic, strong) CurrentUser *currentUser;
@property(nonatomic, strong) FireManAppDelegate *appDelegate;
@end


@implementation FireManViewController {

}


- (BOOL)isOtherViewControllerActive
{
    BOOL isOtherActive = NO;
    if (self.dragAndDropManager.isActive) {
        isOtherActive = YES;
    }
    else if (!self.isViewLoaded) {
         isOtherActive = YES;
    }
    else if (!self.view.window) {
        isOtherActive = YES;  // different view controller is active
    }
    else if (self.isSegueActive) {
        isOtherActive = YES;
    }
    return isOtherActive;
}

- (NSArray *)sortedManagedTasks
{
    if (!_sortedManagedTasks) {
        // os_log ("sortedManagedTasks..rebuild");
        _sortedManagedTasks = [self sort:self.incident.managedTasks withKey:@"sortOrderValue"];
    }
    return _sortedManagedTasks;
}

- (NSMutableDictionary *)divViewDictionary
{
    // os_log ("divViewDictionary");
    if (!_divViewDictionary) {
        // os_trace ("divViewDictionary..rebuild");
        _divViewDictionary = [NSMutableDictionary dictionaryWithCapacity:11];
    }
    return _divViewDictionary;
}

- (IncidentTemplate *)incidentTemplate
{
    // os_trace ("incidentTemplate");
    if (!_incidentTemplate) {
        _incidentTemplate = self.fireDepartment.incidentTemplate;
        if (!_incidentTemplate) {
            _incidentTemplate = [IncidentTemplate fetchFor:self.fireDepartment];
            if (!_incidentTemplate) {
              // create one
              _incidentTemplate = [IncidentTemplate createFor:self.fireDepartment];
            }
            self.fireDepartment.incidentTemplate = _incidentTemplate;
        }
   
    }
    return _incidentTemplate;
}


- (DragDropManager *) dragAndDropManager
{
    if (!_dragAndDropManager) {
        _dragAndDropManager = [[DragDropManager alloc] init];
    }
    return _dragAndDropManager;
}



#pragma mark - view action notifications
// Name of operational division, team
- (IBAction)nameButtonPressed:(UIButton *)sender {
    os_activity_label_useraction("group nameButtonPressed");
    [self maybeDoSegueWithIdentifier:@"namelistSegue" sender:sender ];
}


- (IBAction)addUnitsToStageButton:(UIButton *)sender
{
    os_activity_label_useraction("Add units to staging group");
    [self maybeDoSegueWithIdentifier:@"addUnitsToStagingGroupSegue" sender:sender];
}

- (BOOL)maybeDoSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    BOOL didSegue = YES;
    if ([self shouldPerformSegueWithIdentifier:identifier sender:sender]) {
        [self performSegueWithIdentifier:identifier sender:sender];
    }
    else {
        didSegue = NO;
    }
    return didSegue;
}

- (IBAction)coordinatorButtonPress:(UIButton *)sender
{
    os_activity_label_useraction("group coordinatorButtonPress");
    [self maybeDoSegueWithIdentifier:@"unitListSegue" sender:sender];
}


#pragma mark - PAR button processing

- (IBAction)parButtonPressed:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("parButtonPressed");
    if (!self.isLogOnly && self.currentUser.isCoordinator) {
        if (!self.isSegueActive) {
            self.isParCheckActive = YES;
            // os_trace("PAR Activated");
            [self.incident.log action:@"PAR Activated"];
            NSUInteger managedGroupIndex = 0;

            // do active units
            for (ListItem *activeGroup in self.sortedManagedTasks) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:managedGroupIndex inSection:0];
                ManagementViewCell *groupViewCell = (ManagementViewCell *) [self.managementView cellForItemAtIndexPath:indexPath];
                [self activateParForUnitsInGroup:activeGroup groupViewCell:groupViewCell];
                managedGroupIndex++;
            }

            // set par request for units in staging groups
            NSUInteger stagingGroupIndex = 0;
            for (StagingGroup *stagingGroup in self.sortedStagingGroups) {
                [self activateParForUnitsInStagingGroup:stagingGroup stagingGroupIndex:stagingGroupIndex];
                stagingGroupIndex++;
            }

            // reset the parTimer
            self.incident.timeOfLastPar = [NSDate date];
            self.parWarningLabel.hidden = YES;
            sender.tintColor = [UIColor ersBlueColor];
            [self logParStatus];
        }
        [self setNeedsSave];
    }
    else {
        [self coordinatorRequiredAlert];
    }
}

- (void)activateParForUnitsInStagingGroup:(StagingGroup *)stagingGroup stagingGroupIndex:(NSUInteger)stagingGroupIndex
{
    // os_trace("activateParForUnitsInStagingGroup %u",(unsigned int)stagingGroupIndex);
    NSUInteger unitIndex = 0;
    NSSet *units = stagingGroup.units;
    NSUInteger unitCount = units.count;
    for (FireUnit *fireUnit in units) {
        fireUnit.parStatusOk = @NO;
        //[self.incident.log action:@"PAR Needed" unit:fireUnit task:stagingGroup];
    }
    for (unitIndex = 0; unitIndex < unitCount; unitIndex++) {
        NSIndexPath *unitIndexPath = [NSIndexPath indexPathForItem:unitIndex inSection:stagingGroupIndex];
        FireResourceViewCell *unitViewCell = (FireResourceViewCell *) [self.fireResourcesView cellForItemAtIndexPath:unitIndexPath];
        [self enableParOkButton:unitViewCell];
    }
}

- (void)activateParForUnitsInGroup:(ListItem *)activeGroup groupViewCell:(ManagementViewCell *)groupViewCell
{
    // os_trace("activateParForUnitsInGroup");
    ActiveDivisionCollectionView *divView = groupViewCell.activeDivisionView;
    NSArray *sortedUnits = [self sort:activeGroup.assignedUnits];
    NSUInteger index = 0;
    for (FireUnit *fireUnit in sortedUnits) {
        [self activateParForUnitInDivision:divView index:index fireUnit:fireUnit];
        index++;
    }
}

- (void)activateParForUnitInDivision:(ActiveDivisionCollectionView *)divView index:(NSUInteger)index fireUnit:(FireUnit *)fireUnit
{
    // os_trace("activateParForUnitInDivision: %u",(unsigned int)index);
    fireUnit.parStatusOk = @NO;
    //[self.incident.log action:@"PAR Needed " unit:fireUnit];

    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
    FireResourceViewCell *unitViewCell = (FireResourceViewCell *) [divView cellForItemAtIndexPath:unitIndexPath];
    [self enableParOkButton:unitViewCell];
}

- (void)enableParOkButton:(FireResourceViewCell *)unitViewCell
{
    // os_trace("enableParOkButton");
    [unitViewCell parStatusButtonEnabled:YES];
    
}

 - (IBAction)availUnitParIsOkButton:(ParOkButton *)sender
 {
     if (self.currentUser.isCoordinator) {
         FireResourceViewCell *unitCell;
         UIView *view = sender.superview;
         
         // get the Collection View Cell
         while (!unitCell) {
             if ([view isKindOfClass:[FireResourceViewCell class]]) {
                 unitCell = (FireResourceViewCell *) view;
             }
             else {
                 view = view.superview;
                 assert (view);
             }
         }
         assert (unitCell);
         
         // get staging group and fire unit entity
         NSIndexPath *indexPath = [self.fireResourcesView indexPathForCell:unitCell];
         StagingGroup *stagingGroup = self.sortedStagingGroups[(NSUInteger) indexPath.section];
         NSArray *sortedUnits = [self sort:stagingGroup.units withKey:@"prefixedName"];
         FireUnit *unit = sortedUnits[(NSUInteger) indexPath.item];
         
         // clear the par status
         unit.parStatusOk = @YES;
         
         [self disableParOkButton:sender unitCell:unitCell];
         
         NSLog(@"PAR Okay for %@",unit.prefixedName);
         [self.incident.log action:@"PAR Okay" unit:unit task:stagingGroup];
         [self logParStatus];
     }
     else {
         [self coordinatorRequiredAlert];
     }
 }

 - (IBAction)parIsOkButton:(ParOkButton *)sender
{
    os_activity_label_useraction("parIsOkButton");
    if (self.currentUser.isCoordinator) {
        // get units assigned to the group
        NSSet *units = [self assignedUnitsFor:sender.taskId];
        NSUInteger unitViewIndex = (NSUInteger) sender.tag;
        
        // get unit for the specified index
        NSArray *sortedUnits = [self sort:units];
        FireUnit *unit = sortedUnits[unitViewIndex];
        
        // reset the PAR status for the fire unit
        unit.parStatusOk = @YES;
        
        // get index path of the managed group cell
        NSIndexPath *groupIndexPath = [self getGroupIndexPath:sender.taskId];
        ManagementViewCell *groupCell = (ManagementViewCell *) [self.managementView cellForItemAtIndexPath:groupIndexPath];
        ActiveDivisionCollectionView *divView = groupCell.activeDivisionView;
        
        // get fire unit view cell
        NSIndexPath *unitIndexPath = [NSIndexPath indexPathForItem:unitViewIndex inSection:0];
        FireResourceViewCell *unitCell = (FireResourceViewCell *)[divView cellForItemAtIndexPath:unitIndexPath];
        
        // update par status for the unit view cell
        [self disableParOkButton:sender unitCell:unitCell];
        
        NSLog(@"PAR Okay for %@",unit.prefixedName);
        [self.incident.log action:@"PAR Okay" unit:unit task:[self getTaskFor:sender.taskId]];
        [self logParStatus];
    }
    else {
        [self coordinatorRequiredAlert];
    }
}

 - (void)disableParOkButton:(ParOkButton *)sender unitCell:(FireResourceViewCell *)unitCell {
     sender.enabled = NO;
     [unitCell parStatusButtonEnabled:NO];
     //NSAttributedString *title = unitCell.parStatusButton.titleLabel.attributedText;
     //[sender setAttributedTitle:title forState:UIControlStateNormal];
 }

 - (void)logParStatus
{
    // os_trace("logParStatus");
    if (self.isParCheckActive) {
        NSInteger numUnitsPendingPAR = 0;
        numUnitsPendingPAR += [self numActiveUnitsPendingPar];
        numUnitsPendingPAR += [self numStagedUnitsPendingPar];
        if (numUnitsPendingPAR == 0) {
            self.parButtonOutlet.tintColor = [UIColor ersBlackColor];
            [self.incident.log action:@"PAR completed for all units"];
            self.isParCheckActive = NO;         // everyone is accounted for
        }
        self.countOfUnitsNeedingPar = numUnitsPendingPAR;
    }
}

- (NSInteger)numStagedUnitsPendingPar
{
    NSInteger num = 0;
    for (StagingGroup *stagingGroup in self.incident.stagingGroups) {
           for (FireUnit *fireUnit in stagingGroup.units) {
               if (!fireUnit.parStatusOk.boolValue) {
                   num++;
               }
           }
       }
    // os_trace("numStagedUnitsPendingPar: %u", (unsigned) num);
    return num;
}

- (NSInteger)numActiveUnitsPendingPar
{
    NSInteger num = 0;
    for (ListItem *activeGroup in self.incident.managedTasks) {
           for (FireUnit *fireUnit in activeGroup.assignedUnits) {
               if (!fireUnit.parStatusOk.boolValue) {
                   num++;
               }
           }
       }
    NSLog (@"%s %ld", __PRETTY_FUNCTION__,(long) num);
    return num;
}

// returns the index path of the managed group cell for the specified groupId value
- (NSIndexPath *)getGroupIndexPath:(NSString *)groupId {
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSUInteger index = 0;
    for (ListItem *group in self.sortedManagedTasks) {
        if ([group.currentValue isEqualToString:groupId]) {
            break;
        }
        index++;
    }
    if (index < self.sortedManagedTasks.count) {
        NSIndexPath *groupIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
        return groupIndexPath;
    }
    else {
        return nil;   // no match
    }
}


- (IBAction)icButtonPress:(UIButton *)sender {
    NSLog (@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)titleButtonPress:(UIButton *)sender {
    NSLog (@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)showLogButtonPressed:(UIBarButtonItem *)sender
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
         [self maybeDoSegueWithIdentifier:@"showLogSegue" sender:sender];
}


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    BOOL shouldPerform = NO;
    if (!self.destinationVC.isBeingPresented) {
        self.isSegueActive = NO;
    }
    if (self.isSegueActive && (![identifier isEqualToString:@"GetUserIdentification"])) {
        shouldPerform = NO;
    }
    else {
        if ([identifier isEqualToString:@"GetUserIdentification"]) {
            shouldPerform = !self.currentUser.isSingleRoleMode;
        }
        else if (self.viewAppeared && (self.incident || [identifier isEqualToString:@"IncidentSelection"])) {
            if (self.currentUser.isCoordinator) {
                shouldPerform = YES;
            }
            else {
                if ([identifier isEqualToString:@"IncidentSelection"]) {
                    shouldPerform = YES;
                }
                else if ([identifier isEqualToString:@"showLogSegue"]
                         || [identifier isEqualToString:@"segueToSettings"]
                         ) {
                    shouldPerform = YES;
                }
                else {
                    [self coordinatorRequiredAlert];
                    shouldPerform = NO;
                }
            }
        }
    }
    NSLog (@"%s %d", __PRETTY_FUNCTION__, shouldPerform);
    return shouldPerform;
}

- (void) coordinatorRequiredAlert
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    [self alertPopup:@"Incident Commander Only"
             message:@"Action restricted to only the Coordinator for this incident"
         buttonLabel:nil];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, segue.identifier);

    self.isSegueActive = YES;
    self.destinationVC = segue.destinationViewController;

    if( [segue.identifier isEqualToString: @"namelistSegue"] ) {
        ManagementViewCell *cell = [self getCellForSegue:(ManagementViewCellSegue *)segue sender:sender];
        GroupListViewController *viewController = segue.destinationViewController;
        viewController.dummyName = DUMMY_NAME;
        viewController.selectedDivision = [self getTaskFor:cell.taskCurrentValue];
    }
    else if ([segue.identifier isEqualToString:@"editDepartmentSegue"]) {
        EditDepartmentVC *editDepartmentVC = segue.destinationViewController;
        editDepartmentVC.fireDepartment = self.fireDepartment;
    }
    else if( [segue.identifier isEqualToString: @"unitListSegue"] ) {
        [self getCellForSegue:(ManagementViewCellSegue *)segue sender:sender];
    }
    else if ([segue.identifier isEqualToString:@"GetUserIdentification"]) {
        UserIdentificationViewController *vc = segue.destinationViewController;
        vc.fireDepartment = self.fireDepartment;
    }
    else if( [segue.identifier isEqualToString: @"showLogSegue"] ) {
        LogViewController *logViewController = (LogViewController *)((UINavigationController *) segue.destinationViewController).topViewController;
        logViewController.incident = self.incident;
        logViewController.callingViewController = self;
    }
    else if ([segue.identifier isEqualToString:@"selectUnitsSegue"]){
        FireUnitSelectionVC *fireUnitSelectionVC = (FireUnitSelectionVC *) segue.destinationViewController;
        fireUnitSelectionVC.incident = self.incident;
        fireUnitSelectionVC.callingViewController = self;
        if (!self.fireDepartment.resourceList) {
            self.fireDepartment.resourceList = [DepartmentResources fetchFor:self.fireDepartment];
        }
    }
    else if ([segue.identifier isEqualToString:@"stagingSupervisorSegue"]){
        [self anchorForSegue:(FireunitListSegue *) segue sender:sender];
    }
    else if ([segue.identifier isEqualToString:@"IncidentSelection"]){
        IncidentSelectionViewController *incidentSelectionViewController = segue.destinationViewController;
        // self.fireDepartment = nil;     // force reload of fire department from database
        incidentSelectionViewController.fireDepartment = self.fireDepartment;
        incidentSelectionViewController.callingViewController = self;
        incidentSelectionViewController.delegate = self;
        incidentSelectionViewController.selectedIncident = self.incident;
    }
    else if ([segue.identifier isEqualToString:@"icSelectionSegue"]){
        UINavigationController *navVC = segue.destinationViewController;
        IcSelectionViewController *vc = (IcSelectionViewController *)navVC.topViewController;
        vc.incident = self.incident;
        vc.selectedFireUnit = self.incident.commandUnit;
    }
    else if ([segue.identifier isEqualToString:@"segueToSettings"]){
        UINavigationController *navVC = segue.destinationViewController;
        SettingsViewController *vc = (SettingsViewController *)navVC.topViewController;
        vc.fireDepartmentName = self.fireDepartment.name;
        vc.device = self.device;
        vc.activeLimit = [self.preference numberFor:PREF_ACTIVE_LIMIT];
        vc.parLimit = [self.preference numberFor:PREF_PAR_LIMIT];
        vc.defaultStagingTime = [self.preference numberFor:PREF_STAGING_TIME];
        vc.user = self.device.user;
    }
    else if ([segue.identifier isEqualToString:@"addUnitsToStagingGroupSegue"]){
        FireUnitSelectionVC *fireUnitSelectionVC = (FireUnitSelectionVC *) segue.destinationViewController;
        fireUnitSelectionVC.incident = self.incident;
        fireUnitSelectionVC.callingViewController = self;
        fireUnitSelectionVC.popoverPresentationController.delegate = self;
        UIButton *button = (UIButton *) sender;
        AddToStagingGroupSegue *addUnitsSegue = (AddToStagingGroupSegue *) segue;
        UIView *buttonParentView = button.superview;
        addUnitsSegue.anchorFrame = buttonParentView.frame;
        addUnitsSegue.anchorInView = buttonParentView.superview;
        NSInteger index = buttonParentView.tag;
        fireUnitSelectionVC.stagingGroupIndex = index;
    }
    else if ([segue.identifier isEqualToString:@"addStagingLevelToIncidentSegue"]) {
        UINavigationController *navVC = segue.destinationViewController;
        StagingLevelViewController *vc = (StagingLevelViewController *)navVC.topViewController;
        vc.fireDepartment = self.fireDepartment;
    }
    else if ([segue.identifier isEqualToString:@"editStagingGroupsSegue"]) {
        UINavigationController *nvc = segue.destinationViewController;
        EditStagingGroupsViewController *vc = (EditStagingGroupsViewController *) nvc.topViewController;
        vc.incident = self.incident;
    }
    else if ([segue.identifier isEqualToString:@"editListsOfListsSegue"]) {
        UINavigationController *nvc = segue.destinationViewController;
        EditListOfListsVC *vc = (EditListOfListsVC *) nvc.topViewController;
        vc.incident = self.incident;
    }
    else {
        NSLog (@"Unknown segue: %@", segue.identifier);
        assert (segue.identifier);
        self.isSegueActive = NO;
        self.destinationVC = nil;
    }

}

 - (void) anchorForSegue:(FireunitListSegue *)segue sender:(id)sender
 {
     NSLog (@"%s", __PRETTY_FUNCTION__);
    // get header view based on button tag
     UIButton *button = (UIButton *) sender;
     segue.anchorView = button;
     segue.cell = nil;
     FireUnitListVC *fireUnitListVC = segue.destinationViewController;
     fireUnitListVC.headerView = [self headerViewFor:button];
     fireUnitListVC.stagingGroup = self.sortedStagingGroups[(NSUInteger) button.tag];
 }

 - (StagingHeaderView *) headerViewFor:(UIView *)view
 {
     NSLog (@"%s", __PRETTY_FUNCTION__);
     UIView *superView = view.superview;
     while (superView && ![superView isKindOfClass:[StagingHeaderView class]]) {
         superView = superView.superview;
     }
     assert (superView);
     return (StagingHeaderView *) superView;
 }

- (ManagementViewCell *)getCellForSegue:(ManagementViewCellSegue *)segue sender:(id)sender
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
     // get selected cell from button tag
     UIButton *button = (UIButton *) sender;
     NSIndexPath *indexPath = [NSIndexPath indexPathForItem:button.tag inSection:0];
     segue.cell = (ManagementViewCell *) [self.managementView cellForItemAtIndexPath:indexPath];
     // NSLog(@"selected index=%i", indexPath.item);
     return segue.cell;
 }

// Called on the delegate when the user has taken action to dismiss the popover. This is not called when the popover is dimissed programatically.
- (void)presentationControllerDidDismiss:(UIPopoverPresentationController *)popoverPresentationController
{
    NSLog (@"%s", __PRETTY_FUNCTION__);

    if (!self.isResetting) {
        UIViewController *viewController = popoverPresentationController.presentedViewController;
        if ([viewController isKindOfClass:[FireUnitSelectionVC class] ]) {
            [self setSelectedUnitsAsAvailable:(FireUnitSelectionVC *)viewController];
        }
    }
    self.isSegueActive = NO;
    self.destinationVC = nil;
 }

- (void)setSelectedUnitsAsAvailable:(FireUnitSelectionVC *)vc
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSInteger index = vc.stagingGroupIndex;
    for (FireUnit *selectedFireUnit in vc.selectedUnits) {
        [self createActiveUnit:selectedFireUnit atStagingGroupIndex:index];
    }
    [self.fireResourcesView reloadData];
}

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    BOOL canPerform = NO;
    if (action == @selector(unwindMaster:)) {
            canPerform = YES;
    }
    else if (action == @selector(unwindToMain:)) {
        canPerform = YES;
    }
    else if (action == @selector(unwindUserIdentification:)) {
        canPerform = YES;
    }
    NSLog (@"%s %d", __PRETTY_FUNCTION__,canPerform);
    return canPerform;
}

 - (IBAction)unwindMaster:(UIStoryboardSegue *)unwindSegue
 {
     NSLog (@"%s", __PRETTY_FUNCTION__);
     self.destinationVC = nil;
     self.isSegueActive = NO;
     if (!self.context || !self.fireDepartment || self.isResetting) {              // icloud store must have changed
         self.isSegueActive = NO;
         return;
     }
     if (!self.incident) {
         [self needsPerformIncidentSelection];
     }
 }

- (IBAction)unwindUserIdentification:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.destinationVC = nil;
    self.isSegueActive = NO;
    if (!self.context || !self.fireDepartment || self.isResetting) {              // icloud store must have changed
        self.isSegueActive = NO;
        return;
    }
    // todo get user id and attach user entity to the device entity
    if ([unwindSegue.identifier isEqualToString:@"unwindFromUserIdentification"]) {
        UserIdentificationViewController *vc = unwindSegue.sourceViewController;
        if (vc.isUpdate) {
            self.currentUser.userEntity = vc.user;
            [self updateUserIdButton];
            [self updateTerminateIncidentButton];
            [self updateGestureRecognizers];
            [self setNeedsSave];
        }
    }
    NSLog(@"%s %@", __PRETTY_FUNCTION__, self.incident);

    if (!self.incident) {
        [self needsPerformIncidentSelection];
    }
}

- (void) updateGestureRecognizers
{
    //NSLog (@"%s", __PRETTY_FUNCTION__);
    [self.dragAndDropManager gestureRecognizersForView:self.unitMovementView
                                               enabled:self.currentUser.isCoordinator];
    
}

- (void) updateUserIdButton {
    NSLog (@"%s", __PRETTY_FUNCTION__);
    [self.userIdButton setTitle:self.currentUser.userIdString
                       forState:UIControlStateNormal];
}

- (IBAction)unwindToMain:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"%s %@",__PRETTY_FUNCTION__, unwindSegue.identifier);
    self.destinationVC = nil;
    if (!self.context || !self.fireDepartment || self.isResetting) {              // icloud store must have changed
        self.isSegueActive = NO;
        return;
    }
    if ([unwindSegue.identifier isEqualToString:@"unwindFromDone"]) {
        if (!self.isLogOnly) {
            FireUnitSelectionVC *viewController = unwindSegue.sourceViewController;
            [self setSelectedUnitsAsAvailable:viewController];
        }
        [self setNeedsSave];
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromCancel"]){
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromGroupListView"]) {
        GroupListViewController *vc = unwindSegue.sourceViewController;
        NSString *newName = vc.selectedGroupName;
        NSInteger groupIndex = vc.groupIndex;
        [self changeGroupName:newName forGroupIndex:groupIndex];
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromFireUnitList"]) {
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromShowLog"]) {
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromShowLogDone"]) {
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromIncidentSelection"]
            || [unwindSegue.identifier isEqualToString:@"cancelFromIncidentSelection"]) {
        // set up for selected incident
        IncidentSelectionViewController *viewController = unwindSegue.sourceViewController;
        self.incident = viewController.selectedIncident;
 
        if (self.incident) {
            NSLog(@"incident=%@",self.incident.name);
            [self updateTitleButton];
            [self setupIncident:self.incident isNew:viewController.isNewIncident];
            [self setNeedsSave];
            self.isSegueActive = NO;
        }
        else {
            self.isSegueActive = NO;
            [self needsPerformIncidentSelection];
        }
    }
    else if ([unwindSegue.identifier isEqualToString:@"UnwindFromTerminate"]) {
        assert (false);             // this else if is not currently used
        // GroupListViewController *viewController = unwindSegue.sourceViewController;
        [self setNeedsSave];
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"commanderSelectionBackUnwind"]) {
        IcSelectionViewController *vc = unwindSegue.sourceViewController;
        [self setIncidentCommander:vc];
        [self setNeedsSave];
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"cancelFromSettingsView"]) {
        self.isSegueActive = NO;
        if (!self.incident) {
            [self needsPerformIncidentSelection];
        }
    }
    else if ([unwindSegue.identifier isEqualToString:@"doneFromSettingsView"]) {
        self.isSegueActive = NO;
        SettingsViewController *vc = unwindSegue.sourceViewController;
        if (vc.isUpdate) {
            if (vc.user) {
                self.device.user = vc.user;
                self.currentUser.userEntity = vc.user;
                [self updateUserIdButton];
                [self updateTerminateIncidentButton];
            }
            // update or create preferences for the incident
            self.fireDepartment.name = vc.fireDepartmentName;
            [self createOrUpdatePreference:PREF_NAME withValue:vc.fireDepartmentName];
            [self createOrUpdatePreference:PREF_PAR_LIMIT withValue:vc.parLimit.stringValue];
            [self createOrUpdatePreference:PREF_STAGING_TIME withValue:vc.defaultStagingTime.stringValue];
            [self createOrUpdatePreference:PREF_ACTIVE_LIMIT withValue:vc.activeLimit.stringValue];
            [self createOrUpdatePreference:PREF_SCAN_PERIOD withValue:@"14"];
            self.preference = [Preferences createWith:self.incident.preferences];

            // update department values if requested
            if (vc.isUseListSetupAsDefault) {
                [self makeListConfigDefault];
            }
            if (vc.isUseStagingAsDefault) {
                [self makeStagingConfigDefault];
            }
            if (vc.isUseTimesAsDefault) {
                [self makeTimeConfigDefault];
            }
            [self setNeedsSave];
        }
        if (!self.incident) {
            [self needsPerformIncidentSelection];
        }
    }
    else if ([unwindSegue.identifier isEqualToString:@"stagingGroupSelectionBackUnwind"]) {
        StagingLevelViewController *vc = unwindSegue.sourceViewController;
        if (vc.addingStagingGroup) {
            [self.incident addStagingGroupsObject:[StagingGroup from:vc.addingStagingGroup]];
            [self setNeedsSave];
        }
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"unwindFromEditTaskLists"]) {
        self.cachedTaskLists = nil;
        [self.taskTableView reloadData];
        [self setNeedsSave];
        self.isSegueActive = NO;
    }
    else if ([unwindSegue.identifier isEqualToString:@"editStagingGroupUnwind"]) {
        EditStagingGroupsViewController *vc = unwindSegue.sourceViewController;
        if (vc.stagingGroups) {
            [self.incident addStagingGroups:vc.stagingGroups];
            // sort staging groups
            _sortedStagingGroups = nil;
            [self.fireResourcesView reloadData];
            [self setNeedsSave];
        }
        self.isSegueActive = NO;
    }
    else {
        self.isSegueActive = NO;
    }
}

// save current list configuration default for future incidents
- (void)makeListConfigDefault
{
    os_activity_label_useraction("makeListConfigDefault");
    NSLog (@"%s", __PRETTY_FUNCTION__);
    // get lists for this incident
    NSArray *taskLists = self.incident.sortedTaskLists;  // array of SortedTaskList
    IncidentTemplate *template = self.incidentTemplate;

    // clear existing list setup from template
    NSSet *currentTaskLists = [template taskLists];
    if (currentTaskLists && currentTaskLists.count > 0) {
        [template removeTaskLists:currentTaskLists];
        for (IncidentTaskList *listToDelete in currentTaskLists) {
            [self.context deleteObject:listToDelete];
        }
    }

    // copy new setup into the default template settings
    for (SortedTaskList *taskList in taskLists) {
        IncidentTaskList *currentIncidentTaskList = (IncidentTaskList *)taskList.managedListObject;
        DepartmentTaskList *departmentTaskList = currentIncidentTaskList.fromDepartmentList;
        NSUInteger sortOrder = [currentIncidentTaskList.sortOrderValue unsignedIntegerValue];
        [template addTaskListToTemplate:departmentTaskList sortValue:sortOrder];
     }
    [self setNeedsSave];
}

// save current Staging configuration default for future incidents
- (void)makeStagingConfigDefault
{
    os_activity_label_useraction("makeStagingConfigDefault");
    NSLog (@"%s", __PRETTY_FUNCTION__);
    IncidentTemplate *template = self.incidentTemplate;

    // get stages for this incident
    NSArray *stages = self.incident.sortedStagingLevels;

    // clear template of existing staging group setup
    NSSet *currentStagingGroupDefaults = template.stagingGroups;
    if (currentStagingGroupDefaults && currentStagingGroupDefaults.count > 0) {
        [template removeStagingGroups:currentStagingGroupDefaults];
        for (StagingGroup *stagingGroupToDelete in currentStagingGroupDefaults) {
            [self.context deleteObject:stagingGroupToDelete];
        }
    }

    // copy them into the default settings
    for (StagingGroup *stagingGroup in stages) {
        [self addToPredefined:stagingGroup];
        [template addStagingGroupToTemplate:stagingGroup];
    }
    [self setNeedsSave];
}

// adds a staging group to the list of predefined groups if its not yet in the predefined set
- (void)addToPredefined:(StagingGroup *)group
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    BOOL isPredefined = NO;

    // get available predefined stages
    NSSet *defaultStages = self.fireDepartment.predefinedStagingGroups;
    for (DefaultStage *defaultStage in defaultStages) {
        if ([defaultStage.name isEqualToString:group.name]) {
            defaultStage.timeRequired = group.timeRequired;
            isPredefined = YES;
            break;
        }
    }

    // if not predefined, add to the predefined groups
    if (!isPredefined) {
        [self.fireDepartment makeDefaultStage:group.name timeRequired:group.timeRequired.integerValue];
    }
}

// save current Time configuration default for future incidents
- (void)makeTimeConfigDefault
{
    os_activity_label_useraction("makeTimeConfigDefault");
    NSLog (@"%s", __PRETTY_FUNCTION__);
    // update department preferences with incident preferences
    [self.fireDepartment addPreferences:[Preference updatePreferences:self.fireDepartment.preferences
                                                                 with:self.incident.preferences]];
    [self setNeedsSave];
}

// on return from IcSelectionViewController set Incident Commander
- (void)setIncidentCommander:(IcSelectionViewController *)vc
{
    os_activity_label_useraction("setIncidentCommander");
    NSLog (@"%s", __PRETTY_FUNCTION__);
    if (vc.selectedFireUnit) {
        // set new commander if selected unit is different
        if (![self.incident.commandUnit.prefixedName isEqualToString:vc.selectedFireUnit.prefixedName]){
            FireUnit *previousCommandUnit = self.incident.commandUnit;
            [self.incident.log action:@"removed as Incident Command" unit:previousCommandUnit];
            self.incident.commandUnit = vc.selectedFireUnit;
            [self updateCellForFireUnit:previousCommandUnit];

            NSAttributedString *attributedString;
            attributedString = [NSAttributedString attributedStringFrom:self.incidentInfo.icString
                                                        foregroundColor:self.titleColor
                                                        backgroundColor:self.titleBgColor
                                                                   size:self.titleSize];

            [self.icButton setAttributedTitle:attributedString forState:UIControlStateNormal];
            self.icButton.highlighted = NO;
            [self.incident.log action:@"Set as Incident Commander" unit:vc.selectedFireUnit];
            if (vc.selectedFireUnit.coordinatorFor) {
                ListItem *group = vc.selectedFireUnit.coordinatorFor;
                [self.incident.log action:[NSString stringWithFormat:@"Remove as coordinator for %@",group.name]
                                     unit:vc.selectedFireUnit task:group];
                vc.selectedFireUnit.coordinatorFor = nil;
                [self updateGroupView:group coordinator:nil];

                // [self.managementView reloadData];
            }
            [self updateCellForFireUnit:vc.selectedFireUnit];
            [self.incident.log action:@"set as Incident Command" unit:vc.selectedFireUnit];
        }
    }
    [self setNeedsSave];
}

- (void) updateCellForFireUnit:(FireUnit *) fireUnit
{
    NSLog (@"%s %@", __PRETTY_FUNCTION__, fireUnit.name);
    if (fireUnit.stagingGroup) {
        // update in stagingGroup
        [self updateStagingGroupFireUnitCell:fireUnit];
    }
    else if (fireUnit.assignedTo) {
        // update cell in active division
        [self updateActiveFireUnitCell:fireUnit];
    }
}

- (void)updateActiveFireUnitCell:(FireUnit *)fireUnit
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSUInteger activeGroupIndex = 0;
    for (ListItem *activeGroup in self.sortedManagedTasks) {
            if ([activeGroup isEqual:fireUnit.assignedTo]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:activeGroupIndex inSection:0];
                ManagementViewCell *groupViewCell = (ManagementViewCell *) [self.managementView cellForItemAtIndexPath:indexPath];
                ActiveDivisionCollectionView *divView = groupViewCell.activeDivisionView;
                NSArray *sortedUnits = [self sort:activeGroup.assignedUnits];
                NSUInteger index = 0;
                for (FireUnit *activeUnit in sortedUnits) {
                    if ([activeUnit.prefixedName isEqualToString:fireUnit.prefixedName]) {
                        [self updateCellForUnit:fireUnit collectionView:divView section:0 index:index];
                        break;
                    }
                    index++;
                }
                break;
            }
            activeGroupIndex++;
        }
}

- (void)updateStagingGroupFireUnitCell:(FireUnit *)fireUnit
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSUInteger section = 0;
    for (StagingGroup *stagingGroup in self.sortedStagingGroups) {
            if ([stagingGroup isEqual:fireUnit.stagingGroup]) {
                // find cell index
                NSArray *stagingGroupUnits = [self sort:stagingGroup.units withKey:@"prefixedName"];
                NSUInteger index = 0;
                for (FireUnit *stagingGroupUnit in stagingGroupUnits) {
                    if ([stagingGroupUnit.prefixedName isEqualToString:fireUnit.prefixedName]) {
                        [self updateCellForUnit:fireUnit collectionView:self.fireResourcesView section:section index:index];
                        break;
                    }
                    index++;
                }
                break;
            }
            section++;
        }
}

- (void)updateGroupView:(ListItem *)group coordinator:(FireUnit *)coordinatorUnit
 {
     NSLog (@"%s", __PRETTY_FUNCTION__);
     // get index path of the managed group cell
     NSIndexPath *groupIndexPath= [self getGroupIndexPath:group.currentValue];

     // get management view cell
     if (groupIndexPath) {
         ManagementViewCell *cell = (ManagementViewCell *)[self.managementView cellForItemAtIndexPath:groupIndexPath];
         [self setGroupCoordinator:cell.coordinatingUnitButton Unit:coordinatorUnit];
         [self updateCellForFireUnit:coordinatorUnit];
     }
 }

 - (Preference *)createOrUpdatePreference:(NSString *)name withValue:(NSString *)value
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    Preference *preference;
    
    // if preference is for my department name get it directly
    if ([name isEqualToString:PREF_NAME]) {
        preference = [self getPreference:name];
    }
    else {
        // find and update preference in incident
        preference = [self getPreference:name preferences:self.incident.preferences];
    }
    
    // if not in incident create it using department preference
    if (!preference) {
        Preference *deptPreference = [self getPreference:name preferences:self.fireDepartment.preferences];
        if (!deptPreference) {
            // create department preference
            deptPreference = [Preference createWithName:name withValue:value withContext:self.context];
            deptPreference.forFireDepartment = self.fireDepartment;
        }
        if (deptPreference) {
            // create incident preference
            preference = [Preference createWithName:deptPreference.name
                                          withValue:deptPreference.value withContext:self.context];
            preference.forIncident = self.incident;
        }
    }
    
    assert (preference);
    
    preference.value = value;
    return preference;
}

- (Preference *)getPreference:(NSString *)name preferences:(NSSet *)preferences
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    Preference *preference = nil;
    for (Preference *p in preferences){
        if ([p.name isEqualToString:name]){
            preference = p;
            break;
        }
    }
    return preference;
}

- (Preference *)getPreference:(NSString *)name
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Preference"];

    // Set predicate to fetch preference with the specified name
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    [request setPredicate:predicate];

    NSManagedObjectContext *moc = self.context;
    NSError *error = nil;

    NSArray *fetchedArray = [moc executeFetchRequest:request error:&error];

    if (fetchedArray == nil || fetchedArray.count == 0)
    {
        return nil;
    }

    Preference *preference = fetchedArray[0];
    return preference;
}

- (NSAttributedString *)getAttributedString:(NSString *)string
                            foregroundColor:(UIColor *)fgColor
                            backgroundColor:(UIColor *)bgColor
                                       size:(CGFloat)size
{
    NSMutableAttributedString *attributedTitle=[[NSMutableAttributedString alloc] initWithString:string];
    NSUInteger length= attributedTitle.length;

    UIFont *font=[UIFont fontWithName:@"Helvetica-Bold" size:size];

    [attributedTitle addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, length)];
    [attributedTitle addAttribute:NSForegroundColorAttributeName value:fgColor range:NSMakeRange(0, length)];
    [attributedTitle addAttribute:NSBackgroundColorAttributeName value:bgColor range:NSMakeRange(0, length)];
    return attributedTitle.copy;
}

// Create active unit by cloning the selected department unit
- (void)createActiveUnit:(FireUnit *)selectedFireUnit
     atStagingGroupIndex:(NSInteger) groupIndex
{
    NSLog (@"%s", __PRETTY_FUNCTION__);

    FireUnit *fireUnit = selectedFireUnit.clone;
    [self.incident addAssignedUnitsObject:fireUnit];             // all units belonging to the incident
    [self.incident addActiveUnitsObject:fireUnit];
    fireUnit.activeStartTime = nil;
    fireUnit.forIncident = self.incident;
    StagingGroup *stagingGroup = self.sortedStagingGroups[(NSUInteger) groupIndex];
    [stagingGroup addUnitsObject:fireUnit];
    NSString *action = [NSString stringWithFormat:@"fire unit added to staging group:%@",stagingGroup.name];
    [self.incident.log action:action unit:fireUnit task:stagingGroup];
}


#pragma mark - collection view data source methods

//
// UICollectViewDataSource protocol implementation
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSLog (@"%s %@", __PRETTY_FUNCTION__,[[collectionView class] description]);
    NSInteger sectionCount = 1;
    if (self.isLogOnly) {
        sectionCount = 0;
    }
    if ([collectionView isKindOfClass:[FireResourcesCollectionView class]]) {
        sectionCount = self.incident.stagingGroups.count;
    }
    NSLog (@"number of sections=%ld",(long) sectionCount);
    return sectionCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog (@"%s %@", __PRETTY_FUNCTION__, collectionView.description);
    NSInteger count = 0;
    if ([collectionView isKindOfClass:[FireResourcesView class]]) {
        if ([collectionView isKindOfClass:[FireResourcesCollectionView class]]) {
            StagingGroup *stagingGroup = self.sortedStagingGroups[(NSUInteger) section];
            count = stagingGroup.units.count;
        }
        else {
            assert([collectionView isKindOfClass:[ActiveDivisionCollectionView class]]);
            ActiveDivisionCollectionView *divisionView = (ActiveDivisionCollectionView *)collectionView;
            count = [self countOfUnitsFor:divisionView.taskId];
            // NSLog(@"numberOfItems for view %@ = %i",divisionView.taskId,count);
        }
    }
    else if ([collectionView isKindOfClass:[ManagementCollectionView class]]) {
        count = self.incident.managedTasks.count;
    }
    else {
        NSLog(@"%@",@"unsupported collection view");
        assert(false);
    }
    NSLog (@"number of items=%ld",(long) count);
    return count;
}


// Get count of units assigned to a task
//
- (NSInteger)countOfUnitsFor:(NSString *)taskName
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    NSSet *assignedUnits = [self assignedUnitsFor:taskName];
    if (assignedUnits) {
        return [self assignedUnitsFor:taskName].count;
    }
    else {
        return 0;
    }
}

// Get assigned units for a task
//
-(NSSet *) assignedUnitsFor:(NSString *)taskName
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    ListItem *task = [self getTaskFor:taskName];
    if (task) {
        return task.assignedUnits;
    }
    else {
        return nil;
    }
}

// get managed task or group using the specified value
-(ListItem *) getTaskFor:(NSString *)value
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
   for (ListItem *task in self.incident.managedTasks) {
        if ([task.currentValue isEqualToString:value]) {
            return task;
        }
    }
    //assert([@"is a managedTask =" stringByAppendingString:value] == nil);
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog (@"%s", __PRETTY_FUNCTION__);
    if ([collectionView isKindOfClass:[FireResourcesView class]]) {
        return [self getCellFor:collectionView indexPath:indexPath];
    }
    else if ([collectionView isKindOfClass:[ManagementCollectionView class]]) {
        return [self getManagementCell:collectionView indexPath:indexPath];
    }
    else {
        NSLog(@"%@",@"unsupported collection view");
        assert(false);
    }
    return nil;
}


 - (UICollectionViewCell *)getManagementCell:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
     UICollectionViewCell *viewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ManagementViewCell" forIndexPath:indexPath];
     ManagementViewCell *managementCell = (ManagementViewCell *)viewCell;
     NSLog (@"%s", __PRETTY_FUNCTION__);

     // get active task item for the index
     NSInteger index = indexPath.item;
     if (index >= self.sortedManagedTasks.count) {
         _sortedManagedTasks = nil;              // force a re-read and re-sort
     }
     NSArray *tasks = self.sortedManagedTasks;
     ListItem *activeTask = tasks[(NSUInteger) index];

     // setup up the task management cell
     [managementCell.buttonLabel setTitle:activeTask.name forState:UIControlStateNormal];
     managementCell.buttonLabel.tag = index;
     managementCell.coordinatingUnitButton.tag = index;
     managementCell.taskCurrentValue = activeTask.currentValue;

     // set up coordinating unit field
     [self setGroupCoordinator:managementCell.coordinatingUnitButton Unit:activeTask.coordinatingUnit];

     // setup active division/task view in management cell
 //    ActiveDivisionCollectionView *divView = [self makeActiveDivisionView];
 //    managementCell.activeDivisionView = divView;
     ActiveDivisionCollectionView *divView = managementCell.activeDivisionView;
     divView.dataSource = self;
     divView.delegate = self;
     divView.dragDelegate = self;
     divView.taskId = activeTask.currentValue;
     divView.accessibilityIdentifier = [NSString stringWithFormat:@"divView-%@",activeTask.currentValue];
     divView.tag = index;
     [self updateActiveDivisionCache:divView];
     [self reloadActiveDivisionView:divView.taskId];
     // NSLog(@"build management cell for %@ and taskId %@", activeTask.name, activeTask.currentValue);
     [self.dragAndDropManager addDragArea:divView];
     [(id <DragDropAcceptor>) divView addTypeToAccept:[FireResourceViewCell getDragIdentifier]];
     return managementCell;
 }

 - (void)setGroupCoordinator:(UIButton *)coordinatorButton Unit:(FireUnit *)unit
 {
     os_activity_label_useraction("setGroupCoordinator");
     NSLog (@"%s", __PRETTY_FUNCTION__);
     if (unit) {
         [coordinatorButton setTitle:unit.prefixedName forState:UIControlStateNormal];
         [coordinatorButton setTitleColor: self.colorForCoordinatorIsSet forState:UIControlStateNormal];
         [coordinatorButton setBackgroundColor:[UIColor whiteColor]];
     }
     else {
         [coordinatorButton setTitle:@"? ? ?" forState:UIControlStateNormal];
         [coordinatorButton setTitleColor: self.colorForNoCoordinator forState:UIControlStateNormal];
         [coordinatorButton setBackgroundColor:[UIColor ersLightGoldColor]];
     }
 }

 - (void)updateActiveDivisionCache:(ActiveDivisionCollectionView *)divView
 {
     (self.divViewDictionary)[divView.taskId] = divView;
 }

 - (void) reloadActiveDivisionView:(NSString *)taskId
 {
     ActiveDivisionCollectionView *divView = (self.divViewDictionary)[taskId];
     divView.hidden = YES;
     [divView reloadData];
     divView.hidden = NO;
 }

 - (void) reloadAllActiveDivisionViews
 {
     for (ActiveDivisionCollectionView *divView in self.divViewDictionary.allValues) {
         divView.hidden = YES;
         [divView reloadData];
         divView.hidden = NO;
     }
 }


 - (UICollectionViewCell *)getCellFor:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    FireResourcesView *viewForCell = (FireResourcesView *) collectionView;
     FireResourceViewCell *fireResCell;
    if ([collectionView isKindOfClass:[FireResourcesCollectionView class]]) {
        fireResCell = (FireResourceViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"FireResource" forIndexPath:indexPath];
        fireResCell.activeUnit = NO;
        [fireResCell.parStatusButton addTarget:self action:@selector(availUnitParIsOkButton:) forControlEvents:UIControlEventTouchUpInside];
    }
     else {
        fireResCell = (FireResourceViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"FireResource" forIndexPath:indexPath];
        fireResCell.activeUnit = YES;
        [fireResCell.parStatusButton addTarget:self action:@selector(parIsOkButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    assert(fireResCell);
    if (fireResCell) {
        if (fireResCell.parStatusButton) {
            [fireResCell parStatusButtonEnabled:NO];
        }
        fireResCell.owningView = nil;
        NSSet *units;
        NSInteger section = indexPath.section;
        NSInteger index = indexPath.item;
        NSString *blank = @"";
        NSArray *sortedUnits;

        // determine whether to use a task assignment list or a staging group to get units
        if ([collectionView isKindOfClass:[FireResourcesCollectionView class]]) {
            StagingGroup *stagingGroup = self.sortedStagingGroups[(NSUInteger) section];
            units = stagingGroup.units;
            blank = @"";
            sortedUnits = [self sort:units withKey:@"prefixedName"];
        }
        else {
            units = [self assignedUnitsFor:viewForCell.taskId];
            sortedUnits = [self sort:units];
        }

        // get unit for the specified index
        FireUnit *unit = sortedUnits[(NSUInteger) index];
        assert(unit);

        // determine the unit label with age and color
        NSString *unitLabel = [NSString stringWithFormat:@"%@%@", blank, unit.prefixedName];
        FireCellColor *statusColors = [self getCellColor:unit];
        fireResCell = [self configureCell:fireResCell unit:unit unitLabel:unitLabel cellColors:statusColors];
        fireResCell.owningView = viewForCell;
        if (fireResCell.parStatusButton) {
            [fireResCell.parStatusButton setTag:index];
            [fireResCell.parStatusButton setTaskId:viewForCell.taskId];
        }
        // NSLog(@"build cell for %@ in view %@ section %i", unit.prefixedName, viewForCell.taskId, section);
    }
    return fireResCell;
}


// getting header for section in staging groups
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view = nil;

    if ([collectionView isKindOfClass:[FireResourcesCollectionView class]]) {
        if (kind == UICollectionElementKindSectionHeader) {
            NSUInteger section = (NSUInteger) indexPath.section;
            StagingGroup *stagingGroup = self.sortedStagingGroups[section];
            StagingHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"stagingHeader" forIndexPath:indexPath];
            NSString *headerText = [NSString stringWithFormat:@"%@",stagingGroup.name];
            if (stagingGroup.coordinatingUnit) {
                headerText = [NSString stringWithFormat:@"%@ - %@",stagingGroup.name, stagingGroup.coordinatingUnit.prefixedName];
            }
            [headerView.StageNameButton setTitle:headerText forState:UIControlStateNormal];
            headerView.StageNameButton.tag = indexPath.section;
            headerView.tag = indexPath.section;
            view = headerView;
        }
    }
    return view;
}

- (FireResourceViewCell *)configureCell:(FireResourceViewCell *)viewCell
                                   unit:(FireUnit *)unit unitLabel:(NSString *)unitLabel cellColors:(FireCellColor *)cellColors
{
    // NSString *ageLabel = [NSString stringWithFormat:@"\n(%2i)", age];
    viewCell.contentView.backgroundColor = cellColors.bgColor;
    viewCell.cellColors = cellColors;
    viewCell.cellTitle = unitLabel;
    viewCell.hidden = NO;
    if (unit.parStatusOk.boolValue) {
        [viewCell parStatusButtonEnabled:NO];
    }

    // os_trace("configureCell for FireResourceViewCell");

    if (!unit.parStatusOk.boolValue) {
        [self enableParOkButton:viewCell];
    }
    [viewCell.parStatusButton setTag:viewCell.tag];

    // set coordinator flag
    [self updateCoordinatorForCell:viewCell unit:unit];

    return viewCell;
}

 - (void)updateCoordinatorForCell:(FireResourceViewCell *)viewCell unit:(FireUnit *)unit {
     if (unit.commanderFor) {
         // incident commander
         viewCell.coordinatorLabel.text = @"IC";
         [viewCell coordinatorLabelEnabled:YES];
     }
     else if (unit.coordinatorFor
             && [unit isGroupMember:unit.coordinatorFor]) {
             viewCell.coordinatorLabel.text = @"S";
             [viewCell coordinatorLabelEnabled:YES];
     }
     else {
         [viewCell coordinatorLabelEnabled:NO];
     }
 }

 - (CGFloat)agedColor:(CGFloat)ratio startColor:(CGFloat)startColor endColor:(CGFloat)endColor
{
    CGFloat agedColor;
    CGFloat colorChange;
    if (endColor == startColor) {
        agedColor = endColor;
    }
    else if (endColor > startColor) {
        colorChange = endColor - startColor;
        agedColor = startColor + colorChange * ratio;
    }
    else {
        colorChange = startColor - endColor;
        agedColor = startColor - colorChange * ratio;
    }
    return agedColor;
}

- (FireCellColor *)getCellColor:(FireUnit *)unit
{
    UIColor *whiteText = [UIColor whiteColor];
    UIColor *blackText = [UIColor ersBlackColor];
    UIColor *unitReadyColor = [UIColor ersGreenColor];
    UIColor *cellBackgroundColor = unitReadyColor;
    UIColor *cellTextColor = whiteText;
    NSInteger age = unit.activeAge;

    if (unit.assignedTo) {
        NSInteger redAge = [self.preference integerValueFor:PREF_ACTIVE_LIMIT];
        NSInteger yellowAge = (redAge - (redAge/4)) - 1;
        NSInteger pinkAge = (redAge - (redAge/8)) - 1;

        if (age > yellowAge) {
            // unitLabel = [unitLabel stringByAppendingString:ageLabel];
            cellBackgroundColor = [UIColor ersLightGoldColor];
            cellTextColor = blackText;
        }
        if (age > pinkAge) {
            cellBackgroundColor = [UIColor ersPinkColor];
            cellTextColor = blackText;
        }
        if (age > redAge) {
            cellBackgroundColor = [UIColor ersRedColor];
            cellTextColor = whiteText;
        }
    }
    else if (unit.stagingGroup) {
        cellTextColor = whiteText;
        StagingGroup *stagingGroup = unit.stagingGroup;
        UIColor *startingColor = [UIColor ersPinkColor];
        UIColor *endingColor = unitReadyColor;

        // build label to show age
                    // unitLabel = [unitLabel stringByAppendingString:ageLabel];
        // compute the color
        if (stagingGroup.timeRequired.integerValue <= 0 || age >= stagingGroup.timeRequired.integerValue) {
            cellBackgroundColor = unitReadyColor;
        }
        else if (age == 0) {
            cellBackgroundColor = startingColor;
        }
        else {
            CGFloat startBlue;
            CGFloat startGreen;
            CGFloat startRed;
            CGFloat startAlpha;
            CGFloat endBlue;
            CGFloat endGreen;
            CGFloat endRed;
            CGFloat endAlpha;
            [startingColor getRed:&startRed green:&startGreen blue:&startBlue alpha:&startAlpha];
            [endingColor getRed:&endRed green:&endGreen blue:&endBlue alpha:&endAlpha];
            CGFloat fAge = age;
            CGFloat fRqd = stagingGroup.timeRequired.integerValue;
            CGFloat ratio = fAge / fRqd;
            CGFloat blue = [self agedColor:ratio startColor:startBlue endColor:endBlue];
            CGFloat green = [self agedColor:ratio startColor:startGreen endColor:endGreen];
            CGFloat red = [self agedColor:ratio startColor:startRed endColor:endRed];
            CGFloat alpha = [self agedColor:ratio startColor:startAlpha endColor:endAlpha];
            cellBackgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        }
    }
    return [FireCellColor bgColor:cellBackgroundColor textColor:cellTextColor];
}


#pragma mark - task table view data source

 - (NSArray *)cachedTaskLists
 {
     if (!_cachedTaskLists) {
         _cachedTaskLists = self.incident.sortedTaskLists;
     }
     return _cachedTaskLists;
 }

 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
    // Return the number of sections.
     NSInteger numberOfSections = self.cachedTaskLists.count;
     if (numberOfSections == 0)
         numberOfSections = 1;
     return numberOfSections;
 }

 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
     NSInteger numberOfRows = 0;
     SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];

     if (sortedList && sortedList.expand){
         // Return the number of rows in the section + header row
         numberOfRows = sortedList.list.count + 1;
     }
     else {
         numberOfRows = 1;
     }

     return numberOfRows;
 }

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     UITableViewCell *cell = nil;
     NSString *cellIdentifier = @"TaskViewCell";
     NSInteger section = indexPath.section;
     NSInteger row = indexPath.row;

     if (row == 0) {
         cellIdentifier = @"TaskViewHeaderCell";
     }

     if (self.cachedTaskLists.count == 0) {
         if (section == 0) {
             cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
             cell.textLabel.text = @"Empty List";
         }
     }
     else {
         SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];
         cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
         cell.accessoryType = UITableViewCellAccessoryNone;

         if (row == 0) {
             // get header
             NSString *format = @"%@"; // list is not expanded format
             if (sortedList.expand) {
                 format = @"%@";   // list is expanded format
             }
             cell.textLabel.text = [NSString stringWithFormat:format, sortedList.name];
             cell.backgroundColor = self.HeaderBackgroundColor;
             cell.textLabel.backgroundColor = self.HeaderBackgroundColor;
             if (sortedList.isCompleted) {
                 cell.accessoryType = cell.accessoryType = UITableViewCellAccessoryCheckmark;
             }
         }
         else {
             NSInteger listIndex = row - 1;
             if (sortedList.list.count)  {
                 // set text
                 ListItem *listItem = sortedList.list[(NSUInteger) listIndex];
                 cell.textLabel.text = listItem.name;
                 // set check mark state
                 if (listItem.isChecked)   {
                     cell.accessoryType = cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 }
             }
         }
     }
     cell.textLabel.font = [UIFont fontWithName:@".HelveticaNeueUI-Bold" size:12];
     cell.tag = section * 100 + row;
     return cell;
 }

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    //BOOL isEditable = YES;

    // no deleting or moving in this view.  Only allow editing from the EditListOfLists view controller
    BOOL isEditable = NO;

    if (indexPath.row == 0
            && self.taskEditStyle == UITableViewCellEditingStyleInsert) {
        isEditable = YES; // allow insert of a task item from the header insert button
    }

    // get number of rows to display
    SortedTaskList *sortedTaskList = self.cachedTaskLists[(NSUInteger)indexPath.section];

    // last row can not be deleted or moved
    if (indexPath.row == sortedTaskList.maxRow && self.taskEditStyle == UITableViewCellEditingStyleDelete) {
        isEditable = NO;
    }
    return isEditable;
}


// Support editing the table view.  (not currently supported from main view)
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (editingStyle == UITableViewCellEditingStyleInsert) {
//        NSDecimalNumber *one = [NSDecimalNumber one];
//        SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger)indexPath.section];
//
//        if (indexPath.row == 0) {
//
//            // insert item in list
//
//            NSDecimalNumber *newSortOrderValue = nil;
//            NSUInteger itemIndex = 0;
//
//            if (indexPath.row == sortedList.maxRow) {
//                newSortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:(NSUInteger)sortedList.maxRow exponent:1 isNegative:NO];
//            }
//            else {
//                itemIndex = (NSUInteger) sortedList.maxRow;
//                ListItem *listItem = sortedList.list[itemIndex];
//                newSortOrderValue = [listItem.sortOrderValue decimalNumberBySubtracting:one];
//            }
//
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//            ListItem *newListItem = [ListItem createItemNamed:@"new list item"
//                                                    withValue:itemIndex
//                                                  withContext:sortedList.managedListObject.managedObjectContext];
//            newListItem.sortOrderValue = newSortOrderValue;
//
//            // add new item to sorted list of items
//            NSMutableArray *newList = sortedList.list.mutableCopy;
//            if (indexPath.row == sortedList.maxRow) {
//                [newList addObject:newListItem];
//            }
//            else {
//                [newList insertObject:newListItem atIndex:itemIndex];
//            }
//
//            sortedList.list = newList.copy;
//
//            // add new item to data model list
//            List *list = sortedList.managedListObject;
//            [list addItemsObject:newListItem];
//
//            // add table view cell
//            NSArray *indexPaths = @[indexPath];
//            [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
//        }
//
//        // do segue to get the description
//        self.activeCell = [tableView cellForRowAtIndexPath:indexPath];
//        self.activeIndexPath = indexPath;
//        [self performSegueWithIdentifier:@"ListDescriptionSegue" sender:tableView];
//
//    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Task Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentUser.isCoordinator) {
        return indexPath;
    }
    else {
        [self coordinatorRequiredAlert];
        return nil;
    }
}


 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     if (self.currentUser.isCoordinator) {
         NSUInteger section = (NSUInteger) indexPath.section;
         NSInteger row = indexPath.row;
         SortedTaskList *sortedTaskList = self.cachedTaskLists[section];
         List *taskList = sortedTaskList.managedListObject;
         
         if (row == 0) {
             // header selected -- toggle the list expansion state
             BOOL expanded = sortedTaskList.expand;
             sortedTaskList.expand = !expanded;
             taskList.expanded = @(!expanded);
         }
         else {
             if (!self.isLogOnly) {
                 // row selected toggle the list item check mark
                 BOOL listIsComplete = taskList.isCompleted;
                 NSUInteger itemIndex = (NSUInteger) row - 1;
                 ListItem *listItem = sortedTaskList.list[itemIndex];
                 listItem.isChecked = !listItem.isChecked;
                 [self.incident.log task:listItem isChecked:listItem.isChecked];
                 if (listIsComplete != taskList.isCompleted) {
                     [self.incident.log list:taskList isCompleted:taskList.isCompleted];
                 }
             }
         }
         NSIndexSet *sections = [NSIndexSet indexSetWithIndex:(NSUInteger)section];
         [self.taskTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
         [self setNeedsSave];
     }
     else {
         // restricted to incident coordinator
     }
 }



// Allows customization of the editingStyle for a particular cell located at 'indexPath'. If not implemented, all editable cells will have UITableViewCellEditingStyleDelete set for them when the table has editing property set to YES.
 - (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     return self.taskEditStyle;
 }

 - (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     return @"Delete ";
 }

// Controls whether the background is indented while editing.  If not implemented, the default is YES.  This is unrelated to the indentation level below.  This method only applies to grouped style table views.
// - (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath;

// The willBegin/didEnd methods are called whenever the 'editing' property is automatically changed by the table (allowing insert/delete/move). This is done by a swipe activating a single row
//  - (void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath;
// - (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath;

// Moving/reordering

// Allows customization of the target row for a particular row as it is being moved/reordered
 - (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
 {
     NSInteger sourceRow = sourceIndexPath.row;
     NSInteger targetSection = proposedDestinationIndexPath.section;
     NSInteger targetRow = proposedDestinationIndexPath.row;

     if (sourceRow == 0) {
        targetRow = 0;
     } else if (targetRow == 0) {
        targetRow++;
     } else {
         SortedTaskList *targetList = self.cachedTaskLists[(NSUInteger)targetSection];
         if (targetRow > targetList.maxRow) {
            targetRow = targetList.maxRow;
         }
     }
     return [NSIndexPath indexPathForRow:targetRow inSection:targetSection];
 }

//- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
//{
//    SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger)indexPath.section];
//    if (indexPath.row != 0 && indexPath.row != sortedList.maxRow) {
//        NSUInteger itemIndex = (NSUInteger)indexPath.row - 1;
//        ListItem *listItem = sortedList.list[itemIndex];
//        listItem.isChecked = !listItem.isChecked;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 35;
    SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger)indexPath.section];
    if (indexPath.row == sortedList.maxRow) {
        rowHeight = 11;
    }
    return rowHeight;
}

#pragma mark - drag and drop

// sort supplied set into an ordered array using a default sort key of "sortOrderValue"
- (NSArray *)sort:(NSSet *)set
{
    return [self sort:set withKey:@"sortOrderValue"];
}

// sort supplied set into an ordered array using specified sort key
- (NSArray *) sort: (NSSet *)set withKey:(NSString *)key
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                 ascending:YES
                                                comparator:^(id obj1, id obj2)  {return [obj1 compare:obj2];}];
    NSArray *sortDescriptors = @[sortDescriptor];
    return [set sortedArrayUsingDescriptors:sortDescriptors];
}

- (UIPasteboard *) getPasteboard
{
    UIPasteboard *pasteBoard = [UIPasteboard pasteboardWithName:@"com.boydsconsulting.fireManagement" create:YES];
    return pasteBoard;
}

//
// DragDropDelegate protocol implementation
//

// copyItem
- (void) copyItem:(UIView *) viewBeingDragged
          atPoint:(CGPoint) point
         fromView:(id<DragDropAcceptor>)sourceView
      dragContext:(DragContext *) dragContext
{
    FireResourcesView *view = (FireResourcesView *) sourceView;
    
    // get index information for selected item
    NSIndexPath *itemIndexPath = [view indexPathForItemAtPoint:point];
    NSUInteger index = (NSUInteger)itemIndexPath.item;

    NSArray *units;
    if ([view isKindOfClass:[FireResourcesCollectionView class]]) {
        NSUInteger section = (NSUInteger) itemIndexPath.section;
        StagingGroup *stagingGroup = self.sortedStagingGroups[section];
        dragContext.fromGroup = stagingGroup;
        units = [self sort:stagingGroup.units withKey:@"prefixedName"];
    }
    else {
        dragContext.fromGroup = [self getTaskFor:view.taskId];
        units = [self sort:[self assignedUnitsFor:view.taskId]];
    }

    if (index < units.count ) {
        FireUnit *unit = units[index];
        dragContext.movingObject = unit;
        // NSLog(@"copy index %i with name %@ in Division %@", index, unit.prefixedName, view.taskId);
    }

    // remove from view
    [self removeObject:dragContext.movingObject FromGroup:dragContext.fromGroup];
}

- (void)removeObject:(id) object FromGroup:(id) group
{
    assert ([object isKindOfClass:[FireUnit class]]);
    assert ([group isKindOfClass:[ListItem class]]);
    FireUnit *fireUnit = (FireUnit *) object;
    ListItem *fireGroup = (ListItem *) group;
    if ([fireGroup isKindOfClass:[StagingGroup class]]) {
        StagingGroup *stagingGroup = (StagingGroup *)fireGroup;
        [stagingGroup removeUnitsObject:fireUnit];
    }
    else {
        [fireGroup removeAssignedUnitsObject:fireUnit];
    }
}


- (void) deleteItem:(UIView *) viewBeingDragged
            atPoint:(CGPoint) point
           fromView:(id <DragDropAcceptor>) sourceView
        dragContext:(DragContext *) dragContext
{
    if ([dragContext.fromGroup isKindOfClass:[ListItem class]]) {
        ListItem *group = (ListItem *)dragContext.fromGroup;
        FireUnit *fireUnit = (FireUnit *)dragContext.movingObject;
        if ([group isKindOfClass:[StagingGroup class]]) {
            // finalize removal from staging group
            fireUnit.activeStartTime = [NSDate date];
            if ([fireUnit.prefixedName isEqualToString:group.coordinatingUnit.prefixedName]) {
                fireUnit.coordinatorFor = nil;
                [self.incident.log action:[NSString stringWithFormat:@"%@ removed as supervisor for staging group %@", fireUnit.prefixedName, group.name]
                                     unit:fireUnit
                                     task:group];
            }
        }
        else {
            // finalize removal from a group or division
            ListItem *task = group;
            if ([task.coordinatingUnit.prefixedName isEqualToString:fireUnit.prefixedName]) {
                [self.incident.log action:[NSString stringWithFormat:@"%@ removed as supervisor for group %@", fireUnit.prefixedName, task.name]
                                     unit:fireUnit
                                     task:task];
                task.coordinatingUnit = nil;
                if (task.assignedUnits.count == 1) {
                    task.coordinatingUnit = task.assignedUnits.anyObject;
                    [self.incident.log action:[NSString stringWithFormat:@"%@ set as supervisor for group %@", task.coordinatingUnit.prefixedName, task.name]
                                         unit:task.coordinatingUnit
                                         task:task];
                }
            }

        }
        [self.incident.log action:@"remove from group" unit:fireUnit task:group];

        // NSLog(@"delete unit:%@ from division:%@", fireUnit.prefixedName, group.name);
        [self reloadViewFor:group];
        [self setNeedsSave];
    }
}


 - (void) moveItem: (id <DraggableUIView> )viewBeingDragged
           atPoint:(CGPoint) fromPoint
           toPoint:(CGPoint) toPoint
           forView: (id <DragDropAcceptor>) sourceView
 {
// empty method

 }



- (NSArray *)moveUnit:(FireResourcesView *)view
                index:(NSUInteger)index
      toItemIndexPath:(NSIndexPath *)toItemIndexPath
                units:(NSArray *)units
{
    if (index >= units.count) {
        return units;
    }

    FireUnit *movingUnit = units[index];

    // NSLog(@"copy index %i with name %@ - division %@", index, movingUnit.prefixedName, view.taskId);

    // get index information for item at drop point (if any)
    NSMutableArray *newUnits = [units mutableCopy];

    if (toItemIndexPath) {
        NSUInteger toIndex = (NSUInteger) toItemIndexPath.item;
        [newUnits insertObject:movingUnit atIndex:toIndex];
        // NSLog(@"insert name %@ at index %i - division %@", movingUnit.prefixedName, toIndex, view.taskId);
        if (toIndex < index) {
            // item inserted before deletion location
            index++;
        }
    }
    else {
        [newUnits addObject:movingUnit];
    }

    // remove item from pool of available units
    //
    [newUnits removeObjectAtIndex:index];
    // NSLog(@"delete index %i for division %@", index, view.taskId);
    [self setSortValuesFor:newUnits];
    [self setNeedsSave];
    return newUnits;
}


 - (void)setSortValuesFor:(NSMutableArray *)array
 {
    NSDecimalNumber *num = [NSDecimalNumber decimalNumberWithString:@"1.0"];
    NSDecimalNumber *unitNumber = num;
    for (FireUnit *fireUnit in array) {
        fireUnit.sortOrderValue = unitNumber;
        unitNumber = [unitNumber decimalNumberByAdding:num];
    }
 }

 - (void) cancelMove:(DragContext *) dragContext
 {
     ListItem *group = dragContext.fromGroup;
     FireUnit *fireUnit = dragContext.movingObject;

     if ([group isKindOfClass:[StagingGroup class]]){
         StagingGroup *stagingGroup = (StagingGroup *) group;
         [stagingGroup addUnitsObject:fireUnit];
     }
     else {
         [group addAssignedUnitsObject:fireUnit];
     }
     [self reloadViewFor:group];
 }


 - (void) pasteItem: (id <DraggableUIView>)viewBeingDragged
           atPoint:(CGPoint) point
            toView: (id <DragDropAcceptor>) targetView
        dragContext:(DragContext *) dragContext
{
    // get fireUnit
    FireUnit *fireUnit;
    fireUnit = (FireUnit *) dragContext.movingObject;
    assert(fireUnit != nil);

    if ([targetView isKindOfClass:[FireResourcesView class]]) {
        FireResourcesView *toView = (FireResourcesView *) targetView;

        NSIndexPath *itemIndexPath = [toView indexPathForItemAtPoint:point];

        if ([toView isKindOfClass:[FireResourcesCollectionView class]]) {
            // move unit to a staging group
            if (!itemIndexPath) {
                itemIndexPath = [self determineSectionAtPoint:point inCollectionView:toView];
            }
            [self addFireUnit:fireUnit toStagingGroupView:toView itemIndexPath:itemIndexPath];
        }
        else {
            // move unit to its new task
            ListItem *task = [self getTaskFor:toView.taskId];
            NSArray *units = [self sort:task.assignedUnits];

            units = [self movefireUnit:fireUnit atIndexPath:itemIndexPath units:units toView:toView ];
            [self.incident.log action:[NSString stringWithFormat:@"%@ assigned to group %@", fireUnit.prefixedName, task.name]
                                 unit:fireUnit
                                 task:task];


            // start the active age for the fire unit
            if (!fireUnit.activeStartTime) {
                fireUnit.activeStartTime = NSDate.date;
            }
            task.assignedUnits = [NSSet setWithArray:units];
            // ???? [self reloadDataForManagementCellAtIndex:toView.tag];
        }
    }
    else if ([targetView isKindOfClass:[FireStationView class]]) {
        FireStationView *stationView = (FireStationView *)targetView;
        stationView.hidden = YES;

        NSString *logMsg = [NSString stringWithFormat:@"Released Unit %@ from incident",fireUnit.prefixedName];
        [self.incident.log action:logMsg unit:fireUnit];
        
        if (fireUnit.coordinatorFor) {
            ListItem *group = fireUnit.coordinatorFor;
            fireUnit.coordinatorFor = nil;
            [self reloadViewFor:group];
            [self.incident.log action:@"Unit removed as group coordinator" unit:fireUnit task:group];
        }
        if (fireUnit.commanderFor) {
            [self.incident.log action:@"Unit removed as Incident Commander" unit:fireUnit];
        }
        fireUnit.assignedTo = nil;
        fireUnit.activeForIncident = nil;
        fireUnit.forIncident = nil;
        fireUnit.availableInIncident = nil;

        // remove fire unit from the incident
        [self.context deleteObject:fireUnit];

        stationView.hidden = NO;
    }
    [self setNeedsSave];

    // NSLog(@"pasteItem ended");
}

- (void)reloadViewFor:(ListItem *) group
{
    if ([group isKindOfClass:[StagingGroup class]]) {
        [self.fireResourcesView reloadData];
    }
    else {
        // get index path of the managed group cell
        NSIndexPath *groupIndexPath= [self getGroupIndexPath:group.currentValue];
        if (groupIndexPath) {
            NSArray *indexPaths = @[groupIndexPath];
            [self.managementView reloadItemsAtIndexPaths:indexPaths];
        }
    }
}

- (NSIndexPath *)determineSectionAtPoint:(CGPoint)dropPoint inCollectionView:(FireResourcesView *)view
{
    NSUInteger numberOfSections = (NSUInteger) view.numberOfSections;
    NSIndexPath *indexPath = nil;
    NSIndexPath *prevSectionIndexPath = nil;
    for (NSUInteger section = 0; section < numberOfSections; section++) {
        indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
        UICollectionViewLayoutAttributes *layoutAttributes = [view layoutAttributesForSupplementaryElementOfKind:UICollectionElementKindSectionHeader
                                                    atIndexPath:indexPath];
        CGPoint sectionStartingPoint = layoutAttributes.frame.origin;
        if (dropPoint.y >= sectionStartingPoint.y) {
            if ((section + 1) >= numberOfSections) {
                break;
            }
            prevSectionIndexPath = indexPath;
        }
        else {
            indexPath = prevSectionIndexPath;
            break;
        }
    }
    return indexPath;
}

// add a fireUnit to a staging group
- (void)addFireUnit:(FireUnit *)fireUnit
 toStagingGroupView:(FireResourcesView *)toView
      itemIndexPath:(NSIndexPath *)itemIndexPath
{
    if (fireUnit) {
        // determine target staging group
        NSUInteger toSection = (NSUInteger) itemIndexPath.section;
        StagingGroup *stagingGroup = self.sortedStagingGroups[toSection];

        // clear coordinator status if coordinator for different staging group
        if ([fireUnit.coordinatorFor isKindOfClass:[StagingGroup class]]
                && ![fireUnit.coordinatorFor isEqual:stagingGroup]) {
            fireUnit.coordinatorFor = nil;
        }

        // add unit to staging group
        NSArray *units = [self sort:stagingGroup.units withKey:@"prefixedName"];
        units = [self movefireUnit:fireUnit atIndexPath:itemIndexPath units:units toView:toView];
        stagingGroup.units = [NSSet setWithArray:units];
        [self.incident.log action:[NSString stringWithFormat:@"%@ - add to staging group %@", fireUnit.prefixedName, stagingGroup.name] unit:fireUnit task:stagingGroup];
        fireUnit.activeStartTime = NSDate.date;
        [self logParStatus];
        [self setNeedsSave];
    }
}

- (void)reloadDataForManagementCellAtIndex:(NSInteger)index
 {
     NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
     NSArray *indexPaths = @[indexPath];
     [self.managementView reloadItemsAtIndexPaths:indexPaths];
     // NSLog(@"reloaded management cell:%i", index);
 }

 - (NSArray *)movefireUnit:(FireUnit *)fireUnit atIndexPath:(NSIndexPath *)itemIndexPath
                        units:(NSArray *)units
                toView:(FireResourcesView *)toView
               {
     // get index information for item at drop point (if any)
     NSMutableArray *newUnits = [units mutableCopy];
     if (itemIndexPath && newUnits.count > 0) {
         NSUInteger index = (NSUInteger) [itemIndexPath item];
         // NSLog(@"inserting name %@ at index %i in Division %@", fireUnit.prefixedName, index, toView.taskId);
         [newUnits insertObject:fireUnit atIndex:index];
 //        [toView insertItemsAtIndexPaths:[NSArray arrayWithObject:itemIndexPath]];
     }
     else {
         // NSLog(@"added name %@ to Division %@", fireUnit.prefixedName, toView.taskId);
         [newUnits addObject:fireUnit];
     }
     [self setSortValuesFor:newUnits];
                   [self setNeedsSave];
     return newUnits.copy;
 }


- (FireUnit *)getFireUnitWithName:(NSString *)name inSet:(NSSet *)set {
     for (FireUnit *fireUnit in set){
         if ([fireUnit.prefixedName isEqualToString:name]) {
             return fireUnit;
         }
     }
     return nil;
 }

 #pragma mark - setup views

 - (void) setFireResourcesView:(FireResourcesView *)newView
{
    _fireResourcesView = newView;
    _fireResourcesView.accessibilityIdentifier = @"fireResourcesCollection";
    _fireResourcesView.dataSource = self;
    _fireResourcesView.delegate = self; 
    _fireResourcesView.dragDelegate = self;
    _fireResourcesView.taskId = @"resourcesView";
    _fireResourcesView.backgroundColor = [UIColor ersViewBackgroundColor];

    [self.dragAndDropManager addDragArea:_fireResourcesView];
    [(id <DragDropAcceptor>) _fireResourcesView addTypeToAccept:[FireResourceViewCell getDragIdentifier]];
}

- (void) setFireStationView:(FireStationView *)fireStationView
{
    _fireStationView = fireStationView;
    _fireStationView.accessibilityIdentifier = @"fireStationView";
    _fireStationView.dragDelegate = self;
    _fireStationView.taskId = @"fireStationView";
    [self.dragAndDropManager addDragArea:_fireStationView];
    [(id <DragDropAcceptor>) _fireStationView addTypeToAccept:[FireResourceViewCell getDragIdentifier]];
}


- (void) setManagementView:(ManagementCollectionView *) activeTaskManagementView
{
    _managementView = activeTaskManagementView;
    _managementView.accessibilityIdentifier = @"incidentManagementView";
    _managementView.dataSource = self; 
    _managementView.delegate = self;
    _managementView.backgroundColor = [UIColor ersViewBackgroundColor];
}

- (void)setTaskTableView:(TaskTableView *)taskTableView
{
    _taskTableView = taskTableView;
    _taskTableView.accessibilityIdentifier = @"taskTableView";
    _taskTableView.dataSource = self;
    _taskTableView.delegate = self;
    _taskTableView.backgroundColor = [UIColor ersViewBackgroundColor];
}

- (void)setUnitMovementView:(UnitMovementView *)unitMovementView
{
    _unitMovementView = unitMovementView;
}

- (void) setMainToolbar:(UIToolbar *)toolbar
{
    _mainToolbar = toolbar;
    _mainToolbar.accessibilityIdentifier = @"mainToolbar";
}

- (void) setIsSegueActive:(BOOL)isSegueActive
{
    NSLog(@"%s %d", __PRETTY_FUNCTION__, isSegueActive);
    _isSegueActive = isSegueActive;
}

#pragma mark - Initial Department Settings

- (void)addTemplateList: (DepartmentTaskList *)departmentTaskList sortValue: (NSUInteger)sortValue
{
    IncidentTemplate *template = self.incidentTemplate;
    [template addTaskListToTemplate:departmentTaskList sortValue:sortValue];
}

- (FireDepartment *) createPrototypeFireDepartment:(NSString *) departmentName
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    FireDepartment *fireDepartment = [FireDepartment createWithName:departmentName withContext:self.context];
    fireDepartment.neighborPrefix = @"-";
    fireDepartment.id = @0;
    // create empty unit resource list
    fireDepartment.resourceList = [DepartmentResources withUniqueId:[NSString stringWithFormat:@"resourceList-%@", fireDepartment.uniqueId] createInContext:self.context];
    
    [fireDepartment addGroupNamesObject:[GroupName createDefault:@"Div-1" forDepartment:fireDepartment]];
    [fireDepartment addGroupNamesObject:[GroupName createDefault:@"Vent" forDepartment:fireDepartment]];
    [fireDepartment addGroupNamesObject:[GroupName createDefault:@"Search" forDepartment:fireDepartment]];
    [fireDepartment addGroupNamesObject:[GroupName createDefault:@"RIC" forDepartment:fireDepartment]];

    // create default staging areas
    IncidentTemplate *template = self.incidentTemplate;
    StagingGroup *available = [StagingGroup fromDefaultStage:[fireDepartment makeDefaultStage:@"Available" timeRequired:0]];
    [template addStagingGroupToTemplate:available];
    StagingGroup *rehab = [StagingGroup fromDefaultStage:[fireDepartment makeDefaultStage:@"Rehab" timeRequired:20]];
    [template addStagingGroupToTemplate:rehab];

    // create prototype roles
    Role *adminRole = [Role createWithName:@"Administrator" withPermission:FDAdministrator context:self.context];
    Role *icUserRole = [Role createWithName:@"Incident Coordinator" withPermission:FDIncidentManager context:self.context];
    Role *adminAndIcRole = [Role createWithName:@"AdminAndIC" withPermission:FDAdminAndManager context:self.context];

    [fireDepartment addRolesObject:adminRole];
    [fireDepartment addRolesObject:adminAndIcRole];
    [fireDepartment addRolesObject:[Role createWithName:@"View Only" withPermission:FDViewOnly context:self.context]];
    [fireDepartment addRolesObject:icUserRole];

    // create prototype admin user
    // User *admin = [adminRole createUserWithName:@"admin1"];
    // admin.name = @"Dummy Admin";
    // admin.id = @"Da123?";
    // admin.id2 = @"da123";
    
    // create prototype user with both roles
    [self defaultUser];

    // create prototype tasklists
    DepartmentTaskList *utilitiesDeptList = [self createTaskListNamed:@"Utilities"
                withItemNames:@[@"Gas", @"Electric", @"Water"]
                forDepartment:fireDepartment];

    DepartmentTaskList *benchmarkDeptList = [self createTaskListNamed:@"Bench Marks"
                withItemNames:@[@"Primary Search", @"Under Control", @"Loss Stopped", @"Secondary Search", @"PARS"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"Groups"
                withItemNames:@[@"Fire Attack", @"B/U Lines", @"Primary Search", @"RIC", @"Vent"]
                forDepartment:fireDepartment];

    DepartmentTaskList *offensiveDeptList = [self createTaskListNamed:@"Offensive"
                withItemNames:@[@"Rescue", @"Exposure", @"Confinement", @"Extinguishment", @"Overhaul", @"Salvage"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"Defensive"
                withItemNames:@[@"Red Cross", @"Shelter", @"Chaplain"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"High Rise"
                withItemNames:@[@"Fire Attack Div",
                        @"RIC",
                        @"Lobby Control",
                        @"Top Control", @"Resource Staging", @"Level II Staging", @"Rescue/Safe Refuge"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"MCI"
                withItemNames:@[@"Triage", @"Rescue", @"Treatment", @"Transportation", @"Level II Staging"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"MayDay"
                withItemNames:@[@"Emg. Traffic-Lost FF",
                        @"Move Radio Traffic",
                        @"Request More Alarms",
                        @"PAR",
                        @"Activate RIC (Rescue Group)",
                        @"Make New RIC", @"Safety Group", @"Call AFD IMT", @"Treatment/Transport Groups"]
            forDepartment:fireDepartment];

    [self createTaskListNamed:@"Emergency Abandonment"
                withItemNames:@[@"Emg. Traffic-Abandon Building",
                        @"Not All Answers", @"Dispatch->Abandon Building", @"Hi/Low Siren 10 Seconds"]
            forDepartment:fireDepartment];

    // create initial incident template
    [self addTemplateList:utilitiesDeptList sortValue:0];
    [self addTemplateList:benchmarkDeptList sortValue:1];
    [self addTemplateList:offensiveDeptList sortValue:2];
   
    return fireDepartment;
}


- (User *)defaultUser
{
    User *user = [User userWithName:@"Chief" onDevice:self.device.deviceId context:self.fireDepartment.managedObjectContext];
     if (!user) {
        Role *adminAndIcRole = [Role roleWithName:@"AdminAndIC" context:self.context];
        if (!adminAndIcRole) {
            adminAndIcRole = [Role createWithName:@"AdminAndIC" withPermission:FDAdminAndManager context:self.context];
        }
         user = [adminAndIcRole createUserWithName:@"Chief" onDevice:self.device.deviceId];
    }
    if (!user.inDepartment) {
         user.inDepartment = self.fireDepartment;
    }
    user.name = @"Chief";
    user.id = @"chief1";
    user.id2 = @"chief1";
    return user;
}

- (DepartmentTaskList *)createTaskListNamed:(NSString *)listName
                              withItemNames:(NSArray *)itemNames
                                  forDepartment:(FireDepartment *) fireDepartment
{
    NSUInteger value = fireDepartment.taskLists.count + 1;
    DepartmentTaskList *taskList = [DepartmentTaskList createWithName:listName
                                                            sortValue:value
                                                          withContext:self.context];
    taskList.uniqueId = [NSString stringWithFormat:@"taskList-%@-%@", listName, fireDepartment.uniqueId];
    [taskList createListItems:itemNames];
    [fireDepartment addTaskListsObject:taskList];
    return taskList;
}

- (void) makeResources: (NSArray *)resourceList forDepartment:(FireDepartment *)fireDepartment
{
    NSAssert(fireDepartment.resourceList, @"resourceList created with department");
    
    NSUInteger index = 1;
    for (NSString *resourceName in resourceList) {
        FireUnit *fireUnit = [FireUnit createWithName:resourceName withContext:self.context];
        fireUnit.parStatusOk = @YES;
        fireUnit.sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:index++ exponent:1 isNegative:NO];
        fireUnit.uniqueId = [resourceName stringByAppendingFormat:@"+%@",fireDepartment.name];
        [fireDepartment.resourceList addFireUnitsObject:fireUnit];
    }
}

- (void)makeResourcesFor: (FireDepartment *)fireDepartment
              prefixWith:(NSString *)prefix
             startNumber:(NSUInteger) startValue
               endNumber:(NSUInteger) endValue
{
    // add equipment
    NSMutableArray *resourceNames = [NSMutableArray arrayWithCapacity:10];
    for (NSInteger engNumber = startValue; engNumber <= endValue; engNumber++) {
        [resourceNames addObject: [prefix stringByAppendingFormat:@"%ld", (long)engNumber]];
    }

    [self makeResources:resourceNames forDepartment:fireDepartment];
}

- (void)setupNewIncident
{
    os_activity_label_useraction("setupNewIncident");
    
    // Link incident to this fire department
    [self.fireDepartment addIncidentsObject:self.incident];
    self.incident.incidentCoordinator = self.device.user;

    // get preferences and make them incident specific
    NSSet *deptPreferences = self.fireDepartment.preferences;     // set of Preferences
    for (Preference *p in deptPreferences) {
        Preference *incidentPreference = [Preference createWithName:p.name
                                                          withValue:p.value
                                                        withContext:self.context];
        incidentPreference.forIncident = self.incident;
    }

    // get lists for incident from incident template
    IncidentTemplate *template = self.incidentTemplate;
    for (IncidentTaskList *taskList in template.taskLists) {
        IncidentTaskList *incidentTaskList = [IncidentTaskList fromDepartmentList:taskList.fromDepartmentList withContext:self.context];
        incidentTaskList.sortOrderValue = taskList.sortOrderValue;
        incidentTaskList.forIncident = self.incident;
    }

    // add empty management task/group/division
    [self createDummyGroup];
    
    // create empty staging areas
    [self createStagingAreas];
}


- (void)createStagingAreas
{
    // create groups from the fire department incident template
    IncidentTemplate *template = self.incidentTemplate;
    NSSet *stagingGroups = template.stagingGroups;
    for (StagingGroup *stagingGroup in stagingGroups) {
         [self.incident addStagingGroupsObject:[StagingGroup from:stagingGroup]];
    }
}

#pragma mark - core data setup

 - (void)createDummyGroup
 {
     NSUInteger nextValue = (NSUInteger) self.incident.nextGroupValue;
     ListItem *emptyTask = [ListItem createItemNamed:DUMMY_NAME withValue:nextValue withContext:self.context];
     [self.incident addManagedTasksObject:emptyTask];
 }


 #pragma mark - view notifications

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self resetCurrentUser];
    self.viewAppeared = NO;
    self.isResetting = NO;
    self.view.userInteractionEnabled = NO;
    _sortedManagedTasks = nil;
    _isLogOnly = NO;
    self.parWarningLabel.textColor = [UIColor whiteColor];
    self.parWarningLabel.backgroundColor = [UIColor ersRedColor];
    self.parWarningLabel.hidden = YES;
    _colorForCoordinatorIsSet = [UIColor ersBlackColor];
    _colorForNoCoordinator = [UIColor ersRedColor];
    self.titleColor =[UIColor whiteColor];
    self.titleBgColor = [UIColor ersGreenColor];
    self.titleSize = 17.0f;
    self.userActivity = nil;

    _HeaderBackgroundColor = [UIColor ersListHeaderColor];
    _defaultStageNames = @[@"Available", @"Rehab", @"RIC"];
    [[UITableView appearance] setBackgroundColor:[UIColor ersViewBackgroundColor]];
    [[UIToolbar appearance] setBarTintColor:[UIColor ersToolbarBackgroundColor]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor ersToolbarBackgroundColor]];

    UIApplication *app = [UIApplication sharedApplication];
    self.appDelegate = (FireManAppDelegate *) app.delegate;

    // create document for Core Data
    self.context = self.appDelegate.managedObjectContext;
    
    self.incidentInfo = [[IncidentInfo alloc] init];
    self.barProgressView.progress = 0.0;
    [self.barProgressView.superview bringSubviewToFront:self.barProgressView];
    self.barProgressView.hidden = NO;
    self.progressActive = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ensemblesMergedChanges:) name:@"ensemblesMergedChanges" object:nil];
        [self startTimer];
}

- (void)startTimer
{
    if (self.timer) {
        [self stopTimer];
    }
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.1
                                                  target:self
                                                selector:@selector(elapsedTimer:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    self.currentUser.isSingleRoleMode = true;
    self.isResetting = NO;
    [super viewDidAppear:animated];
    self.viewAppeared = YES;
    [self updateModelAndView];
    [self verifyUserAndIncident];
    self.view.userInteractionEnabled = self.appDelegate.databaseReady;}

- (NSString *)setupDeviceAndIncident
{
    NSString *deviceId = self.device.deviceId;
    if (!self.device.owningDepartment) {
        self.device.owningDepartment = self.fireDepartment;
    }
    if (self.device.currentIncident) {
        self.incident = [self.fireDepartment incidentMatching:self.device.currentIncident];
        if (self.incident) {
            [self setupIncident:self.incident isNew:NO];
        }
    }
    else {
        self.incident = nil;
    }
    return deviceId;
}

- (void)updateModelAndView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.isSegueActive = NO;
    [self startTimer];
    _fireDepartment = nil;
    NSString *deptName = self.fireDepartment.name;  // sets up fireDepartment
    NSString *deviceId = self.device.deviceId;      // setup self.device

    [self updateUserIdButton];

    NSLog (@"FireManViewController: viewDidAppear dept=%@ device=%@ incident=%@", deptName, deviceId, self.incident.name);
}


- (void)viewWillDisappear:(BOOL)animated
{
    self.viewAppeared = NO;
    [super viewWillDisappear:animated];
}


 - (void)dismissViewControllers:(UIViewController *)controller
 {
     if (controller) {
         UIViewController *presentedViewController = controller.presentedViewController;
         if (presentedViewController) {
             [self dismissViewControllers:presentedViewController];
         }
         NSLog(@"dismissing viewController: %@", controller.nibName);
         [controller dismissViewControllerAnimated:NO completion:nil];
     }
 }

-(void) ensemblesMergedChanges:(NSNotification *)notif
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsRefreshView];
    });
}

- (void) performIncidentSelection
{
    os_activity_label_useraction("performIncidentSelection");
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(performIncidentSelection) object:nil];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (!self.isSegueActive) {
        [self performSegueWithIdentifier:@"IncidentSelection" sender:self];
    }
}

- (void) needsPerformIncidentSelection
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(performIncidentSelection) object:nil];
    [self performSelector:@selector(performIncidentSelection) withObject:nil afterDelay:1.0];  // delay
}

// core data save
- (void)save
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(save) object:nil];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.context performBlock:^{
        NSError *error;
        if (![self.context save:&error]) {
            NSLog(@"Save error: %@", error);
        }
    }];
}

- (void)setNeedsSave
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(save) object:nil];
    [self performSelector:@selector(save) withObject:nil afterDelay:0.3];
}


// update View after the data model has been changed by an iCloud update
- (void)updateViewAfterICloudNotification: (NSNotification *) notification
{
    os_activity_label_useraction("updateViewAfterICloudNotification");
    _isLogOnly = NO;
    NSLog(@"%s", __PRETTY_FUNCTION__);
    BOOL isIncidentUpdated = NO;
    BOOL isNewDepartmentEntity = NO;
    // BOOL isFireUnitsUpdate = NO;
    BOOL isTaskListUpdate = NO;
    BOOL isManagedGroup = NO;
    BOOL isStagingGroupUpdate = NO;
    NSSet *insertsAndUpdates = [self.fireDepartment cloudUpdates:notification];
    // NSSet *deletes = [self.fireDepartment cloudDeletes:notification];
    NSManagedObjectContext *moc = self.context;
    
    // do whatever you need to with the NSManagedObjectID
    // you can retrieve the object from with [moc objectWithID:objID]
    NSLog(@"number of inserts and updates: %ld",(unsigned long)insertsAndUpdates.count);
    
    for (NSManagedObjectID *objID in insertsAndUpdates) {
        NSManagedObject *obj = [moc objectWithID:objID];
        NSLog(@"obj:%@", obj);
        
        // look for new or updated fire department object
        
        if ([obj isKindOfClass:[FireDepartment class]]) {
            FireDepartment *department = (FireDepartment *) obj;
            NSLog(@"FireDepartment update: %@",department);
            if (department.id.integerValue == 0) {
                if (department != self.fireDepartment) {
                    isNewDepartmentEntity = YES;
                }
                self.fireDepartment = nil;
                if (isNewDepartmentEntity) {
                    break;
                }
            }
        }
        else if (self.incident) {
            if ([obj isKindOfClass:[Incident class]]) {
                Incident *incident = (Incident *) obj;
                if ([incident isEqualValue:self.incident]) {
                    NSLog(@"Incident update: %@",incident);
                    _cachedTaskLists = nil;
                    _sortedManagedTasks = nil;
                    _sortedStagingGroups = nil;
                    isIncidentUpdated = YES;
                }
                break;
            }
            else if ([obj isKindOfClass:[FireUnit class]]) {
                FireUnit *fireUnit = (FireUnit *) obj;
                if ([fireUnit.forIncident isEqualValue:self.incident]) {
                    NSLog(@"fireUnit update: %@",fireUnit);
                    [self updateCellForFireUnit:fireUnit];
                }
            }
            else if ([obj isKindOfClass:[IncidentTaskList class]]) {
                IncidentTaskList *incidentTaskList = (IncidentTaskList *) obj;
                if ([incidentTaskList.forIncident isEqualValue:self.incident]) {
                    NSLog(@"incidentTaskList update: %@",incidentTaskList);
                    _cachedTaskLists = nil;
                    isTaskListUpdate = YES;
                }
            }
            else if ([obj isKindOfClass:[StagingGroup class]]) {
                StagingGroup *stagingGroup = (StagingGroup *) obj;
                if ([stagingGroup.forIncident isEqualValue:self.incident]) {
                    NSLog(@"stagingGroup update: %@",stagingGroup);
                    _sortedStagingGroups = nil;
                    isStagingGroupUpdate = YES;
                }
            }
            else if ([obj isKindOfClass:[ListItem class]]) {
                ListItem *listItem = (ListItem *) obj;
                if ([listItem.managedByIncident isEqualValue:self.incident]) {
                    NSLog(@"managedGroup update: %@",listItem);
                    _sortedManagedTasks = nil;
                    //[self reloadViewFor:listItem];
                    isManagedGroup = YES;
                }
                else if (listItem.forList) {
                    List *list = listItem.forList;
                    if ([list isKindOfClass:[IncidentTaskList class]]) {
                        IncidentTaskList *incidentTaskList = (IncidentTaskList *)list;
                        if ([incidentTaskList.forIncident isEqualValue:self.incident]) {
                            NSLog(@"incidentTaskList update: %@",incidentTaskList);
                            isTaskListUpdate = YES;
                        }
                    }
                }
            }
        }
    }
    
    
    NSString *deptName = self.fireDepartment.name; //set up fire department if needed
    if (!self.incident || isIncidentUpdated || isNewDepartmentEntity) {
        [self setNeedsRefreshView];
    }
    else {
        if (isTaskListUpdate) {
            [self setNeedsRefreshView];
        }
        if (isManagedGroup) {
            [self setNeedsRefreshView];
        }
        if (isStagingGroupUpdate) {
            [self setNeedsRefreshView];
        }
    }
    NSLog(@"update for firedepartment: %@",deptName);
}


- (void) refreshView
 {
     [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshView) object:nil];
     if (self.progressActive || self.isSegueActive) {
         // keep waiting
         [self setNeedsRefreshView];
     }
     else {
         _fireDepartment = nil;
         _incident = nil;
         _cachedTaskLists = nil;
         _sortedManagedTasks = nil;
         _sortedStagingGroups = nil;
         NSString *fireDepartmentName = self.fireDepartment.name;
         self.currentUser.device = self.device;
         self.incident = self.currentUser.currentIncident;
         if (self.incident) {
             self.isLogOnly = self.incident.stopTime != nil;
             [self updateUserIdButton];
             [self.fireResourcesView reloadData];
             [self.managementView reloadData];
             _cachedTaskLists = nil;
             [self.taskTableView reloadData];
             [self updateTitleButton];
             [self updateIcButton];
             [self updateTerminateIncidentButton];
         }

         [self updateUserIdButton];
         if (!self.currentUser.userEntity) {
             // get user identification
             NSLog(@"%s %@",__PRETTY_FUNCTION__,fireDepartmentName);
             [self performSegueWithIdentifier:@"GetUserIdentification" sender:self];
         }
         [self updateGestureRecognizers];

         if (!self.incident) {
             [self needsPerformIncidentSelection];
         }
     }
 }

 - (void)setNeedsRefreshView
 {
     [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshView) object:nil];
     [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.9];  // delay for couple of seconds
 }

- (void)setNeedsRefreshTaskTableView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshTaskTableView) object:nil];
    [self performSelector:@selector(refreshTaskTableView) withObject:nil afterDelay:1.0];  // delay for couple of seconds
}

- (void) refreshTaskTableView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshTaskTableView) object:nil];
    [self.taskTableView reloadData];
}

- (void)setNeedsRefreshStagingGroups
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshStagingGroups) object:nil];
    [self performSelector:@selector(refreshStagingGroups) withObject:nil afterDelay:1.0];  // delay for a second
}

- (void) refreshStagingGroups
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshStagingGroups) object:nil];
    [self.fireResourcesView reloadData];
}

 // update fire department entity with local changes
 - (void)updateDepartment:(FireDepartment *)updatedDepartment
 {
     FireDepartment *currDept = self.fireDepartment;

     // copy updates to updated department if it is not same department entity
     if (![currDept isEqual:updatedDepartment]) {
         self.fireDepartment = [updatedDepartment mergeDept:currDept];
         self.fireDepartment = nil; // force reset up of fireDeparment info
     }
 }

 - (FireDepartment *)fireDepartment
{
    if (!_fireDepartment) {
        [self setupDepartment];
    }
    return _fireDepartment;
}

- (Incident *)incident
{
    if (!_incident) {
        if (self.currentUser) {
            if (self.currentUser.incidentId) {
                _incident = [Incident fetchIncident:self.currentUser.incidentId
                                           withDate:self.currentUser.incidentCreatedDate
                                        withContext:self.context];
            }
        }
    }
    if (_incident && (!_incident.forDepartment)) {       // fire department entity changed by cloud update
        Incident *matchingIncident = [self.fireDepartment incidentMatching:_incident];
        if (matchingIncident) {
            _incident = matchingIncident;
        }
        else {
            _incident.forDepartment = self.fireDepartment;
        }
    }
    return _incident;
}

- (void) resetCurrentUser
{
    _currentUser = nil;
}

- (CurrentUser *) currentUser
{
    if (!_currentUser) {
        _currentUser = [CurrentUser sharedSingletonObject];
        _currentUser.context = self.context;
    }
    return _currentUser;
}

#pragma mark - device identification

- (Device *)device
{
    if (!_device) {
        _device = self.currentUser.device;
         NSLog (@"FireManViewController: device name =%@", self.currentUser.deviceName);
    }
    return _device;
}


- (void)resetCachedInfo
{
    _sortedManagedTasks = nil;
    _sortedStagingGroups = nil;
    _defaultStageNames = nil;
    _cachedTaskLists = nil;
    _divViewDictionary = nil;
}

// fetch the department information or create a default department prototype
- (void) setupDepartment
{
    NSLog (@"%s",__PRETTY_FUNCTION__);
    [self resetCachedInfo];

    NSString *departmentName = [self getFireDepartmentName];
    if (!departmentName) {
        departmentName = @"My Department";
    }
    _fireDepartment = [FireDepartment fetchDepartmentWithId:0 withContext:self.context];

    if (!_fireDepartment) {
        // create default department and department preferences
        _fireDepartment = [self createPrototypeFireDepartment:departmentName];
        [self setDefaultPreferences:departmentName];
    }
    _fireDepartment.name = departmentName;
    if (!_fireDepartment.resourceList) {
        _fireDepartment.resourceList = [DepartmentResources fetchFor:_fireDepartment];
    }
    
    if (!_fireDepartment.incidentTemplate) {
        _fireDepartment.incidentTemplate = [IncidentTemplate fetchFor:_fireDepartment];
    }
        
    if (!self.fireDepartment.preferences) {
        [self setDefaultPreferences:departmentName];
    }
    [_fireDepartment deleteDuplicateObjects];

    NSInteger scanPeriod = [self.preference integerValueFor:PREF_SCAN_PERIOD];
    if (scanPeriod <= 0) {
        [self setDefaultPreferences:departmentName];
    }
    
    // get department level preferences
    self.preference = [Preferences createWith:self.fireDepartment.preferences];
    self.context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
    [self setupDeviceAndIncident];
}


// Get Fire Department Name from Preferences
- (NSString *)getFireDepartmentName
{
    Preference *departmentNamePreference = [self getPreference:PREF_NAME];
    if (departmentNamePreference) {
        return departmentNamePreference.value;
    }
    else {
        return nil;
    }
}

- (NSArray *)sortedStagingGroups
{
    if (!_sortedStagingGroups) {
        _sortedStagingGroups = [self sort:self.incident.stagingGroups];
    }
    return _sortedStagingGroups;
}

- (void) setupIncident: (Incident *) incident isNew: (BOOL) isNew
{
    // os_trace("setupIncident");
    // different incident -- clear task list cache and rebuilt for new incident
    self.cachedTaskLists = nil;
    _sortedManagedTasks = nil;
    _sortedStagingGroups = nil;

    if (isNew) {
        // set up incident with initial groups and lists from fire department
        [self setupNewIncident];

        // setup log
        self.incident.logForIncident = [IncidentLog withContext:self.context];
        [self.incident beginIncident];
    }
    self.startTime = self.incident.startTime;
    self.device.currentIncident = self.incident;  // current incident will be retrieved from device by currentUser
    [self resetDragAndDropManager];
    
    // log incident coordinator
    if (self.currentUser.isCoordinator) {
        // make this device have priority in merges
        self.context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        NSString *msg = [NSString stringWithFormat:@"Incident Coordinator: %@",self.currentUser.name];
        [self.incident.log action:msg];        
    }
    else {
        self.context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
    }
    
    // Setup runtime preferences
    self.preference = [Preferences createWith:self.incident.preferences];

    // setup drag and drop
    [self.dragAndDropManager setGestureRecognizersForView: self.unitMovementView];
    
    //
    if (self.currentUser.isCoordinator) {
        [self.dragAndDropManager gestureRecognizersForView:self.unitMovementView enabled:YES];
    }

    // setup incident information structure
    self.incidentInfo = [[IncidentInfo alloc] init];
    self.incidentInfo.incident = self.incident;
    
    // Hide Par indicator
    self.parWarningLabel.hidden = YES;

    // invalidate any existing timer and start a new elapsed time timer
    [self startTimer];
    [self setNeedsSave];

    [self updateTerminateIncidentButton];
    [self.fireResourcesView reloadData];
    [self.managementView reloadData];
    [self.taskTableView reloadData];

    self.titleColor =[UIColor whiteColor];
    self.titleBgColor = [UIColor ersGreenColor];
    if (self.isLogOnly) {
        self.titleBgColor = [UIColor ersLightGoldColor];
        self.titleColor = [UIColor ersBlackColor];
    }
    self.titleSize = 17.0f;

    [self updateTitleButton];

    [self updateIcButton];

}

- (void)updateIcButton
{
    NSAttributedString *attributedString;

    attributedString = [NSAttributedString attributedStringFrom:self.incidentInfo.icString
                                                foregroundColor:self.titleColor
                                                backgroundColor:self.titleBgColor
                                                           size:self.titleSize];

    [self.icButton setAttributedTitle:attributedString forState:UIControlStateNormal];
    self.icButton.highlighted = NO;
}

- (void)updateTitleButton
{
    NSAttributedString *attributedString;
    attributedString = [NSAttributedString attributedStringFrom:self.incidentInfo.titleString
                                                foregroundColor:self.titleColor
                                                backgroundColor:self.titleBgColor
                                                           size:self.titleSize];

    [self.titleButton setAttributedTitle:attributedString
                                forState:UIControlStateNormal];
    self.titleButton.highlighted = NO;
}

- (void)updateTerminateIncidentButton
 {
     self.isLogOnly = NO;
     if (self.incident) {
         if (self.incident.stopTime) {
             [self.terminateIncidentButton setTitle:@"Reopen Incident"];
             self.isLogOnly = YES;
         }
         else {
             if (self.currentUser.isCoordinator) {
                 [self.terminateIncidentButton setTitle:@"Terminate Incident"];
             }
             else {
                 if ([self.currentUser hasPermission:FDIncidentManager]) {
                     [self.terminateIncidentButton setTitle:@"Takeover Incident"];
                 }
                 else {
                     [self.terminateIncidentButton setTitle:@""];
                 }
             }
         }
     }
     else {
         [self needsPerformIncidentSelection];
     }

 }

 - (void)stopTimer {
     NSLog(@"%s", __PRETTY_FUNCTION__);
     if (self.timer) {
         [self.timer invalidate];
         self.timer = nil;
     }
 }

 - (void)resetDragAndDropManager
{
    // os_trace("resetDragAndDropManager");
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // clear everything
    [self.dragAndDropManager reset];

    // add available resources view as a drag area
    [self.dragAndDropManager addDragArea:self.fireResourcesView];
    [(id <DragDropAcceptor>) self.fireResourcesView addTypeToAccept:[FireResourceViewCell getDragIdentifier]];

    // add fire station view as a drag area
    [self.dragAndDropManager addDragArea:self.fireStationView];
    [(id <DragDropAcceptor>) self.fireStationView addTypeToAccept:[FireResourceViewCell getDragIdentifier]];

}

- (void)setDefaultPreferences:(NSString *)fireDepartmentName
{
    [self createOrUpdatePreference:PREF_NAME withValue:fireDepartmentName];
    [self createOrUpdatePreference:PREF_PAR_LIMIT withValue:@"10"];
    [self createOrUpdatePreference:PREF_STAGING_TIME withValue:@"19"];
    [self createOrUpdatePreference:PREF_ACTIVE_LIMIT withValue:@"20"];
    [self createOrUpdatePreference:PREF_SCAN_PERIOD withValue:@"14"];
    [self createOrUpdatePreference:PREF_MAX_USERS withValue:@"2"];
}


-(void) incrementProgress:(NSInteger)increment max:(NSInteger) maxValue
{
    if (self.progressActive) {
        if (self.barProgressView.progress == 1.0) {
            self.barProgressView.hidden = YES;
            [self.barProgressView.superview sendSubviewToBack:self.barProgressView];
            self.progressActive = NO;
        }
        else {
            float currentPercent = self.barProgressView.progress;
            float currentValue = maxValue * currentPercent;
            currentValue += increment;
            [self.barProgressView setProgress:(currentValue/maxValue) animated:YES];
        }
    }
}

- (void)elapsedTimer:(id)elapsedTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self incrementProgress:1 max:30];
        if (self.view.userInteractionEnabled) {
            BOOL updateStatus = [self verifyUserAndIncident];
            
            if (updateStatus) {
                NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:self.startTime];
                [self.appDelegate elapsedTimer:elapsedTime];
                if (self.incident.stopTime) {
                    elapsedTime = [self.incident.stopTime timeIntervalSinceDate:self.startTime];
                }
                self.incidentInfo.elapsedTime = [[GbElapsedTime alloc] initWithTimeInterval:elapsedTime];
                self.managementHeader.text = self.incidentInfo.elapsedTimeString;
                self.managementHeader.textColor = [UIColor whiteColor];
                NSInteger scanPeriod = [self.preference integerValueFor:PREF_SCAN_PERIOD];
                if (!self.incident.commandUnit) {
                    NSAttributedString *attributedString;
                    attributedString = [NSAttributedString attributedStringFrom:self.incidentInfo.icString
                                                                foregroundColor:[UIColor ersRedColor]
                                                                backgroundColor:[UIColor whiteColor]
                                                                           size:self.titleSize];
                    
                    [self.icButton setAttributedTitle:attributedString forState:UIControlStateNormal];
                    self.icButton.highlighted = NO;
                }
                if ((!self.isOtherViewControllerActive) && (!self.isLogOnly) && scanPeriod && self.incidentInfo.elapsedTime.secs % scanPeriod == 0) {
                    // Update the UI
                    [self statusUpdate];
                }
            }
        }
    });
}

// verify that user and incident are defined
// and that the database is ready
//
- (BOOL)verifyUserAndIncident
{
    BOOL isDatabaseReady = self.appDelegate.databaseReady;
    BOOL isVerified = NO;
    BOOL isUserIdNeeded = NO;
    if (self.currentUser.isSingleRoleMode) {
        if (!self.currentUser.userEntity) {
            self.currentUser.userEntity = self.defaultUser;
            [self updateUserIdButton];
        }
    }

    if (self.progressActive) {
        if (self.currentUser.userEntity && isDatabaseReady) {
            [self.barProgressView setProgress:1.0 animated:YES];
        }
        return NO;
    }
    
    self.view.userInteractionEnabled = isDatabaseReady;
    if (isDatabaseReady) {
                            //NSLog(@"%s %@", __PRETTY_FUNCTION__,@"database ready");
        isVerified = YES;
        if (!self.currentUser.userEntity && !self.isSegueActive) {
            // get user identification
            isVerified = NO;
            NSLog(@"%s %@", __PRETTY_FUNCTION__,@"Get User id");
            [self performSegueWithIdentifier:@"GetUserIdentification" sender:self];
            isUserIdNeeded = YES;
        }
        [self updateGestureRecognizers];

        if (!isUserIdNeeded && !self.incident) {
            isVerified = NO;
            if (!self.isSegueActive) {
                // get incident definition
                NSLog(@"%s %@", __PRETTY_FUNCTION__,@"Get incident");
                [self needsPerformIncidentSelection];
            }
            else {
                // NSLog(@"%s %@", __PRETTY_FUNCTION__,@"Get incident, but segue active");
            }
        }
    }
    return isVerified;
}


- (void)statusUpdate
{
    if (self.isOtherViewControllerActive) {
        // keep waiting
        [self performSelector:@selector(statusUpdate) withObject:nil afterDelay:5.0];
    }
    if (!self.isLogOnly) {
        // update par button status
        NSInteger numberOfUnits = self.incident.assignedUnits.count;
        if (numberOfUnits > 0) {
            NSTimeInterval elapsedTimeSincePar = [[NSDate date] timeIntervalSinceDate:self.incident.timeOfLastPar];
            GbElapsedTime *minsSincePar = [[GbElapsedTime alloc] initWithTimeInterval:elapsedTimeSincePar];
            UIColor *parNeededColor = [UIColor ersRedColor];
            if (minsSincePar.totalElapsedMinutes >= [self.preference integerValueFor:PREF_PAR_LIMIT]) {
                // set par needed button color
                if (![self.parButtonOutlet.tintColor isEqual:parNeededColor]) {
                    self.parButtonOutlet.tintColor = parNeededColor;
                    if (self.currentUser.isCoordinator) {
                        [self.incident.log action:@"PAR needed"];
                        self.parWarningLabel.hidden = NO;
                        [self logParStatus];
                    }
                }
            }
            else {
                if (self.countOfUnitsNeedingPar > 0) {
                    self.parButtonOutlet.tintColor = parNeededColor;
                }
                else {
                    self.parButtonOutlet.tintColor = [UIColor ersBlackColor];
                }
            }
        }
        if (!self.incident.commandUnit) {
            [self.icButton setTitleColor:[UIColor ersRedColor] forState:UIControlStateNormal];
            self.icButton.highlighted = !self.icButton.highlighted;
        }
        else {
            [self.icButton setTitleColor:[UIColor ersBlackColor] forState:UIControlStateNormal];
            self.icButton.highlighted = NO;
        }
        [self updateFireUnitCells];
    }
    
    if ([self.context hasChanges]) {
        [self setNeedsSave];
    }
}


- (void)updateFireUnitCells
{
    // update cells in divisions
    NSUInteger managedGroupIndex = 0;
    for (ListItem *activeGroup in self.sortedManagedTasks) {
        [self updateFireUnitCellsForActiveGroup:activeGroup groupIndex:managedGroupIndex];
        managedGroupIndex++;
    }

    // update cells in staging groups
    NSUInteger section = 0;
    for (StagingGroup *stagingGroup in self.sortedStagingGroups) {
        [self updateFireUnitCellsForStagingGroup:stagingGroup section:section];
        section++;
    }
}

- (void)updateFireUnitCellsForStagingGroup:(StagingGroup *)stagingGroup section:(NSUInteger)section
{
    NSArray *stagingGroupUnits = [self sort:stagingGroup.units withKey:@"prefixedName"];
    NSUInteger index = 0;
    for (FireUnit *fireUnit in stagingGroupUnits) {
        [self updateCellForUnit:fireUnit collectionView:self.fireResourcesView section:section index:index];
        index++;
    }
}

- (void)updateFireUnitCellsForActiveGroup:(ListItem *)activeGroup groupIndex:(NSUInteger)groupIndex
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:groupIndex inSection:0];
    ManagementViewCell *groupViewCell = (ManagementViewCell *) [self.managementView cellForItemAtIndexPath:indexPath];
    ActiveDivisionCollectionView *divView = groupViewCell.activeDivisionView;
    NSArray *sortedUnits = [self sort:activeGroup.assignedUnits];
    NSUInteger index = 0;
    for (FireUnit *fireUnit in sortedUnits) {
        [self updateCellForUnit:fireUnit collectionView:divView section:0 index:index];
        index++;
    }
}

- (void)updateCellForUnit:(FireUnit *)fireUnit
           collectionView:(UICollectionView *)collectionView
                  section:(NSUInteger)section
                    index:(NSUInteger)index
{
    NSIndexPath *unitIndexPath = [NSIndexPath indexPathForItem:index inSection:section];
    FireResourceViewCell *unitViewCell = (FireResourceViewCell *) [collectionView cellForItemAtIndexPath:unitIndexPath];
    FireCellColor *unitColor = [self getCellColor:fireUnit];
    unitViewCell.cellColors = unitColor;
    if (fireUnit.parStatusOk.boolValue) {
        [unitViewCell parStatusButtonEnabled:NO];
    }
    [self updateCoordinatorForCell:unitViewCell unit:fireUnit];
}


 - (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)groupNameChanged:(UIButton *)sender {
    assert(NO);
    if (self.fireDepartment) {                       // icloud store changed?
        NSString *newName = sender.titleLabel.text;
        NSInteger index = sender.tag;
        [self changeGroupName:newName forGroupIndex:index];
    }
    self.isSegueActive = NO;
}

- (void) changeGroupName:(NSString *) newGroupName forGroupIndex:(NSInteger) index
{
    os_activity_label_useraction("changeGroupName");
    if (self.fireDepartment && newGroupName) {
        NSString *newName = [newGroupName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (newName.length > 0) {
            ListItem *activeTask= [self getManagedTaskAt:index];
            NSString *previousName = activeTask.name;
            if (![previousName isEqualToString:newName]) {
                _sortedManagedTasks = nil;
                [self.incident.log action:[NSString stringWithFormat:@"group name changed from: %@ to: %@",previousName, newName] task:activeTask];
                activeTask.name = newName;
                if ([previousName isEqualToString:DUMMY_NAME]) {
                    [self createDummyGroup];
                    [self.managementView reloadData];
                }
                if ([newName isEqualToString:FD_GROUP_TERMINATED]) {
                    activeTask.managedByIncident = nil;
                    [self.dragAndDropManager removeDragAreaWithId:activeTask.currentValue];
                    [self.managementView reloadData];
                }
            }
        }
    }
}

 - (ListItem *)getManagedTaskAt:(NSInteger)index {
     NSArray *tasks = self.sortedManagedTasks;
     ListItem *activeTask = tasks[(NSUInteger) index];
     return activeTask;
 }

- (IBAction)loginButtonTap:(UIButton *)sender {
}

- (IBAction)StageIdentifierPressed:(UIButton *)sender
{
}

 - (IBAction)terminateIncidentButtonPressed:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("terminateIncidentButtonPressed");
    if (self.incident.stopTime) {
        // reopen incident?
        [self yesNoAlert:REOPEN_INCIDENT_TITLE
                         message:@"Do you want to reopen this Incident?"
               titleForYesButton:RE_OPEN];
    }
    else {
        // close incident?
        if (self.currentUser.isCoordinator) {
            [self yesNoAlert:TERMINATE_INCIDENT_TITLE
                             message:@"Do you want to terminate this Incident?"
                   titleForYesButton:TERMINATE];
        }
        else {
            if ([self.currentUser hasPermission:FDIncidentManager]) {
                [self yesNoAlert:TAKE_OVER_INCIDENT_TITLE
                                 message:@"Do you want to take over coordination of this Incident?"
                       titleForYesButton:TAKE_OVER];
            }
        }
    }
}
     
#pragma mark - Alert msg
- (void) alertPopup:(NSString *)title message:(NSString *)message buttonLabel:(NSString *)buttonLabel
{
    // os_trace("alertPopup");
    if (!buttonLabel) {
        buttonLabel = @"OK";
    }
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertController* __weak weakAlert = alert;
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:buttonLabel
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

 - (void) yesNoAlert:(NSString *)title message:(NSString *)detailedMessage titleForYesButton:(NSString *)yesButtonTitle
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:detailedMessage
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController* __weak weakAlert = alert;
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:noAction];

    UIAlertAction* yesAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self alertClicked:action];
                                                          [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:yesAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)alertClicked:(UIAlertAction *)alertAction
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", alertAction.title);
    
    if ([alertAction.title isEqualToString:RE_OPEN]) {
        self.incident.stopTime = nil;
        [self.incident.log action:@"--------[ Reopened Incident ]--------"];
        self.incident.incidentCoordinator = self.currentUser.userEntity;
        [self setupIncident:self.incident isNew:NO];
        NSString *actionMsg = [NSString stringWithFormat:@"Incident Coordinator: %@", self.currentUser.name ];
        [self.incident.log action:actionMsg];
    }
    else if ([alertAction.title isEqualToString:TERMINATE]) {
        self.incident.stopTime = [NSDate date];
        [self.incident.log action:@"================> Terminated Incident <================"];
        [self setupIncident:self.incident isNew:NO];
    }
    else if ([alertAction.title isEqualToString:TAKE_OVER]) {
        [self.incident.log action:@"================> Changed Incident Coordinator <================"];
        self.incident.incidentCoordinator = self.currentUser.userEntity;
        NSString *actionMsg = [NSString stringWithFormat:@"Incident Coordinator: %@", self.currentUser.name ];
        [self.incident.log action:actionMsg];
        [self setupIncident:self.incident isNew:NO];
    }
}

#pragma mark - IncidentSelectionDelegate

- (void) incidentDeleted:(Incident *)deletedIncident
{
    if ([self.incident.name isEqualToString:deletedIncident.name]) {
        self.incident = nil;
        self.incidentInfo.incident = nil;
        [self updateTitleButton];
    }
}

// prevent deletion for displayed incident
- (BOOL) canDelete:(Incident *)incident
{
    if ([self.incident.name isEqualToString:incident.name]) {
        return false;
    }
    return true;
}

@end
