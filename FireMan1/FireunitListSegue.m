//
//  FireunitListSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/16/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireunitListSegue.h"
#import "FireManViewController.h"
#import "FireUnitListVC.h"
#import "ManagementViewCell.h"
#import "FireDepartment.h"
#import "DepartmentResources.h"


@implementation FireunitListSegue


- (void)perform
{

    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect

    // Set the size of the popover because it will be max sized otherwise

    FireManViewController *sourceController = (FireManViewController *)self.sourceViewController;
    FireUnitListVC *destinationController = (FireUnitListVC *)self.destinationViewController;
    destinationController.fireDepartment = sourceController.fireDepartment;
    destinationController.incident = sourceController.incident;
    destinationController.currentCell = self.cell;
    destinationController.fireUnitListDelegate = sourceController;

    NSInteger numUnits = sourceController.incident.availableUnits.count;
    numUnits += sourceController.incident.activeUnits.count;
    NSInteger len = 12 * 44;
    if (numUnits < 12) {
        len = numUnits * 44;
    }
    
    CGSize size = CGSizeMake(350, len);
    destinationController.preferredContentSize = size;

    CGRect popoverFrame;
    UIView *popoverInView;
    if (self.cell) {
        popoverFrame = self.cell.coordinatingUnitButton.frame;
        popoverInView = self.cell.contentView;
    }
    else {
        popoverFrame = self.anchorView.frame;
        popoverInView = self.anchorView.superview;
    }
    
    // present the controller
    destinationController.modalPresentationStyle = UIModalPresentationPopover;
    [sourceController presentViewController:destinationController animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [destinationController popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    popController.sourceView = popoverInView;
    popController.sourceRect = popoverFrame;
    popController.delegate = sourceController;
    

//    
//    // -----
//    
//    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:destinationController];
//
//    destinationController.popoverVwController = pop;
//
//     pop.delegate = sourceController;
//
//    [pop presentPopoverFromRect:popoverFrame
//                         inView:popoverInView
//       permittedArrowDirections:UIPopoverArrowDirectionAny
//                       animated:YES];

}


@end
