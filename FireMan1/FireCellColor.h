//
// Created by Gerald Boyd on 4/25/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FireCellColor : NSObject
@property (strong, nonatomic) UIColor *bgColor;
@property (strong, nonatomic) UIColor *textColor;
+ (FireCellColor *) bgColor:(UIColor *)bgColor textColor:(UIColor *) textColor;
@end