//
//  Created by jve on 4/1/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class DragContext;
@class DragDropManager;
@protocol DragDropAcceptor;
@protocol DraggableUIView;


// Protocol to be implemented by class that is the delegate
// for drag-drop operations
@protocol DragDropDelegate <NSObject>
- (void) copyItem: (UIView *)viewBeingDragged atPoint:(CGPoint) point fromView:(id <DragDropAcceptor>) sourceView dragContext:(DragContext *) dragContext;
- (void) deleteItem: (UIView *)viewBeingDragged atPoint:(CGPoint)point fromView: (id <DragDropAcceptor>) sourceView dragContext:(DragContext *) dragContext;
- (void) pasteItem: (id <DraggableUIView>)viewBeingDragged atPoint:(CGPoint) point toView: (id <DragDropAcceptor>)targetView dragContext:(DragContext *) dragContext;
- (void) moveItem: (id <DraggableUIView> )viewBeingDragged atPoint:(CGPoint) fromPoint toPoint:(CGPoint) toPoint forView: (id <DragDropAcceptor>) sourceView;
- (void) cancelMove: (DragContext *) dragContext;
@end

// Protocol to be implemented by a draggable view
@protocol DraggableUIView <NSObject>
@property (weak, nonatomic) id<DragDropAcceptor> owningView;
+ (NSString *) getDragIdentifier;
@end

// protocol to be implemented by the view
// that is a source or destination for a drag-drop operation
@protocol DragDropAcceptor <NSObject>
@property (strong, nonatomic) NSString *taskId;
@property (strong, nonatomic) id <DragDropDelegate> dragDelegate;
- (void) setDragDropManager: (DragDropManager *) dragDropManager;

// source view for drag
- (void) startingDrag: (DragDropManager *) dragManager atPoint: (CGPoint) point;
- (void) dragFromEnded: (UIView *) viewBeingDragged;

// target view for drag
- (void) addTypeToAccept: (NSString *) dragIdentifier;
- (BOOL) droppingItemOn: (UIView *) viewBeingDragged atPoint: (CGPoint) point;
- (BOOL) isDropTargetFor: (UIView *) viewBeingDragged atPoint: (CGPoint) point;
- (BOOL) removeFromView: (UIView *) viewBeingDragged;
- (BOOL) addImage:(UIView *) viewBeingDragged atPoint: (CGPoint) point;
- (void) dragToEnded;

- (UIView *)viewAt:(CGPoint)point;
@end

// DragDropManager
@interface DragDropManager : NSObject

@property(nonatomic, retain) DragContext *dragContext;
@property(nonatomic, retain, readonly) NSArray *dropAreas;
@property (nonatomic, readonly) BOOL isActive;

- (void) reset;
- (void)removeDragAreaWithId:(NSString *)taskId;

- (void) addDragArea: (id <DragDropAcceptor>) dragArea;
- (void) addDragType: (NSString *) dragType forView: (id <DragDropAcceptor>) dragArea;
- (void) dragging:(id)sender;
- (BOOL) isValidDropItem: (id <DraggableUIView>) dragSubject
                 forView: (id <DragDropAcceptor>) dragArea;
- (id) init;
- (void) removeFromView: (UIView *) viewBeingDragged;
- (void) addImage:(UIView *) viewBeingDragged atPoint: (CGPoint) point;
- (void) setGestureRecognizersForView: (UIView *)view;
- (void) gestureRecognizersForView:(UIView *)view
                           enabled:(BOOL) enable;


@end


