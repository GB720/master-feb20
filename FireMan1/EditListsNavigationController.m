//
//  EditListsNavigationController.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/9/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "EditListsNavigationController.h"

@interface EditListsNavigationController ()

@end

@implementation EditListsNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}


@end
