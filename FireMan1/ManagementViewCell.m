//
//  ManagementViewCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 3/29/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "ManagementViewCell.h"
#import "ActiveDivisionCollectionView.h"
#import "FireResourcesView.h"
#import "ManagementHeaderView.h"

CGFloat VIEW_SPACING = 6;

@interface ManagementViewCell ()

@end

@implementation ManagementViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

/*
- (void) setActiveDivisionView:(ActiveDivisionCollectionView *)anotherActiveDivisionView
{
    for (UIView *subView in self.contentView.subviews) {
        if ([subView isKindOfClass:[ActiveDivisionCollectionView class]]) {
            [subView removeFromSuperview];
        }
    }

    _activeDivisionView = anotherActiveDivisionView;
    [self.contentView addSubview:anotherActiveDivisionView];
}
*/

- (ActiveDivisionCollectionView *)activeDivisionView
{
    if (!_activeDivisionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(70, 47);
        layout.minimumInteritemSpacing = 6;
        layout.minimumLineSpacing = 6;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        ActiveDivisionCollectionView *divView = [[ActiveDivisionCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [divView registerClass:[FireResourceViewCell class] forCellWithReuseIdentifier:@"FireResource"];
        _activeDivisionView = divView;
        [self.contentView addSubview:divView];
    }
    return _activeDivisionView;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGRect myBounds = self.contentView.bounds;
    CGFloat headerSize = myBounds.origin.y;
    for (UIView *subView in self.contentView.subviews) {
        if ([subView isKindOfClass:[ManagementHeaderView class]]) {
            headerSize = subView.bounds.origin.y + subView.bounds.size.height + VIEW_SPACING;

        }
    }
    self.activeDivisionView.frame = CGRectMake(myBounds.origin.x , headerSize, myBounds.size.width, myBounds.size.height - headerSize);
}

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate 
{
}

#pragma mark - cell reuse

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.taskCurrentValue = nil;
    [self.activeDivisionView removeFromSuperview];
    _activeDivisionView = nil;
    [self setNeedsLayout];
    // NSLog (@"ManagementViewCell: PrepareForReuse");
    // [[NSBundle mainBundle] loadNibNamed:@"Active Division Collection View" owner:self options:nil];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
