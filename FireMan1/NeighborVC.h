//
//  NeighborVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/10/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeighborVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) NSString * neighborName;
@property (strong, nonatomic) NSString * neighborPrefix;
@property (strong, nonatomic) NSArray *departments;
@end
