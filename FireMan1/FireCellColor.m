//
// Created by Gerald Boyd on 4/25/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "FireCellColor.h"


@implementation FireCellColor {

}
+ (FireCellColor *)bgColor:(UIColor *)bgColor textColor:(UIColor *)textColor
{
    FireCellColor *cellColors = [[FireCellColor alloc] init];
    cellColors.textColor = textColor;
    cellColors.bgColor = bgColor;
    return cellColors;
}

@end