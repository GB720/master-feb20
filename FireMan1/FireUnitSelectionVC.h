//
//  FireUnitSelectionVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/4/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Incident;
@class FireUnit;
@class StagingHeaderView;

@interface FireUnitSelectionVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) Incident *incident;
@property (nonatomic, strong) UIViewController *callingViewController;
@property (nonatomic, strong) NSArray *selectedUnits;
@property (nonatomic, strong) StagingHeaderView *stagingGroupHeader;

@property (nonatomic) NSInteger stagingGroupIndex;

- (void) addFireUnit:(NSString *) fireUnitName forSection:(NSInteger) section;
- (IBAction)unwindFromAddNeighbor:(UIStoryboardSegue *)unwindSegue;
@end
