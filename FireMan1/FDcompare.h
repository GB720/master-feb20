//
// Created by Gerald Boyd on 1/16/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FDcompare <NSObject>
- (BOOL) isEqualValue:(id <FDcompare>) object2;
- (BOOL) isOlderThan:(id <FDcompare>) object2;
@end