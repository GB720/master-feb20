//
//  EditListOfListsVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/4/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "EditListOfListsVC.h"
#import "Incident+Create.h"
#import "SortedTaskList.h"
#import "ListItem+Status.h"
#import "IncidentTaskList.h"
#import "IncidentTaskList+Create.h"
#import "ListItem+Create.h"
#import "IncidentLog.h"
#import "IncidentLog+Log.h"
#import "List+Status.h"
#import "EditTaskViewCell.h"
#import "AddListFromDepartmentVC.h"
#import "DepartmentTaskList.h"
#import "UIColor+EmerResStrat.h"


@interface EditListOfListsVC ()
- (IBAction)itemDescriptionEdited:(UITextField *)sender;
- (IBAction)itemDescriptionEditDidEnd:(UITextField *)sender;
- (IBAction)descriptionDidEndOnExit:(UITextField *)sender;


@property (nonatomic, strong)NSArray *cachedTaskLists;
@property (nonatomic, strong) UIColor *headerBackgroundColor;
@property (nonatomic, strong) UIColor *descriptionBackgroundColor;
@property(nonatomic, readwrite) NSIndexPath *activeIndexPath;
@end

@implementation EditListOfListsVC {

}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // _headerBackgroundColor = [UIColor colorWithRed:0.81 green:0.905 blue:0.878 alpha:1.0];
    _headerBackgroundColor = [UIColor ersListHeaderColor];
    _descriptionBackgroundColor = [UIColor ersTextFieldBackgroundColor];
    _cachedTaskLists = nil;
    self.tableView.editing = YES;
    self.tableView.accessibilityIdentifier = @"listOfTaskLists";
    _context = self.incident.managedObjectContext;

    // Get lists from fire department

    // sort lists

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma text field notifications

- (IBAction)itemDescriptionEdited:(UITextField *)textField
{
    // value changed
    NSInteger section = textField.tag / 100;
    NSInteger row = textField.tag % 100;
    NSInteger itemIndex = row - 1;

    // get edited description
    NSString *newDescription = textField.text;

    // update the ListItem
    SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];
    ListItem *listItem = sortedList.list[(NSUInteger) itemIndex];
    listItem.name = newDescription;

    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
}

- (void) resetActiveIndexPath:(NSIndexPath *)indexPath
{
    if ([self.activeIndexPath isEqual:indexPath]) {
        self.activeIndexPath = nil;
    }
}


- (IBAction)itemDescriptionEditDidEnd:(UITextField *)textField
{
    NSInteger section = textField.tag / 100;
    NSInteger row = textField.tag % 100;

    // clear active cell edit
    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];

    if (_cachedTaskLists
            && section < self.cachedTaskLists.count) {
        SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];
        if (row < sortedList.maxRow) {
            // reload row
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            NSArray *indexPaths = @[indexPath];
            [self.tableView reloadRowsAtIndexPaths:indexPaths
                                  withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (IBAction)descriptionDidEndOnExit:(UITextField *)sender
{
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    textField.selected = NO;
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Table view data source


- (NSArray *)cachedTaskLists
{
    if (!_cachedTaskLists) {
        _cachedTaskLists = self.incident.sortedTaskLists;
    }
    return _cachedTaskLists;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger numberOfSections = self.cachedTaskLists.count;
    if (numberOfSections == 0)
        numberOfSections = 1;
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];

    if (sortedList && sortedList.expand){
        // Return the number of rows in the section + header row
        numberOfRows = sortedList.list.count + 1;
    }
    else {
        numberOfRows = 1;
    }

    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellIdentifier = @"TaskViewCell";
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;

    if (row == 0) {
        cellIdentifier = @"TaskViewHeaderCell";
    }

    if (self.activeIndexPath
            && self.activeIndexPath.section == section
            && self.activeIndexPath.row == row) {
        cellIdentifier = @"EditTaskViewCell";
    }

    NSInteger cellTag;
    cellTag = section * 100 + row;

    if (self.cachedTaskLists.count == 0) {
        if (section == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            cell.textLabel.text = @"Empty List";
        }
    }
    else {
        SortedTaskList *sortedList = self.cachedTaskLists[(NSUInteger) section];

        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;

        if (row == 0) {
            // get header
            NSString *format = @"< %@ >"; // list is not expanded format
            if (sortedList.expand) {
                format = @"> %@ <";   // list is expanded format
            }
            cell.textLabel.text = [NSString stringWithFormat:format, sortedList.name];
            cell.backgroundColor = self.headerBackgroundColor;
            cell.textLabel.backgroundColor = self.headerBackgroundColor;
            if (sortedList.isCompleted) {
                cell.accessoryType = cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        else {
            NSInteger listIndex = row - 1;
            if (sortedList.list.count)  {
                // set text
                ListItem *listItem = sortedList.list[(NSUInteger) listIndex];
                if ([cell isKindOfClass:[EditTaskViewCell class] ]) {
                    EditTaskViewCell *editCell = (EditTaskViewCell *) cell;
                    editCell.editTaskDescTextField.text = listItem.name;
                    editCell.editTaskDescTextField.tag = cellTag;
                    editCell.editTaskDescTextField.delegate = self;
                    editCell.editTaskDescTextField.selected = YES;
                    editCell.editTaskDescTextField.backgroundColor = self.descriptionBackgroundColor;
                    editCell.editTaskDescTextField.clearsOnBeginEditing = NO;
                    editCell.editTaskDescTextField.clearButtonMode = UITextFieldViewModeAlways;
                    [editCell.editTaskDescTextField becomeFirstResponder];
                }
                else {
                    cell.textLabel.text = listItem.name;
                    // set check mark state
                    if (listItem.isChecked)   {
                        cell.accessoryType = cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    }
                }
            }
        }
    }
    cell.textLabel.font = [UIFont fontWithName:@".HelveticaNeueUI-Bold" size:12];
    cell.tag = cellTag;
    return cell;
}

// support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    BOOL isEditable = YES;

    // get group associated with the indexPath
    SortedTaskList *sortedTaskList = self.cachedTaskLists[(NSUInteger)indexPath.section];
    if (sortedTaskList.expand) {
        isEditable = YES;
        if ([indexPath isEqual:self.activeIndexPath])
            isEditable = NO;
    }
    else {
        // only groups can be deleted when list is not expanded
        if (indexPath.row > 0) {
            isEditable = NO;
        }
    }
    return isEditable;
}

// deletes the specified ListItem or List from the data model
- (void)deleteListItem:(NSIndexPath *)path
{
    NSUInteger section = (NSUInteger) path.section;
    NSUInteger row = (NSUInteger) path.row;
    
    SortedTaskList *sortedTaskList = self.cachedTaskLists[section];
    if (sortedTaskList) {
        IncidentTaskList *list = (IncidentTaskList *) sortedTaskList.managedListObject;
        if (list) {
            if (row == 0) {
                // remove a list
                NSSet *taskLists = self.incident.taskLists;
                for (IncidentTaskList *taskList in taskLists) {
                    if ([list isEqual:taskList]) {
                        [self.incident removeTaskListsObject: list];
                        list.fromDepartmentList = nil;
                        [self.context deleteObject:list];
                        break;
                    }
                }
            }
            else {
                // remove a list item
                NSUInteger itemIndex = row - 1;
                ListItem *listItem = sortedTaskList.list[itemIndex];
                [list removeItemsObject:listItem];
                [self.context deleteObject:listItem];
            }
        }
    }


    // rebuild the sorted task lists
    self.cachedTaskLists = nil;

    // redisplay
    [self.tableView reloadData];
}

// Support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *itemIndexPath;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteListItem:indexPath];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSUInteger section = (NSUInteger) indexPath.section;
        SortedTaskList *sortedList = self.cachedTaskLists[section];

        if (indexPath.row == 0) {
            // insert item in list

            NSDecimalNumber *newSortOrderValue = nil;
            NSUInteger itemIndex = (NSUInteger)sortedList.maxRow;

                newSortOrderValue = [NSDecimalNumber
                        decimalNumberWithMantissa:(NSUInteger)sortedList.maxRow
                                         exponent:1
                                       isNegative:NO];


            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            ListItem *newListItem = [ListItem createItemNamed:@"new list item" withValue:itemIndex withContext:sortedList.managedListObject.managedObjectContext];
            newListItem.sortOrderValue = newSortOrderValue;

            // add new item to sorted list of items
            NSMutableArray *newList = sortedList.list.mutableCopy;
            [newList addObject:newListItem];
            sortedList.list = newList.copy;

            // add new item to data model list
            List *list = sortedList.managedListObject;
            [list addItemsObject:newListItem];

            // add table view cell
            itemIndexPath = [NSIndexPath indexPathForRow:itemIndex inSection:section];
            NSArray *indexPaths = @[itemIndexPath];

            // set the row for editing to get the description
            self.activeIndexPath = itemIndexPath;

            // insert the row
            [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];

            [tableView selectRowAtIndexPath:itemIndexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
        }
    }
}

// support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSInteger fromSection = fromIndexPath.section;
    NSInteger fromRow = fromIndexPath.row;
    NSInteger toSection = toIndexPath.section;
    NSInteger toRow = toIndexPath.row;
    BOOL isSectionHeader = NO;

    if (fromRow > 0) {
        fromRow--;
    }
    else {
        isSectionHeader = YES;
    }
    if (toRow > 0) {
        toRow--;
    }

    if ([fromIndexPath compare:toIndexPath] != NSOrderedSame) {
        NSDecimalNumber *one = [NSDecimalNumber one];
        SortedTaskList *sortedTaskList = (SortedTaskList *)self.cachedTaskLists[(NSUInteger)fromSection];
        List *list = sortedTaskList.managedListObject;
        SortedTaskList *insertionSortedTaskList = (SortedTaskList *)self.cachedTaskLists[(NSUInteger) toSection];
        List *insertionList = insertionSortedTaskList.managedListObject;
        NSMutableIndexSet *indexSet = nil;

        if (isSectionHeader) {
            // adjust sort order of the entire list
            NSDecimalNumber *insertionListValue = insertionList.sortOrderValue;
            if (fromSection > toSection) {
                list.sortOrderValue = [insertionListValue decimalNumberBySubtracting:one];
            }
            else {
                list.sortOrderValue = [insertionListValue decimalNumberByAdding:one];
            }
        }
        else {
            // adjust sort order of the item
            indexSet = [NSMutableIndexSet indexSetWithIndex:(NSUInteger)fromSection];
            ListItem *listItem = (ListItem *)sortedTaskList.list[(NSUInteger)fromRow];
            NSDecimalNumber *sortOrderValue = nil;

            if (insertionSortedTaskList.maxRow == 1 || toIndexPath.row == insertionSortedTaskList.maxRow){
                sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:(NSUInteger)sortedTaskList.maxRow
                                                                   exponent:1 isNegative:NO];
            }
            else {
                ListItem *insertionItem = (ListItem *)insertionSortedTaskList.list[(NSUInteger) toRow];
                NSDecimalNumber *insertionListValue = insertionItem.sortOrderValue;
                if (fromSection == toSection && fromRow < toRow) {
                    sortOrderValue = [insertionListValue decimalNumberByAdding:one];
                }
                else {
                    sortOrderValue = [insertionListValue decimalNumberBySubtracting:one];
                }
            }
            listItem.sortOrderValue = sortOrderValue;
            if (fromSection != toSection)
            {
                // move item to different list
                [list removeItemsObject:listItem];
                [insertionList addItemsObject:listItem];
                insertionList.expanded = @YES;
                [indexSet addIndex:(NSUInteger)toSection];
            }
        }
        self.cachedTaskLists = nil;
        [self.tableView reloadData];
    }
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isMovable = YES;
    SortedTaskList *sortedTaskList = self.cachedTaskLists[(NSUInteger)indexPath.section];
    if (indexPath.row == 0 && sortedTaskList.expand) {
        // can not move list while it is expanded
        isMovable = NO;
    }
        // Return NO if you do not want the item to be re-orderable.
    return isMovable;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get group associated with the indexPath
    SortedTaskList *sortedTaskList = self.cachedTaskLists[(NSUInteger)indexPath.section];
    if (sortedTaskList.expand) {
        if (indexPath.row == 0) {
            return UITableViewCellEditingStyleInsert;  // allow insert of task items when expanded
        }
        else {
            return UITableViewCellEditingStyleDelete; // allow delete of task items when expanded
        }
    }
    else {
        // not expanded
        if (indexPath.row == 0) {
            return UITableViewCellEditingStyleDelete; //allow delete of entire list
        }
        else {
            return UITableViewCellEditingStyleNone;
        }
    }
}

#pragma mark - Task Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    SortedTaskList *sortedTaskList = self.cachedTaskLists[section];
    List *taskList = sortedTaskList.managedListObject;

    NSMutableArray *itemPaths = [NSMutableArray arrayWithCapacity:3];

    // reload and close the description cell being edited
    if (self.activeIndexPath) {
        [itemPaths addObject:self.activeIndexPath];
    }

    if (row == 0) {
        // header selected -- toggle the list expansion state
        BOOL expanded = sortedTaskList.expand;
        sortedTaskList.expand = !expanded;
        taskList.expanded = @(!expanded);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndex:(NSUInteger)section];
        [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else {
        // new row selected - set it for editing
        self.activeIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [itemPaths addObject:self.activeIndexPath];
    }

    // reload the cells being edited or selected if not in a section being collapsed
    BOOL reloadRows = NO;
    if (itemPaths.count > 0) {
        reloadRows = YES;
        if (row == 0
                && self.activeIndexPath
                && self.activeIndexPath.section == section) {
            // don't refresh the editable rows since the section was collapsed
            reloadRows = NO;
        }
    }

    if (reloadRows) {
        [tableView reloadRowsAtIndexPaths:itemPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}



#pragma mark - Navigation

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    return action == @selector(unwindFromAddList:);
}

- (IBAction)unwindFromAddList:(UIStoryboardSegue *)unwindSegue
{
    // context can cease to exist if iCloud store is changed during the segue
    if (self.context) {
        if ([unwindSegue.identifier isEqualToString:@"unwindFromAddList"])
        {
            AddListFromDepartmentVC *vc = (AddListFromDepartmentVC *)unwindSegue.sourceViewController;
            if (vc.selectedDepartmentLists) {
                NSUInteger sortValue = self.cachedTaskLists.count + 1;
                for (DepartmentTaskList *departmentTaskList in vc.selectedDepartmentLists) {
                    if (departmentTaskList.listName) {
                        IncidentTaskList *incidentTaskList = [IncidentTaskList fromDepartmentList:departmentTaskList withContext:self.context];
                        [incidentTaskList setSortValue:[NSDecimalNumber decimalNumberWithMantissa:sortValue++ exponent:1 isNegative:NO]];
                        incidentTaskList.forIncident = self.incident;
                    }
                }
                self.cachedTaskLists = nil; // force reload of the information
                [self.tableView reloadData];
            }
        }
    }
}

//
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].

    if ([segue.identifier isEqualToString:@"addListToIncidentSegue"]) {
        AddListFromDepartmentVC *vc = segue.destinationViewController;

        // Pass the fire department object to the new view controller.
        vc.fireDepartment = self.incident.forDepartment;
        vc.incident = self.incident;
    }

}


@end
