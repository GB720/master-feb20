//
//  LogViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Incident;
@class FireManViewController;

@interface LogViewController : UITableViewController <MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) Incident *incident;
@property (weak, nonatomic) FireManViewController *callingViewController;
@end
