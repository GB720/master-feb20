//
//  FireunitListSegue.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/16/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManagementViewCellSegue.h"


@interface FireunitListSegue : ManagementViewCellSegue
@property (nonatomic, weak) UIView *anchorView;
@end
