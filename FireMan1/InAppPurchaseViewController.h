//
//  InAppPurchaseViewController.h
//  TotalCommand
//
//  Created by user136147 on 4/5/18.
//  Copyright © 2018 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface InAppPurchaseViewController : UITableViewController < SKProductsRequestDelegate, SKPaymentTransactionObserver>
@property(nonatomic, strong) SKProductsRequest *request;
@property(nonatomic, strong) NSArray <SKProduct *> *products;
@end
