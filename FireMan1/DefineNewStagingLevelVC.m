//
//  DefineNewStagingLevelVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "DefineNewStagingLevelVC.h"
#import "StagingGroup.h"
#import "FireDepartment.h"
#import "DefaultStage.h"
#import "UIColor+EmerResStrat.h"

@interface DefineNewStagingLevelVC ()
@property(nonatomic, strong) UIColor *defaultColor;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;

@property(nonatomic, copy) NSString *originalName;

- (IBAction)nameEntered:(UITextField *)sender;
- (IBAction)timeRequiredEntered:(UITextField *)sender;

@end

@implementation DefineNewStagingLevelVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _defaultColor = nil;
    if (_editingStagingGroup) {
        self.originalName = self.stagingGroupName;
        self.nameTextField.text = self.stagingGroupName;
        self.timeTextField.text = [NSString stringWithFormat:@"%li",(long)self.timeRequiredInStage.integerValue];
    }
    self.tableView.accessibilityIdentifier = @"defineNewStagingLevelTable";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![self isUnique:self.stagingGroupName]) {
        self.stagingGroupName = nil;
    }
}

#pragma mark - Data field notifications

- (IBAction)nameEntered:(UITextField *)sender
{
    NSString *newName = sender.text;
    if (![self isUnique:newName])  {
        if (!self.defaultColor) {
            self.defaultColor = sender.backgroundColor;            
        }
        sender.backgroundColor = [UIColor ersRedColor];
    }
    else {
        sender.backgroundColor = self.defaultColor;
    }
    _stagingGroupName = newName;
}

// check that the stage name is unique
- (BOOL)isUnique:(NSString *)name
{
    if (self.editingStagingGroup && [name isEqualToString:self.originalName]) {
        return YES;     // treat original name as unique when editing
    }
    for (DefaultStage *defaultStage in self.fireDepartment.predefinedStagingGroups){
        if ([name isEqualToString:defaultStage.name]) {
            return NO;
        }
    }
    return YES;
}

- (IBAction)timeRequiredEntered:(UITextField *)sender
{
    NSInteger timeRequired = sender.text.integerValue;
    _timeRequiredInStage = @(timeRequired);
}
@end
