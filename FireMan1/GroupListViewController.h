//
//  GroupListViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 4/25/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FireDepartment;
@class ManagementViewCell;
@class Incident;
@class ListItem;

@interface GroupListViewController : UIViewController
        <UITableViewDelegate,
        UITableViewDataSource>
@property (strong, nonatomic) FireDepartment *fireDepartment;
@property(nonatomic, weak) ManagementViewCell *currentCell;
@property (nonatomic, weak) ListItem *selectedDivision;
@property (nonatomic, strong) NSString *dummyName;
@property (nonatomic, strong) NSString *selectedGroupName;
@property (nonatomic) NSInteger groupIndex;
@property(nonatomic, weak) Incident *incident;
- (void) createNewName: (NSString *) newName;
- (IBAction)unwindToGroupList:(UIStoryboardSegue *)unwindSegue;
@end
