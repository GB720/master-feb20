//
//  IcSelectionViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 8/7/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Incident;
@class FireUnit;

@interface IcSelectionViewController : UITableViewController

@property(nonatomic, strong) Incident *incident;
@property(nonatomic, strong) FireUnit *selectedFireUnit;
@end
