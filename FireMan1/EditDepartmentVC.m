//
//  EditDepartmentVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 1/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "EditDepartmentVC.h"
#import "DepartmentMasterVC.h"
#import "FireDepartment+Create.h"
#import "EditableItem.h"
#import "DepartmentDetailVC.h"
#import "UIColor+EmerResStrat.h"


@interface EditDepartmentVC ()
@property (strong, nonatomic) IBOutlet UIView *leftContainerView;
@property (strong, nonatomic) IBOutlet UIView *rightContainerView;
@property (nonatomic, strong) NSMutableArray *masterStateStack;
@property (nonatomic, strong) id <FDedit> rightSideItem;
@property (nonatomic, strong) NSArray *leftSideEditableItems;
@property (nonatomic) NSInteger leftSideIndex;
@property (nonatomic) BOOL isleftSideAutoSelect;
@property (nonatomic, strong) NSString *leftSideTitle;
@end

@implementation EditDepartmentVC {
    DepartmentMasterVC *leftSideVC;
    DepartmentDetailVC *rightSideVC;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Edit Department";
    [self.view setBackgroundColor:[UIColor ersViewBackgroundColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    // do nothing
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   // assert (![segue.identifier isEqualToString:@"rightViewSegue"]);

    if ([segue.identifier isEqualToString:@"leftViewSegue"]) {
        UINavigationController *navVC = segue.destinationViewController;
        leftSideVC = (DepartmentMasterVC *) navVC.topViewController;
        assert ([leftSideVC conformsToProtocol:@protocol(EditorView)]);
        leftSideVC.editableItems = self.fireDepartment.editableItems;
        leftSideVC.title = self.fireDepartment.name;
        leftSideVC.isAutoSelect = NO;
        leftSideVC.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"rightViewSegue"]) {
        rightSideVC = (DepartmentDetailVC *) segue.destinationViewController;
        assert ([rightSideVC conformsToProtocol:@protocol(EditorView)]);
        rightSideVC.masterItem = _rightSideItem;
        rightSideVC.delegate = self;
    }

}


#pragma mark - master view stack
-(NSMutableArray *)masterStateStack
{
    if (!_masterStateStack) {
        _masterStateStack = [NSMutableArray arrayWithCapacity:10];
    }
    return _masterStateStack;
}

-(void) pushState:(id) state
{
    // save the master parameters of interest
    [self.masterStateStack addObject:state];
}

- (id) popState
{
    id lastObj = self.masterStateStack.lastObject;
    if (lastObj) {
        [self.masterStateStack removeLastObject];
    }
    return lastObj;
}

- (void)rightSideItem:(id <FDedit>) item
{
    // indicate change of view coming
    [rightSideVC closingView];

    // set new view items
    _rightSideItem = item;
    rightSideVC.masterItem = item;

    // update right side view
    [rightSideVC loadNewView];
}

-(void) leftSideEditableItems:(NSArray *)items index:(NSInteger)leftSideIndex
                        title:(NSString *)leftSideTitle isAutoSelect:(BOOL) isLeftSideAutoSelect;
{
    [leftSideVC setViewItems:items];
    [leftSideVC setViewIndex:leftSideIndex];
    [leftSideVC setViewTitle:leftSideTitle];
    [leftSideVC setIsAutoSelect:isLeftSideAutoSelect];
    [leftSideVC loadNewView];
}

- (NSInteger) stackSize
{
    return self.masterStateStack.count;
}

- (void)didCloseViews
{
    [rightSideVC closingView];
    [leftSideVC closingView];

    rightSideVC.masterItem = nil;
    [leftSideVC setViewItems:nil];
}


- (void) closingViews
{
    [rightSideVC closingView];
    [leftSideVC closingView];
}

- (void)reloadViews
{
    [rightSideVC loadNewView];
    [leftSideVC loadNewView];
}

- (void) masterItemDidUpdate
{
    [leftSideVC selectedItemDidUpdate];
}

- (void) backButton
{
    [leftSideVC showPreviousView];
}
@end
