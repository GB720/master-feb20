//
// Created by Gerald Boyd on 4/24/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (EmerResStrat)
+ (UIColor *)ersRedColor;
+ (UIColor *)ersGoldColor;
+ (UIColor *)ersGreenColor;
+ (UIColor *)ersBlueColor;
+ (UIColor *)ersGrayColor;
+ (UIColor *)ersGreyColor;
+ (UIColor *)ersPinkColor;

+ (UIColor *)ersTextFieldBackgroundColor;

+ (UIColor *)ersListHeaderColor;

+ (UIColor *)ersActiveButtonColor;

+ (UIColor *)ersViewBackgroundColor;

+ (UIColor *)ersToolbarBackgroundColor;

+ (UIColor *)ersLightGoldColor;

+ (UIColor *)ersLightGreenColor;

+ (UIColor *)ersCyanColor;
+ (UIColor *)ersBlackColor;
@end