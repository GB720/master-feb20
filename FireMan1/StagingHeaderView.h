//
//  StagingHeaderView.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/14/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StagingHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIButton *StageNameButton;

@end
