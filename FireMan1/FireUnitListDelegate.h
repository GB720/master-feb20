//
//  FireUnitListDelegate.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/8/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FireUnitListDelegate <NSObject>
- (void) setIsSegueActive:(BOOL) isActive;
- (UIColor *) colorForCoordinatorIsSet;
- (void) setNeedsRefreshView;
- (UICollectionView *) managementView;
@end
