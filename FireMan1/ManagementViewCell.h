//
//  ManagementViewCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 3/29/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActiveDivisionCollectionView.h"
#import "DragDropManager.h"

@interface ManagementViewCell : UICollectionViewCell
- (void)prepareForReuse;

@property (weak, nonatomic) IBOutlet UITextField *GroupNameTextBox;
@property (weak, nonatomic) IBOutlet UIButton *buttonLabel;
@property (weak, nonatomic) IBOutlet UIButton *coordinatingUnitButton;

@property (strong, nonatomic) IBOutlet ActiveDivisionCollectionView *activeDivisionView;
@property (strong, nonatomic) NSString *taskCurrentValue;
@end
