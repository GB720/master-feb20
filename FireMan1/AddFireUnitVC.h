//
//  AddFireUnitVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FireUnitSelectionVC;
@class UnitSelectionHeader;

@interface AddFireUnitVC : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) UIView *anchor;
@property (nonatomic) NSInteger section ;
@property(nonatomic, strong) NSString *unitName;
@end
