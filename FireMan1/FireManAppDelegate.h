//
//  FireManAppDelegate.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Ensembles/Ensembles.h>
#import "model/Device.h"


@interface FireManAppDelegate : UIResponder <UIApplicationDelegate, CDEPersistentStoreEnsembleDelegate>
- (void)elapsedTimer:(NSTimeInterval)elapsedTime;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;
// @property (nonatomic, strong) CDEPersistentStoreEnsemble *ensemble;
@property(nonatomic, readonly) BOOL databaseReady;

@end
