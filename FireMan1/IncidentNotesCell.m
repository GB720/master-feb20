//
//  IncidentNotesCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 7/29/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IncidentNotesCell.h"

@implementation IncidentNotesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
