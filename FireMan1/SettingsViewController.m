//
//  SettingsViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 8/9/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
#import "CurrentUser.h"
#import "UIColor+EmerResStrat.h"
#import "User+Create.h"
#import "UserIdentificationViewController.h"


static NSInteger TOTAL_ROWS = 12;

static NSInteger ROW_SIZE = 44;
static NSInteger TIMING_SECTION_SIZE = 2;
static NSInteger TIMING_SECTION = 0;
static NSInteger SET_DEFAULTS_SECTION_SIZE = 3;
static NSInteger SET_DEFAULTS_SECTION = 1;
static NSInteger USER_ID_SECTION_SIZE = 1;
static NSInteger USER_ID_SECTION = 2;
static NSInteger ABOUT_SECTION_SIZE = 2;
static NSInteger ABOUT_SECTION = 3;
static NSInteger NUMBER_OF_SECTIONS = 4;
static NSInteger WIDTH = 500;

@interface SettingsViewController ();

// active time limit
@property (weak, nonatomic) IBOutlet UISlider *activeTimeSlider;

- (NSInteger)numberOfSections;
@property (nonatomic) BOOL  isNameChanging;

- (IBAction)activeTimeSliderChange:(UISlider *)sender;
@property (weak, nonatomic) IBOutlet UITextField *activeLimitText;
@property (weak, nonatomic) IBOutlet UITextField *activeUserName;
- (IBAction)activeLimitEdit:(UITextField *)sender;


// par time limit
@property (weak, nonatomic) IBOutlet UISlider *parTimeSlider;
- (IBAction)parTimeSliderChange:(UISlider *)sender;
@property (weak, nonatomic) IBOutlet UITextField *parTimeText;
- (IBAction)parTimeEdit:(UITextField *)sender;

// defaults for future incidents
@property (weak, nonatomic) IBOutlet UISwitch *useCurrentListSettingsAsDefault;
- (IBAction)switchedCurrentListSettingsAsDefault:(UISwitch *)sender;
- (IBAction)stagingDefaultSwitched:(UISwitch *)sender;
- (IBAction)timeDefaultSwitched:(UISwitch *)sender;

@property (strong, nonatomic) IBOutlet UILabel *userNameField;
@property (strong, nonatomic) CurrentUser *currentUser;
- (IBAction)nameEdit:(UITextField *)sender;

@property (strong, nonatomic) IBOutlet UITextField *releaseNumber;

@property (strong, nonatomic) IBOutlet UITextField *buildNumber;

@property(nonatomic) BOOL unwindSegueNeeded;



@end

@implementation SettingsViewController {
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.activeLimitText.delegate = self;
    self.parTimeText.delegate = self;
    self.activeUserName.delegate = self;
    self.isNameChanging = NO;
    self.unwindSegueNeeded = YES;
    self.currentUser = [CurrentUser sharedSingletonObject];
    [self configureView];
}

- (void) viewWillDisappear:(BOOL)animated
{
    //if (self.unwindSegueNeeded) {
    //    [self performSegueWithIdentifier:@"doneFromSettingsView" sender:self];
    // }
    [super viewWillDisappear:animated];
}

- (void)configureView
{
    self.activeUserName.text = [NSString stringWithFormat:@"%@", self.user.name];
    self.activeLimitText.text = [NSString stringWithFormat:@"%2i",self.activeLimit.intValue];
    self.activeTimeSlider.value = self.activeLimit.floatValue;
    self.parTimeText.text = [NSString stringWithFormat:@"%2i",self.parLimit.intValue];
    self.parTimeSlider.value = self.parLimit.floatValue;
    self.releaseNumber.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.buildNumber.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    self.useCurrentListSettingsAsDefault.on = NO;
    // self.userNameField.text = [NSString stringWithFormat:@"User: %@", self.user.name];
    CGFloat len = ((TOTAL_ROWS + 1) * ROW_SIZE) + self.numberOfSections * ROW_SIZE;
    CGSize size = CGSizeMake(WIDTH, len);
    self.preferredContentSize = size;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSections
{
    // return self.currentUser.isSingleUserMode ? NUMBER_OF_SECTIONS - 1 : NUMBER_OF_SECTIONS;
    return NUMBER_OF_SECTIONS;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == TIMING_SECTION) {
        return TIMING_SECTION_SIZE;
    }
    else if (section == SET_DEFAULTS_SECTION) {
        return SET_DEFAULTS_SECTION_SIZE;
    }
    else if (section == USER_ID_SECTION) {
        return USER_ID_SECTION_SIZE;
    }
    else if (section == ABOUT_SECTION) {
        return ABOUT_SECTION_SIZE;
    }
    return 0;
}

#pragma mark - segue

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString:@"setNewUserSegue"]) {
        UserIdentificationViewController *vc = segue.destinationViewController;
        vc.fireDepartment = self.user.inDepartment;
    }
    else {
        if ([segue.identifier isEqualToString:@"doneFromSettingsView"]) {
            if (self.isNameChanging) {
                [self nameEdit:self.activeUserName];
            }
        }
        self.unwindSegueNeeded = NO;
    }
}

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    return action == @selector(unwindUserIdentification:);
}

- (IBAction)unwindUserIdentification:(UIStoryboardSegue *)unwindSegue
{
    if ([unwindSegue.identifier isEqualToString:@"unwindFromUserIdentification"]) {
        UserIdentificationViewController *vc = unwindSegue.sourceViewController;
        if (vc.isUpdate && vc.user) {
            self.user = vc.user;
            self.isUpdate = YES;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self configureView];
        }
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

#pragma mark - Table View delegate

// Called before the user changes the selection. Return a new indexPath, or nil, to change the proposed selection.
//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    NSInteger section = indexPath.section;
//    if (section == SET_DEFAULTS_SECTION) {
//        if ([self.currentUser hasPermission:FDAdministrator]) {
//            return indexPath;
//        }
//        else {
//            // show alert message?
//            return nil;
//        }
//    }
//    return indexPath;
//}

#pragma mark - notifications and outlets

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length > 0) {
        [textField resignFirstResponder];
        return YES;
    }
    else {
        textField.backgroundColor = [UIColor ersRedColor];
    }
    return NO;
}


- (IBAction)activeTimeSliderChange:(UISlider *)sender
{
    if (self.currentUser.isCoordinator) {
        NSNumber *value = @(sender.value);
        NSString *sliderValue = [NSString stringWithFormat:@"%2i",value.intValue];
        self.activeLimitText.text = sliderValue;
        self.activeLimit = value;
        self.isUpdate = YES;
    }
    else {
        // not authorized to change times for incident
        [self accessRestriction:self.activeLimitText slider:sender toValue:self.activeLimit];
    }
}


- (IBAction)activeLimitEdit:(UITextField *)sender
{
    if (self.currentUser.isCoordinator) {
        float floatValue = sender.text.floatValue;
        self.activeTimeSlider.value = floatValue;
        self.activeLimit = @(floatValue);
        self.isUpdate = YES;
    }
    else {
        // not authorized to change times for incident
        [self accessRestriction:sender slider:self.activeTimeSlider toValue:self.activeLimit];
    }
}

- (IBAction)parTimeSliderChange:(UISlider *)sender
{
    if (self.currentUser.isCoordinator) {
        NSNumber *value = @(sender.value);
        NSString *sliderValue = [NSString stringWithFormat:@"%2i",value.intValue];
        self.parTimeText.text = sliderValue;
        self.parLimit = value;
        self.isUpdate = YES;
    }
    else {
        // not authorized to change times for incident
        [self accessRestriction:self.parTimeText slider:sender toValue:self.parLimit];
    }
}

- (IBAction)parTimeEdit:(UITextField *)sender
{
    if (self.currentUser.isCoordinator) {
        float floatValue = sender.text.floatValue;
        self.parTimeSlider.value = floatValue;
        self.parLimit = @(floatValue);
        self.isUpdate = YES;
    }
    else {
        // not authorized to change times for incident
        [self accessRestriction:sender slider:self.parTimeSlider toValue:self.parLimit];
    }

}

- (IBAction)stagingDefaultSwitched:(UISwitch *)sender
{
    self.isUseStagingAsDefault = [self checkDefaultSwitchAllowed:sender];
}

- (IBAction)timeDefaultSwitched:(UISwitch *)sender
{
    self.isUseTimesAsDefault = [self checkDefaultSwitchAllowed:sender];
}

- (IBAction)switchedCurrentListSettingsAsDefault:(UISwitch *)sender
{
    self.isUseListSetupAsDefault = [self checkDefaultSwitchAllowed:sender];
}

- (BOOL) checkDefaultSwitchAllowed:(UISwitch *) sender
{
    BOOL isAllowed = NO;
    if (sender.on) {
        if ([self.currentUser hasPermission:FDAdministrator]) {
            self.isUpdate = YES;
            isAllowed = YES;
        }
        else {
            // not admin - not authorized
            isAllowed = NO;
            sender.on = NO;
            [self adminAlert];
        }
    }
    return isAllowed;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
                        reason:(UITextFieldDidEndEditingReason) reason
{
    if (textField == self.activeUserName) {
        if (reason == UITextFieldDidEndEditingReasonCommitted) {
            [self createNewUser:textField];
        }
    }
}

- (void) createNewUser:(UITextField *) sender
{
    NSString *nameText = sender.text;
    if (nameText && nameText.length > 0) {
        if (self.user) {
            if (![self.user.name isEqualToString:nameText]) {
                FireDepartment *fireDepartment = self.device.owningDepartment;
                if (fireDepartment) {
                    User *user = [self getOrMakeUser:nameText context:fireDepartment.managedObjectContext];
                    if (user) {
                        self.user = user;
                        self.isUpdate = YES;
                    }
                }
            }
        }
    }
}

- (IBAction)nameChange:(UITextField *)sender
{
    self.isNameChanging = YES;
}

- (IBAction)nameEdit:(UITextField *)sender
{
    [self createNewUser:sender];
}

- (User *) getOrMakeUser: (NSString *)userName
                 context:(NSManagedObjectContext *)context
{
    User *user = [User userWithName:userName onDevice:self.device.deviceId context:context];
    if (!user) {
        Role *adminAndIcRole = [Role roleWithName:@"AdminAndIC" context:context];
        if (!adminAndIcRole) {
            adminAndIcRole = [Role createWithName:@"AdminAndIC" withPermission:FDAdminAndManager context:context];
        }
        user = [adminAndIcRole createUserWithName:userName onDevice:self.device.deviceId];
    }
    if (!user) {
        // no new user, use current
        user = _currentUser.userEntity;
    }
    else {
        user.name = userName;
        user.id = userName;
        user.id2 = userName;
    }
    return user;
}

#pragma mark - Alert msg
- (void) accessRestriction:(UITextField *) textField slider:(UISlider *)slider toValue:(NSNumber *)number
{
    // put up an alert message
    [self coordinatorAlert];
    
    // reset value
    textField.text = [NSString stringWithFormat:@"%2i",number.intValue];
    slider.value = number.floatValue;
}

- (void) coordinatorAlert
{
    [self alertMsg:@"Only the Coordinator for this incident may change this value"
        alertTitle:@"Incident Commander Only"];
}

- (void) adminAlert
{
    [self alertMsg:@"Only Administrators may set this value"
        alertTitle:@"Administrator Only"];
}

- (void) alertMsg:(NSString *) msg alertTitle:(NSString *)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertController* __weak weakAlert = alert;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
