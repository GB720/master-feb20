//
//  ShowLogSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "ShowLogSegue.h"
#import "FireManViewController.h"
#import "LogViewController.h"
#import "ManagementCollectionView.h"

@implementation ShowLogSegue

- (void)perform
{

    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect
    // Set the size of the popover because it will be max sized otherwise

    FireManViewController *sourceController = (FireManViewController *)self.sourceViewController;
    LogViewController *destinationController = (LogViewController *)((UINavigationController *)self.destinationViewController).topViewController;
    destinationController.callingViewController = sourceController;
    
    // ---------
    // present the controller
    destinationController.modalPresentationStyle = UIModalPresentationPopover;
    [sourceController presentViewController:destinationController animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [destinationController popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    // in case we don't have a bar button as reference
// CGRect rect = sourceController.mainToolbar.viewForBaselineLayout.frame;
    
    CGRect rect =  sourceController.mainToolbar.viewForFirstBaselineLayout.frame;
    rect.size.width = 20;

    popController.sourceView = sourceController.mainToolbar.viewForFirstBaselineLayout;
    popController.sourceRect = rect;

}

@end
