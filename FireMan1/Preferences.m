//
// Created by Gerald Boyd on 7/9/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Preferences.h"


@implementation Preferences

- (NSString *)valueFor:(NSString *)preferenceName
{
    return (self.preferenceDictionary)[preferenceName];
}

- (NSUInteger)integerValueFor:(NSString *)preferenceName
{
    NSString *value = [self valueFor:preferenceName];
    return (NSUInteger) value.integerValue;
}

- (NSNumber *) numberFor:(NSString *) preferenceName
{
    NSString *value = [self valueFor:preferenceName];
    return @(value.integerValue);
}

- (Preferences *)initWith:(NSDictionary *)preferences
{
    self = [super init];
    if (self) {
        _preferenceDictionary = preferences;
    }
    return self;
}

+ (Preferences *) createWith: (NSSet *) preferences
{
    NSMutableDictionary *prefDictionary = [[NSMutableDictionary alloc] initWithCapacity:20];
    for (Preference *p in preferences) {
        prefDictionary[p.name] = p.value;
    }
    Preferences *preferencesObject = [[Preferences alloc ] initWith:prefDictionary.copy];

    return preferencesObject;
}

@end
