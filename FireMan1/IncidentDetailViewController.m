//
//  IncidentDetailViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 7/29/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IncidentDetailViewController.h"
#import "IncidentNameCell.h"
#import "UIColor+EmerResStrat.h"

@interface IncidentDetailViewController () <UITextFieldDelegate, UITextViewDelegate>

- (IBAction)nameTextChanged:(UITextField *)sender;


@end

@implementation IncidentDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.incidentNameField.delegate = self;
    self.incidentNotesField.delegate = self;
    self.notesChanged = NO;
    self.nameChanged = NO;
    self.tableView.accessibilityIdentifier = @"incidentDetailView";
    [self configureView];
    
}

- (void)configureView
{
    self.incidentNotesField.text = self.note;
    self.incidentDateField.text = self.startDate;
    self.incidentNameField.text = self.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}


#pragma mark - Table view delegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row != 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - field changed notifications

- (IBAction)nameTextChanged:(UITextField *)sender
{
    self.nameChanged = YES;
    self.name = sender.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length > 0) {
        [textField resignFirstResponder];
        return YES;
    }
    else {
        textField.backgroundColor = [UIColor ersRedColor];
    }
    return NO;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.notesChanged = YES;
    self.note = textView.text;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.notesChanged = YES;
    self.note = textView.text;
}


@end
