//
//  MessageViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 7/23/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageViewController : UIViewController
@property (nonatomic, strong) NSString *message;
@end
