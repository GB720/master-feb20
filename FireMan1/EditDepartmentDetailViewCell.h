//
//  EditDepartmentDetailViewCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/7/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditDepartmentDetailViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *itemTextField;
@property (strong, nonatomic) IBOutlet UILabel *itemNameLabel;
@end
