//
//  SettingsViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 8/9/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User+Create.h"
#import "Device.h"

@class Incident, Device;

@interface SettingsViewController : UITableViewController  <UITextFieldDelegate, UITableViewDelegate>

@property (nonatomic) BOOL  isUpdate;
@property (nonatomic, strong) NSString *fireDepartmentName;
@property (nonatomic, strong) Device *device;
@property (nonatomic, strong) NSNumber *activeLimit;
@property (nonatomic, strong) NSNumber *defaultStagingTime;
@property (nonatomic, strong) NSNumber *parLimit;
@property (nonatomic) BOOL isUseListSetupAsDefault;
@property (nonatomic) BOOL isUseStagingAsDefault;
@property (nonatomic) BOOL isUseTimesAsDefault;
@property (nonatomic, strong) User *user;
@end
