//
//  DepartmentMasterVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditContainer.h"
#import "EditorView.h"
#import "DepartmentEdit.h"

@class FireDepartment;
@class EditableItem;

@interface DepartmentMasterVC : DepartmentEdit <UITextFieldDelegate, EditorView>
@property(nonatomic) NSInteger index;
@property(nonatomic) BOOL isAutoSelect;
@property (nonatomic, strong) id <EditContainer> delegate;
- (id)state;
@end
