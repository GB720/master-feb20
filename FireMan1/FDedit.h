//
// Created by Gerald Boyd on 1/23/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FDPermittedValues;

static NSString *const FILLER_EMPTY = @"<???????>";

@protocol FDedit <NSObject>
- (NSString *) itemName;
- (NSArray *) editableItems;
- (id <FDPermittedValues>) permittedValues;
- (NSString *) value;
- (void) save:(NSString *)newValue itemname:(NSString *)itemName;
- (NSDecimalNumber *) sortValue;
- (void) setSortValue:(NSDecimalNumber *) value;
- (id)ownerObject;
@optional
- (BOOL) mayAddEditableItemForItemName: (NSString *) itemName;
- (NSArray *)addEditableItemForItemName:(NSString *)itemName;
- (NSArray *) deleteEditableItem:(id <FDedit>) item forItemName:(NSString *)itemName;
- (BOOL) isReorderable;
- (void) setIsReorderable:(BOOL) value;
- (BOOL) isDeletable;
- (void) setIsDeletable:(BOOL)value;
- (BOOL) hasSortValue;
- (void) setHasSortValue:(BOOL) value;
- (BOOL) mayDeleteEditableItem;
- (BOOL) mayAddEditableItem;
- (void) setEditableItems:(NSArray *)editableItemArray;
- (void) setPermittedValues:(id <FDPermittedValues>)permittedValuesObject;
- (void) updateRelationshipWith:(id <FDedit>)entity;
- (id <FDPermittedValues>) permittedValuesFor:(NSString *) itemName;
- (void) saveValueObject:(id) valueObject forItemName:(NSString *)itemName;
@end