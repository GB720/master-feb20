//
//  SingletonBase.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/19/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//
//  Based on Ron Lisle's SingletonByChoice
//
//  This class should be used as the parent class for singletons.
//
//

#import <Foundation/Foundation.h>

@interface SingletonBase : NSObject

+ (SingletonBase *)sharedSingletonObject;

@end