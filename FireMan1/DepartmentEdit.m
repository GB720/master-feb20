//
// Created by Gerald Boyd on 3/7/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "DepartmentMasterVC.h"
#import "DepartmentDetailVC.h"
#import "FDPermittedValues.h"


@interface DepartmentEdit ()

@end


@implementation DepartmentEdit {

}
// sort supplied set into an ordered array using a default sort key of "sortOrderValue"
- (NSArray *)sort:(NSSet *)set
{
    id <FDedit> item = set.anyObject;
    if ([item respondsToSelector:@selector(hasSortValue)] && item.hasSortValue) {
        if ([item respondsToSelector:@selector(sortValue)]
                && (item.sortValue != [NSDecimalNumber notANumber])) {
            return [self sort:set withKey:@"sortValue"];
        }
    }
    return [self sort:set withKey:@"value"];
}

// sort supplied set into an ordered array using specified sort key
- (NSArray *) sort: (NSSet *)set withKey:(NSString *)key
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                 ascending:YES
                                                comparator:^(id obj1, id obj2)  {return [obj1 compare:obj2];}];
    NSArray *sortDescriptors = @[sortDescriptor];
    return [set sortedArrayUsingDescriptors:sortDescriptors];
}

- (void)rebuiltSortValues:(NSArray *)array
{
    NSUInteger itemNumber = 1;
    for (id <FDedit> item in array) {
        item.sortValue = [NSDecimalNumber decimalNumberWithMantissa:itemNumber exponent:1 isNegative:NO];
        itemNumber++;
    }
}

- (BOOL)value:(NSString *)value uniqueIn:(NSArray *)itemArray
{
    BOOL isUnique = YES;
    for (id <FDedit> eachItem in itemArray) {
        if ([value isEqualToString:eachItem.value]) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (void)reloadTableRow:(NSInteger)row section:(NSInteger)section
{
    NSInteger maxRows = [self.tableView numberOfRowsInSection:section];
    if (row < maxRows) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            NSArray *indexPaths = @[indexPath];
            [self.tableView reloadRowsAtIndexPaths:indexPaths
                                  withRowAnimation:UITableViewRowAnimationAutomatic];
        }
}

- (void)clearSortedCache
{
    _sortedItems = nil;
}

- (NSArray *)sortedItems
{
    if (!_sortedItems) {
        if (self.masterItem) {
            if (self.masterItem.editableItems) {
                self.editableItems = self.masterItem.editableItems;
            }
            else if (self.masterItem.permittedValues) {
                self.editableItems = self.masterItem.permittedValues.editableItems;
            }
        }
        _sortedItems = [self sort:[NSSet setWithArray:self.editableItems]];
        [self rebuiltSortValues:_sortedItems];
    }
    return _sortedItems;
}

- (void)closeKeyboard:(UITextField *)textField
{
    textField.selected = NO;
    if (textField.isFirstResponder) {
            [textField resignFirstResponder];
        }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger row = textField.tag;
    NSInteger itemIndex = row - 1;
    id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
    if (![item.value isEqualToString:self.textFieldContents]) {
        // something changed in field
        if ([self value:self.textFieldContents uniqueIn:self.sortedItems]) {
            [self closeKeyboard:textField];
            return YES;
        }
        else {
            textField.backgroundColor = self.errorBackgroundColor;
        }
        return NO;
    }
    else {
        // nothing changed
        [self closeKeyboard:textField];
        return YES;
    }
}

- (void)unselectActiveEditRow
{
    if (self.activeIndexPath) {
        [self.tableView selectRowAtIndexPath:self.activeIndexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        [self resetActiveIndexPath:self.activeIndexPath];
        [self selectedItemDidUpdate];
    }
}

- (void) resetActiveIndexPath:(NSIndexPath *)indexPath
{
    if ([self.activeIndexPath isEqual:indexPath]) {
        self.activeIndexPath = nil;
    }
}

- (void) selectedItemDidUpdate
{
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    if (selectedRow) {
        NSMutableArray *indexPaths = (NSMutableArray *) [@[selectedRow] mutableCopy];
        if ([self isKindOfClass:[DepartmentDetailVC class]]) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:0 inSection:0]]; // in case insert button changes
        }
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        // [self.tableView selectRowAtIndexPath:selectedRow animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }
}
@end