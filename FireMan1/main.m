//
//  main.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FireManAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FireManAppDelegate class]));
    }
}
