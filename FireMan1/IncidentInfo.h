//
// Created by Gerald Boyd on 5/14/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class GbElapsedTime;
@class Incident;


@interface IncidentInfo : NSObject

@property (strong, nonatomic) GbElapsedTime *elapsedTime;
@property (strong, nonatomic) NSString *infoString;
@property (weak, nonatomic) Incident *incident;
@property (strong, nonatomic) NSString *elapsedTimeString;
@property (strong, nonatomic) NSString *icString;
@property (strong, nonatomic) NSString *titleString;
- (IncidentInfo *) init;
@end