//
//  EditListOfListsVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/4/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Incident;

@interface EditListOfListsVC : UITableViewController <UITextFieldDelegate>
- (IBAction)unwindFromAddList:(UIStoryboardSegue *)unwindSegue;

@property (nonatomic, weak) Incident *incident;
@property(nonatomic, strong) NSManagedObjectContext *context;
@end
