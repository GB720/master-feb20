//
// Created by Gerald Boyd on 9/17/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//


#import "FireUnitCellStatus.h"
#import "StagingGroup.h"
// #import "ListItem.h"

@class ListItem;

@implementation FireUnitCellStatus {

}
- (FireUnitCellStatus *)initWith: (UIColor *)statusColor
                         forUnit:(NSString *)prefixedName
        assignedGroup:(ListItem *)assignedTo
{
    self = [super init];
    if (self) {
        _statusColor = statusColor;
        _unitName = prefixedName;
        _assignedGroup = assignedTo;
    }
    return self;
}

@end
