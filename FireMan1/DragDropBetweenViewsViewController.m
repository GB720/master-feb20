//
//  DragDropBetweenViewsViewController.m
//  TestIOS
//
//  Created by Jacob von Eyben on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DragDropBetweenViewsViewController.h"
#import "DragDropManager.h"

@implementation DragDropBetweenViewsViewController


@synthesize viewA = _viewA;
@synthesize viewB = _viewB;
@synthesize dragDropManager = _dragDropManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    _viewA = (UIView <DragDropAcceptor> *) [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    [_viewA setBackgroundColor:[UIColor ersGreenColor]];
    _viewA.tag = 1;
    _viewB = (UIView <DragDropAcceptor> *) [[UIView alloc] initWithFrame:CGRectMake(0, 220, 320, 200)];
    [_viewB setBackgroundColor:[UIColor ersGoldColor]];
    _viewB.tag = 2;

    [[self view] addSubview:_viewA];
    [[self view] addSubview:_viewB];
    //[[self view] addSubview:_viewB];

    //add elements to drag and drop
    id <DraggableUIView> dragDropView1 = (id <DraggableUIView>) [[UIView alloc] initWithFrame:CGRectMake(100, 100, 50, 50)];
    id <DraggableUIView> dragDropView2 = (id <DraggableUIView>) [[UIView alloc] initWithFrame:CGRectMake(100, 100, 50, 50)];
    [_viewA addSubview:dragDropView1];
    [_viewB addSubview:dragDropView2];
    [dragDropView1 setBackgroundColor:[UIColor ersRedColor]];
    [dragDropView2 setBackgroundColor:[UIColor ersBlueColor]];
    NSMutableArray *draggableSubjects = [[NSMutableArray alloc] initWithObjects:dragDropView1, dragDropView2, nil];
    NSMutableArray *droppableAreas = [[NSMutableArray alloc] initWithObjects:_viewA, _viewB, nil];
    _dragDropManager = [[DragDropManager alloc] init ];
    [self.dragDropManager addDragSubject:dragDropView1];
    [self.dragDropManager addDragSubject:dragDropView2];
    [self.dragDropManager addDragArea:self.viewA];
    [self.dragDropManager addDragArea:self.viewB];
    
    UIPanGestureRecognizer * uiTapGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:_dragDropManager action:@selector(dragging:)];
    [[self view] addGestureRecognizer:uiTapGestureRecognizer];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end