//
//  EditStagingGroupsViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/25/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "EditStagingGroupsViewController.h"
#import "Incident.h"
#import "Incident+Create.h"
#import "ListItem.h"
#import "StagingGroup+Create.h"
#import "StagingLevelViewController.h"
#import "DefineNewStagingLevelVC.h"
#import "UIColor+EmerResStrat.h"

@interface EditStagingGroupsViewController ()
@property (nonatomic, strong) NSArray *sortedStagingLevels;
@property (nonatomic, readwrite) NSSet *stagingGroups;
@property(nonatomic, strong) StagingGroup *editStagingGroup;
@property (nonatomic, strong) UIColor *cellColor;
@property (nonatomic) BOOL isViewReady;
@end

@implementation EditStagingGroupsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isViewReady = NO;
    _sortedStagingLevels = self.incident.sortedStagingLevels;
    _cellColor = [UIColor ersTextFieldBackgroundColor];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.editing = YES;
    self.tableView.delegate = self;
    self.tableView.accessibilityIdentifier = @"editIncidentStagesTable";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
    self.isViewReady = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.sortedStagingLevels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get table view cell
    static NSString *CellIdentifier = @"editStagingGroupViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure the cell...
    StagingGroup *stagingGroup = self.sortedStagingLevels[(NSUInteger) indexPath.row];
    cell.textLabel.text = stagingGroup.name;
    NSString *timeString = [NSString stringWithFormat:@"time reqd: %li",(long)stagingGroup.timeRequired.integerValue];
    cell.detailTextLabel.text = timeString;
    cell.accessoryView.tag = indexPath.row;
    cell.backgroundColor = self.cellColor;
    cell.textLabel.backgroundColor = self.cellColor;
    cell.detailTextLabel.backgroundColor = self.cellColor;
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        StagingGroup *stagingGroup = self.sortedStagingLevels[(NSUInteger) indexPath.row];
        [self.incident removeStagingGroupsObject:stagingGroup];
        self.sortedStagingLevels = self.incident.sortedStagingLevels;
        [self setSortValuesFor:self.sortedStagingLevels];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StagingGroup *stagingGroup = self.sortedStagingLevels[(NSUInteger) indexPath.row];
    if (stagingGroup.units.count == 0) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSUInteger fromRow = (NSUInteger) fromIndexPath.row;
    StagingGroup *stagingGroup = self.sortedStagingLevels[fromRow];
    NSUInteger toRow = (NSUInteger) toIndexPath.row;
    StagingGroup *insertionGroup = self.sortedStagingLevels[toRow];
    NSDecimalNumber *insertionSortValue = insertionGroup.sortOrderValue;
    NSDecimalNumber *pointFive = [NSDecimalNumber decimalNumberWithString:@".5"];
    NSDecimalNumber *itemSortValue;
    if (fromRow < toRow) {
        itemSortValue = [insertionSortValue decimalNumberByAdding:pointFive];
    }
    else {
        itemSortValue = [insertionSortValue decimalNumberBySubtracting:pointFive];
    }
    stagingGroup.sortOrderValue = itemSortValue;
    self.sortedStagingLevels = self.incident.sortedStagingLevels;
    [self setSortValuesFor:self.sortedStagingLevels];
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    self.editStagingGroup = self.sortedStagingLevels[(NSUInteger) indexPath.row];
    [self performSegueWithIdentifier:@"editStagingGroupSegue" sender:self];
}



#pragma mark - Navigation

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return self.isViewReady;
}

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueToAddStagingLevel"]) {
        UINavigationController *nvc = segue.destinationViewController;
        StagingLevelViewController *vc = (StagingLevelViewController *) nvc.topViewController;
        vc.fireDepartment = self.incident.forDepartment;
        vc.activeIncident = self.incident;
    }
    if ([segue.identifier isEqualToString:@"editStagingGroupSegue"]) {
        UINavigationController *nvc = segue.destinationViewController;
        DefineNewStagingLevelVC *vc = (DefineNewStagingLevelVC *)nvc.topViewController;
        vc.fireDepartment = self.incident.forDepartment;
        vc.stagingGroupName = self.editStagingGroup.name;
        vc.timeRequiredInStage = self.editStagingGroup.timeRequired;
        vc.editingStagingGroup = YES;
    }
    else
    {
        self.stagingGroups = [NSSet setWithArray:self.sortedStagingLevels];
    }
}


- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    if (action == @selector(unwindAddStagingLevelToIncident:)) {
        return YES;
    }
    else if (action == @selector(unwindAddStagingLevel:)) {
        return YES;
    }
    return NO;
}

- (IBAction) unwindAddStagingLevel:(UIStoryboardSegue *)unwindSegue
{
    if (self.incident.managedObjectContext) {
        if ([unwindSegue.identifier isEqualToString:@"unwindAddStagingLevel"]) {
            DefineNewStagingLevelVC *vc = unwindSegue.sourceViewController;
            if (vc.stagingGroupName) {
                self.editStagingGroup.name = vc.stagingGroupName;
                self.editStagingGroup.timeRequired = vc.timeRequiredInStage;
                [self.tableView reloadData];
            }
        }
    }
}


- (IBAction)unwindAddStagingLevelToIncident:(UIStoryboardSegue *)unwindSegue
{
    if (self.incident.managedObjectContext) {
        if ([unwindSegue.identifier isEqualToString:@"unwindAddStagingLevelToIncident"])
        {
            // add staging level to list for the incident
            StagingLevelViewController *vc = unwindSegue.sourceViewController;
            StagingGroup *addingStagingGroup = vc.addingStagingGroup;
            if (addingStagingGroup) {
                // set sort value to put new staging group last in list
                addingStagingGroup.sortOrderValue = [NSDecimalNumber decimalNumberWithString:@"1000.0"];;
                [self.incident addStagingGroupsObject:addingStagingGroup];

                // re-display list for this view controller
                self.sortedStagingLevels = self.incident.sortedStagingLevels;
                [self setSortValuesFor:self.sortedStagingLevels];
                self.stagingGroups = [NSSet setWithArray:self.sortedStagingLevels];
                [self.tableView reloadData];
            }
        }
    }
}

- (void)setSortValuesFor:(NSArray *)array
{
    NSDecimalNumber *num = [NSDecimalNumber decimalNumberWithString:@"1.0"];
    NSDecimalNumber *sortNumber = num;
    for (StagingGroup *stagingGroup in array) {
        stagingGroup.sortOrderValue = sortNumber;
        sortNumber = [sortNumber decimalNumberByAdding:num];
    }
}


@end
