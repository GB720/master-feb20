//
// Created by Gerald Boyd on 5/14/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "IncidentInfo.h"
#import "Incident.h"
#import "GbElapsedTime.h"

#import "FireUnit+Identification.h"

@implementation IncidentInfo

- (id) init
{
        self = [super init];
        if (self) {

        }
        return self;
}

- (NSString *)infoString
{
    NSString *info = [NSString stringWithFormat:@"%@ %@ %@",
                    self.elapsedTimeString, self.titleString, self.icString];
    return info;
}

- (NSString *)elapsedTimeString
{
    return [NSString stringWithFormat:@"%02lu:%02lu:%02lu",
                    (unsigned long)self.elapsedTime.hours,
                    (unsigned long)self.elapsedTime.mins,
                    (unsigned long)self.elapsedTime.secs];
}

- (NSString *)icString
{
    NSString *ic;
    if (self.incident && self.incident.commandUnit) {
        ic = [NSString stringWithFormat: @"IC: %@", self.incident.commandUnit.prefixedName];
    }
    else {
        ic = @"IC: ???";
    }
    return ic;
}

- (NSString *)titleString
{
    NSString *incidentName = @"Title";
    NSString *incidentStatus = @"";
    if (self.incident) {
        incidentName = self.incident.name;
        if (self.incident.stopTime) {
            incidentStatus = @"CLOSED ";
        }
    }
    NSString *title = [NSString stringWithFormat:@"%@Incident: %@",incidentStatus,incidentName];
    return title;

}

@end