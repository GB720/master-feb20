//
// Created by Gerald Boyd on 1/23/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDedit.h"


@interface EditableItem : NSObject <FDedit>
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, readonly) NSString * localizedItemName;
@property (nonatomic, strong) NSArray *editableItems;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) id <FDPermittedValues> permittedValues;
@property (nonatomic, strong) id <FDedit> ownerObject;
@property (nonatomic, strong) NSDecimalNumber *sortValue;
@property (nonatomic) BOOL isReorderable;
@property (nonatomic) BOOL isDeletable;
@property (nonatomic) BOOL hasSortValue;


+ (instancetype)owner:(id <FDedit>)ownerObject fromString:(NSString *)name;

+ (instancetype)owner:(id <FDedit>)ownerObject named:(NSString *)itemName;

+ (instancetype)owner:(id <FDedit>)ownerObject named:(NSString *)itemName fromString:(NSString *)value;

+ (instancetype)owner:(id <FDedit>)ownerObject named:(NSString *)itemName fromArray:(NSArray *)array;

+ (instancetype)owner:(id <FDedit>)ownerObject named:(NSString *)itemName fromArray:(NSArray *)array sortValue:(NSInteger)sortValue;

+ (instancetype)owner:(id) ownerObject value:(NSString *) value;

+ (instancetype)owner:(id)ownerObject value:(NSString *)value itemName:(NSString *)itemName;

+ (instancetype)owner:(id)ownerObject value:(NSString *)value permittedValues:(id <FDPermittedValues>)permittedValues itemName:(NSString *)itemName;

+ (instancetype)owner:(id)ownerObject value:(NSString *)value permittedValues:(id <FDPermittedValues>)permittedValues itemName:(NSString *)itemName sortValue:(NSInteger)sortValue;

+ (instancetype)owner:(id)ownerObject value:(NSString *)value itemName:(NSString *)itemName sortValue:(NSInteger) sortValue;

+ (instancetype)owner:(id)ownerObject value:(NSString *)value itemName:(NSString *)itemName decimalSortValue:(NSDecimalNumber *)decimalSortValue;

- (void) save: (NSString *) newValue;

@end