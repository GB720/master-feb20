//
//  Created by jve on 4/2/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "DragContext.h"
#import "DragDropManager.h"

@protocol DragDropAcceptor;
@interface DragContext ()

@end

@implementation DragContext {

    UIView *_draggedView;
    CGPoint _originalPosition;
    UIView *_originalView;
    CGPoint _originalPointInSuperView;
}
@synthesize draggedView = _draggedView;
@synthesize originalView = _originalView;
@synthesize originalPointInSuperView = _originalPointInSuperView;


- (id)initWithDraggedView:(UIView *)draggedView {
    self = [super init];
    if (self) {
        _draggedView = draggedView;
        _originalPosition = _draggedView.frame.origin;
        _originalView = nil;
        id <DraggableUIView> dragSubject = (id <DraggableUIView>) draggedView;
        _originalView = (UIView *) dragSubject.owningView;
        _originalPointInSuperView = [_originalView convertPoint:_originalPosition
                                                       fromView:_originalView];
        //   copy the image to drag
        CGPoint origin = draggedView.bounds.origin;
        CGSize size = draggedView.bounds.size;
        CGRect rect = CGRectMake(origin.x, origin.y, size.width, size.height);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [draggedView.layer renderInContext:context];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageView *draggableSubject = [[UIImageView alloc] initWithImage:image];
        self.viewCopyToDrag = draggableSubject;
        draggedView.hidden = YES;
    }
    return self;
}

- (void)dealloc {
    _draggedView = nil;
    _originalView = nil;
    _viewCopyToDrag = nil;
}

- (void) removeDragImage
{
    [_viewCopyToDrag removeFromSuperview];
}

- (void)snapToOriginalPosition
{
    CGPoint originalLocation = [self.movingInView convertPoint:_originalPosition
                                                   fromView:_originalView];

    [UIView animateWithDuration:0.3 animations:^() {
        self->_viewCopyToDrag.frame = CGRectMake(originalLocation.x, originalLocation.y, self.draggedView.bounds.size.width, self.draggedView.bounds.size.height);
        // _viewCopyToDrag.alpha = 0.1;
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            id <DragDropAcceptor> sourceView = (id <DragDropAcceptor>)self.originalView;
            [sourceView.dragDelegate cancelMove:self];
            [self removeDragImage];
        });
    }];
}
@end
