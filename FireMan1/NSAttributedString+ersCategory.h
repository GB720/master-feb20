//
// Created by Gerald Boyd on 4/28/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (ersCategory)
+ (NSAttributedString *)attributedStringFrom:(NSString *)string foregroundColor:(UIColor *)fgColor backgroundColor:(UIColor *)bgColor size:(CGFloat)size;
@end