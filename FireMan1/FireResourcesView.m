//
//  FireResourcesView.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "FireResourcesView.h"
#import "FireResourceViewCell.h"
#import "DragDropManager.h"
#import "DragContext.h"

@interface FireResourcesView()
@property (nonatomic, weak) DragDropManager *dragManager;
@end

@implementation FireResourcesView

@synthesize taskId = _viewId;
@synthesize dragDelegate = _dragDelegate;

#pragma mark - DragDropAcceptor
-(void) setDragDropManager:(DragDropManager *)dragDropManager
{
    self.dragManager = dragDropManager;
}

// target for drops
- (void) addTypeToAccept:(NSString *)dragIdentifier
{
    [self.dragManager addDragType:dragIdentifier
                          forView:self];
}

- (BOOL) removeFromView: (UIView *) viewBeingDragged
{
    return NO;
}

- (BOOL) addImage:(UIView *) viewBeingDragged atPoint: (CGPoint) point
{
    return NO;
}


- (BOOL) isDropTargetFor: (UIView *) viewBeingDragged atPoint: (CGPoint) point
{
    BOOL isValidDropTarget = YES;
    UICollectionViewCell *cell = (UICollectionViewCell *)viewBeingDragged;
    id <DraggableUIView> itemBeingDropped = (id <DraggableUIView>) viewBeingDragged;
    DragContext *dragContext = self.dragManager.dragContext;
    
    NSIndexPath *itemIndexAtPoint = [self indexPathForItemAtPoint:point];
    
    // dropping item back on itself?
    id <DragDropAcceptor> original = (id <DragDropAcceptor>) dragContext.originalView;
    if (itemIndexAtPoint && (self.taskId == original.taskId)) {
        NSIndexPath *cellIndex = [self indexPathForCell:cell];
        if ([cellIndex compare:itemIndexAtPoint] == NSOrderedSame)
        {
            isValidDropTarget = NO;
        }
    }
    // check that the item is a valid drop for this view
    else if (![self.dragManager isValidDropItem:itemBeingDropped
                                        forView:self]) {
        isValidDropTarget = NO;
    }
    return isValidDropTarget;
}

- (BOOL) droppingItemOn:(UIView *)viewBeingDragged atPoint:(CGPoint)point
{
    if ([self isDropTargetFor:viewBeingDragged atPoint:point]) {
        // todo
    }
    return YES;
}

-(void) dragToEnded
{
    [self reloadData];
}

// return the view for the cell at the point of the drag (if one is found)
- (UIView *)viewAt:(CGPoint)point {
    NSIndexPath *itemIndexPath = [self indexPathForItemAtPoint:point];
    if (itemIndexPath) {
        UICollectionViewCell *cell = [self cellForItemAtIndexPath:itemIndexPath];
        return cell;
    }
    return nil;
}


// source view notification of drag starting
- (void) startingDrag:(DragDropManager *) dragManager
              atPoint:(CGPoint) point
{
    NSIndexPath *itemIndexPath = [self indexPathForItemAtPoint:point];
    UICollectionViewCell *cell = [self cellForItemAtIndexPath:itemIndexPath];
    cell.selected = YES;
}

// source view notification of drag ending
- (void) dragFromEnded: (UIView *) viewBeingDragged
{
   [self reloadData];
}

- (void) doReloadData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(doReloadData) object:nil];
        [super reloadData];
    });
}

- (void)reloadData
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(doReloadData) object:nil];
    [self performSelector:@selector(doReloadData) withObject:nil afterDelay:0.1];
}


#pragma mark - FireResourcesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
