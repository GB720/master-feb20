//
//  UnitMovementView.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/5/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "UnitMovementView.h"

@implementation UnitMovementView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
