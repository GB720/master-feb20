//
//  DragDropBetweenViewsViewController.h
//  TestIOS
//
//  Created by Jacob von Eyben on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DragDropManager.h"

@class DragDropManager;

@interface DragDropBetweenViewsViewController : UIViewController {
@private
    UIView <DragDropAcceptor> * _viewA;
    UIView <DragDropAcceptor> * _viewB;
    DragDropManager *_dragDropManager;
}
@property(nonatomic, retain) UIView <DragDropAcceptor> *viewA;
@property(nonatomic, retain) UIView <DragDropAcceptor> *viewB;
@property(nonatomic, retain) DragDropManager *dragDropManager;


@end
