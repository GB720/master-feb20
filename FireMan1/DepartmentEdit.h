//
// Created by Gerald Boyd on 3/7/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FDedit.h"
#import "EditContainer.h"

@protocol FDedit;


@interface DepartmentEdit : UITableViewController
- (NSArray *)sort:(NSSet *)set;

- (void)rebuiltSortValues:(NSArray *)array;

- (BOOL)value:(NSString *)value uniqueIn:(NSArray *)itemArray;

- (void)reloadTableRow:(NSInteger)row section:(NSInteger)section;

- (void)clearSortedCache;

- (void)closeKeyboard:(UITextField *)textField;

- (void)unselectActiveEditRow;

- (void)selectedItemDidUpdate;
- (void) resetActiveIndexPath:(NSIndexPath *)indexPath;

@property(nonatomic, copy) NSString *textFieldContents;
@property(nonatomic, strong) UIColor *errorBackgroundColor;

@property (nonatomic, strong) UIColor *descriptionBackgroundColor;
@property (nonatomic, strong) NSIndexPath *activeIndexPath;
@property (nonatomic, strong) NSArray *sortedItems;


@property (nonatomic, strong) NSArray *editableItems;
@property(nonatomic, strong) id <FDedit> masterItem;
@property(nonatomic, strong) id <EditContainer> delegate;
@end