//
//  RightSideHeaderCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 3/24/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightSideHeaderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *heading;
@property (strong, nonatomic) IBOutlet UIButton *addItemButton;

@end
