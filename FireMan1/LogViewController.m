//
//  LogViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <os/activity.h>
#import <MessageUI/MessageUI.h>
#import "LogViewController.h"
#import "FireManViewController.h"
#import "Log.h"
#import "IncidentLog.h"
#import "LogItem.h"
#import "GbElapsedTime.h"
#import "UIColor+EmerResStrat.h"
#import "NSString+ersFormat.h"

static NSInteger VIEW_LENGTH = 550;
static NSInteger VIEW_WIDTH = 750;

@interface LogViewController ()
- (IBAction)sortByTime:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *defaultSortButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sortByUnitButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sortByGroupButton;
- (IBAction)sortByGroupPressed:(UIBarButtonItem *)sender;
- (IBAction)sortByUnitPressed:(UIBarButtonItem *)sender;
- (IBAction)emailLogButtonPressed:(UIBarButtonItem *)sender;

@property (nonatomic, strong) NSArray *sortedLogItems;
@property (nonatomic, strong) NSSet *logItems;
@property(nonatomic, strong) UIColor *defaultButtonTint;
@property(nonatomic, strong) UIColor *activeButtonColor;
@end

@implementation LogViewController {
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self performSegueWithIdentifier:@"unwindFromShowLog" sender:self];
    self.callingViewController.isSegueActive = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    os_activity_label_useraction("Show Incident Log");
    _activeButtonColor = [UIColor ersActiveButtonColor];
    IncidentLog *log = self.incident.logForIncident;
    _logItems = log.logItems;
    self.sortedLogItems = [self sortLogItems:self.logItems sortKey:@"itemNumber" ascending:YES ];
    self.defaultButtonTint = self.defaultSortButton.tintColor;
    self.defaultSortButton.tintColor = self.activeButtonColor;
    self.tableView.accessibilityIdentifier = @"logViewTableView";
    CGSize size = CGSizeMake(VIEW_WIDTH, VIEW_LENGTH);
    self.preferredContentSize = size;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.sortedLogItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"logItemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSUInteger index = (NSUInteger) indexPath.item;

    // Configure the cell...
    cell.textLabel.font = [cell.textLabel.font fontWithSize:12];
    if (index % 3){
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor whiteColor];
    }
    else {
        cell.textLabel.backgroundColor = [UIColor ersLightGoldColor];
        cell.backgroundColor = [UIColor ersLightGoldColor];
    }
    LogItem *logItem = self.sortedLogItems[index];

    cell.textLabel.text = [self stringFromLogItem:logItem];
    return cell;
}

- (NSString *)stringFromLogItem:(LogItem *)logItem
{
    NSString *unitName = @"---";
    NSString *taskName = @"---";
    if (logItem.unitName) {
        unitName = logItem.unitName;
    }
    if (logItem.taskName) {
        taskName = logItem.taskName;
    }
    unitName = [unitName minSize:8];
    taskName = [taskName minSize:16];
    NSTimeInterval elapsedTimeInterval = [logItem.timeStamp timeIntervalSinceDate:self.incident.startTime];
    GbElapsedTime *elapsedTime = [[GbElapsedTime alloc] initWithTimeInterval:elapsedTimeInterval];

    NSString *formattedString = [NSString stringWithFormat:@"%02lu:%02lu:%02lu - : %@: %@: %@",
                                                           (unsigned long) elapsedTime.hours,
                                                           (unsigned long) elapsedTime.mins,
                                                           (unsigned long) elapsedTime.secs,
                                                           unitName, taskName,
                                                           logItem.action];
    return formattedString;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - button notifications

- (IBAction)sortByTime:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("sort log by Time");
    [self setButtonActive:sender];
    self.sortedLogItems = [self sortLogItems:self.logItems sortKey:@"itemNumber" ascending:YES ];
    [self.tableView reloadData];
}

- (IBAction)sortByGroupPressed:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("sort log by Group");
    [self setButtonActive:sender];
    NSArray *keys = @[@"taskName", @"itemNumber"];
    self.sortedLogItems = [self sortLogItems:self.logItems sortKeys:keys ascending:YES ];
    [self.tableView reloadData];
}

- (IBAction)sortByUnitPressed:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("sort log by Unit");
    [self setButtonActive:sender];
    NSArray *keys = @[@"unitName", @"itemNumber"];
    self.sortedLogItems = [self sortLogItems:self.logItems sortKeys:keys ascending:YES ];
    [self.tableView reloadData];
}

#pragma mark - email related

- (IBAction)emailLogButtonPressed:(UIBarButtonItem *)sender
{
    os_activity_label_useraction("Sending log as email");

    if ([MFMailComposeViewController canSendMail]) {
        // get a new new MailComposeViewController object
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];

        // his class should be the delegate of the mc
        mc.mailComposeDelegate = self;

        // set a mail subject ... but you do not need to do this :)
        NSString *subject = [NSString stringWithFormat:@"Log File for Incident:%@",self.incident.name];
        [mc setSubject:subject];

        // set some basic plain text as the message body ... but you do not need to do this :)
        [mc setMessageBody:[self stringFromLogItems:self.sortedLogItems] isHTML:NO];

        // set some recipients ... but you do not need to do this :)
        // [mc setToRecipients:[NSArray arrayWithObjects:@"first.address@test.com", @"second.address@test.com", nil]];

        //[mc addAttachmentData:<#(NSData *)attachment#> mimeType:<#(NSString *)mimeType#> fileName:<#(NSString *)filename#>];

        // displaying our view controller on the screen with standard transition
        [self presentViewController:mc animated:YES completion:nil];
    }
    else {
        // mail is not set up on this device
        [self showAlert:@"E-Mail Not Enabled" message:@"E-Mail is not supported on this device"];
    }
}


#pragma mark - Alert msg
- (void) showAlert:(NSString *)title message:(NSString *)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController* __weak weakAlert = alert;
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancelAction];
    [self presentViewController:weakAlert animated:YES completion:nil];
}

- (NSString *)stringFromLogItems:(NSArray *)logItems
{
    NSMutableString *resultString = [NSMutableString stringWithCapacity:4000];
    for (LogItem *logItem in logItems) {
        [resultString appendFormat:@"%@\n", [self stringFromLogItem:logItem]];
    }
    return resultString;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];

    // mail composer finished
    if (result == MFMailComposeResultCancelled) {

    }
    else if (result == MFMailComposeResultFailed) {
        os_activity_label_useraction("email error when sending log");
        NSString *errMsg = [NSString stringWithFormat:@"%@%ld",@"The E-Mail send action failed with error:",(long)error.code];
        
        [self showAlert:@"Send Operation Failed." message:errMsg];
    }
    else if (result == MFMailComposeResultSaved) {

    }
    else if (result == MFMailComposeResultSent) {
        os_activity_label_useraction("Log emailed");
        NSString *alertMsg = @"Email sent";
        
        [self showAlert:@"Log emailed." message: alertMsg];
    }
}

#pragma mark - utility and sorting methods

// sort the items based on the provided array of keys
- (NSArray *)sortLogItems:(NSSet *)set sortKeys:(NSArray *)keys ascending:(BOOL)isAscending
{
    NSMutableArray *sortDescriptors = [NSMutableArray arrayWithCapacity:4];
    for (NSString *key in keys) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:isAscending selector:@selector(compare:)];
        [sortDescriptors addObject:sortDescriptor];
    }
    return [self.logItems sortedArrayUsingDescriptors:sortDescriptors.copy];
}


// sort the log items based on the key specified
- (NSArray *)sortLogItems:(NSSet *)logItems sortKey:(NSString *)key ascending:(BOOL)isAscending
{
    NSArray *keys = @[key];
    return [self sortLogItems:logItems sortKeys:keys ascending:isAscending];
}

- (void)setButtonActive:(UIBarButtonItem *)sender {
    [self setDefaultButtonTints];
    sender.tintColor = self.activeButtonColor;
    sender.title = [NSString stringWithFormat:@"Sorted %@", sender.title];
}

- (void)setDefaultButtonTints
{
    self.defaultSortButton.tintColor = self.defaultButtonTint;
    self.sortByUnitButton.tintColor = self.defaultButtonTint;
    self.sortByGroupButton.tintColor = self.defaultButtonTint;
    self.sortByUnitButton.title = @"By Unit";
    self.sortByGroupButton.title = @"By Group";
    self.defaultSortButton.title = @"By Time";
}


@end
