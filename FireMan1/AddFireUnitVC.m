//
//  AddFireUnitVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "AddFireUnitVC.h"
#import "FireUnitSelectionVC.h"

@interface AddFireUnitVC ()
@property (weak, nonatomic) IBOutlet UITextField *fireUnitNameField;

@end

@implementation AddFireUnitVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.fireUnitNameField.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.unitName = textField.text;
    [self performSegueWithIdentifier:@"unwindFromAddUnit" sender:self];
    return YES;
}


@end
