//
//  FireResourceViewCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/26/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DragDropManager.h"

@class ParOkButton;
@class FireCellColor;

@interface FireResourceViewCell : UICollectionViewCell <DraggableUIView>
@property (strong, nonatomic) IBOutlet UILabel *coordinatorLabel;
@property (strong, nonatomic) IBOutlet ParOkButton *parStatusButton;
@property (nonatomic) BOOL activeUnit;
@property (strong, nonatomic) NSString *cellTitle;
@property (strong, nonatomic) FireCellColor *cellColors;

- (void)coordinatorLabelEnabled:(BOOL)isEnabled;
- (void)parStatusButtonEnabled:(BOOL)isEnabled;

@end
