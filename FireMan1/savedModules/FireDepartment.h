//
//  FireDepartment.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "LoggingEntity.h"
#import "FireVehicle.h"

@interface FireDepartment : LoggingEntity
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic, readonly) NSArray *availableUnits;
@property (nonatomic, readonly) NSInteger numberAvailableUnits;

// return unit to the available pool
-(void) releaseUnit: (FireVehicle *) fireUnit;

-(FireVehicle *) reserveUnit: (NSUInteger) index;

-(FireVehicle *) availableUnitAt: (NSUInteger) index;

-(void) insertUnit:(FireVehicle *) fireUnit atIndex: (NSUInteger) index;


@end
