//
//  Division.m
//  FireMan1
//
//  Created by Gerald Boyd on 4/1/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "Division.h"

@interface Division()
@property (strong, nonatomic) NSMutableArray *fireUnits;

@end
@implementation Division
- (id) init
{
    self = [super init];
    if (self){
        self.taskName = @"divName";
        _fireUnits = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *) fireVehicles
{
    return [self.fireUnits copy];
}

- (void) addUnit: (FireVehicle *)unit
{
    [self.fireUnits addObject: unit];
}

- (void) insertUnit: (FireVehicle *)unit atIndex:(NSUInteger)index
{
    [self.fireUnits insertObject:unit atIndex:index];
}

- (void) removeUnitAtIndex:(NSUInteger)index
{
    [self.fireUnits removeObjectAtIndex:index];
}
@end
