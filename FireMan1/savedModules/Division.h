//
//  Division.h
//  FireMan1
//
//  Created by Gerald Boyd on 4/1/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "LoggingEntity.h"
#import "FireVehicle.h"

@interface Division : LoggingEntity
@property (strong, nonatomic) NSString *taskName;
@property (strong, nonatomic) NSArray *fireVehicles;

- (void) addUnit: (FireVehicle *)unit;
- (void) insertUnit: (FireVehicle *)unit atIndex:(NSUInteger)index;
- (void) removeUnitAtIndex:(NSUInteger)index;
@end
