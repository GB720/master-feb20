//
//  FireDepartment.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireDepartment.h"

@interface FireDepartment ()
@property (strong, nonatomic, readwrite) NSArray *availableUnits;
@property (strong, nonatomic) NSMutableArray *currentAvailableUnits;
@property (nonatomic, readwrite) NSInteger numberAvailableUnits;
@end

@implementation FireDepartment


-(id) init
{
    self = [super init];
    if (self) {
        self.currentAvailableUnits = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSArray *) getAvailableUnits
{
    return [self.currentAvailableUnits copy];
}

-(NSInteger) numberAvailableUnits
{
    NSInteger count = self.currentAvailableUnits.count;
    return count;
}

-(FireVehicle *) availableUnitAt: (NSUInteger) index
{
    id unit = [self.currentAvailableUnits objectAtIndex:index];
    if ((unit) && ([unit isKindOfClass:[FireVehicle class]])) {
        return (FireVehicle *) unit;
    }
    return nil;
}

-(void) releaseUnit: (FireVehicle *) fireUnit
{
    [self.currentAvailableUnits addObject: fireUnit];
}

-(FireVehicle *) reserveUnit: (NSUInteger) index
{
    FireVehicle *unit = [self availableUnitAt:index];
    [self.currentAvailableUnits removeObjectAtIndex:index];
    return unit;
}

-(void) insertUnit:(FireVehicle *) fireUnit atIndex: (NSUInteger) index
{
    [self.currentAvailableUnits insertObject:fireUnit atIndex:index];
}
@end

