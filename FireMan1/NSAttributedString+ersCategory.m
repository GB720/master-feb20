//
// Created by Gerald Boyd on 4/28/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "NSAttributedString+ersCategory.h"


@implementation NSAttributedString (ersCategory)
+ (NSAttributedString *)attributedStringFrom:(NSString *)string
                             foregroundColor:(UIColor *)fgColor
                             backgroundColor:(UIColor *)bgColor
                                        size:(CGFloat)size
{
    NSMutableAttributedString *attributedTitle=[[NSMutableAttributedString alloc] initWithString:string];
    NSUInteger length= attributedTitle.length;

    UIFont *font=[UIFont fontWithName:@"Helvetica-Bold" size:size];

    [attributedTitle addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, length)];
    [attributedTitle addAttribute:NSForegroundColorAttributeName value:fgColor range:NSMakeRange(0, length)];
    [attributedTitle addAttribute:NSBackgroundColorAttributeName value:bgColor range:NSMakeRange(0, length)];
    return attributedTitle.copy;
}

@end