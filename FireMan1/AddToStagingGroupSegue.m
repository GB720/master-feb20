//
//  AddToStagingGroupSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 9/19/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "AddToStagingGroupSegue.h"
#import "FireManViewController.h"
#import "FireUnitSelectionVC.h"
#import "StagingHeaderView.h"

@implementation AddToStagingGroupSegue


- (void)perform
{
    
    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect
    
    // Set the size of the popover because it will be max sized otherwise
    
    FireManViewController *sourceController = (FireManViewController *)self.sourceViewController;
    FireUnitSelectionVC *destinationController = (FireUnitSelectionVC *)self.destinationViewController;
    destinationController.incident = sourceController.incident;
    destinationController.callingViewController = sourceController;
    
    // ---------
    // present the controller
    destinationController.modalPresentationStyle = UIModalPresentationPopover;
    [sourceController presentViewController:destinationController animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [destinationController popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
    
    popController.sourceView = self.anchorInView;
    popController.sourceRect = self.anchorFrame;
    popController.delegate = sourceController;
    
   
//    CGSize size = CGSizeMake(640, 600);
//    pop.popoverContentSize = size;
//
}

@end
