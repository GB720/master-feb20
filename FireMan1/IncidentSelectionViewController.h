//
//  IncidentSelectionViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 7/17/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "IncidentSelectionDelegate.h"
#import "FireDepartment.h"

@class Incident;

@interface IncidentSelectionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) FireDepartment *fireDepartment;
@property (nonatomic, weak) UIViewController *callingViewController;
@property(nonatomic, strong) Incident *selectedIncident;
@property(nonatomic) BOOL isNewIncident;
- (IBAction)newIncidentButton:(UIBarButtonItem *)sender;
- (IBAction)editButton:(UIBarButtonItem *)sender;
- (IBAction)cancelButton:(UIBarButtonItem *)sender;
@property(nonatomic) BOOL isLogOnly;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)unwindToIncidentSelection:(UIStoryboardSegue *)unwindSegue;
@property (nonatomic, strong) id <IncidentSelectionDelegate> delegate;
@end 
