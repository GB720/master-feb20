//
//  DepartmentDetailVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/24/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableItem.h"
#import "EditorView.h"
#import "DepartmentEdit.h"

@protocol EditContainer;

@interface DepartmentDetailVC : DepartmentEdit <UITextFieldDelegate, EditorView>

@end
