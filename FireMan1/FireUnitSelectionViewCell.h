//
//  FireUnitSelectionViewCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/5/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FireUnitSelectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
