//
//  DefineNewStagingLevelVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StagingGroup;
@class FireDepartment;

@interface DefineNewStagingLevelVC : UITableViewController
@property (nonatomic, strong) NSString *stagingGroupName;
@property (nonatomic, strong) NSNumber *timeRequiredInStage;
@property (nonatomic, weak) FireDepartment *fireDepartment;
@property (nonatomic) BOOL editingStagingGroup;
@end
