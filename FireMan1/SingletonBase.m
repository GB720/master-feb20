//
//  SingletonBase.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/19/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//
//  This class should be used as the parent class for singletons.

#import "SingletonBase.h"

@implementation SingletonBase

+ (SingletonBase *)sharedSingletonObject
{
    static dispatch_once_t onceToken = 0;
    
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

@end