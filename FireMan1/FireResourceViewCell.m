//
//  FireResourceViewCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/26/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireResourceViewCell.h"
#import "ParOkButton.h"
#import "UIColor+EmerResStrat.h"
#import "FireCellColor.h"
#import "NSAttributedString+ersCategory.h"

NSInteger PAR_STATUS_BUTTON_TAG = 102;
NSInteger COORDINATOR_LABEL_TAG = 101;
NSInteger NAME_LABEL_TAB = 100;


@interface FireResourceViewCell ()
@property(nonatomic) CGFloat labelHeight;
@property(nonatomic) CGFloat labelWidth;
@property (nonatomic) CGFloat coordinatorLabelWidth;
@property (nonatomic) CGFloat coordinatorLabelHeight;
@property (nonatomic) CGFloat coordinatorX;
@property(nonatomic) CGFloat coordinatorY;
@end

@implementation FireResourceViewCell {
    NSString *_cellTitle;
    FireCellColor *_cellColors;
    CGFloat _coordinatorY;
}

#pragma mark - DraggableUIView

@synthesize owningView = _owningView;

+ (NSString *)getDragIdentifier
{
    return @"fire-resource";
}


#pragma mark - FireResourceViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _activeUnit = true;
    }
    return self;
}

#pragma mark - cell reuse

- (void)prepareForReuse
{
    if (self.parStatusButton) {
        [self.parStatusButton setAttributedTitle:nil forState:UIControlStateNormal];
        [self.parStatusButton setAttributedTitle:nil forState:UIControlStateDisabled];
        [self.parStatusButton setTitle:@"" forState:UIControlStateNormal];
        [self.parStatusButton setSelected:NO];
        [self.parStatusButton setTitleColor:[UIColor ersBlackColor] forState:UIControlStateNormal];
        [self.parStatusButton setTaskId:@""];
        [self.parStatusButton setEnabled:NO];
    }
    [self coordinatorLabelEnabled:NO];
    _cellTitle = nil;
    _cellColors = nil;
}

#pragma mark - properties

- (UILabel *)coordinatorLabel
{
    if (!_coordinatorLabel || _coordinatorLabelWidth == 0) {
        _coordinatorLabelWidth = self.bounds.size.width / 4;
        _coordinatorLabelHeight = self.bounds.size.height / 4;
        _coordinatorX = (self.bounds.size.width - _coordinatorLabelWidth) - 2;
        _coordinatorY = (self.bounds.size.height - _coordinatorLabelHeight) - 3;
        _coordinatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(_coordinatorX, _coordinatorY, _coordinatorLabelWidth, _coordinatorLabelHeight)];
        _coordinatorLabel.tag = COORDINATOR_LABEL_TAG;
        CGFloat fontSize = 13.0;
        _coordinatorLabel.font = [_parStatusButton.titleLabel.font fontWithSize:fontSize];
        _coordinatorLabel.textColor = [UIColor whiteColor];
        _coordinatorLabel.backgroundColor = [UIColor ersBlackColor];
        _coordinatorLabel.hidden = YES;    // default
        [self.contentView addSubview:_coordinatorLabel];
    }
    return _coordinatorLabel;
}

- (void) coordinatorLabelEnabled:(BOOL)isEnabled
{
    UIView *subview = [self.contentView viewWithTag:COORDINATOR_LABEL_TAG];
    if (!subview) {
        // create coordinator label subview
        subview = self.coordinatorLabel;
    }
    [self subviewEnabled:isEnabled subview:subview];
}

- (void) subviewEnabled:(BOOL)isEnabled subview:(UIView *)subview
{
    if (isEnabled) {
        [self.contentView bringSubviewToFront:subview];
        subview.hidden = NO;
    }
    else {
        if (subview) {
            [self.contentView sendSubviewToBack:subview];
            subview.hidden = YES;
        }
    }
}

- (void) parStatusButtonEnabled:(BOOL)isEnabled
{
    ParOkButton *button = self.parStatusButton;
    button.enabled = isEnabled;
    [self updateTitle];
}

- (ParOkButton *)parStatusButton
{
    if (!_parStatusButton) {
        // create par button
        CGFloat width = self.bounds.size.width - 8;
        CGFloat height = self.bounds.size.height - 8;
        CGFloat xOffset = self.bounds.origin.x + 4;
        CGFloat yOffset = self.bounds.origin.y + 4;
        
        _parStatusButton = [ParOkButton buttonWithType:UIButtonTypeCustom];
        [_parStatusButton setEnabled:NO];
        [_parStatusButton setTag:PAR_STATUS_BUTTON_TAG];
        [_parStatusButton setFrame:CGRectMake(xOffset, yOffset, width, height)];
        [_parStatusButton setTitle:@"" forState:UIControlStateNormal];
        [_parStatusButton setSelected:NO];
        [_parStatusButton setTitleColor:[UIColor ersBlackColor] forState:UIControlStateNormal];
        [_parStatusButton setShowsTouchWhenHighlighted:YES];
        [_parStatusButton.titleLabel setNumberOfLines:2];
        [_parStatusButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_parStatusButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.contentView addSubview:_parStatusButton];
        [self.contentView bringSubviewToFront:_parStatusButton];
    }
    return _parStatusButton;
}

- (void)setCellTitle:(NSString *)title
{
    if (![title isEqualToString:_cellTitle]) {
        _cellTitle = title;
        // set new value in cell
        if (!_cellColors) {
            _cellColors = [FireCellColor bgColor:[UIColor whiteColor] textColor:[UIColor blackColor]];
        }
    }
    [self updateTitle];
}

- (void)updateTitle {
    CGFloat fontSize = 16.0; // default size, decreased for longer names
    CGFloat sizeAdjust = 0;
    if (!_cellTitle) {
        _cellTitle = @"";
    }
    if (!_cellColors) {
        _cellColors = [FireCellColor bgColor:[UIColor whiteColor] textColor:[UIColor blackColor]];
    }
    NSUInteger labelLength = _cellTitle.length;
    if (labelLength > 5) {
        sizeAdjust = (CGFloat) (labelLength - 4);
    }
    fontSize = fontSize - sizeAdjust;
    UIColor *buttonColor = _cellColors.bgColor;
    UIColor *textColor = _cellColors.textColor;
    NSString *buttonText = [_cellTitle stringByAppendingFormat:@"\n "];
    UIControlState controlState = UIControlStateDisabled;
    if (self.parStatusButton.isEnabled)
    {
        buttonColor = [UIColor whiteColor];
        
        // set text color black and add a question mark
        buttonText = [_cellTitle stringByAppendingFormat:@"\n?"];
        textColor = [UIColor blackColor];
        controlState = UIControlStateNormal;
    }
    [_parStatusButton setBackgroundColor:buttonColor];
    [_parStatusButton.titleLabel setNumberOfLines:2];
    [_parStatusButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    NSAttributedString *attributedStringTitle = [NSAttributedString attributedStringFrom:buttonText foregroundColor:textColor backgroundColor:buttonColor size:fontSize];
    [_parStatusButton setAttributedTitle:attributedStringTitle forState:controlState];
}

- (NSString *)cellTitle
{
    return _cellTitle;
}

- (void)setCellColors:(FireCellColor *)cellColors
{
        _cellColors = cellColors;
        // set new value in cell
        if (!_cellTitle) {
            _cellTitle = @"";
        }
        self.contentView.backgroundColor = _cellColors.bgColor;
        [self updateTitle];
}


#pragma mark - utilities

@end
