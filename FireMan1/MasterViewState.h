//
//  MasterViewState.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/28/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MasterViewState : NSObject
@property (nonatomic, strong) NSArray *editableItems;
@property (nonatomic) NSInteger index;
@property (nonatomic) BOOL isAutoSelect;
@property (nonatomic, strong) NSString *title;
@end
