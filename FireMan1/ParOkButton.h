//
//  ParOkButton.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/25/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParOkButton : UIButton
@property (nonatomic, strong) NSString *taskId;
@end
