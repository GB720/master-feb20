//
//  AddNewUnitSegue.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UnitSelectionHeader;

@interface AddNewUnitSegue : UIStoryboardSegue
@property (weak, nonatomic) UnitSelectionHeader *cell;
@end
