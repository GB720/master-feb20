//
//  FireStationView.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/14/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DragDropManager.h"

@interface FireStationView : UIView <DragDropAcceptor>

@end
