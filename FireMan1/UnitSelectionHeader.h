//
//  UnitSelectionHeader.h
//  FireMan1
//
//  Created by Gerald Boyd on 6/6/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitSelectionHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIButton *addUnitButton;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
