//
//  FireUnitListVCView.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/16/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireUnitListVC.h"
#import "FireDepartment.h"
#import "Incident.h"
#import "ManagementViewCell.h"
#import "FireManViewController.h"
#import "FireUnit+Identification.h"
#import "ListItem+Create.h"
#import "ManagementCollectionView.h"
#import "Incident+Create.h"
#import "IncidentLog+Log.h"
#import "StagingGroup+Create.h"
#import "StagingHeaderView.h"
#import "UIColor+EmerResStrat.h"
#import "NSAttributedString+ersCategory.h"

static NSUInteger const MAX_UNITS_IN_LIST = 12;
static NSUInteger const LIST_WIDTH = 300;
static NSUInteger const ROW_HEIGHT = 44;

@interface FireUnitListVC ()
@property (strong, nonatomic) NSArray *fireUnits;
@end

@implementation FireUnitListVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    ListItem *activeGroup = [self getManagedTaskWithValue:self.currentCell.taskCurrentValue];
    _fireUnits = [self.incident sortedFireUnitsWithActiveGroup:activeGroup];
    NSInteger len = _fireUnits.count * ROW_HEIGHT;
    if (_fireUnits.count > MAX_UNITS_IN_LIST) {
        len = MAX_UNITS_IN_LIST * ROW_HEIGHT;
    }
    CGSize size = CGSizeMake(LIST_WIDTH, len);
    self.preferredContentSize = size;


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows = self.fireUnits.count;

    // Return the number of rows in the section.
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"fireunitListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIColor *textColor = [UIColor ersBlackColor];
    UIColor *whiteText = [UIColor whiteColor];

    // Configure the cell...
    NSUInteger itemIndex = (NSUInteger) indexPath.item;
    FireUnit *fireUnit = self.fireUnits[itemIndex];
    cell.textLabel.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = fireUnit.prefixedName;
    NSString *fireUnitDetail = @"No Group";

    // set background color based on current activity and assignment
    if ([fireUnit.assignedTo.currentValue isEqualToString:self.currentCell.taskCurrentValue])
    {
        cell.textLabel.backgroundColor = [UIColor ersGreenColor];
        textColor = whiteText;
        fireUnitDetail = fireUnit.assignedTo.name;
    }
    else if (fireUnit.assignedTo) {
        cell.textLabel.backgroundColor = [UIColor ersLightGoldColor];
        fireUnitDetail = fireUnit.assignedTo.name;
    }
    else if (fireUnit.stagingGroup) {
        if (fireUnit.activeAge < fireUnit.stagingGroup.timeRequired.integerValue) {
            cell.textLabel.backgroundColor = fireUnit.stagingColor;
        }
        fireUnitDetail = [NSString stringWithFormat:@"%@",fireUnit.stagingGroup.name];
    }
    if (fireUnit.coordinatorFor) {
        if ([fireUnit.coordinatorFor.name isEqualToString:FD_GROUP_TERMINATED]) {
            fireUnitDetail = [NSString stringWithFormat:@"%@", fireUnitDetail];
        }
        else {
            cell.textLabel.backgroundColor = [UIColor ersRedColor];
            textColor = whiteText;
            fireUnitDetail = [NSString stringWithFormat:@"%@, Coordinator for: %@", fireUnitDetail, fireUnit.coordinatorFor.name];
        }
    }

    if (fireUnit.commanderFor) {
        cell.textLabel.backgroundColor = [UIColor ersRedColor];
        textColor = whiteText;
        fireUnitDetail = [NSString stringWithFormat:@"%@, Incident Commander", fireUnitDetail];
    }
    cell.textLabel.textColor = textColor;
    cell.detailTextLabel.text = fireUnitDetail;
    cell.detailTextLabel.attributedText = [NSAttributedString attributedStringFrom:fireUnitDetail
                                                                   foregroundColor:[UIColor ersBlackColor]
                                                                   backgroundColor:[UIColor clearColor]
                                                                              size:14.0];
    return cell;
}


#pragma mark - Table view delegate

- (ListItem *) getManagedTaskWithValue: (NSString *)value
{
    NSSet *managedTasks = self.incident.managedTasks;
    for (ListItem *managedTask in managedTasks) {
        if ([managedTask.currentValue isEqualToString:value]) {
            return managedTask;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger itemIndex = (NSUInteger) indexPath.item;
    FireUnit *fireUnit = self.fireUnits[itemIndex];
    if (self.currentCell) {
        ListItem *managedTask = [self getManagedTaskWithValue:self.currentCell.taskCurrentValue];
        if (managedTask) {
            FireUnit *previousCoordinator = [self setCoordinatorUnit:fireUnit forGroup:managedTask];
            [self.currentCell.coordinatingUnitButton setTitle:fireUnit.prefixedName forState:UIControlStateNormal];
            [self.currentCell.coordinatingUnitButton setTitleColor:self.fireUnitListDelegate.colorForCoordinatorIsSet forState:UIControlStateNormal];
            if (fireUnit.stagingGroup || (previousCoordinator && previousCoordinator.stagingGroup)) {
                [self.fireUnitListDelegate setNeedsRefreshView];
            }
            else {
                [self.fireUnitListDelegate.managementView reloadData];
            }
        }
    }
    else if (self.headerView) {
        [self setCoordinatorUnit:fireUnit forGroup:self.stagingGroup];
        NSString *headerText = [NSString stringWithFormat:@"%@ - %@", self.stagingGroup.name, fireUnit.prefixedName];
        [self.headerView.StageNameButton setTitle:headerText forState:UIControlStateNormal];
        [self.fireUnitListDelegate setNeedsRefreshView];
    }
    self.fireUnits = nil;
    [self performSegueWithIdentifier:@"unwindFromFireUnitList" sender:self];
}

// returns the previous coordinating unit
- (FireUnit *)setCoordinatorUnit:(FireUnit *)fireUnit forGroup:(ListItem *)group {
    FireUnit *previousCoordinator = group.coordinatingUnit;
    if (fireUnit.coordinatorFor)
            {
                // remove as coordinator
                ListItem *task = fireUnit.coordinatorFor;
                task.coordinatingUnit = nil;
                [self.fireUnitListDelegate setNeedsRefreshView];
                [self.incident.log action:[NSString stringWithFormat:@"%@ removed as supervisor for group %@",fireUnit.prefixedName, task.name]
                                     unit:fireUnit task:task];
            }
    group.coordinatingUnit = fireUnit;
    [self.incident.log action:[NSString stringWithFormat:@"%@ set as supervisor for group: %@",fireUnit.prefixedName, group.name]
                         unit:fireUnit task:group];
    return previousCoordinator;
}

@end
