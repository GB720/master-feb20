//
//  AddListFromDepartmentVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/11/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FireDepartment;
@class DepartmentTaskList;
@class Incident;

@interface AddListFromDepartmentVC : UITableViewController

@property(nonatomic, strong) FireDepartment *fireDepartment;
@property(nonatomic, strong) Incident *incident;
@property(nonatomic, readonly) NSArray *selectedDepartmentLists;
@end
