//
//  IncidentSelectionDelegate.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/13/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Incident.h"

@protocol IncidentSelectionDelegate <NSObject>
- (void) incidentDeleted:(Incident *)deletedIncident;
- (BOOL) canDelete:(Incident *)incident;
@end
