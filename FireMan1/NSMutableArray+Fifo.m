//
// Created by Gerald Boyd on 10/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "NSMutableArray+Fifo.h"


@implementation NSMutableArray (Fifo)
- (void)enqueue:(id) item
{
    [self addObject:item];
}

- (id) dequeue
{
    id item = [self firstObject];
    if (item) {
        [self removeObject:item];
    }
    return item;
}

@end