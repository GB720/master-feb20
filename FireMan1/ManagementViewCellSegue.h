//
// Created by Gerald Boyd on 5/17/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class ManagementViewCell;


@interface ManagementViewCellSegue : UIStoryboardSegue
@property (weak, nonatomic) ManagementViewCell *cell;
@end