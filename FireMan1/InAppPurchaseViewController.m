//
//  InAppPurchaseViewController.m
//  TotalCommand
//
//  Created by user136147 on 4/5/18.
//  Copyright © 2018 Gerald Boyd. All rights reserved.
//

#import "InAppPurchaseViewController.h"

@interface InAppPurchaseViewController ()

@end

@implementation InAppPurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // TODO read the product definitions from the StoreKit
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"product_ids"
                                         withExtension:@"plist"];
    NSArray *productIdentifiers = [NSArray arrayWithContentsOfURL:url];
    for (int i = 0; i < productIdentifiers.count; i++) {
        NSString *prodId = productIdentifiers[i];
        NSLog(@"prodId=%@", prodId);
    }
    [self validateProductIdentifiers:productIdentifiers];
    // check for products in store
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self performSegueWithIdentifier:@"unwindFromInAppPurchse" sender:self];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
{
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
                                          initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
    
    // Keep a strong reference to the request.
    self.request = productsRequest;
    self.request.delegate = self;
    [productsRequest start];
}

// SKProductsRequestDelegate protocol method
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    self.products = response.products;
    
    for (NSString *invalidIdentifier in response.invalidProductIdentifiers)
    {
        // Handle any invalid product identifiers.
        NSLog(@"Invalid Product Identifier: %@", invalidIdentifier);
    }
    
    // NSUInteger numberOfProducts = self.products.count;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.products.count > 0)
        return 1;
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.products.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productCell" forIndexPath:indexPath];
    NSArray *unitStrings = @[@"day", @"week", @"month", @"year"];
    const NSUInteger YEAR = 3;
    
    // Configure the cell...
    SKProduct *product = self.products[indexPath.row];
    NSString *desc = product.localizedDescription;
    NSString *title = product.localizedTitle;
    NSDecimalNumber *price = product.price;
    NSString *unitString = unitStrings[YEAR];
    NSUInteger numUnits = 1;
    if (@available(iOS 11.2, *)) {
        SKProductPeriodUnit periodUnit = product.subscriptionPeriod.unit;
        unitString = unitStrings[periodUnit];
        numUnits = product.subscriptionPeriod.numberOfUnits;
    }
    NSString *fullTitle = [NSString stringWithFormat:@"%@ %@ %@", title, @"  for ", price.stringValue];
    cell.textLabel.text = fullTitle;
    cell.textLabel.accessibilityValue = cell.textLabel.text;
    cell.detailTextLabel.text = desc;
    cell.detailTextLabel.accessibilityValue = desc;
    cell.tag = indexPath.row;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)paymentQueue:(nonnull SKPaymentQueue *)queue updatedTransactions:(nonnull NSArray<SKPaymentTransaction *> *)transactions {
    // TODO
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    // TODO
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    // TODO
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    // TODO
}

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
    return parentSize;
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
        // TODO
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    // TODO
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
        // TODO
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
        // TODO
}

- (void)setNeedsFocusUpdate {
        // TODO
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
    return true; 
}

- (void)updateFocusIfNeeded {
        // TODO 
}

@end
