//
//  CreateNewListVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/10/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "CreateNewListVC.h"
#import "DepartmentTaskList.h"
#import "FireDepartment.h"
#import "SortedTaskList.h"
#import "EditListNameCell.h"
#import "EditListItemCell.h"
#import "UIColor+EmerResStrat.h"


@interface CreateNewListVC ()
- (IBAction)listNameFieldChanged:(UITextField *)sender;
- (IBAction)listNameFieldEditDidEnd:(UITextField *)sender;
- (IBAction)itemNameFieldChanged:(UITextField *)sender;
- (IBAction)itemNameFieldEditDidEnd:(UITextField *)sender;
- (IBAction)addNewItemButtonPress:(UIButton *)sender;
@property (nonatomic, readwrite) NSMutableArray *taskList;
@property (nonatomic, readwrite) NSString *listName;
@property (nonatomic, readwrite) NSMutableArray *updatedTaskList;
@property (nonatomic, readwrite) NSString *updatedListName;



@property(nonatomic, readwrite) NSIndexPath *activeIndexPath;
@property(nonatomic, strong) UIColor *textFieldBackgroundColor;
@end

@implementation CreateNewListVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _textFieldBackgroundColor = [UIColor ersTextFieldBackgroundColor];

    self.tableView.editing = YES;
    self.tableView.accessibilityIdentifier = @"createNewListTableView";
    if (self.currentList) {
        SortedTaskList *sortedTaskList = [[SortedTaskList alloc] initWithList:self.currentList];
        _taskList = [NSMutableArray arrayWithArray:sortedTaskList.itemNames];
        _listName = sortedTaskList.name;
    }
    else {
        _taskList = [NSMutableArray arrayWithCapacity:10];
        _listName = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // one task list with multiple task list items
    // one button in the second section
    return 2;
}

// number of row for each section of the table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _taskList.count + 1;  // list name plus the number of list items
    }
    else if (self.listName) {
            return 1;  // one row in the second section for the add new item button
    }
    return 0;
}

// get the appropriate cell for the table row and populate it
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSInteger tag = row;
    UITableViewCell *returnCell;
    if (section == 0) {
        if (row == 0) {
            if ((self.activeIndexPath && self.activeIndexPath.row == 0) || !self.listName) {
                // return editable list name header
                EditListNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editListNameCell" forIndexPath:indexPath];
                [self setupTextField:cell.listNameTextField name:self.listName defaultName:@"Enter list name" tag:0];
                returnCell = cell;
            }
            else {
                // return the list name header cell
                returnCell = [tableView dequeueReusableCellWithIdentifier:@"listTitleCell" forIndexPath:indexPath];
                returnCell.textLabel.text = self.listName;
            }
        }
        else {
            NSUInteger itemIndex = (NSUInteger) (row - 1);
            if (self.activeIndexPath.row == row) {
                // return editable list item cell
                EditListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editListItemCell" forIndexPath:indexPath];
                [self setupTextField:cell.itemTextField name:self.taskList[itemIndex] defaultName:@"Enter item description" tag:tag];
                returnCell = cell;
            }
            else {
                // return the list item cell
                returnCell = [tableView dequeueReusableCellWithIdentifier:@"listItemCell"  forIndexPath:indexPath];
                returnCell.textLabel.text = self.taskList[itemIndex];
            }
        }
    }
    else {
        // return the cell for the add-list-item button
        returnCell = [tableView dequeueReusableCellWithIdentifier:@"addNewItemButtonCell"  forIndexPath:indexPath];
    }
    return returnCell;
}

- (void)setupTextField:(UITextField *)field name:(NSString *)name defaultName:(NSString *)defaultName tag:(NSInteger)tag
{
    field.tag = tag;
    field.delegate = self;
    if (!name || ([name isEqualToString:defaultName])) {
        field.text = defaultName;
        field.clearsOnInsertion = YES;
    }
    else {
        field.text = name;
        field.clearsOnBeginEditing = NO;
    }
    field.selected = YES;
    field.backgroundColor = self.textFieldBackgroundColor;
    field.clearButtonMode = UITextFieldViewModeAlways;
    [field becomeFirstResponder];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 0) {
        // can not move or edit the add-item button
        return NO;
    }
    else if ([indexPath isEqual:self.activeIndexPath]) {
        return NO;
    }
    else if (indexPath.row == 0) {
        // can not delete or move the list title header cell
        return NO;
    }

    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteListItem:indexPath];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

// deletes the specified ListItem or List from the data model
- (void)deleteListItem:(NSIndexPath *)indexPath
{
    NSUInteger itemIndex = (NSUInteger) (indexPath.row - 1);  // allow for title cell
    [self.taskList removeObjectAtIndex:itemIndex];

    // redisplay
    [self.tableView reloadData];
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSInteger toSection = toIndexPath.section;
    NSUInteger fromRow = (NSUInteger) fromIndexPath.row;
    NSInteger toRow = toIndexPath.row;
    BOOL isSectionHeader = NO;

    if (toSection == 0) {
        // adjust for indexpath including the list name title cell
        if (fromRow > 0) {
            fromRow--;
        }
        else {
            isSectionHeader = YES;
        }
        if (toRow > 0) {
            toRow--;
        }

        // adjust the item storage list
        if ([fromIndexPath compare:toIndexPath] != NSOrderedSame) {
            if (isSectionHeader) {
                // ignore -- can't move section header
                assert (!isSectionHeader);
            }
            else {
                NSString *itemToMove = (self.taskList)[fromRow];
                [self.taskList removeObjectAtIndex:fromRow];
                [self.taskList insertObject:itemToMove atIndex:(NSUInteger) toRow];
            }
        }
    }
    [self.tableView reloadData];    // reload in case user moved a row to bad location
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 0) {
        return NO;
    }
    else {
        if (indexPath.row == 0) {
            // can not move list title cell
            return NO;
        }
    }

    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - Task Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    NSMutableArray *itemPaths = [NSMutableArray arrayWithCapacity:3];
    BOOL reloadRows = YES;

    // reload and close the description cell being edited
    if (self.activeIndexPath) {
        // get previous cell set for description edit
        [itemPaths addObject:self.activeIndexPath];
    }

    if (section == 0) {
        // row selected - set it for description editing
        self.activeIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [itemPaths addObject:self.activeIndexPath];
    }
    else {
        reloadRows = NO;
    }

    if (reloadRows) {
        [tableView reloadRowsAtIndexPaths:itemPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // unwind occurring
    self.updatedTaskList = self.taskList.copy;
    self.updatedListName = self.listName;
}


#pragma text field notifications

- (IBAction)listNameFieldChanged:(UITextField *)textField
{
    // value changed
    NSUInteger row = 0;

    // get edited description
    NSString *newDescription = textField.text;

    // update the List name
    self.listName = newDescription;
    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];

}

- (void) resetActiveIndexPath:(NSIndexPath *)indexPath
{
    if ([self.activeIndexPath isEqual:indexPath]) {
        self.activeIndexPath = nil;
    }
}

- (IBAction)listNameFieldEditDidEnd:(UITextField *)textField
{
    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [self.tableView reloadData];
}

- (IBAction)itemNameFieldChanged:(UITextField *)textField
{
    // value changed
    NSInteger row = textField.tag;
    NSUInteger itemIndex = (NSUInteger) (row - 1);
    assert(row > 0);

    // get edited description
    NSString *newDescription = textField.text;

    // update the ListItem
    (self.taskList)[itemIndex] = newDescription;
    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
}

- (IBAction)itemNameFieldEditDidEnd:(UITextField *)textField
{
    NSInteger row = textField.tag;

    // clear active cell edit
    [self resetActiveIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    [self.tableView reloadData];

}

- (IBAction)addNewItemButtonPress:(UIButton *)sender
{
    // add new item to list
    NSString *newItemDesc = @"Enter item description";
    [self.taskList addObject:newItemDesc];

    // add tableview cell
    NSUInteger newItemRow = self.taskList.count;  // on screen index includes the header cell
    NSIndexPath *newItemIndexPath = [NSIndexPath indexPathForRow:newItemRow inSection:0];
    self.activeIndexPath = newItemIndexPath;

    // insert the new row
    NSArray *indexPaths = @[newItemIndexPath];
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    // [self.tableView selectRowAtIndexPath:newItemIndexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
}

#pragma text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    textField.selected = NO;
    [textField resignFirstResponder];
    return YES;
}

@end
