//
//  DepartmentDetailVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 1/23/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "DepartmentDetailVC.h"
#import "EditableItem.h"
#import "DepartmentMasterVC.h"
#import "EditDepartmentDetailViewCell.h"
#import "EditContainer.h"
#import "RightSideHeaderCell.h"
#import "UIColor+EmerResStrat.h"
#import "FDPermittedValues.h"

@interface DepartmentDetailVC ()
- (IBAction)addItemButtonPress:(id)sender;

@property(nonatomic) BOOL isDetail;
@property(nonatomic, strong) NSIndexPath *activeIndexPath;

- (IBAction)editingChanged:(UITextField *)sender;
@property(nonatomic) BOOL isEditNewCell;

@property(nonatomic) BOOL isPermittedValueMode;

- (IBAction)editDidEnd:(UITextField *)sender;

@end

@implementation DepartmentDetailVC {
}
@dynamic activeIndexPath;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self clearSortedCache];
    self.isEditNewCell = NO;
    self.isPermittedValueMode = NO;
    self.tableView.editing = YES;
    self.tableView.accessibilityIdentifier = @"Details Table";
    self.tableView.accessibilityLabel = @"Edit department details table";

    self.descriptionBackgroundColor = [UIColor ersTextFieldBackgroundColor];
    self.errorBackgroundColor = [UIColor ersRedColor];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (self.masterItem) {
        return 1;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    self.isDetail = YES;
    
    // Return the number of rows in the section.
    if (!self.masterItem) {
        count = 0;
        self.isPermittedValueMode = NO;
    }
    else if (self.masterItem.editableItems) {
        // get count of rows to display
        count = self.masterItem.editableItems.count;
        if (count == 0) {
            count = 1;  //  1 row for empty indicator
        }
        count++;   // add a row for the header

        // get title
        if (self.masterItem.itemName) {
            self.title = self.masterItem.itemName;
        }
        self.isPermittedValueMode = NO;
    }
    else if (self.masterItem.permittedValues) {
        // get count of rows to display
        count = self.masterItem.permittedValues.count;
        if (count == 0) {
            count = 1;  //  1 row for empty indicator
        }
        count++;   // add a row for the header

        // get title
        if (self.masterItem.itemName) {
            self.title = [NSString stringWithFormat:@"%@ (choose one)",self.masterItem.itemName];
        }
        self.isPermittedValueMode = YES;
    }
    else if (self.masterItem.value) {
        count = 2;     //  one row for header and one row for value
        self.isPermittedValueMode = NO;
    }
    if (self.masterItem && count == 0) {
        count = 1;
        self.isDetail = NO;
        self.isPermittedValueMode = NO;
    }
    
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    NSInteger itemIndex = row - 1;
    BOOL isClearOnBeginEditing = NO;
    NSIndexPath *currentActiveIndexPath; 
    

    NSString *cellIdentifier = @"editDepartmentBasicCell";

    if (!self.activeIndexPath) {
        NSInteger index = [self checkForEmptyValueIn:self.sortedItems];
        if (index >= 0) {
            self.activeIndexPath = [NSIndexPath indexPathForRow:index + 1 inSection:section];
        }
    }

    if (row == 0) {
        cellIdentifier = @"rightSideTitle";
    }
    else {
        if (self.activeIndexPath
                && self.activeIndexPath.section == section
                && self.activeIndexPath.row == row) {
            cellIdentifier = @"editDepartmentEditDetailCell";
            id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
            if ([item.value isEqualToString:FILLER_EMPTY]) {
                isClearOnBeginEditing = YES;
            }
        }
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                            forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.editingAccessoryType = UITableViewCellAccessoryNone;
    cell.editingAccessoryView = nil;
    cell.accessoryView = nil;
    
    NSInteger cellTag = row;
    cell.tag = cellTag;

    // Configure the cell...
    currentActiveIndexPath = self.activeIndexPath;
    if (self.isDetail) {
        if (row == 0) {
            [self setupHeaderCell:cell];
        }
        else {
            if (self.sortedItems.count) {
                // editable items
                id <FDedit> item;
                id editableItem;
                editableItem = self.sortedItems[(NSUInteger) itemIndex];

                if ([editableItem conformsToProtocol:@protocol(FDedit)]) {
                    item = editableItem;

                    if (item.itemName && !item.value) {
                        cell.textLabel.textAlignment = NSTextAlignmentLeft;
                        cell.textLabel.text = item.itemName;
                        cell.textLabel.accessibilityValue = cell.textLabel.text;
                    }
                    else if (item.value) {
                        if ([cellIdentifier isEqualToString:@"editDepartmentEditDetailCell"]) {
                            [self setupEditCell:cell cellTag:cellTag item:item clearOnBeginEditing:isClearOnBeginEditing ];
                        }
                        else {
                            [self setupPlainCell:cell item:item];
                        }
                    }
                    if (!self.isPermittedValueMode
                        && (item.itemName || item.value)
                            && (item.editableItems || item.permittedValues)) {
                        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
                        cell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
                        cell.editingAccessoryView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DetailArrow.png"]];
                        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DetailArrow.png"]];
                        cell.accessoryView.tag = cellTag;
                        cell.accessoryView.backgroundColor = [UIColor whiteColor];
                    }
                }
            }
            else {
                cell.textLabel.text = @"None";
                cell.textLabel.accessibilityValue = cell.textLabel.text;
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            }
        }
    }
    self.activeIndexPath = currentActiveIndexPath;
    return cell;
}

- (NSInteger)checkForEmptyValueIn:(NSArray *)array
{
    NSInteger index = 0;
    for (id <FDedit> item in array) {
        assert ([item conformsToProtocol:@protocol(FDedit)]);
        if ([item.value isEqualToString:FILLER_EMPTY]) {
            return index;
        }
        index++;
    }
    return -1;   // -1 means no empties found in array
}


- (void)setupHeaderCell:(UITableViewCell *)cell
{
// set up the header
    RightSideHeaderCell *headerCell = (RightSideHeaderCell *) cell;
    headerCell.heading.text = self.title;
    headerCell.heading.accessibilityValue = headerCell.heading.text;
    headerCell.accessoryType = UITableViewCellAccessoryNone;
    headerCell.backgroundColor = [UIColor ersListHeaderColor];
    headerCell.contentView.backgroundColor = [UIColor ersListHeaderColor];
    headerCell.heading.backgroundColor = [UIColor clearColor];

    BOOL canEnableAddButton = [self canAddItems];

    if (canEnableAddButton) {
        [headerCell.contentView bringSubviewToFront:headerCell.addItemButton];
        headerCell.addItemButton.hidden = NO;
        [headerCell.addItemButton setEnabled:YES];
    }
    else {
        headerCell.addItemButton.hidden = YES;
        [headerCell.addItemButton setEnabled:NO];
    }
}

//
// checks whether the master item can add editable items and
// checks whether a field of an item is being edited and
// checks whether items of the masterItem.itemName type can be added.
//
- (BOOL)canAddItems
{
    BOOL canAddItem = YES;
    if (!self.activeIndexPath) {
        if ([self.masterItem respondsToSelector:@selector(mayAddEditableItem)]
                && self.masterItem.mayAddEditableItem) {
            if ([self.masterItem respondsToSelector:@selector(mayAddEditableItemForItemName:)]) {
                canAddItem = [self.masterItem mayAddEditableItemForItemName:self.masterItem.itemName];
            }
        }
        else {
            canAddItem = NO;
        }
    }
    else {
        canAddItem = NO;
    }
    return canAddItem;
}

- (void)setupPlainCell:(UITableViewCell *)cell item:(id <FDedit>)item
{
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.editingAccessoryType = UITableViewCellAccessoryNone;
    cell.accessoryView = nil;
    cell.editingAccessoryView = nil;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor whiteColor];

    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    NSString *cellSeparator = @"";
    NSString *itemName = item.itemName;
    if (item.itemName && item.value) {
        cellSeparator = @": ";
    }
    if (!itemName) {
        itemName = @"";
    }
    NSString *itemValue = item.value;
    if (!itemValue) {
        itemValue = @"";
    }
    NSString *cellText = [NSString stringWithFormat:@"%@%@%@", itemName, cellSeparator, itemValue];
    cell.textLabel.text = cellText;
    cell.textLabel.accessibilityValue = cell.textLabel.text;
}

- (void)setupEditCell:(UITableViewCell *)cell cellTag:(NSInteger)cellTag item:(id <FDedit>)item clearOnBeginEditing:(BOOL)isClearOnBeginEditing
{
    self.textFieldContents = nil;

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.accessoryView = nil;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor whiteColor];

    EditDepartmentDetailViewCell *editCell = (EditDepartmentDetailViewCell *) cell;
    editCell.itemTextField.text = item.value;
    if (item.itemName) {
        editCell.itemNameLabel.text = [NSString stringWithFormat:@"%@:", item.itemName];
        editCell.itemNameLabel.hidden = NO;
        [editCell.itemNameLabel setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal];
    }
    else {
        editCell.itemNameLabel.text = @"";
        editCell.itemNameLabel.hidden = YES;
        [editCell.itemNameLabel setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal];
    }
    editCell.itemTextField.hidden = NO;
    editCell.itemTextField.tag = cellTag;
    editCell.itemTextField.delegate = self;
    editCell.itemTextField.selected = YES;
    editCell.itemTextField.backgroundColor = self.descriptionBackgroundColor;
    editCell.itemTextField.clearsOnBeginEditing = isClearOnBeginEditing;
    editCell.itemTextField.clearButtonMode = UITextFieldViewModeAlways;
    editCell.itemTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    [editCell.itemNameLabel invalidateIntrinsicContentSize];
    [editCell.itemNameLabel sizeToFit];
    [editCell.itemTextField sizeToFit];
    [editCell.contentView bringSubviewToFront:editCell.itemTextField];
    [editCell.itemTextField becomeFirstResponder];
}


// support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    BOOL isEditable = YES;

    if ([indexPath isEqual:self.activeIndexPath]) {
            isEditable = NO;
    }

    return isEditable;
}

// Support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteListItem:indexPath];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle editingStyle = UITableViewCellEditingStyleNone;

    if (indexPath.row > 0) {  // row zero is the table header
        if (!self.activeIndexPath
                || self.activeIndexPath.row != indexPath.row) {
            NSUInteger itemIndex = (NSUInteger) indexPath.row - 1;
            if (self.sortedItems.count) {
                id <FDedit> item = self.sortedItems[itemIndex];
                if ([self.masterItem respondsToSelector:@selector(mayDeleteEditableItem)]
                        && self.masterItem.mayDeleteEditableItem) {
                    if ([item respondsToSelector:@selector(isDeletable)]
                            && item.isDeletable) {
                        editingStyle = UITableViewCellEditingStyleDelete;
                    }
                }
            }
        }
    }
    return editingStyle;
}


// deletes the specified ListItem or List from the data model
- (void)deleteListItem:(NSIndexPath *)path
{
    NSUInteger row = (NSUInteger) path.row;
    NSUInteger itemIndex = row - 1;

    id <FDedit> editableItem = self.sortedItems[itemIndex];
    id <FDedit> o = self.masterItem;
    if ([o respondsToSelector:@selector(deleteEditableItem:forItemName:)]) {
        NSArray *newEditableItems = [o deleteEditableItem:editableItem forItemName:self.masterItem.itemName];
        // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        // NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
        if ([self.masterItem respondsToSelector:@selector(setEditableItems:)]) {
            self.masterItem.editableItems = newEditableItems;
        }
        // [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        [self loadNewView];
    }

}


// support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSUInteger fromRow = (NSUInteger)fromIndexPath.row;
    NSUInteger toRow = (NSUInteger)toIndexPath.row;
    BOOL isHeaderRow = NO;

    if (fromRow > 0) {
        fromRow--;
    }
    else {
        isHeaderRow = YES;
    }
    if (toRow > 0) {
        toRow--;
    }

    if ([fromIndexPath compare:toIndexPath] != NSOrderedSame) {
        NSDecimalNumber *one = [NSDecimalNumber one];
        id <FDedit> editableItem = self.sortedItems[fromRow];

        if (!isHeaderRow) {
            // adjust sort order of the item
            NSDecimalNumber *sortOrderValue = nil;

            if (self.sortedItems.count == 1 || toIndexPath.row == self.sortedItems.count){
                sortOrderValue = [NSDecimalNumber decimalNumberWithMantissa:(NSUInteger)self.sortedItems.count
                                                                   exponent:1 isNegative:NO];
                sortOrderValue = [sortOrderValue decimalNumberByAdding:one];
            }
            else {
                id <FDedit> insertionItem = self.sortedItems[toRow];
                NSDecimalNumber *insertionListValue = insertionItem.sortValue;
                if (fromRow < toRow) {
                    sortOrderValue = [insertionListValue decimalNumberByAdding:one];
                }
                else {
                    sortOrderValue = [insertionListValue decimalNumberBySubtracting:one];
                }
            }
            editableItem.sortValue = sortOrderValue;
        }
        [self loadNewView];
    }
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isMovable = NO;
    if (indexPath.row > 0) {
        NSUInteger itemIndex = (NSUInteger) indexPath.row - 1;
        if (self.sortedItems.count && (self.sortedItems.count > itemIndex)) {
            id <FDedit> item = self.sortedItems[itemIndex];
            if ([item respondsToSelector:@selector(isReorderable)] && item.isReorderable) {
                isMovable = YES;
            }
        }
    }
    return isMovable;
}

#pragma mark - table delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[EditDepartmentDetailViewCell class]]) {
        EditDepartmentDetailViewCell *editCell = (EditDepartmentDetailViewCell *) cell;
        [editCell.itemNameLabel sizeToFit];
        [editCell.itemTextField sizeToFit];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = (NSUInteger) indexPath.section;
    NSInteger row = indexPath.row;
    NSInteger itemIndex = row - 1;

    if (row == 0) {
        return;   // no selection on header
    }
    if ([self.activeIndexPath isEqual:indexPath]) {
        return;  // do nothing - already selected
    }
    if (self.sortedItems.count == 0) {
        return;
    }
    id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
    // UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSMutableArray *itemPaths = [NSMutableArray arrayWithCapacity:3];   // cells to bne reloaded

    // reload and close the description cell being edited
    if (self.activeIndexPath) {
        UITableViewCell *activeCell = [tableView cellForRowAtIndexPath:self.activeIndexPath];
        if ([activeCell isKindOfClass:[EditDepartmentDetailViewCell class]]) {

            // update item being actively edited if value changed
            NSUInteger editingItemIndex = (NSUInteger) (self.activeIndexPath.row - 1);
            id <FDedit> editingItem = self.sortedItems[editingItemIndex];
            [self updateItem:editingItem row:self.activeIndexPath.row];
            
            // close first responder (keyboard)
            EditDepartmentDetailViewCell *editCell = (EditDepartmentDetailViewCell *)activeCell;
            if (editCell.itemTextField.isFirstResponder) {
                [editCell.itemTextField resignFirstResponder];
                self.activeIndexPath = nil;
                [self resetActiveIndexPath:self.activeIndexPath];
            }
        }
        if (self.activeIndexPath) {
            [itemPaths addObject:self.activeIndexPath];
            [self resetActiveIndexPath:self.activeIndexPath];
        }
    }

    BOOL isUpdateLeftView = NO;
    if (item.editableItems
            || item.permittedValues
            || self.isPermittedValueMode) {
        // bring up items in the detail view
        // cell.tag = itemIndex;
        isUpdateLeftView = YES;
    }
    else if (item.value) {
        // new row selected - set it for editing
        self.activeIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [itemPaths addObject:self.activeIndexPath];
    }

    if (itemPaths.count > 0) {
        [itemPaths addObject:[NSIndexPath indexPathForItem:0 inSection:0]]; // reload header in case insert button changed
        [tableView reloadRowsAtIndexPaths:itemPaths withRowAnimation:UITableViewRowAnimationNone];
    }

    if (isUpdateLeftView) {
        if (self.isPermittedValueMode) {
            [self.masterItem.permittedValues selectValue:item.value];
            self.isPermittedValueMode = NO;
            [self.delegate masterItemDidUpdate];
            [self.delegate backButton];
        }
        else {
            if (item.editableItems || [item respondsToSelector:@selector(updateRelationshipWith:)]) {
                [self clearSortedCache];
                [self slideRightPanelIntoMaster:itemIndex];
            }
        }
    }

}

- (void)slideRightPanelIntoMaster:(NSInteger)itemIndex
{
    NSString *title;
    title = [self getTitleFromMasterItem];
    [self.delegate leftSideEditableItems:self.sortedItems index:itemIndex title:title isAutoSelect:YES];
}

- (NSString *)getTitleFromMasterItem
{
    NSString *title;
    if (self.masterItem.itemName) {
        title = self.masterItem.itemName;
    }
    else if (self.masterItem.value) {
        title = self.masterItem.value;
    }
    return title;
}

#pragma text field notifications

- (IBAction)editingChanged:(UITextField *)textField
{
    self.textFieldContents = textField.text;
    textField.backgroundColor = self.descriptionBackgroundColor;
}


- (IBAction)editDidEnd:(UITextField *)textField
{
    // ignore for now
}

- (void)updateItem:(id <FDedit>)item row:(NSInteger)row
{
    if (self.textFieldContents
        && ((!item.value) || ![item.value isEqualToString:self.textFieldContents])) {
        // something changed
        [item save:self.textFieldContents itemname:nil];
        if (self.activeIndexPath.row == row) {
            [self unselectActiveEditRow];
        }
        else {
            [self reloadTableRow:row section:0];
        }
        [self clearSortedCache];
        [self performSelector:@selector(loadNewView) withObject:nil afterDelay:2];
        [self.delegate masterItemDidUpdate];
        
    }
    else {
        // nothing changed
        if (self.activeIndexPath.row == row) {
            [self unselectActiveEditRow];
        }
        [self reloadTableRow:row section:0];
    }
    self.textFieldContents = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.activeIndexPath && (self.activeIndexPath.row == textField.tag)) {
        NSInteger row = textField.tag;
        NSInteger itemIndex = row - 1;
        id <FDedit> item = self.sortedItems[(NSUInteger) itemIndex];
        [self updateItem:item row:row];
    }
    self.textFieldContents = nil;
}




#pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     assert (![segue.identifier isEqualToString:@"segueToMaster"]);

 }

#pragma mark - EditorView

- (void)loadNewView
{
    // get selected item
    [self.tableView setEditing:NO];
    [self clearSortedCache];
    self.viewTitle = [self getTitleFromMasterItem];
    [self.tableView reloadData];
    [self.tableView setEditing:YES animated:YES];
}

- (void)setViewIndex:(NSInteger)index1
{
    // ignore for right side
}

- (void)setViewTitle:(NSString *)title
{
    self.title = title;
}

- (void)setIsAutoSelect:(BOOL)isAutoSelect
{
    // ignore for right side
}

- (void)setViewItems:(NSArray *)items
{
    // ignore for right side
}

/*
- (void) selectedItemDidUpdate
{
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    if (selectedRow) {
        NSArray *indexPaths = [NSArray arrayWithObject:selectedRow];
        [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
*/

- (void)closingView
{
    [self.tableView setEditing:NO];
    if (self.activeIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.activeIndexPath];
        if (cell && [cell isKindOfClass:[EditDepartmentDetailViewCell class]]) {
            EditDepartmentDetailViewCell *editCell = (EditDepartmentDetailViewCell *)cell;
            if (editCell.itemTextField.isFirstResponder) {
                [editCell.itemTextField resignFirstResponder];
            }
        }
        [self resetActiveIndexPath:self.activeIndexPath];
    }
    [self clearSortedCache];
}

- (void)selectedItemDidUpdate
{
        // do nothing
}

- (void)showPreviousView
{
        // do nothing
}


- (IBAction)addItemButtonPress:(id)sender
{
    if ([self.masterItem respondsToSelector:@selector(addEditableItemForItemName:)]) {
        NSArray *newEditableItems = [self.masterItem addEditableItemForItemName:self.masterItem.itemName];
        if ([self.masterItem respondsToSelector:@selector(setEditableItems:)]) {
            self.masterItem.editableItems = newEditableItems;
        }
        [self clearSortedCache];   // causes sorted item cache to be re-loaded and re-sorted

        // locate new item in sorted items
        NSInteger itemNeedsFillingIndex = [self checkForEmptyValueIn:self.sortedItems];
        if (itemNeedsFillingIndex >= 0) {
            id <FDedit> editableItem = self.sortedItems[(NSUInteger) itemNeedsFillingIndex];
            if (editableItem.editableItems) {
                [self slideRightPanelIntoMaster:itemNeedsFillingIndex];
                return;
            }
        }
        [self performSelector:@selector(loadNewView) withObject:nil afterDelay:0];
    }
}
@end
