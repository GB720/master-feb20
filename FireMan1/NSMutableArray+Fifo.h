//
// Created by Gerald Boyd on 10/6/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Fifo)
-(void) enqueue:(id) item;
-(id) dequeue;
@end