//
//  EditStagingGroupsViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/25/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Incident;

@interface EditStagingGroupsViewController : UITableViewController <UITableViewDelegate>
- (IBAction)unwindAddStagingLevelToIncident:(UIStoryboardSegue *)unwindSegue;
@property (nonatomic,weak) Incident *incident;
@property (nonatomic, readonly) NSSet *stagingGroups;
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
@end
