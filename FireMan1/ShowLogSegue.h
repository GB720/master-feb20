//
//  ShowLogSegue.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/21/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowLogSegue : UIStoryboardSegue
@property (weak, nonatomic) id sender;
@end
