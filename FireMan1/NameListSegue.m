//
//  NameListSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 4/24/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "NameListSegue.h"
#import "FireManViewController.h"
#import "GroupListViewController.h"
#import "ManagementViewCell.h"
#import "ManagementCollectionView.h"
#import "FireDepartment.h"

@implementation NameListSegue

- (void)perform
{

    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect
    // Set the size of the popover because it will be max sized otherwise

    FireManViewController *sourceController = (FireManViewController *)self.sourceViewController;
    GroupListViewController *destinationController = (GroupListViewController *)self.destinationViewController;
    destinationController.fireDepartment = sourceController.fireDepartment;
    destinationController.incident = sourceController.incident;
    Incident *incident = sourceController.incident;
    destinationController.currentCell = self.cell;
    destinationController.groupIndex = self.cell.tag;
    destinationController.selectedGroupName = nil;

    NSInteger numRows = sourceController.fireDepartment.groupNames.count + 1;
    NSInteger numGroups = incident.managedTasks.count - 1; // allow for dummy group
    numRows -= numGroups;
    if (numRows > 12) {
        numRows = 12;
    }
    if (numRows < 2) {
        numRows = 2;
    }
    NSInteger popoverHeight = numRows * 44;
    CGSize size = CGSizeMake(250, popoverHeight);
    destinationController.preferredContentSize = size;

    // present the controller
    destinationController.modalPresentationStyle = UIModalPresentationPopover;
    [sourceController presentViewController:destinationController animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [destinationController popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    popController.sourceView = self.cell.contentView;
    popController.sourceRect = self.cell.buttonLabel.frame;

//
//
//    [pop presentPopoverFromRect:self.cell.buttonLabel.frame
//                         inView:self.cell.contentView
//       permittedArrowDirections:UIPopoverArrowDirectionAny
//                       animated:YES];
//
}


@end
