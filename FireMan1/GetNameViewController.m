//
//  GetNameViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/8/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "GetNameViewController.h"
#import "GroupListViewController.h"
#import "Incident.h"
#import "FireDepartment.h"
#import "GroupName.h"
#import "UIColor+EmerResStrat.h"

@interface GetNameViewController ()
- (IBAction)getNameTextField:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UITextField *getNameField;
- (IBAction)getNameEditingChanged:(UITextField *)sender;

@end

@implementation GetNameViewController
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // NSLog(@"%@", @"GetNameViewController segue");
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _groupName = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _getNameField.delegate = self;
    _getNameField.accessibilityIdentifier = @"getNameField";
    self.preferredContentSize = CGSizeMake(320, 120);
    [self.getNameField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *text = [self checkName:textField];
    if (text) {
        [textField resignFirstResponder];
        return YES;
    }
    return NO;
}

- (IBAction)getNameEditingChanged:(UITextField *)sender
{
    [self checkName:sender];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getNameTextField:(UITextField *)sender
{
    NSString *text = [self checkName:sender];
    
    if (text) {
        self.groupName = text.capitalizedString;
        [self performSegueWithIdentifier:@"UnwindFromNewName" sender:self];
    }
}

- (NSString *)checkName:(UITextField *)sender {
    NSString *text = [sender.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    sender.backgroundColor = [UIColor whiteColor];
    if (text.length > 0 && [self isUniqueName:text]) {
        return text;
    }
    else {
        sender.backgroundColor = [UIColor ersRedColor];
        return nil;
    }
}

- (BOOL)isUniqueName:(NSString *)newName {
    BOOL isUnique = YES;
    FireDepartment *fireDepartment = self.incident.forDepartment;
    for (GroupName *groupName in fireDepartment.groupNames) {
        if ([newName.uppercaseString isEqualToString:groupName.name.uppercaseString]) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (IBAction)valueChanged:(UITextField *)sender
{
    // NSLog(@"getNameTextField valueChanged to %@", sender.text);
}
@end
