//
//  ListDescriptionSegue.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "ListDescriptionSegue.h"
#import "ListDescriptionVC.h"
#import "FireManViewController.h"

@implementation ListDescriptionSegue


- (void)perform
{

    // Add your own animation code here.
    // Grab the source view controller which is either a custom controller or a UITableViewController
    // Grab the destination controller
    // Create a UIPopoverController initialized with the destination controller
    // Get the cell of the currently selected row
    // Store the popover as a property on the destination controller (that way you can dismiss it and it will not be deallocated)
    // Present the popover using the cell's frame as the CGRect
    // Set the size of the popover because it will be max sized otherwise

    FireManViewController *sourceController = (FireManViewController *)self.sourceViewController;
    ListDescriptionVC *destinationController = (ListDescriptionVC *)self.destinationViewController;
    destinationController.fireDepartment = sourceController.fireDepartment;
    destinationController.incident = sourceController.incident;
    destinationController.currentCell = self.cell;
    destinationController.callingViewController = sourceController;
    destinationController.indexPath = self.indexPath;
    destinationController.tableView = self.tableView;

    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:destinationController];

    destinationController.popoverVwController = pop;

    CGSize size = CGSizeMake(240, 80);
    pop.popoverContentSize = size;


    [pop presentPopoverFromRect:self.cell.textLabel.frame
                         inView:self.cell.contentView
       permittedArrowDirections:UIPopoverArrowDirectionAny
                       animated:YES];

    //[[self sourceViewController] presentViewController:destinationController animated:YES completion:nil];

}


@end
