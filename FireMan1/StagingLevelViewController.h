//
//  StagingLevelViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/24/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FireDepartment;
@class StagingGroup;
@class Incident;

@interface StagingLevelViewController : UITableViewController
@property (nonatomic, weak) FireDepartment *fireDepartment;
@property (nonatomic, weak) Incident *activeIncident;
@property (nonatomic, strong) StagingGroup *addingStagingGroup;

- (IBAction)unwindAddStagingLevel:(UIStoryboardSegue *)unwindSegue;



@end
