//
//  UserIdentificationViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 8/25/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "UserIdentificationViewController.h"
#import "UIColor+EmerResStrat.h"
#import "FireDepartment+Create.h"


static NSInteger WIDTH = 300;
static NSInteger HEIGHT = 160;
static NSInteger USER_NAME_TAG = 0;
static NSInteger PIN_TAG = 1;

@interface UserIdentificationViewController ()
- (IBAction)idNumberEdited:(UITextField *)sender;
- (IBAction)userIdEdited:(UITextField *)sender;
@property (strong, nonatomic) IBOutlet UITextField *userIdOutlet;
@property (strong, nonatomic) IBOutlet UITextField *pinOutlet;
- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender;
- (IBAction)editingChanged:(UITextField *)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControlOutlet;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *pin;
@property (nonatomic, readwrite) User *user;
@end

@implementation UserIdentificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isUpdate = NO;
    self.userIdOutlet.delegate = self;
    self.pinOutlet.delegate = self;
    self.segmentedControlOutlet.selectedSegmentIndex = UISegmentedControlNoSegment;
    self.preferredContentSize = CGSizeMake(WIDTH,HEIGHT);
    [self.userIdOutlet becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
    [nc addObserver:self selector:@selector(needsUpdateViewAfterICloud:) name:@"iCloudUpdatedDepartment" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"unwindFromUserIdentification" sender:self];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    });
}

- (void)needsUpdateViewAfterICloud: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // if fire department updated
        NSSet *insertsAndUpdates = [self.fireDepartment cloudUpdates:notification];

        if ([self.fireDepartment isFireDepartmentUpdate:insertsAndUpdates]) {
            [self performSegueWithIdentifier:@"unwindFromUserIdentification" sender:self];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
        }
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender
{
    if ([identifier isEqualToString:@"unwindFromUserIdentification"]) {
        // get user entity for this user id
        
        // does user entity exist for the user id
        // is user id unique
        // does pin match for user id
    }
    return YES;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)idNumberEdited:(UITextField *)sender
{
    self.pin = sender.text;
}

- (IBAction)userIdEdited:(UITextField *)sender
{
    self.userName = sender.text;
}

- (void)validateAndUnwind
{
    // check validity
    if (self.userName && (self.userName.length > 0)) {
        User *user = [User userWithName:self.userName onDevice:nil context:self.fireDepartment.managedObjectContext];
        if (user && [self.pin isEqualToString:user.id]) {
            if (![self.user.name isEqualToString:user.name]) {
                self.user = user;
                self.isUpdate = YES;
            }
            else {
                self.isUpdate = NO;     // same name
            }
            [self performSegueWithIdentifier:@"unwindFromUserIdentification" sender:self];
        }
        else {
            NSLog(@"%s %@ %@ %@",__PRETTY_FUNCTION__, self.userName, self.pin, user);
            // show alert - invalid user name or id number
            [self alertMsg:@"Invalid user name or id number" alertTitle:@"Invalid"];
        }
    }
    else {
        // empty user name
        [self alertMsg:@"Invalid user name" alertTitle:@"Invalid"];
    }
}

- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender
{
    NSUInteger selectedSegmentIndex = (NSUInteger) [sender selectedSegmentIndex];
    self.segmentedControlOutlet.selectedSegmentIndex = UISegmentedControlNoSegment;
    
    if (selectedSegmentIndex == 0) {
        // cancel button
        self.isUpdate = NO;
        [self performSegueWithIdentifier:@"unwindFromUserIdentification" sender:self];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self validateAndUnwind];
    }
}


#pragma mark - text field delegate
- (IBAction)editingChanged:(UITextField *)textField
{
    if (textField.tag == USER_NAME_TAG) {
        self.userName = textField.text;
    }
    else if (textField.tag == PIN_TAG) {
        self.pin = textField.text;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // do nothing
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == PIN_TAG) {
        [self validateAndUnwind];
    }
    return YES;
}


#pragma mark - Alert msg
- (void) alertMsg:(NSString *) msg alertTitle:(NSString *)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertController* __weak weakAlert = alert;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
