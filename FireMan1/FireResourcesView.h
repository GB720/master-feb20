//
//  FireResourcesView.h
//  FireMan1
//
//  Created by Gerald Boyd on 2/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DragDropManager.h"
#import "FireResourceViewCell.h"


@interface FireResourcesView : UICollectionView <DragDropAcceptor>

@end
