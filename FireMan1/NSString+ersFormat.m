//
// Created by Gerald Boyd on 4/30/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "NSString+ersFormat.h"


@implementation NSString (ersFormat)
- (NSString *)minSize:(NSInteger)minSize
{
    NSString *newString = self;
    if (self.length < minSize) {
            newString = [self stringByPaddingToLength:(NSUInteger) minSize withString:@" " startingAtIndex:0];
    }
    return newString;
}

@end