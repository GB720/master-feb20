//
//  TableViewHeaderCell.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/27/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "TableViewHeaderCell.h"

@implementation TableViewHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
