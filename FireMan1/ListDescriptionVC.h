//
//  ListDescriptionVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FireDepartment;
@class Incident;
@class FireManViewController;

@interface ListDescriptionVC : UIViewController  <UITextFieldDelegate>
@property (weak, nonatomic) FireDepartment *fireDepartment;
@property (weak, nonatomic) Incident *incident;
@property (weak, nonatomic) UITableViewCell *currentCell;
@property (weak, nonatomic) FireManViewController *callingViewController;
@property (strong, nonatomic) UIPopoverController *popoverVwController;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) UITableView *tableView;
@end
