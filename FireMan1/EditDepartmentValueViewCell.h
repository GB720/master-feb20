//
//  EditDepartmentValueViewCell.h
//  FireMan1
//
//  Created by Gerald Boyd on 1/30/14.
//  Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditDepartmentValueViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *itemTextField;
@end
