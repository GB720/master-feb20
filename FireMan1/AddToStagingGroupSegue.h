//
//  AddToStagingGroupSegue.h
//  FireMan1
//
//  Created by Gerald Boyd on 9/19/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StagingHeaderView;

@interface AddToStagingGroupSegue : UIStoryboardSegue
@property (nonatomic, strong) UIView *anchorInView;
@property (nonatomic) CGRect anchorFrame;
@end
