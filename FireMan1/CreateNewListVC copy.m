//
//  CreateNewListVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 10/10/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "CreateNewListVC.h"
#import "DepartmentTaskList.h"
#import "FireDepartment.h"
#import "SortedTaskList.h"
#import "EditListNameCell.h"
#import "EditListItemCell.h"

@interface CreateNewListVC ()
- (IBAction)listNameFieldChanged:(UITextField *)sender;
- (IBAction)listNameFieldEditDidEnd:(UITextField *)sender;
- (IBAction)itemNameFieldChanged:(UITextField *)sender;
- (IBAction)itemNameFieldEditDidEnd:(UITextField *)sender;
- (IBAction)addNewItemButtonPress:(UIButton *)sender;
@property (nonatomic, strong) NSMutableArray *taskList;
@property (nonatomic, strong) NSString *listName;

@property(nonatomic, readwrite) NSIndexPath *activeIndexPath;
@property(nonatomic, strong) UIColor *textFieldBackgroundColor;
@end

@implementation CreateNewListVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    //_textFieldBackgroundColor = [UIColor colorWithRed:1.0 green:.686 blue:.047 alpha:1.0];

    _textFieldBackgroundColor = [UIColor ersTextFieldBackgroundColor];
    self.tableView.editing = YES;
    if (self.currentList) {
        SortedTaskList *sortedTaskList = [[SortedTaskList alloc] initWithList:self.currentList];
        _taskList = [NSMutableArray arrayWithArray:sortedTaskList.list];
        _listName = sortedTaskList.name;
    }
    else {
        _taskList = [NSMutableArray arrayWithCapacity:10];
        _listName = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // one task list with multiple task list items
    // one button in the second section
    return 2;
}

// number of row for each section of the table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _taskList.count + 1;  // list name plus the number of list items
    }
    else {
        return 1;  // one row in the second section for the add new item button
    }
}

// get the appropriate cell for the table row and populate it
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSInteger tag = row;
    UITableViewCell *returnCell;
    if (section == 0) {
        if (row == 0) {
            if (self.activeIndexPath.row == 0 || !self.listName) {
                // return editable list name header
                EditListNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editListNameCell" forIndexPath:indexPath];
                [self setupTextField:cell.listNameTextField name:self.listName defaultName:@"Enter list name" tag:0];
                returnCell = cell;
            }
            else {
                // return the list name header cell
                returnCell = [tableView dequeueReusableCellWithIdentifier:@"listTitleCell" forIndexPath:indexPath];
                returnCell.textLabel.text = self.listName;
            }
        }
        else {
            NSUInteger itemIndex = row - 1;
            if (self.activeIndexPath.row == row) {
                // return editable list item cell
                EditListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editListItemCell" forIndexPath:indexPath];
                [self setupTextField:cell.itemTextField name:self.taskList[itemIndex] defaultName:@"Enter item description" tag:tag];
                returnCell = cell;
            }
            else {
                // return the list item cell
                returnCell = [tableView dequeueReusableCellWithIdentifier:@"listItemCell"  forIndexPath:indexPath];
                returnCell.textLabel.text = self.taskList[itemIndex];
            }
        }
    }
    else {
        // return the cell for the add-list-item button
        returnCell = [tableView dequeueReusableCellWithIdentifier:@"addNewItemButtonCell"  forIndexPath:indexPath];
    }
    return returnCell;
}

- (void)setupTextField:(UITextField *)field name:(NSString *)name defaultName:(NSString *)defaultName tag:(NSInteger)tag
{
    field.tag = tag;
    field.delegate = self;
    if (!name)   {
        field.text = name;
        field.clearsOnBeginEditing = NO;
    }
    else {
        field.text = defaultName;
        field.clearsOnBeginEditing = YES;
    }
    field.selected = YES;
    field.backgroundColor = self.textFieldBackgroundColor;
    field.clearButtonMode = UITextFieldViewModeAlways;
    [field becomeFirstResponder];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (IBAction)listNameFieldChanged:(UITextField *)sender {
}

- (IBAction)listNameFieldEditDidEnd:(UITextField *)sender {
}

- (IBAction)itemNameFieldChanged:(UITextField *)sender {
}

- (IBAction)itemNameFieldEditDidEnd:(UITextField *)sender {
}

- (IBAction)addNewItemButtonPress:(UIButton *)sender {
}
@end
