//
//  CreateNewListVC.h
//  FireMan1
//
//  Created by Gerald Boyd on 10/10/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DepartmentTaskList;
@class FireDepartment;

@interface CreateNewListVC : UITableViewController <UITextFieldDelegate>
@property (nonatomic, weak) FireDepartment *fireDepartment;
@property (nonatomic, strong) DepartmentTaskList *currentList;
@property (nonatomic, readonly) NSMutableArray *updatedTaskList;
@property (nonatomic, readonly) NSString *updatedListName;
@end
