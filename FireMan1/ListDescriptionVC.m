//
//  ListDescriptionVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 5/28/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "ListDescriptionVC.h"
#import "FireDepartment.h"
#import "Incident.h"
#import "FireManViewController.h"
#import "SortedTaskList.h"
#import "ListItem.h"

@interface ListDescriptionVC ()
@property (weak, nonatomic) IBOutlet UITextField *listDescriptionTextField;

@end

@implementation ListDescriptionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _listDescriptionTextField.delegate = self;
    _listDescriptionTextField.accessibilityIdentifier = @"listDescriptionTextField";

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.popoverVwController.isPopoverVisible) {
            [self.popoverVwController dismissPopoverAnimated:YES];
        }
    });
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self performSegueWithIdentifier:@"unwindFromListDescription" sender:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textEntered:(UITextField *)sender
{
    SortedTaskList *sortedTaskList = self.callingViewController.cachedTaskLists[(NSUInteger)self.indexPath.section];
    // NSLog (@"Text entered: %@", sender.text);
    if (self.indexPath.row == 0) {
        // update name of list
        List *managedList = sortedTaskList.managedListObject;
        managedList.listName = sender.text;
        sortedTaskList.name = sender.text;
    }
    else {
        // update list item description
        NSUInteger itemIndex = (NSUInteger) self.indexPath.row - 1;
        ListItem *listItem = sortedTaskList.list[itemIndex];
        listItem.name = sender.text;
    }
    // update cell
    self.currentCell.textLabel.text = sender.text;

    // remove popover
    [self.popoverVwController dismissPopoverAnimated:YES];
    self.popoverVwController = nil;
    self.callingViewController.cachedTaskLists = nil;
    [self.tableView reloadData];
    self.callingViewController.isSegueActive = NO;
}


@end
