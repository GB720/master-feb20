//
//  IcSelectionViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 8/7/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IcSelectionViewController.h"
#import "Incident.h"
#import "Incident+Create.h"
#import "FireUnit+Identification.h"
#import "ListItem+Create.h"
// #import "FireUnit.h"
#import "StagingGroup+Create.h"
#import "EditDepartmentVC.h"
#import "UIColor+EmerResStrat.h"
#import "NSAttributedString+ersCategory.h"

static NSUInteger MAX_UNITS_IN_LIST = 12;

@interface IcSelectionViewController ()

@property(nonatomic, strong) NSArray *fireUnits;
@property(nonatomic) BOOL unwindSegueNeeded;
@end

@implementation IcSelectionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _fireUnits = self.incident.sortedFireUnits;
    NSInteger len = _fireUnits.count * 44;
    if (_fireUnits.count > MAX_UNITS_IN_LIST) {
        len = MAX_UNITS_IN_LIST * 44;
    }
    CGSize size = CGSizeMake(300, len);
    self.preferredContentSize = size;
    self.unwindSegueNeeded = YES;
    self.tableView.accessibilityIdentifier = @"icSelectionTableView";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    if (self.unwindSegueNeeded) {
        [self performSegueWithIdentifier:@"commanderSelectionBackUnwind" sender:self];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    self.unwindSegueNeeded = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.fireUnits.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"icSelectionCell";
    UIColor *textColor = [UIColor ersBlackColor];
    UIColor *whiteText = [UIColor whiteColor];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure the cell...
    NSUInteger itemIndex = (NSUInteger) indexPath.item;
    FireUnit *fireUnit = self.fireUnits[itemIndex];
    cell.textLabel.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = fireUnit.prefixedName;

    // set background color based on current activity and assignment
    NSString *assignmentDetails = @"No Group";

    // assigned to a group?
    if (fireUnit.assignedTo) {
        cell.textLabel.backgroundColor = [UIColor ersLightGoldColor];
        assignmentDetails = [NSString stringWithFormat:@"%@",fireUnit.assignedTo.name];
    }
    else if (fireUnit.stagingGroup) {
        // in staging group?
        if (fireUnit.activeAge < fireUnit.stagingGroup.timeRequired.integerValue) {
            cell.textLabel.backgroundColor = fireUnit.stagingColor;
            textColor = whiteText;
        }
        assignmentDetails = [NSString stringWithFormat:@"%@",fireUnit.stagingGroup.name];
    }

    // incident commander?
    if (fireUnit.coordinatorFor) {
        if ([fireUnit.coordinatorFor.name isEqualToString:FD_GROUP_TERMINATED]) {
            assignmentDetails = [NSString stringWithFormat:@"%@",
                                 assignmentDetails];
        }
        else {
            cell.textLabel.backgroundColor = [UIColor ersRedColor];
            textColor = whiteText;
            assignmentDetails = [NSString stringWithFormat:@"%@, Coordinator For: %@",
                                                       assignmentDetails, fireUnit.coordinatorFor.name];
        }
    }

    cell.textLabel.textColor = textColor;
    // cell.detailTextLabel.text = assignmentDetails;
    cell.detailTextLabel.attributedText = [NSAttributedString attributedStringFrom:assignmentDetails
                                                                   foregroundColor:[UIColor ersBlackColor]
                                                                   backgroundColor:[UIColor clearColor]
                                                                              size:14.0];
    return cell;

}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger index = (NSUInteger) indexPath.row;
    self.selectedFireUnit = self.fireUnits[index];
    [self performSegueWithIdentifier:@"commanderSelectionBackUnwind" sender:self];
}



@end
