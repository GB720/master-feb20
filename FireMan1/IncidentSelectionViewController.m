//
//  IncidentSelectionViewController.m
//  FireMan1
//
//  Created by Gerald Boyd on 7/17/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "IncidentSelectionViewController.h"
// #import "Incident+Create.h"
#import "UserIdentificationViewController.h"
#import "IncidentSelectionCell.h"
#import "IncidentDetailViewController.h"
#import "IncidentLog+Log.h"
#import "EditDepartmentVC.h"
#import "UIColor+EmerResStrat.h"
#import "CurrentUser.h"
#import "FireDepartment+Create.h"


@class DepartmentMasterVC, Incident;

static NSInteger MAX_ITEMS_IN_LIST = 12;
static NSInteger MIN_LEN = 400;
static NSInteger ROW_SIZE = 44;
static NSInteger MIN_WIDTH = 400;

@interface IncidentSelectionViewController ()

@property (nonatomic, strong) NSArray *incidents;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *createIncidentButtonOutlet;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editDepartmentButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButtonOutlet;
@property (strong, nonatomic) NSString *createIncidentButtonTitle;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButtonOutlet;
@property (strong, nonatomic) NSString *cancelTitle;
@property (strong, nonatomic) NSString *editDepartmentTitle;
@property (strong, nonatomic) NSString *editButtonTitle;
@property (nonatomic) BOOL isFirstTime;
@property (strong, nonatomic) CurrentUser *currentUser;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *userIdButtonOutlet;
@end

@implementation IncidentSelectionViewController
static NSString *const detailSegue = @"incidentDetailSegue";
static NSString *const DELETE_TITLE = @"Delete";

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentUser = [CurrentUser sharedSingletonObject];
    self.isFirstTime = YES;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.accessibilityIdentifier = @"incidentTable";
    self.createIncidentButtonOutlet.tintColor = [UIColor ersBlueColor];
    self.cancelButtonOutlet.tintColor = [UIColor ersBlueColor];
    self.editDepartmentButton.tintColor = [UIColor ersBlueColor];
    self.editButtonOutlet.tintColor = [UIColor ersBlueColor];
    self.createIncidentButtonTitle = nil;
    self.cancelTitle = nil;
    self.editDepartmentTitle = nil;
    [self showButtons:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
    [nc addObserver:self selector:@selector(needsUpdateViewAfterICloud:) name:@"iCloudUpdatedDepartment" object:nil];
    self.incidents = self.fireDepartment.sortedIncidents;
    NSInteger len = 0;
    if (self.incidents && self.incidents.count > 0) {
        len = (self.incidents.count + 1) * 44;
        if (self.incidents.count > MAX_ITEMS_IN_LIST) {
            len = MAX_ITEMS_IN_LIST * ROW_SIZE;
        }
    }
    if (len < MIN_LEN) {
        len = MIN_LEN;
    }
    CGSize size = CGSizeMake(MIN_WIDTH, len);
    self.preferredContentSize = size;
    [self showButtons:YES];
}

- (NSArray *)incidents
{
    if (!_incidents) {
        _incidents = self.fireDepartment.sortedIncidents;
    }
    return _incidents;
}
- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_incidents = nil;
        [self performSegueWithIdentifier:@"unwindFromIncidentSelection" sender:self];
    });
}

- (void)needsUpdateViewAfterICloud: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_incidents = nil;
        // if fire department updated
        NSSet *insertsAndUpdates = [self.fireDepartment cloudUpdates:notification];

        if ([self.fireDepartment isFireDepartmentUpdate:insertsAndUpdates]) {
            [self performSegueWithIdentifier:@"unwindFromIncidentSelection" sender:self];
        }
    });
}


- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // todo Issue 7 if device is has sharing option show all incidents
    // if device does not has sharing option show incidents belonging to device as selectable
    //        and other incidents as requiring sharing option?
    
    // Return the number of rows in the section.
    NSInteger numberOfIncidents = self.incidents.count;
    return numberOfIncidents;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IncidentSelectionViewCell";
    IncidentSelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSInteger index = indexPath.item;

    // Configure the cell...
    // NSString *prepend = @"Show log";
    NSString *prepend = @"";
    Incident *incident = self.incidents[(NSUInteger) index];
    if (incident.stopTime) {
        prepend = @"Closed: ";
    }

    cell.textLabel.text = [NSString stringWithFormat:@"%@%@",prepend, incident.name];
    cell.textLabel.tag = index;
    NSString *dateString = [NSDateFormatter localizedStringFromDate:incident.startTime dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
    cell.detailTextLabel.text = dateString;
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    cell.tag = index;

    return cell;
}

#pragma mark - segue notifications

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    BOOL canPerform = YES;
    
    if (action == @selector(unwindToMain:)) {
        canPerform = NO;
    }
    else if (action == @selector(unwindUserIdentification:)) {
        canPerform = NO;
    }
    else if (action == @selector(unwindMaster:)) {
        canPerform = YES;
    }

    return canPerform;
}

- (IBAction)unwindToMain:(UIStoryboardSegue *)unwindSegue
{
    // dummy for comparison
}
- (IBAction)unwindUserIdentification:(UIStoryboardSegue *)unwindSegue
{
    // dummy for comparison
}

- (IBAction)unwindMaster:(UIStoryboardSegue *)unwindSegue
{
    // take no action
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    BOOL shouldPerform = YES;
    if ([identifier isEqualToString:@"cancelFromIncidentSelection"]) {
        if (!self.selectedIncident) {
            [self alertMsg:@"An incident must be selected or started" alertTitle:@"Alert"];
            shouldPerform = YES;
        }
    }
    return shouldPerform;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"incidentDetailSegue"]) {
        NSInteger index = [self findTag:sender];
        if (index >= 0) {
            UINavigationController *nav = segue.destinationViewController;
            IncidentDetailViewController *detailViewController = (IncidentDetailViewController *) nav.topViewController;
            self.selectedIncident = self.incidents[(NSUInteger) index];
            detailViewController.name = self.selectedIncident.name;
            NSString *dateString = [NSDateFormatter localizedStringFromDate:self.selectedIncident.startTime
                                                                  dateStyle:NSDateFormatterMediumStyle
                                                                  timeStyle:NSDateFormatterMediumStyle];
            detailViewController.startDate = dateString;
            detailViewController.note = self.selectedIncident.note;
        }
    }
    else if ([segue.identifier isEqualToString:@"changeUserIdSegue"]) {
        UserIdentificationViewController *vc = segue.destinationViewController;
        User *user = self.currentUser.userEntity;
        vc.fireDepartment = user.inDepartment;
    }
    else if ([segue.identifier isEqualToString:@"editDepartmentSegue"]) {
        EditDepartmentVC *editDepartmentVC = segue.destinationViewController;
        editDepartmentVC.fireDepartment = self.fireDepartment;
    }
}

#pragma mark - segue handling


- (IBAction)unwindToIncidentSelection:(UIStoryboardSegue *)unwindSegue
{
    if (self.fireDepartment.managedObjectContext) {
        if ([unwindSegue.identifier isEqualToString:@"unwindFromIncidentEditDone"]) {
            IncidentDetailViewController *fromVC = unwindSegue.sourceViewController;
            if (fromVC.nameChanged) {
                
                // log the change
                NSString *logMsg = [NSString stringWithFormat:@"Incident name changed from: \"%@\"", self.selectedIncident.name];
                [self.selectedIncident.log action:logMsg];
                NSString *logMsg2 = [NSString stringWithFormat:@"Incident name changed to: \"%@\"", fromVC.name];
                [self.selectedIncident.log action:logMsg2];
                
                // change the name
                self.selectedIncident.name = fromVC.name;
                [self.tableView reloadData];
            }
            if (fromVC.notesChanged) {
                
                // log the note change and update the note
                self.selectedIncident.note = fromVC.note;
                NSString *logMsg = [NSString stringWithFormat:@"Incident note changed: \"%@\"", fromVC.note];
                [self.selectedIncident.log action:logMsg];
            }
            else if ([unwindSegue.identifier isEqualToString:@"unwindFromIncidentEditCancel"]) {
                // ignore name and note changes..
            }
        }
    }
}

#pragma table view handling

- (NSInteger)findTag:(id)sender
{
    if ([sender isKindOfClass:[UIView class]]) {
        UIView *view = (UIView *) sender;
        while (view && (![view isKindOfClass:[UITableViewCell class]])) {
                view = view.superview;
        }
        if (view) {
            return view.tag;
        }
    }
    return -1;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger index = (NSUInteger) indexPath.row;
    self.selectedIncident = self.incidents[index];

    // perform segue
    [self performSegueWithIdentifier:detailSegue sender:self];
}


// Support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL canDelete = false;
    Incident *incident = self.incidents[(NSUInteger) indexPath.row];
    canDelete = [self.delegate canDelete:incident];
    if (canDelete)
        canDelete = (incident.stopTime != nil);
    return canDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Incident *incident = self.incidents[(NSUInteger) indexPath.row];
        if ([self.selectedIncident.name isEqualToString:incident.name]) {
            self.selectedIncident = nil;  // deleting the currect selected incident
        }
        if (self.delegate) {
            [self.delegate incidentDeleted:incident];
        }
        // delete the incident
        incident.forDepartment = nil;
        NSManagedObjectContext *moc = incident.managedObjectContext;
        [moc deleteObject:incident];
        self.incidents = self.fireDepartment.sortedIncidents;   // get sorted incidents from department
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView setEditing:NO animated:YES];   // go out of delete mode after one delete
        self.editButtonTitle = DELETE_TITLE;
        [self showButtons:YES];
        if (!self.selectedIncident) {
            [self performSegueWithIdentifier:@"unwindFromIncidentSelection" sender:self];
        }
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isEditing) {
        // ignore
        [self alertMsg:@"Only closed, unselected incidents may be deleted." alertTitle:@"Warning"];
    }
    else {
        NSUInteger index = (NSUInteger) indexPath.item;
        self.isLogOnly = NO;
        Incident *incident = self.incidents[index];
        self.selectedIncident = incident;
        self.isNewIncident = NO;
        [self performSegueWithIdentifier:@"unwindFromIncidentSelection" sender:self];
        if (incident.stopTime) {
            [self alertMsg:@"Selected incident is currently closed." alertTitle:@"Warning"];
        }
    }
}

- (NSString *)nameForIncidentId:(NSInteger)id
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    NSString *incidentName = [NSString stringWithFormat:@"%ld-%@", (long)id, dateString];
    NSLog(@"Created incident name %@",incidentName);
    return incidentName;
}

- (IBAction)newIncidentButton:(UIBarButtonItem *)sender
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if (!self.tableView.isEditing) {
        // fetch desired incident or set up a new incident
        NSInteger id = self.fireDepartment.incidents.count + 1;
        NSString *incidentName = [self nameForIncidentId:id];
        Incident *newIncident = [Incident createWithName:incidentName
                                             withContext:self.fireDepartment.managedObjectContext];
        self.selectedIncident = newIncident;
        newIncident.forDepartment = self.fireDepartment;
        newIncident.id = @(id);
        self.isNewIncident = YES;
        [self performSegueWithIdentifier:@"unwindFromIncidentSelection" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - button handling

- (IBAction)editButton:(UIBarButtonItem *)sender
{

    NSString *title = DELETE_TITLE;
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
    if (self.tableView.isEditing) {
        sender.title = [NSString stringWithFormat:@"Cancel %@s", title];
        [self showButtons:NO];
        if (self.isFirstTime) {
            [self alertMsg:@"Only closed, unselected incidents can deleted." alertTitle:@"Note"];
            self.isFirstTime = NO;
        }
    }
    else {
        sender.title = title;
        self.editButtonTitle = title;
        [self showButtons:YES];
    }
    [self.tableView reloadData];
}

- (void) showButtons:(BOOL) doShow
{
    BOOL isEnabled = doShow;
    BOOL isHidden = !doShow;
    BOOL isAdmin =[self.currentUser.userEntity hasPermission:FDAdministrator];
    self.createIncidentButtonOutlet.enabled = NO;
    self.cancelButtonOutlet.enabled = NO;
    self.editDepartmentButton.enabled = NO;
    self.editButtonOutlet.enabled = NO;
    
    if (self.currentUser.isSingleRoleMode ) {
        self.userIdButtonOutlet.enabled = NO;
        self.userIdButtonOutlet.title = @".";
    }
    else {
        self.userIdButtonOutlet.enabled = YES;
    }

    if (isHidden) {
        self.createIncidentButtonTitle = self.createIncidentButtonOutlet.title;
        self.cancelTitle = self.cancelButtonOutlet.title;
        self.editDepartmentTitle = self.editDepartmentButton.title;
        self.editButtonTitle = self.editButtonOutlet.title;
        self.createIncidentButtonOutlet.title = @".";
        self.cancelButtonOutlet.title = @".";
        self.editDepartmentButton.title = @".";
        self.editButtonOutlet.title = @".";
        if (isAdmin) {
            self.editButtonOutlet.title = self.editButtonTitle;
            self.editButtonOutlet.enabled = YES;
        }
    }
    else if (self.createIncidentButtonTitle) {
        // determine buttons to show based on user permission
        if ([self.currentUser.userEntity hasPermission:FDIncidentManager]) {
            self.createIncidentButtonOutlet.title = self.createIncidentButtonTitle;
            self.createIncidentButtonOutlet.enabled = isEnabled;
        }
        if (isAdmin) {
            self.editDepartmentButton.title = self.editDepartmentTitle;
            self.editDepartmentButton.enabled = isEnabled;
            self.editButtonOutlet.title = self.editButtonTitle;
            self.editButtonOutlet.enabled = isEnabled;
        }
        self.cancelButtonOutlet.title = self.cancelTitle;
    }
    self.cancelButtonOutlet.enabled = isEnabled;
}


- (IBAction)cancelButton:(UIBarButtonItem *)sender
{
    [self.tableView setEditing:NO animated:YES];
    [self performSegueWithIdentifier:@"cancelFromIncidentSelection" sender:self];
}

#pragma mark - alert handling

- (void) alertMsg:(NSString *) msg alertTitle:(NSString *)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController* __weak weakAlert = alert;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [weakAlert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
