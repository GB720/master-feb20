//
// Created by Gerald Boyd on 9/17/13.
// Copyright (c) 2013 Gerald Boyd. All rights reserved.
//


#import <Foundation/Foundation.h>

@class StagingGroup;
@class ListItem;


@interface FireUnitCellStatus : NSObject {}
@property (strong, nonatomic) UIColor *statusColor;
@property (strong, nonatomic) NSString *unitName;
@property(nonatomic, strong) ListItem *assignedGroup;

- (FireUnitCellStatus *)initWith: (UIColor *)statusColor
                         forUnit:(NSString *)prefixedName
                    assignedGroup:(ListItem *)assignedTo;

@end