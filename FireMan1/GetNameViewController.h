//
//  GetNameViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/8/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GroupListViewController;
@class Incident;

@interface GetNameViewController : UIViewController <UITextFieldDelegate>

- (IBAction)valueChanged:(UITextField *)sender;
@property (nonatomic, strong) NSString *groupName;
@property (nonatomic, weak) Incident *incident;
@end
