//
// Created by Gerald Boyd on 2/12/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EditorView <NSObject>
- (void) loadNewView;
- (void) setViewIndex:(NSInteger) index;
- (void) setViewTitle:(NSString *) title;
- (void) setIsAutoSelect:(BOOL) isAutoSelect;
- (void) setViewItems:(NSArray *) items; // array of items that support FDedit protocol
- (void) closingView;
- (void) selectedItemDidUpdate;
- (void) showPreviousView;
@end