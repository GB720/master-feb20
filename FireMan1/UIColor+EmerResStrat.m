//
// Created by Gerald Boyd on 4/24/14.
// Copyright (c) 2014 Gerald Boyd. All rights reserved.
//

#import "UIColor+EmerResStrat.h"


CGFloat changeColorValue(CGFloat brightness, NSInteger change);

@implementation UIColor (EmerResStrat)
+ (UIColor *)ersRedColor
{
    return [UIColor colorWithRed:0.8667 green:0.1176 blue:0.1843 alpha:1.0];
}

+ (UIColor *)ersGoldColor
{
    return [UIColor colorWithRed:.9216 green:0.6902 blue:0.2078 alpha:1.0];
}

+ (UIColor *)ersGreenColor
{
    return [UIColor colorWithRed:0.1294 green:0.5215 blue:0.3490 alpha:1.0];
}

+ (UIColor *)ersBlueColor
{
    return [UIColor colorWithRed:0.0235 green:0.4 blue:0.7961 alpha:1.0];
}

+ (UIColor *)ersGrayColor
{
    return [UIColor colorWithRed:0.8157 green:0.7765 blue:0.6941 alpha:1.0];
}

+ (UIColor *)ersGreyColor
{
    return [UIColor ersGrayColor];
}


+ (UIColor *)ersBlackColor
{
    return [UIColor colorWithRed:0.0980 green:0.1569 blue:0.1373 alpha:1.0];
}

+ (UIColor *)ersLightGoldColor
{
    return [self getColor:[UIColor ersGoldColor] saturationChange:-3 brightnessChange:+9];
}

+ (UIColor *) ersLightGreenColor
{
    return [self getColor:[UIColor ersGreenColor] saturationChange:-9 brightnessChange:+8];
}

+ (UIColor *)getColor:(UIColor *)baseColor saturationChange:(NSInteger)saturationChange brightnessChange:(NSInteger)brightnessChange
{
    UIColor *returnColor = baseColor;
    CGFloat colorHue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;
    BOOL ok = [baseColor getHue:&colorHue saturation:&saturation brightness:&brightness alpha:&alpha];
    if (ok) {
        if (brightnessChange) {
            brightness = changeColorValue (brightness, brightnessChange);
        }
        if (saturationChange) {
            saturation = changeColorValue (saturation, saturationChange);
        }
        returnColor = [UIColor colorWithHue:colorHue saturation:saturation brightness:brightness alpha:alpha];
    }
    return returnColor;
}

+ (UIColor *)ersCyanColor
{
    return [UIColor ersBlueColor];
}

+ (UIColor *)ersPinkColor
{
    return [self getColor:[UIColor ersRedColor] saturationChange:-5 brightnessChange:+6];
}

+ (UIColor *)ersTextFieldBackgroundColor
{
    return [UIColor ersGoldColor];
}

+ (UIColor *) ersListHeaderColor
{
    return [UIColor ersLightGreenColor];
}

+ (UIColor *) ersActiveButtonColor
{
    return [UIColor ersGreenColor];
}

+ (UIColor *) ersViewBackgroundColor
{
    return [UIColor ersGreyColor];
}

+ (UIColor *) ersToolbarBackgroundColor
{
    return [self getColor:[self ersGreyColor] saturationChange:0 brightnessChange:+5];
}

@end

// Change the specified color value by change * tenths of the remaining value
//
// the absolute value of "change" must be between 0 and 10
//
// A negative change value means to reduce the color value by tenths
//
// A positive change value means to increase the color value by tenths of the
// remaining portion between the color value and 1.0
CGFloat changeColorValue(CGFloat colorValue, NSInteger change)
{
    if (change != 0) {
        if (colorValue < 1.0 && colorValue > 0.0) {
            if (change > 0 && change <= 10) {
                colorValue = colorValue + (((1.0 - colorValue)/10) * change);
            }
            else if (change < 0 && change >= -10) {
                colorValue = colorValue - ((colorValue / 10) * -change);
            }
        }
    }
    return colorValue;
}