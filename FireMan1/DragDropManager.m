//
//  Created by jve on 4/1/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "DragDropManager.h"
#import "DragContext.h"

@interface DragDropManager ()
@property (nonatomic, strong) NSMutableArray *dragSubjects;
@property (nonatomic, strong) NSMutableArray *dragAreas;
@property (nonatomic, strong) NSMutableDictionary *dropTypes;
@property (nonatomic) BOOL longPress;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressRecognizer;

@property(nonatomic, strong) UIView *draggingInView;

- (void) longPress: (UILongPressGestureRecognizer *) sender;
@end

@implementation DragDropManager {
    
    NSMutableArray *_dragSubjects;
    NSMutableDictionary *_dropTypesAllowed;
    DragContext *_dragContext;
}

@synthesize dragContext = _dragContext;

// Designated Initializer
- (id)init
{
    self = [super init];
    if (self) {
        [self reset];
    }
    return self;
}

- (void) reset
{
    if (_dragAreas) {
        [_dragAreas removeAllObjects];
    }
    if (_dragSubjects) {
        [_dragSubjects removeAllObjects];
    }
    if (_dropTypesAllowed) {
        [_dropTypesAllowed removeAllObjects];
    }
    _dragAreas = [[NSMutableArray alloc]init];
    _dragSubjects = [[NSMutableArray alloc]init];
    _dropTypesAllowed = [[NSMutableDictionary alloc] init];
    _dragContext = nil;
    _longPress = NO;
}

- (BOOL) isActive
{
    return self.dragContext != nil;
}

- (void) removeDragAreaWithId: (NSString *) taskId
{
    NSArray *currentDragAreas = [self.dragAreas copy];
    for (id <DragDropAcceptor> area in currentDragAreas) {
        if ([area.taskId isEqualToString:taskId]) {
            [self.dragAreas removeObject:area];
        }
    }
}

// Add a view that will accept drag and drop operations
- (void) addDragArea: (id <DragDropAcceptor>) dragArea
{
    // remove any duplicate area with same id as new area
    [self removeDragAreaWithId:dragArea.taskId];

    // add new drag area
    [self.dragAreas addObject:dragArea];
    [dragArea setDragDropManager:self];
}

- (void) addDragType:(NSString *)dragType forView:(id<DragDropAcceptor>)dragArea
{
    NSString *viewId = dragArea.taskId;
    NSMutableArray *allowedTypes = (NSMutableArray *)[self.dropTypes valueForKey:viewId];
    if (!allowedTypes){
        allowedTypes = [[NSMutableArray alloc] init];
        [self.dropTypes setValue:allowedTypes forKey:viewId];
    }
    [allowedTypes addObject:dragType];
}

- (NSArray *) dropAreas
{
    return [self.dragAreas copy];
}

//
// handle dragging of a view (if dragContext is active)
- (void)dragObjectAccordingToGesture:(UIGestureRecognizer *)recognizer {
    if (self.dragContext) {
        CGPoint pointOnView = [recognizer locationInView:recognizer.view];
        self.dragContext.viewCopyToDrag.center = pointOnView;
    }
}

// add an image to the specified area
- (void) addImage: (UIView *) viewBeingDragged toArea:dropArea atPoint: (CGPoint) pointInDropView
{
    [dropArea addSubview:viewBeingDragged];
    //change origin to match offset on new super view
    viewBeingDragged.frame = CGRectMake(pointInDropView.x - (viewBeingDragged.frame.size.width / 2), pointInDropView.y - (viewBeingDragged.frame.size.height / 2), viewBeingDragged.frame.size.width, viewBeingDragged.frame.size.height);
}

-(void) removeFromView: (UIView *) viewBeingDragged
{
    [viewBeingDragged removeFromSuperview];
}

- (BOOL) isValidDropItem: (id <DraggableUIView>) dragSubject
                 forView: (id <DragDropAcceptor>) dragArea
{
    // not currently used
    return YES;
}

- (void) addImage:(UIView *) viewBeingDragged
          atPoint: (CGPoint) point
{
    // not currently used
}

//
// setup dragging gesture recognizer
- (void) setGestureRecognizersForView: (UIView *)view
{
    self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
    self.longPressRecognizer.minimumPressDuration = 0.2;
  
    [view addGestureRecognizer:self.longPressRecognizer];
    self.draggingInView = view;
}

//
// Turn on or off the gesture recognizers for the view
- (void) gestureRecognizersForView:(UIView *)view enabled:(BOOL) enable
{
    NSArray *gestureRecognizers = view.gestureRecognizers;
    for (UIGestureRecognizer *gestureRecognizer in gestureRecognizers) {
        gestureRecognizer.enabled = enable;
    }
}

//
// dragging gesture detected
- (void)dragging:(id)sender {
    assert([sender isKindOfClass:[UILongPressGestureRecognizer class]]);
    if ([sender isKindOfClass:[UILongPressGestureRecognizer class]] ) {
        [self longPress:(UILongPressGestureRecognizer *)sender];
    } 
}

//
// process longPress gesture and drag
- (void) longPress: (UILongPressGestureRecognizer *) recognizer
{
    switch (recognizer.state) {
        // long press recognized
        case UIGestureRecognizerStateBegan: {
            // NSLog(@"long press detected");
            self.longPress = YES;

            // possible drag -- check for position on one of the draggable view items
            for (UIView *dragArea in _dragAreas) {
                CGPoint pointInDragArea = [recognizer locationInView:dragArea];
                BOOL pointInsideDragArea = [dragArea pointInside:pointInDragArea withEvent:nil];

                // if on draggable item -- start drag operation
                if (pointInsideDragArea) {
                    id <DragDropAcceptor> sourceView = (id <DragDropAcceptor>) dragArea;
                    UIView *dragSubject = [sourceView viewAt:pointInDragArea];
                    if (![dragSubject conformsToProtocol:@protocol(DraggableUIView)])
                        dragSubject = nil;   // is not draggable
                    if (dragSubject) {
                        // NSLog(@"started dragging an object");

                        self.dragContext = [[DragContext alloc] initWithDraggedView:dragSubject];
                        CGPoint pointInSourceView = self.dragContext.originalPointInSuperView;

                        // dd [dragSubject removeFromSuperview];
                        self.dragContext.movingInView = recognizer.view;
                        [recognizer.view addSubview:self.dragContext.viewCopyToDrag];
                        [self dragObjectAccordingToGesture:recognizer];

                        //
                        // [sourceView startingDrag:self atPoint:pointInSourceView];
                        [sourceView.dragDelegate copyItem:dragSubject atPoint:pointInSourceView fromView:sourceView dragContext:self.dragContext];

                        break;
                    }

                } else {
                    // NSLog(@"started drag outside drag subjects");
                }
            }
            break;
        }
            // long press possible
        case UIGestureRecognizerStatePossible: {
            // do nothing
        }
        case UIGestureRecognizerStateFailed: {
            // NSLog(@"drag recognizer state failed - snapping back to last known location");
            [self.dragContext snapToOriginalPosition];
            // clear drag and drop context
            self.dragContext = nil;
        }
        case UIGestureRecognizerStateCancelled: {
            // NSLog(@"drag cancelled - snapping back to last known location");
            [self.dragContext snapToOriginalPosition];
            id <DragDropAcceptor> sourceView = (id <DragDropAcceptor>)self.dragContext.originalView;

            [sourceView dragFromEnded:nil];

            // clear drag and drop context
            self.dragContext = nil;
        }
        case UIGestureRecognizerStateChanged: {
            // handle dragging of a view
            [self dragObjectAccordingToGesture:recognizer];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            // if drag and drop in progress
            if (self.dragContext) {
                UIView *viewBeingDragged = self.dragContext.draggedView;
                // NSLog(@"ended drag event");
                // CGPoint centerOfDraggedView = viewBeingDragged.center;
                BOOL droppedInAllowedArea = NO;
                id <DragDropAcceptor> sourceView = (id <DragDropAcceptor>)self.dragContext.originalView;

                // find dropArea
                for (UIView *dropArea in self.dropAreas) {
                    CGPoint pointInDropView = [recognizer locationInView:dropArea];
                    //  NSLog(@"tag %li pointInDropView %@ center of dragged view %@", (long)dropArea.tag, NSStringFromCGPoint(pointInDropView), NSStringFromCGPoint(centerOfDraggedView));

                    // if area found
                    if ([dropArea pointInside:pointInDropView withEvent:nil]) {
                        id <DragDropAcceptor> targetView = (id <DragDropAcceptor>) dropArea;

                        // check that target view will accept
                        if ([targetView isDropTargetFor:viewBeingDragged
                                                atPoint:pointInDropView]){
                            droppedInAllowedArea = YES;
                            // NSLog(@"dropped subject %@ on to view tag %li", NSStringFromCGRect(viewBeingDragged.frame), (long)dropArea.tag);
                            if ([targetView droppingItemOn:viewBeingDragged
                                                   atPoint:pointInDropView]) {
                                if (sourceView.taskId != targetView.taskId) {
                                    // finalize removal from source view
                                    [sourceView.dragDelegate deleteItem:viewBeingDragged
                                                                atPoint:self.dragContext.originalPointInSuperView
                                                               fromView:sourceView
                                                            dragContext:self.dragContext];
                                }

                                // drop in target view
                                [targetView.dragDelegate pasteItem:(id <DraggableUIView> )viewBeingDragged
                                                           atPoint:pointInDropView
                                                            toView:targetView
                                                       dragContext:self.dragContext];

                            }
                        }
                        // finish the drag and drop operation
                        if (droppedInAllowedArea) {
                            [targetView dragToEnded];
                            break;
                        }
                    }
                }

                // if not valid drop location - return to original location
                if (!droppedInAllowedArea) {
                    // NSLog(@"release draggable object outside target views - snapping back to last known location");
                    [self.dragContext snapToOriginalPosition];
                }
                else {
                    [self.dragContext removeDragImage];
                }
                // notify source view to redraw
                [sourceView dragFromEnded:viewBeingDragged];
                // NSLog(@"sourceView dragFromEnded:");


                // clear drag and drop context
                self.dragContext = nil;
            } else {
                //NSLog(@"Nothing was being dragged");
            }
            self.longPress = NO;
            break;
        }
    }
}

@end