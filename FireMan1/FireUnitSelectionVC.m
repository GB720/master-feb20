
//
//  FireUnitSelectionVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/4/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import "FireUnitSelectionVC.h"
#import "Incident.h"
#import "FireDepartment.h"
#import "DepartmentResources+Create.h"
#import "FireUnitSelectionViewCell.h"
#import "UnitSelectionCollectionView.h"
#import "UnitSelectionHeader.h"
#import "FireUnit+Identification.h"
#import "NeighborVC.h"
#import "FireDepartment+Create.h"
#import "AddFireUnitVC.h"
#import "FireUnit+Create.h"
#import "StagingHeaderView.h"
#import "UIColor+EmerResStrat.h"

@interface FireUnitSelectionVC ()

- (IBAction)cancelButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)addUnitButtonPress:(UIButton *)sender;
- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *departments;
@property (strong, nonatomic) NSMutableDictionary *departmentUnits;
@property (strong, nonatomic) NSMutableDictionary *headerViews;
@property(nonatomic, strong) NSMutableArray *currentlySelectedUnits;
@property(nonatomic, strong) UIColor *backgroundColorSelected;
@property(nonatomic, strong) UIColor *backgroundColorForUnselected;
@end

@implementation FireUnitSelectionVC {

}

#pragma mark - view load

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) setCollectionView:(UICollectionView *)collectionView
{
    _collectionView = collectionView;

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionView.allowsMultipleSelection = YES;
    self.collectionView.accessibilityIdentifier = @"fireUnitSelectionCollection";

	// Do any additional setup after loading the view.
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    _currentlySelectedUnits = [NSMutableArray arrayWithCapacity:10];
    _backgroundColorSelected = [UIColor ersLightGoldColor];
    _backgroundColorForUnselected = [UIColor ersGreenColor];
    _headerViews = [NSMutableDictionary dictionaryWithCapacity:20];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(iCloudStoreWillChange:) name:@"iCloudStoreWillChange" object:nil];
    if (!self.incident || !self.incident.forDepartment) {
        // initiate unwind to main
        [self performSegueWithIdentifier:@"unwindFromDone" sender:self];
    }
    [self departmentsAndUnitsFor:self.incident.forDepartment ];
}

- (void)iCloudStoreWillChange: (NSNotification *) notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissMe];
    });
}

- (void)dismissMe
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}



- (NSArray *)selectedUnits
{
    return self.currentlySelectedUnits.copy;
}

- (void)departmentsAndUnitsFor:(FireDepartment *)myDepartment {
// need array of departments
    // first department is the one that the incident belongs to
    self.departments = @[myDepartment].mutableCopy;

    // get and sort neighbor departments
    NSSet *neighbors = myDepartment.neighborDepartments;
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(compare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortedNeighbors = [neighbors sortedArrayUsingDescriptors:sortDescriptors];
    [self.departments addObjectsFromArray:sortedNeighbors];

    // for each department
    // need list of units in department minus the units already assigned to the incident
    self.departmentUnits = [NSMutableDictionary dictionaryWithCapacity:10];
    for (FireDepartment *department in self.departments) {
        // create list of unassignedUnits in department that are not already in incident
        NSSet *unassignedUnits = [self findUnassignedUnits: department.resourceList.fireUnits];
        NSArray *sortedUnits = [self sortUnits:unassignedUnits];
        if (sortedUnits){
            (self.departmentUnits)[department.name] = sortedUnits;
        }
    }
}

- (NSArray *)sortUnits:(NSSet *)unsortedUnits
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(compare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    return [unsortedUnits sortedArrayUsingDescriptors:sortDescriptors];
}

- (NSSet *)findUnassignedUnits:(NSSet *)setOfAllUnits
{
    NSMutableSet *unassignedUnits = (NSMutableSet *) setOfAllUnits.mutableCopy;
    for (FireUnit *fireUnit in setOfAllUnits) {
        for (FireUnit *clonedUnit in fireUnit.clonedUnits) {
            if ([clonedUnit.forIncident.name isEqualToString:self.incident.name]) {
                // already assigned to the incident
                [unassignedUnits removeObject:fireUnit];
                break;
            }
        }
    }
    return unassignedUnits.copy;
}

#pragma mark - popover presentation controller delegate


#pragma mark - notifications

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - data structures

- (NSMutableArray *)departments
{
    if (!_departments) {
        [self departmentsAndUnitsFor:self.incident.forDepartment];
    }
    return _departments;
}

- (void) addFireUnit:(NSString *)fireUnitNames forSection:(NSInteger)section
{
    FireDepartment *currDepartment = self.departments[(NSUInteger)section];
    [currDepartment addFireUnit:fireUnitNames];
    self.departments = nil;
    [self.collectionView reloadData];
}

- (BOOL)isUnique:(NSString *)name inDepartment:(FireDepartment *)department
{
    BOOL isUniqueName = YES;
    for (FireUnit *fireUnit in department.resourceList.fireUnits) {
        if ([fireUnit.name isEqualToString:name]) {
            isUniqueName = NO;
            break;
        }
    }
    return isUniqueName;
}

#pragma mark - action notifications


- (IBAction)addUnitButtonPress:(UIButton *)sender
{
    self.selectedUnits = nil;
    [self.currentlySelectedUnits removeAllObjects];
    [self performSegueWithIdentifier:@"addNewUnitSegue" sender:sender];
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:@"unwindFromDone" sender:self];
    [self dismissMe];
}

- (IBAction)cancelButtonPressed:(UIBarButtonItem *)sender
{
    [self.currentlySelectedUnits removeAllObjects];
    [self performSegueWithIdentifier:@"unwindFromDone" sender:self];
    [self dismissMe];
}


#pragma mark - collection view data source
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.departments.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    FireDepartment *department = self.departments[(NSUInteger)section];
    NSArray *departmentUnits = (self.departmentUnits)[department.name];
    return departmentUnits.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.departmentUnits) {
        NSUInteger section = (NSUInteger )indexPath.section;
        NSUInteger item = (NSUInteger) indexPath.item;
        FireDepartment *fireDepartment = self.departments[section];
        NSArray *units = (self.departmentUnits)[fireDepartment.name];
        if (units) {
            FireUnitSelectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"fireUnitSelectionViewCell" forIndexPath:indexPath];
            cell.backgroundColor = self.backgroundColorForUnselected;
            cell.contentView.backgroundColor = self.backgroundColorForUnselected;
            FireUnit *fireUnit = units[item];
            cell.name.text = [fireUnit prefixedNameInIncident:self.incident];
            cell.name.textColor = [UIColor whiteColor];
            cell.name.backgroundColor = self.backgroundColorForUnselected;
            if ([self.currentlySelectedUnits containsObject:fireUnit]) {
                cell.backgroundColor = self.backgroundColorSelected;
                cell.contentView.backgroundColor = self.backgroundColorSelected;
                cell.name.textColor = [UIColor ersBlackColor];
                cell.name.backgroundColor = self.backgroundColorSelected;
            }
            return cell;
        }
    }

    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view = nil;

    if ([collectionView isKindOfClass:[UnitSelectionCollectionView class]]) {
        if (kind == UICollectionElementKindSectionHeader) {
            UnitSelectionHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"unitSelectionHeader" forIndexPath:indexPath];
            FireDepartment *department = self.departments[(NSUInteger)indexPath.section];
            NSString *headerText = [NSString stringWithFormat:@"%@ (%@)",department.name,department.neighborPrefix];
            headerView.title.text = headerText;
            headerView.addUnitButton.tag = indexPath.section;
            view = headerView;
            NSString *sectionId = [NSString stringWithFormat:@"%li",(long)indexPath.section];
            (self.headerViews)[sectionId] = headerView;
        }
    }
    return view;
}

#pragma mark - collection view delegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

//
// selection of an item
// change cell color and add to list of selected units
//
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FireUnit *unit= [self getUnit:indexPath];
    FireUnitSelectionViewCell *cell = (FireUnitSelectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];

    // Add the selected item into the array
    cell.backgroundColor = self.backgroundColorSelected;
    cell.name.backgroundColor = self.backgroundColorSelected;
    cell.name.textColor = [UIColor ersBlackColor];
    [self.currentlySelectedUnits addObject:unit];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FireUnit *unit= [self getUnit:indexPath];
    FireUnitSelectionViewCell *cell = (FireUnitSelectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    // Remove selected item from the array
    cell.backgroundColor = self.backgroundColorForUnselected;
    cell.name.backgroundColor = self.backgroundColorForUnselected;
    cell.name.textColor = [UIColor whiteColor];
    [self.currentlySelectedUnits removeObject:unit];
}

// get fire unit based on the indexPath in the collection
- (FireUnit *)getUnit:(NSIndexPath *)indexPath {
    FireDepartment *department = self.departments[(NSUInteger)indexPath.section];
    NSArray *units = (self.departmentUnits)[department.name];
    FireUnit *unit = units[(NSUInteger) indexPath.item];
    return unit;
}

#pragma mark - segues

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController sender:(id)sender
{
    if (action == @selector(unwindToMain:)) {
        return NO;
    }
    return YES;
 }

- (void) unwindToMain:(id)sender
{
  // dummy selector for comparison
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    // return !self.isAddNeighborActive;
    return YES;
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // NSLog(@"segue=%@",segue.identifier);
    if ([segue.identifier isEqualToString:@"newNeighborSegue"]) {
        NeighborVC *neighborVC = segue.destinationViewController;
        neighborVC.departments = self.departments.copy;
        // clear currently selected units
        [self.currentlySelectedUnits removeAllObjects];
    }
    if ([segue.identifier isEqualToString:@"addNewUnitSegue"]) {
        // determine header cell that is initiating the Segue
        UIButton *addUnitButton = sender;
        NSInteger section = addUnitButton.tag;
        AddFireUnitVC *destinationVC = segue.destinationViewController;
        destinationVC.section = section;
        NSString *sectionId = [NSString stringWithFormat:@"%li",(long)section];

        UnitSelectionHeader *headerView = (self.headerViews)[sectionId];
        destinationVC.anchor = headerView;

        // clear currently selected units
        [self.currentlySelectedUnits removeAllObjects];
    }
}

// unwind from add unit
- (IBAction)unwindFromAddUnit:(UIStoryboardSegue *)unwindSegue
{
    AddFireUnitVC *vc = unwindSegue.sourceViewController;
    [self addFireUnit:vc.unitName forSection:vc.section];
}


// unwind from add neighbor and add a new neighbor if one was defined
- (IBAction)unwindFromAddNeighbor:(UIStoryboardSegue *)unwindSegue
{
    NSManagedObjectContext *context = self.incident.managedObjectContext;
    if (context) {
        if ([unwindSegue.identifier isEqualToString:@"unwindFromAdd"]) {
            NeighborVC *neighborVC = unwindSegue.sourceViewController;
            NSString *newName = neighborVC.neighborName;
            if (newName && newName.length > 0) {
                if ([self isUniqueName:newName]) {
                    FireDepartment *newDepartment = [FireDepartment createWithName:neighborVC.neighborName withContext:context];
                    // create empty unit resource list
                    newDepartment.resourceList = [DepartmentResources withUniqueId:[NSString stringWithFormat:@"resourceList-%@", newDepartment.uniqueId] createInContext:context];
                    newDepartment.neighborPrefix = neighborVC.neighborPrefix;
                    NSInteger numNeighbors = self.incident.forDepartment.neighborDepartments.count + 1;
                    newDepartment.id = @(numNeighbors);
                    [self.incident.forDepartment addNeighborDepartmentsObject:newDepartment];
                    self.departments = nil;
                    [self.collectionView reloadData];
                }
            }
        }
        else if ([unwindSegue.identifier isEqualToString:@"unwindFromCancel"]){
        }
        else {
            // DO NOTHING
        }
    }
}

- (BOOL)isUniqueName:(NSString *)name
{
    BOOL isUnique = YES;
    if (self.departments) {
        for (FireDepartment *fireDepartment in self.departments) {
            if ([fireDepartment.name isEqualToString:name]) {
                isUnique = NO;
                break;
            }
        }
    }
    return isUnique;
}
@end
