//
//  NeighborVC.m
//  FireMan1
//
//  Created by Gerald Boyd on 6/10/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NeighborVC.h"
#import "FireDepartment.h"
#import "UIColor+EmerResStrat.h"

static NSInteger WIDTH = 400;
static NSInteger HEIGHT = 200;

@interface NeighborVC ()

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *departmentPrefix;
@property (weak, nonatomic) IBOutlet UIButton *addDepartmentButton;
- (IBAction)editingChangedName:(UITextField *)sender;
- (IBAction)editingChangedPrefix:(UITextField *)sender;
@property (nonatomic) BOOL userSet;

@end

@implementation NeighborVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.userSet = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.name.delegate = self;
    self.departmentPrefix.delegate = self;
    self.addDepartmentButton.enabled = NO;
    self.addDepartmentButton.adjustsImageWhenDisabled = YES;
    self.preferredContentSize = CGSizeMake(WIDTH,HEIGHT);
}

- (void)viewWillDisappear:(BOOL)animated
{
    //[self performSegueWithIdentifier:@"unwindFromCancel" sender:self];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// When department name changes finish update the prefix if it is not set
- (IBAction)editingChangedName:(UITextField *)sender
{
    self.neighborName = sender.text;
    self.name.backgroundColor = [UIColor whiteColor];
    if (!self.userSet) {
        self.departmentPrefix.backgroundColor = [UIColor ersRedColor];
    }
    if (self.name.text.length > 0) {
        if ([self isUniqueName:self.name.text]) {
            [self checkButtonEnablement];
        }
        else {
            // not a unique name
            self.name.backgroundColor = [UIColor ersRedColor];
            // todo give error message in the dialog box
        }
    }
    [self checkButtonEnablement];
}


// Prefix is limited to last character typed or the first character of the department name
- (IBAction)editingChangedPrefix:(UITextField *)sender
{
    sender.backgroundColor = [UIColor whiteColor];
    if (sender.text.length < 1) {
        self.userSet = NO;
        sender.backgroundColor = [UIColor ersRedColor];
    }
    if (sender.text.length > 3) {
        sender.text = [sender.text substringToIndex:3];
        sender.backgroundColor = [UIColor ersGoldColor];
    }
    if (sender.text.length > 0) {
        sender.text = sender.text.uppercaseString;
        if ([self isUniquePrefix:sender.text]) {
            self.userSet = YES;
        }
        else {
            // not unique prefix
            sender.backgroundColor = [UIColor ersRedColor];
            // todo give error message in the dialog box
        }
    }
    [self checkButtonEnablement];
}

- (void)checkButtonEnablement
{
    self.addDepartmentButton.enabled = self.departmentPrefix.text.length > 0
        && ![[self.departmentPrefix.text substringToIndex:1] isEqualToString:@"?"]
        && self.name.text.length > 0
        && [self isUniqueName:self.name.text]
        && [self isUniquePrefix:self.departmentPrefix.text];
    // self.addDepartmentButton.hidden = !self.addDepartmentButton.enabled;
}


- (BOOL)isUniquePrefix:(NSString *)prefix
{
    BOOL isUnique = YES;
    for (FireDepartment *fireDepartment in self.departments) {
        if ([prefix isEqualToString:fireDepartment.neighborPrefix]) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (BOOL) isUniqueName: (NSString *) newName
{
    BOOL isUnique = YES;
    for (FireDepartment *fireDepartment in self.departments) {
        if ([newName.uppercaseString isEqualToString:fireDepartment.name.uppercaseString]) {
            isUnique = NO;
            break;
        }
    }
    return isUnique;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self checkButtonEnablement];
    if (self.addDepartmentButton.enabled) {
        [self performSegueWithIdentifier:@"unwindFromAdd" sender:self];
        return YES;
    }
    return NO;
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // NSLog(@"segue=%@",segue.identifier);
    if ([segue.identifier isEqualToString:@"unwindFromAdd"]) {
        self.neighborName = self.name.text;
        self.neighborPrefix = self.departmentPrefix.text;
    }
}

@end
