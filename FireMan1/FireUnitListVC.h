//
//  FireUnitListVCViewController.h
//  FireMan1
//
//  Created by Gerald Boyd on 5/16/13.
//  Copyright (c) 2013 Gerald Boyd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FireUnitListDelegate.h"

@class FireDepartment;
@class Incident;
@class ManagementViewCell;
@class FireManViewController;
@class StagingHeaderView;
@class StagingGroup;

@interface FireUnitListVC : UITableViewController

@property(nonatomic, strong) FireDepartment *fireDepartment;
@property(nonatomic, strong) Incident *incident;
@property (nonatomic, strong) ManagementViewCell *currentCell;
@property (nonatomic, weak) StagingHeaderView *headerView;
@property (nonatomic, weak) StagingGroup *stagingGroup;
@property (nonatomic, strong) id <FireUnitListDelegate> fireUnitListDelegate;
@end
