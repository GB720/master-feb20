//
//  FireManAppDelegate.m
//  FireMan1
//
//  Created by Gerald Boyd on 2/11/13.
//  Copyright (c) 2020 Gerald Boyd. All rights reserved.
//

#import <os/activity.h>
#import <CoreData/CoreData.h>
#import "FireManAppDelegate.h"
#import "NSMutableArray+Fifo.h"
#import <Ensembles/Ensembles.h>

#define NUMBER_TICKS_TILL_SAVE 10

@interface FireManAppDelegate ()  <CDEPersistentStoreEnsembleDelegate>

@property(nonatomic, readwrite) BOOL databaseReady;
@property(nonatomic, strong) NSMutableArray *queue;

@end


@implementation FireManAppDelegate {
    NSInteger saveCounter;
    CDEICloudFileSystem *cloudFileSystem;
    CDEPersistentStoreEnsemble *ensemble;
}

// ................

- (NSMutableArray *)queue
{
   if (!_queue) {
        _queue = [[NSMutableArray alloc] initWithCapacity:20];
   }
   return _queue;
}


- (void)setupCoreData
{
    NSError *error;
    
    NSURL *modelURL = [self modelURL];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    [[NSFileManager defaultManager] createDirectoryAtURL:self.storeDirectoryURL withIntermediateDirectories:YES attributes:nil error:NULL];
    
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
    [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    self.managedObjectContext.persistentStoreCoordinator = coordinator;
    self.managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    
}

- (NSURL *)storeDirectoryURL
{
    NSURL *directoryURL = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
    directoryURL = [directoryURL URLByAppendingPathComponent:NSBundle.mainBundle.bundleIdentifier isDirectory:YES];
    return directoryURL;
}

- (void)syncWithCompletion:(void(^)(void))completion
{
    // TODO show busy on view controller
    if (ensemble)
    {
       if (!ensemble.isLeeched) {
            // leech the database
            [ensemble leechPersistentStoreWithCompletion:^(NSError *error) {
                if (error) NSLog(@"Error in leech: %@", error);
                // TODO show not busy on view controller
                // [viewController.activityIndicator stopAnimating];
                // [viewController refresh];
                if (completion) completion();
            }];
        }
        else {
            [ensemble mergeWithCompletion:^(NSError *error) {
                if (error) NSLog(@"Error in merge: %@", error);
                // TODO show not busy on view controller
                if (completion) completion();
            }];
        }
    }
}

- (void) iCloudIdentityChange:(NSNotification *)notif
{
        dispatch_async(dispatch_get_global_queue(0,0),^{
            NSFileManager *fileManager = [NSFileManager defaultManager];
            id currentiCLoudToken = fileManager.ubiquityIdentityToken;
            if (!currentiCLoudToken) {
                self->ensemble = nil;
            }
            else {
                if (!self->ensemble) {
                    [self initializeEnsembles];
                }
            }
        });
}

- (void)localSaveOccurred:(NSNotification *)notif
{
    [self syncWithCompletion:NULL];
}

- (void)cloudDataDidDownload:(NSNotification *)notif
{
    [self syncWithCompletion:NULL];
}


- (void)elapsedTimer:(NSTimeInterval)elapsedTime
{
    saveCounter--;
    if (saveCounter <= 0) {
        [self saveAndMerge];
    }
}

-(void) saveAndMerge
{
    if (self.managedObjectContext.hasChanges) {
        [self.managedObjectContext save:NULL];
    }
    [self syncWithCompletion:NULL];
    saveCounter = NUMBER_TICKS_TILL_SAVE;
}

-(void) initializeEnsembles
{
    // icloud availability
    NSFileManager *fileManager = [NSFileManager defaultManager];
    id currentiCLoudToken = fileManager.ubiquityIdentityToken;
    if (currentiCLoudToken)
    {
        // Set up Ensemble
        NSURL *myStoreURL = [self storeURL];
        // NSString *storeURLString = myStoreURL.absoluteString;
        cloudFileSystem = [[CDEICloudFileSystem alloc] initWithUbiquityContainerIdentifier:nil];
        ensemble = [[CDEPersistentStoreEnsemble alloc] initWithEnsembleIdentifier:(NSString *)@"FireStore13" persistentStoreURL:myStoreURL managedObjectModelURL:[self modelURL] cloudFileSystem:cloudFileSystem];
        ensemble.delegate = self;
    }
    else {
        ensemble = nil;
    }
}

// application delegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.

    self.databaseReady = NO;

    // Ensembles logging
    CDESetCurrentLoggingLevel(CDELoggingLevelVerbose);
        
    NSLog(@"%s initializing Persistent Stack", __PRETTY_FUNCTION__);
    [self setupCoreData];

    [self initializeEnsembles];
    
    saveCounter = 30; // check for changes every x seconds
    
    // listen for change in iCloud identity
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCloudIdentityChange:) name:NSUbiquityIdentityDidChangeNotification object:nil];
    
    // Listen for local saves, and trigger merges
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localSaveOccurred:) name:CDEMonitoredManagedObjectContextDidSaveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cloudDataDidDownload:) name:CDEICloudFileSystemDidDownloadFilesNotification object:nil];
    
    [self syncWithCompletion:NULL];
    
    self.databaseReady = YES;
    NSLog(@"%s initialized Persistent Stack", __PRETTY_FUNCTION__);
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIBackgroundTaskIdentifier identifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    dispatch_async(dispatch_get_global_queue(0,0),^{
        [self.managedObjectContext performBlock:^{
            [self.managedObjectContext save:NULL];
            // give ensembles chance to sync
            [self syncWithCompletion:^{
                [[UIApplication sharedApplication] endBackgroundTask:identifier];
            }];
        }];
    });
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"%s -- became active app", __PRETTY_FUNCTION__);
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self saveAndMerge];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    UIBackgroundTaskIdentifier identifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    dispatch_async(dispatch_get_global_queue(0,0),^{
        [self.managedObjectContext performBlock:^{
            [self.managedObjectContext save:NULL];
            // give ensembles chance to sync
            [self syncWithCompletion:^{
                [[UIApplication sharedApplication] endBackgroundTask:identifier];
            }];
        }];
    });
}

- (NSURL*)storeURL
{
    
    os_activity_label_useraction("Defining storage URL");
    NSURL* documentsDirectory = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
    
    // define storage URL
    NSString *sqlitePathComponent = @"ERS13.sqlite";
    NSLog(@"%s storage URL for DataModel created using path %@", __PRETTY_FUNCTION__, sqlitePathComponent );
    return [documentsDirectory URLByAppendingPathComponent:sqlitePathComponent];
}

- (NSURL*)modelURL
{
    return [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
}

#pragma mark Ensemble Delegate Methods



- (void)persistentStoreEnsemble:(CDEPersistentStoreEnsemble *)ensemble didSaveMergeChangesWithNotification:(NSNotification *)notification
{
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ensemblesMergedChanges" object:nil];
    });
}

- (NSArray *) persistentStoreEnsemble:(CDEPersistentStoreEnsemble *)ensemble globalIdentifiersForManagedObjects:(NSArray *)objects
{
    return [objects valueForKeyPath:@"uniqueId"];
}

-(BOOL) persistentStoreEnsemble:(CDEPersistentStoreEnsemble *)ensemble shouldSaveMergedChangesInManagedObjectContext:(NSManagedObjectContext *)savingContext reparationManagedObjectContext:(NSManagedObjectContext *)reparationContext
{
    return !(self.managedObjectContext.hasChanges || self.managedObjectContext.parentContext.hasChanges);
}

@end
